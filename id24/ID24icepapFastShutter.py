import os
import tabulate
import gevent

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.common.cleanup import cleanup
from bliss.controllers.motors.icepap.comm import _command, _ackcommand, _vdata_header
from bliss.common.logtools import disable_user_output

class ID24IcepapFastShutter:
    
    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self.motor = config.get("mot_shut")
        self.controller = self.motor.controller
        self.motor.state
        self._state_command = f"{self.motor.address}:?ISG ?FPGAREG 1"
        
        self._mode = settings.SimpleSetting(f"{self._name}_hplf_fast_shutter_mode", default_value="Manual")
        #if self._mode.get() == "Manual":
        #    self.manual()
            
    def command(self, com_str):
        return _ackcommand(
            self.controller._cnx,
            com_str
        )

    def manual(self):
        self.mode = "Manual"
    
    def external(self):
        self.mode = "External"
        
    @property
    def mode(self):
        return self._mode.get()
        
    @mode.setter
    def mode(self, new_mode):
        if new_mode == "Manual":
            comm = f"{self.motor.address}:stop"
            self.command(comm)
            comm = f"{self.motor.address}:POS INPOS 0"
            self.command(comm)
            self._mode.set("Manual")
            self.close()
        elif new_mode == "External":
            self.close()
            comm = f"{self.motor.address}:POS INPOS 0"
            self.command(comm)
            comm = f"{self.motor.address}:TRACK INPOS"
            self.command(comm)
            self._mode.set("External")
        else:
            raise RuntimeError("Mode values must be in [\"Manual\" / \"External\"]")
            
    def state(self):
        ret = int(self.command(self._state_command), 16) & 4
        if not ret:
            with disable_user_output():
                self.motor.dial = 1
                self.motor.offset = 0
            return "Open"
        else:
            with disable_user_output():
                self.motor.dial = 0
                self.motor.offset = 0
            return "Closed"

    def open(self):
        if self.mode == "Manual":
            if self.state() == "Closed":
                with disable_user_output():
                    self.motor.move(self.motor.position+1)
            else:
                print("Target Fast Shutter already Open")
            with disable_user_output():
                self.motor.dial = 1
                self.motor.offset = 0
        else:
            print(f"{self._name} in External Mode")
            
    def close(self):
        if self.mode == "Manual":
            if self.state() == "Open":
                with disable_user_output():
                    self.motor.move(self.motor.position+1)
            else:
                print("Target Fast Shutter already Closed")
            with disable_user_output():
                self.motor.dial = 0
                self.motor.offset = 0
        else:
            self.manual()
