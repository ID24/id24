        
import numpy

class ExafsElementDB(object):
    
    def __init__(self, name, config):
        
        # Energy Database
        self.fname_energy = config.get("energy")
        self.energy_names = ["K", "L1", "L2", "L3", "M1", "M2", "M3", "M4"]
        self.read_energy()

        # Natural Width Database
        self.fname_width = config.get("width")
        self.width_names = ["K", "L1", "L2", "L3"]
        self.read_width()
        
    """
    ENERGY DATABASE
    """
    # Read Energy file database
    def read_energy(self):
        self.elem_energy = {}
        try:
            fp = open(self.fname_energy)
            for l in enumerate(fp):
                if l[0] > 2:
                    cont = l[1].split()
                    name = cont[1]
                    self.elem_energy[name] = {}

                    self.elem_energy[name]["K"] = float(cont[2])
                    if len(cont) > 3:
                        self.elem_energy[name]["L1"] = float(cont[3])
                    if len(cont) > 4:
                        self.elem_energy[name]["L2"] = float(cont[4])
                    if len(cont) > 5:
                        self.elem_energy[name]["L3"] = float(cont[5])
                    if len(cont) > 6:
                        if cont[6] != "-":
                            self.elem_energy[name]["M1"] = float(cont[6])
                    if len(cont) > 7:
                        self.elem_energy[name]["M2"] = float(cont[7])
                    if len(cont) > 8:
                        self.elem_energy[name]["M3"] = float(cont[8])
                    if len(cont) > 9:
                        self.elem_energy[name]["M4"] = float(cont[9])
        finally:
            fp.close()
    
    # return energy in keV for elem/hole    
    def energy(self, elem, hole):
        if elem not in self.elem_energy:
            raise RuntimeError(f"Element \"{elem}\" not in the Elements Database")
            
        if hole not in self.energy_names:
            raise RuntimeError(f"Edge \"{hole}\" is not an valid Edge")
            
        if hole not in self.elem_energy[elem]:
            raise RuntimeError(f"Edge \"{hole}\" is not defined for element \"{elem}\"")
            
        val = self.elem_energy[elem][hole] / 1000.0
        
        return val
    
    """
    NATURAL WIDTH DATABASES
    """
    # Read Natuaral Width file database
    def read_width(self):
        self.elem_width = {}
        try:
            fp = open(self.fname_width)
            for l in enumerate(fp):
                if l[0] > 0:
                    cont = l[1].split()
                    name = cont[1]
                    self.elem_width[name] = {}

                    self.elem_width[name]["K"] = float(cont[2])
                    if len(cont) > 3:
                        self.elem_width[name]["L1"] = float(cont[3])
                    if len(cont) > 4:
                        self.elem_width[name]["L2"] = float(cont[4])
                    if len(cont) > 5:
                        self.elem_width[name]["L3"] = float(cont[5])
        finally:
            fp.close()
    
    # return Natural Width in m for elem/hole    
    def width(self, elem, hole):
        if elem not in self.elem_width.keys():
            raise RuntimeError(f"Element \"{elem}\" not in the database")
            
        if hole not in self.width_names:
            raise RuntimeError(f"Edge \"{hole}\" not in the database")
            
        if hole not in self.elem_width[elem]:
            raise RuntimeError(f"Edge \"{hole}\" does not exist for Element \"{elem}\"")
            
        val = self.elem_width[elem][hole]
        val = val / 1000.
        
        return val
                
    

