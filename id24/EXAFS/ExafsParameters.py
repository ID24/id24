import numpy as np
import glob
import os
import click
import tabulate
from ruamel.yaml import YAML
from ruamel.yaml.compat import StringIO

from bliss.common.utils import ColorTags, BOLD
from bliss.common import plot

from bliss import current_session, setup_globals
from bliss.config import settings

class ExafsParamContainer:
    
    def __init__(self, name, config):
        
        self.name = name
        self.config = config
        
        self.elemDB = config.get("elementsDB")
        
        plot_list = {"energy":None, "time":None, "steps": None}

        self._settings_name = f"ExafsParam2_{self.name}"
        self.settings = settings.HashSetting(self._settings_name, default_values=plot_list)

        self.set_list = {}
        self._create_all_params_from_file()
                
    """
    Show Memory loaded paramaters set
    """
    def __info__(self):
        mystr = "Created Parameters Set:\n"
        for name in self.set_list:
            mystr += f"    {name}\n"          
        
        return mystr        
        
    """
    Create a new scan parameters set.
        - Check with the name that it does not exist
        - Create it with default parameters
    """
    def new(self, new_name, elem, hole):
        if new_name in self.set_list:
            raise RuntimeError(f"Parameter Set \"{new_name}\" already exists")
            
        self.set_list[new_name] = ExafsParam(self, new_name, elem, hole)
        current_session.env_dict[new_name] = self.set_list[new_name]
        print(self.set_list[new_name].__info__())
        
    def clean(self):
        param_list = []
        for param in self.set_list:
            param_list.append(param)
        for param in param_list:
            current_session.env_dict.pop(param)
            self.set_list.pop(param)
            
    def reload(self):
        self._create_all_params_from_file()
                   
    """
    Get current directory to save parameters
    """
    def _get_dir(self):
        SCAN_SAVING = current_session.scan_saving
        params_dir = os.path.join(
            SCAN_SAVING.base_path,
            SCAN_SAVING.proposal_name,
            SCAN_SAVING.beamline,
            "exafs_param"
        )
        return params_dir
    """
    Read directory params_dir and build dictionnary with key=name, value=filename
    """
    def _read_dir(self, params_dir):
        set_file_list = {}
        expr = f"{params_dir}/*.yml"
        file_list = glob.glob(expr)
        for path_radix_name_ext in file_list:
            radix_name_ext = path_radix_name_ext.split("/")[-1]
            name =radix_name_ext.split(".")[0]
            set_file_list[name] = path_radix_name_ext
            
        return set_file_list
        
    def _create_param_from_file(self, name):
        self.set_list[name] = ExafsParam(self, name, None, None, load=True)
        current_session.env_dict[name] = self.set_list[name]
        
    def _create_all_params_from_file(self):
        params_dir = self._get_dir()
        file_list = self._read_dir(params_dir)
        for name in file_list:
            self._create_param_from_file(name)
              
    """
    plot
    """    
    def get_plot(self, title, plot_id):    
                                                    
        curve = None                                                             
        if plot_id in self.settings:                                              
            try:
                flint_plot_id = self.settings[plot_id]                            
                #print("READ PLOT", flint_plot_id)
                curve = plot.plot_curve(name=title, existing_id=flint_plot_id)
                #print("OK")
            except Exception as e:                                               
                # In case flint was closed in between                            
                print(e)                                                         
                pass                                                             
        if curve is None:                                                        
            #print("CREATE PLOT", flint_plot_id)
            curve = plot.plot_curve(name=title)                              
        #print("PLOT ID", curve._plot_id)
        self.settings[plot_id] = curve._plot_id 
                                          
        return curve
        
    def plot(self, curve, y_data, x_data, x_cursor_pos=None, y_label="y", x_label="x"):
        try:
            curve.clear_data()
        except:
            # FLINT 1.5 BUG: clear_data can't be called twice
            pass
        data = {y_label: y_data, x_label: x_data}
        if x_cursor_pos is not None:
            ymin, ymax = np.min(y_data), np.max(y_data)
            data["x_cursor"] = [x_cursor_pos, x_cursor_pos]
            data["y_cursor"] = [ymin, ymax]
        curve.add_data(data)
        curve.select_data(x_label, y_label)
        if x_cursor_pos is not None:
            curve.select_data("x_cursor", "y_cursor", color="pink")

class ExafsParam:
    
    def __init__(self, container, name, element, edge_name, load=False):
        
        self._container = container
        self.name = name
        self._data_dir = self._container._get_dir()
        self._elemDB = self._container.elemDB
        
        self._data_file = f"{self._data_dir}/{self.name}.yml"
        
        # Physical constant
        self._me = 9.1093836e-31
        self._qe = 1.6021766e-19
        self._hbarre = 1.054572e-34
        self._KtoE = 2.0 * self._qe * self._me * 1e-20 / (np.power(self._hbarre, 2))
        
        self.Ttype = TtypeValues(self)
        
        if not load:
            self._load_default(element, edge_name)
            self._save_to_file()
        else:
            self._load_from_file()
            
        self._calc()
        self._save_to_file()
        self._plot()
    

    """
        INFO
    """
    def __info__(self):
        
        line1 = [
            "Element", "\"%s\""%self._param_list["Element"],
            "Einitial", "%.1f"%self._param_list["Einitial"],
            "Tinitial", self._param_list["Tinitial"]
        ]
        line2 = [
            "Edge", "\"%s\""%self._param_list["Edge"],
            "EstartXanes", "%.1f"%self._param_list["EstartXanes"],
            "Tfinal", self._param_list["Tfinal"]
        ]
        line3 = [
            "", "",
            "EdgeEnergy", "%.1f"%self._param_list["EdgeEnergy"],
            "Ttype", f"\"{self.Ttype._str()}\""
        ]
        line4 = [
            "", "",
            "EfinXanes", "%.1f"%self._param_list["EfinXanes"],
            "", ""
        ]
        line5 = [
            "", "",
            "Efinal", "%.1f"%self._param_list["Efinal"],
            "", ""
        ]
        line6 = [
            "", "",
            "", "",
            "", ""
        ]
        line7 = [
            "", "",
            "DEpe", self._param_list["DEpe"],
            "Kmax", "%.1f"%self._param_list["Kmax"]
        ]
        line8 = [
            "", "",
            "DEmin", self._param_list["DEmin"],
            "Dk", self._param_list["Dk"]
        ]
        mystr = f"Scan Parameters: {self.name}\n\n"
        mystr += tabulate.tabulate([line1, line2, line3, line4, line5, line6, line7, line8], tablefmt="plain")
        mystr += "\n"

        return mystr
    
    """
    METADATA
    """
    def _get_meta_data(self):
        return self._param_list
        
    """
    laod default parameters
    """
    def _load_default(self, element, edge_name):
        edge_energy = self._elemDB.energy(element, edge_name) * 1000.0

        self._param_list = {}
        self._param_list["Element"] = element
        self._param_list["Edge"]  = edge_name
        self._param_list["EdgeEnergy"] = edge_energy
        self._param_list["Kmax"] = 16
        
        self._param_list["Gamma"] = self._elemDB.width(element, edge_name) * 1000.0
    
        self._param_list["Einitial"] = self._param_list["EdgeEnergy"] - 130.0 * np.power(self._param_list["Gamma"], 1.0/3.0)
        self._param_list["EstartXanes"] = self._param_list["EdgeEnergy"] - 130.0 * np.power(self._param_list["Gamma"], 1.0/3.0) / 10.0        
        self._param_list["Efinal"] = self._param_list["EdgeEnergy"] + np.power(self._param_list["Kmax"], 2) / self._KtoE

        self._param_list["DEpe"] = 5.0
        self._param_list["DEmin"] = 0.5 
        self._param_list["Dk"] = 0.035
        K4 = (self._KtoE * self._param_list["DEmin"] - np.power(self._param_list["Dk"], 2)) / 2.0 / self._param_list["Dk"]
        self._param_list["EfinXanes"] = self._param_list["EdgeEnergy"] + np.power(K4, 2) / self._KtoE

        self._param_list["Ttype"] = 0 # 0 = Tinitial from Einitial to Efinal
                                      # 1 = Linear increase between Tinitial and Tfinal if E>Eedge
                                      # 2 = Quadratic inrease between Tinitial and Tfinal if E>Eedge
        self._param_list["Tinitial"] = 1.0
        self._param_list["Tfinal"] = 1.0
        self._param_list["Nptrans"] = 8
    
    def _format_param(self):
        self._param_list["Einitial"] = int(self._param_list["Einitial"] * 1000.0) / 1000.0
        self._param_list["EstartXanes"] = int(self._param_list["EstartXanes"] * 1000.0) / 1000.0
        self._param_list["EfinXanes"] = int(self._param_list["EfinXanes"] * 1000.0) / 1000.0
        self._param_list["Efinal"] = int(self._param_list["Efinal"] * 1000.0) / 1000.0
        self._param_list["Kmax"] = int(self._param_list["Kmax"] * 1000.0) / 1000.0
        
    """
    Save parameters in file
    """
    def _save_to_file(self):
        self._format_param()
        yaml = YAML(pure=True)
        yaml.default_flow_style = False
        stream = StringIO()
        yaml.dump(self._param_list, stream=stream)
        with open(self._data_file, "w") as file_out:
            file_out.write(stream.getvalue())

    """
    load parameters from file
    """
    def _load_from_file(self):
        if os.path.exists(self._data_file) is None:
            raise RuntimeError(f"Parameter Set file \"{self.name}.yml\" does not exists")
        yaml = YAML(pure=True)
        with open(self._data_file) as file_in:
            self._param_list = yaml.load(file_in)

    """
    update parameters status
    """
    def _update(self):
        self._calc()
        self._plot()
        self._save_to_file()
        print(self.__info__())
        
    def delete(self):
        os.remove(self._data_file)
        current_session.env_dict.pop(self.name)
        self._container.set_list.pop(self.name)
        
    """
        PARAMETERS
    """
    @property
    def Edge(self):
        return self._param_list["Edge"]

    @Edge.setter
    def Edge(self, edge):
        self._param_list["Edge"] = edge
        self._param_list["EdgeEnergy"] = self._elemDB.energy(self._param_list["Element"], edge) * 1000.0
        self._param_list["Einitial"] = self._param_list["EdgeEnergy"] - 130.0 * np.power(self._param_list["Gamma"], 1.0/3.0)
        self._param_list["EstartXanes"] = self._param_list["EdgeEnergy"] - 130.0 * np.power(self._param_list["Gamma"], 1.0/3.0) / 10.0
        K4 = (self._KtoE * self._param_list["DEmin"] - np.power(self._param_list["Dk"], 2)) / 2.0 / self._param_list["Dk"]
        self._param_list["EfinXanes"] = self._param_list["EdgeEnergy"] + np.power(K4, 2) / self._KtoE
        self._param_list["Efinal"] = self._param_list["EdgeEnergy"] + np.power(self._param_list["Kmax"], 2) / self._KtoE
        self._param_list["Gamma"] = self._elemDB.width(self._param_list["Element"], edge)
            
        self._update()

    @property
    def Einitial(self):
        return self._param_list["Einitial"]

    @Einitial.setter
    def Einitial(self, einitial):
        def_energy = self._param_list["EdgeEnergy"]
        if einitial < (def_energy-1000.0) or einitial > (def_energy-1.0):
            raise RuntimeError(f"Einitial musst be in [{def_energy-1000.0}eV - {def_energy-1.0}eV]")            
        self._param_list["Einitial"] = einitial
            
        self._update()

    @property
    def EstartXanes(self):
        return self._param_list["Einitial"]

    @EstartXanes.setter
    def EstartXanes(self, estartxanes):
        min_energy = self._param_list["Einitial"]
        max_energy = self._param_list["EdgeEnergy"]
        if estartxanes < min_energy or estartxanes > max_energy:
            raise RuntimeError(f"EstartXanes musst be in [{min_energy}eV - {max_energy}eV]")            
        self._param_list["EstartXanes"] = estartxanes
            
        self._update()

    @property
    def EdgeEnergy(self):
        return self._param_list["EdgeEnergy"]

    @EdgeEnergy.setter
    def EdgeEnergy(self, energy):
        def_energy = self._elemDB.energy(self._param_list["Element"], self._param_list["Edge"]) * 1000.0
        if energy < (def_energy-50.0) or energy > (def_energy+50.0):
            raise RuntimeError(f"Edge Energy musst be in [{def_energy-50.0}eV - {def_energy+50.0}eV]")
        self._param_list["EdgeEnergy"] = energy
        self._param_list["Einitial"] = self._param_list["EdgeEnergy"] - 130.0 * np.power(self._param_list["Gamma"], 1.0/3.0)
        self._param_list["EstartXanes"] = self._param_list["EdgeEnergy"] - 130.0 * np.power(self._param_list["Gamma"], 1.0/3.0) / 10.0
        K4 = (self._KtoE * self._param_list["DEmin"] - np.power(self._param_list["Dk"], 2)) / 2.0 / self._param_list["Dk"]
        self._param_list["EfinXanes"] = self._param_list["EdgeEnergy"] + np.power(K4, 2) / self._KtoE
        self._param_list["Efinal"] = self._param_list["EdgeEnergy"] + np.power(self._param_list["Kmax"], 2) / self._KtoE
            
        self._update()

    @property
    def EfinXanes(self):
        return self._param_list["EfinXanes"]

    @EfinXanes.setter
    def EfinXanes(self, efinxanes):
        min_energy = self._param_list["EdgeEnergy"]
        max_energy = self._param_list["Efinal"]
        if efinxanes < min_energy or efinxanes > max_energy:
            raise RuntimeError(f"EfinXanes musst be in [{min_energy}eV - {max_energy}eV]")            
        self._param_list["EfinXanes"] = efinxanes
            
        self._update()

    @property
    def Efinal(self):
        return self._param_list["Efinal"]

    @Efinal.setter
    def Efinal(self, efinal):
        def_energy = self._elemDB.energy(self._param_list["Element"], self._param_list["Edge"]) * 1000.0
        if efinal < (def_energy+15.0) or efinal > (def_energy+4000.0):
            raise RuntimeError(f"Efinal musst be in [{def_energy-15.0}eV - {def_energy+4000.0}eV]")            
        self._param_list["Efinal"] = efinal
            
        self._update()

    @property
    def Kmax(self):
        return self._param_list["Kmax"]

    @Kmax.setter
    def Kmax(self, kmax):
        if kmax < 2.0 or kmax > 40:
            raise RuntimeError("Kmax must be in [2-40]")
        self._param_list["Kmax"] = kmax
        self._param_list["Efinal"] = self._param_list["EdgeEnergy"] + np.power(self._param_list["Kmax"], 2) / self._KtoE
            
        self._update()

    @property
    def Dk(self):
        return self._param_list["Dk"]

    @Dk.setter
    def Dk(self, dk):
        if dk < 0.01 or dk > 1.0:
            raise RuntimeError("Dk must be in [0.01-1.0]")
        self._param_list["Dk"] = dk
        K4 = (self._KtoE * self._param_list["DEmin"] - np.power(self._param_list["Dk"], 2)) / 2.0 / self._param_list["Dk"]
        self._param_list["EfinXanes"] = self._param_list["EdgeEnergy"] + np.power(K4, 2) / self._KtoE
            
        self._update()

    @property
    def DEpe(self):
        return self._param_list["DEpe"]

    @DEpe.setter
    def DEpe(self, depe):
        if depe < 0.1 or depe > 100.0:
            raise RuntimeError("DEpe must be in [0.1eV-100.0eV]")
        self._param_list["DEpe"] = depe
            
        self._update()

    @property
    def DEmin(self):
        return self._param_list["DEmin"]

    @DEmin.setter
    def DEmin(self, demin):
        if demin < 0.01 or demin > 50.0:
            raise RuntimeError("DEmin must be in [0.01eV-50.0eV]")
        self._param_list["DEmin"] = demin
        K4 = (self._KtoE * self._param_list["DEmin"] - np.power(self._param_list["Dk"], 2)) / 2.0 / self._param_list["Dk"]
        self._param_list["EfinXanes"] = self._param_list["EdgeEnergy"] + np.power(K4, 2) / self._KtoE
            
        self._update()

    @property
    def Tinitial(self):
        return self._param_list["Tinitial"]

    @Tinitial.setter
    def Tinitial(self, tinitial):
        self._param_list["Tinitial"] = tinitial
        if self._param_list["Ttype"] == 0:
            self._param_list["Tfinal"] = tinitial
            
        self._update()

    @property
    def Tfinal(self):
        return self._param_list["Tfinal"]

    @Tfinal.setter
    def Tfinal(self, tfinal):
        self._param_list["Tfinal"] = tfinal
        if self._param_list["Ttype"] == 0:
            self._param_list["Tinitial"] = tfinal
            
        self._update()

    @property
    def Nptrans(self):
        return self._param_list["Nptrans"]

    @Nptrans.setter
    def Nptrans(self, nptrans):
        if nptrans < 1 or nptrans > 20:
            raise RuntimeError("Nptrans must be in [1-20]")
        self._param_list["Nptrans"] = nptrans
            
        self._update()

    """
        Position and Time Calculations
    """
    def _calc(self):
        
        # user inputs or from database
        Eedge = self._param_list["EdgeEnergy"]

        # Steps scan parameters
        Einitial = self._param_list["Einitial"]
        Efinal = self._param_list["Efinal"]
        EstartXanes = self._param_list["EstartXanes"]
        EfinXanes = self._param_list["EfinXanes"]
        DEpe = self._param_list["DEpe"]
        DEmin = self._param_list["DEmin"]
        Dk = self._param_list["Dk"]

        # Time scan parameters
        Tinitial = self._param_list["Tinitial"]
        Tfinal = self._param_list["Tfinal"]
        Ttype = self._param_list["Ttype"]
        
        # Transition
        nptrans = self._param_list["Nptrans"]

        # Calculations
        E2 = EstartXanes - (DEmin + DEpe) * nptrans / 2.0
        if Einitial > E2:
            np1 = 1
            Einitial = E2 - np1 * DEpe
        else:
            np1 = round((E2 - Einitial) / DEpe)
            if np1<1:
                np1 = 1
            Einitial = E2 - np1 * DEpe
        self._param_list["Einitial"] = Einitial
        
        #### Scan region1
        self.Position = []
        for ii in range(np1):
            self.Position.append(Einitial + ii * DEpe)

        #### Scan region2
        Eregion2 = self.Position[-1]
        for jj in range(nptrans):
            deltaE = (DEmin - DEpe) / (nptrans + 1) * jj + DEpe
            Eregion2 += deltaE
            self.Position.append(Eregion2)

        #### Scan region3
        if Efinal < EfinXanes:
            np3 = round((Efinal - EstartXanes) / DEmin)
        else:
            np3 = round((EfinXanes - EstartXanes) / DEmin)
        for kk in range(np3 + 1):
            self.Position.append(EstartXanes + kk * DEmin)

        #### Scan region4
        if Efinal > EfinXanes:
            Enerregion4 = self.Position[-1]
            while Enerregion4 < Efinal:
                Kregion4 = np.sqrt((Enerregion4 - Eedge) * self._KtoE)
                Kregion4 += Dk
                Enerregion4 = Eedge + np.power(Kregion4, 2) / self._KtoE
                self.Position.append(Enerregion4)
        self._param_list["Efinal"] = self.Position[-1]

        ##### Integration time mesh
        self.Time = []
        self.K = []
        self.TimeSum = []
        temps = 0
        Kmax = np.sqrt(self._KtoE * (self.Position[-1] - Eedge)) 
        for ind in range(len(self.Position)):
            if self.Position[ind] < Eedge:
                self.Time.append(Tinitial)
                temps += Tinitial
                self.TimeSum.append(temps)
                self.K.append(0)
            else:
                deltatime = (Tfinal - Tinitial) * np.power((np.sqrt(self._KtoE * (self.Position[ind] - Eedge)) / Kmax), Ttype)
                self.Time.append(Tinitial + deltatime)
                self.K.append(np.sqrt(self._KtoE * (self.Position[ind] - Eedge)))
                temps = temps + Tinitial + deltatime
                self.TimeSum.append(temps)
        self._param_list["Kmax"] = self.K[-1]
        
    """
        PLOT
    """
    def _get_edge_index(self):
        for i in range(len(self.Position)):
            if self.Position[i] >= self._param_list["EdgeEnergy"]:
                return i
                
    def _plot(self):
        deltaPosition = [self._param_list["DEpe"]]
        for ind in range(len(self.Position)-1):
            deltaPosition.append(self.Position[ind + 1] - self.Position[ind])
        data_x = np.linspace(0, len(self.Position)-1, len(self.Position))
        edge_index = self._get_edge_index()
        
        curve = self._container.get_plot("Energy", "energy")
        self._container.plot(
            curve,
            self.Position,
            data_x,
            x_cursor_pos=edge_index,
            y_label="Energy Position",
            x_label="Scan Points"
        )
            
        curve = self._container.get_plot("Integration Time", "time")
        self._container.plot(
            curve,
            self.Time,
            data_x,
            x_cursor_pos=edge_index,
            y_label="Int. Time",
            x_label="Scan Points"
        )
            
        curve = self._container.get_plot("Scan Steps", "steps")
        self._container.plot(
            curve,
            deltaPosition,
            data_x,
            x_cursor_pos=edge_index,
            y_label="Scan Steps",
            x_label="Scan Points"
        )

class TtypeValues:
    def __init__(self, elem):
        self._elem = elem
        
    def constant(self):
        self._elem._param_list["Ttype"] = 0
        self._elem._calc()
        self._elem._plot()
        
    def linear(self):
        self._elem._param_list["Ttype"] = 1
        self._elem._calc()
        self._elem._plot()
        
    def quadratic(self):
        self._elem._param_list["Ttype"] = 2
        self._elem._calc()
        self._elem._plot()
        
    def _str(self):
        if self._elem._param_list["Ttype"] == 0:
            self._elem._param_list["Tfinal"] = self._elem._param_list["Tinitial"]
            return "Constant"
        elif self._elem._param_list["Ttype"] == 1:
            return "Linear"
        else:
            return "Quadratic"
        

                
