
import os
import tabulate
from ruamel.yaml import YAML
import gevent
import time
import numpy

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.scanning.chain import AcquisitionMaster

from id24.ID24utils import ID24attrList, ID24profile

class ID24cityControl:
    
    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self.city = config.get("city")
        
        master_channel = self._config.get("master_channel", None)

        self._city_channels_dict = {}
        city_channels = self._config.get("channels")
        for city_channel in city_channels:
            name = city_channel.get("city_channel_name")
            device = city_channel.get("city_channel")
            if master_channel is not None:
                device._set_master_channel(master_channel)
            self._city_channels_dict[name] = device
        self.channels = ID24cityChannelAttrList(self._city_channels_dict)
        
        #
        # Oscilloscopes
        #
        oscillos = self._config.get("oscilloscopes", None)
        if oscillos is None or len(oscillos) == 0:
            self._oscillos = None
        else:
            self._oscillos = {}
            for oscillo in oscillos:
                name = oscillo["osc_name"]
                self._oscillos[name] = oscillo["osc_device"]

        #
        # TargetShutter
        #
        self._target_shutter = self._config.get("target_shutter", None)
        self._use_target_shutter = True

        #
        # Oipiom Trig camera
        #
        self._opiom_hplf = self._config.get("opiom_hplf", None)
        self._use_opiom_hplf = True
        
        # Synchronisation object
        self._synchro = CitySynchro(self)
    
    ################################################################
    #####
    ##### User info
    #####
    def __info__(self):
        print(self.channels.__info__())
        return ""
        
    def _get_info(self):
        return ""
        
    def _get_metadata(self):
        md = {}
        
        #
        # Oscillos
        #
        if self._oscillos is not None:
            md["Oscilloscopes"] = {
                "_h5_": {},
                "_image_": {},
                "_ascii_": {},
                "_file_": {},
            }

            # tektro64
            data = self.get_oscillo_profile("tektro64", ["CH1", "CH2", "CH3", "CH4"])
            tektro64 = ID24profile()
            tektro64.set_data(
                "time", data["time"], 
                ["Xh_Frames", "P100_Output", "Drive_Eh1", "Laser_probe"], 
                [data["CH1"]["data"],data["CH2"]["data"],data["CH3"]["data"],data["CH4"]["data"],]
            )
            md["Oscilloscopes"]["_ascii_"]["tektro64_profile"] = {
                "type": "oscillos",
                "data": tektro64,
            }
            
            # tektro54
            data = self.get_oscillo_profile("tektro54", ["CH1", "CH2", "CH3", "CH4"])
            tektro54 = ID24profile()
            tektro54.set_data(
                "time", data["time"], 
                ["Trigger", "RGA", "Intrepid", "Pockels_Monitor"], 
                [data["CH1"]["data"],data["CH2"]["data"],data["CH3"]["data"],data["CH4"]["data"],]
            )
            md["Oscilloscopes"]["_ascii_"]["tektro54_profile"] = {
                "type": "oscillos",
                "data": tektro54,
            }
            
        #
        # City Channels
        #
        for name, dev in self.channels._items.items():
            md[name] = dev._get_metadata()
            
        return md
    
    ################################################################
    #####
    ##### OSCILLOS
    #####
    def _oscillo_start(self):
        if self._oscillos is not None:
            tektro64 = self._oscillos["tektro64"]
            tektro64.device.acq_start()
            tektro54 = self._oscillos["tektro54"]
            tektro54.device.acq_start()

    def get_oscillo_profile(self, oscillo_name, channels, start=False, poll=True, silent=False):
        if self._oscillos is not None:
            oscillo = self._oscillos[oscillo_name]
            if start:
                oscillo.device.acq_start()
            if poll:
                start_time = time.time()
                timeout = 0
                while not oscillo.device.acq_done() and timeout <= 20:
                    if not silent:
                        print(f"Acquiring {oscillo_name}...", end="\r")
                    gevent.sleep(0.1)
                    timeout = time.time()-start_time
 
                if not silent:
                    print("\n")
                    
                if timeout >= 20:
                    print(RED(f"\n    NO DATA FROM OSCILLOSCOPE {oscillo_name}\n"))
                    
            oscillo_data = {}
            for ch in channels:
                oscillo_data[ch] = {}
                if timeout >= 20:
                    oscillo_data[ch]["raw"] = numpy.zeros((3))
                    oscillo_data[ch]["data"] = numpy.zeros((3))
                    oscillo_data["time"] = numpy.zeros((3))
                    oscillo_data[ch]["header"] = "NO DATA"
                else:
                    (length, raw_data, ch_data, time_data, header) = oscillo.device.acq_read_channel(ch, 0)
                    oscillo_data[ch]["raw"] = raw_data
                    oscillo_data[ch]["data"] = ch_data
                    oscillo_data["time"] = time_data
                    oscillo_data[ch]["header"] = header
            
            return oscillo_data
        else:
            return None
        
    def plot_oscillo_profile(self, oscillo_name, channels, start=False):
        data = self.get_oscillo_profile(oscillo_name, channels, start=start)
        if data is not None:
            for key, val in data.items():
                setup_globals.display.plot_curve(
                    f"Oscillo {oscillo_name}",
                    f"{oscillo_name} - {key} Profile",
                    f"Oscillo {oscillo_name}",
                    val["time"],
                    val["data"],
                    "Time (s)",
                    "V"
                )
        else:
            print("    No Oscilloscopes configured....")
        
    
    ################################################################
    #####
    ##### TOOLS
    #####
    def _get_channel_object(self, channel_name):
        return self._city_channels_dict[channel_name]
    
    
    ################################################################
    #####
    ##### RESTORE CHANNELS VALUES
    #####
    def restore_default_values_for_all_channels(self):
        for name, dev in self.channels._items.items():
            print(f"{dev._title} - {dev._channel.get()} - Restore Default Values")
            dev._restore_defaults()
                            
    def _read_gate_file(self):
        if os.path.exists(self._gate_file) is None:
            raise RuntimeError(f"Gate Config file \"{self._config_file}\" does not exist")
        yaml = YAML(pure=True)
        with open(self._gate_file) as file_in:
            conf = yaml.load(file_in)
            
        self._gated_channels = {}
        for src in conf["sources"]:
            source = src["source"]
            self._gated_channels[source] = []
            if src["gated_channels"] is not None:
                for obj in src["gated_channels"]:
                    channel = self._get_channel_object(obj["channel"])
                    if channel._source is not None:
                        raise RuntimeError("ID24cityControl: Channel {channel_name} - get Source {channel._source} != set Source {source}")
                    self._gated_channels[source].append(channel)
                    channel._source = source
        
        for source in self._gated_channels.keys():
            self.mask(source)
        
        for name, channel in self._city_channels_dict.items():
            if channel._source is None:
                channel._gate_off()
                channel._burst_off()
            else:
                channel._gate_config(channel._source)
                channel._burst_on(1)
                
    def unmask(self, source):
        #if source in self._gated_channels.keys():
        #    self.city.command(f"SGATE {source} ON")
        self.city.command(f"SGATE {source} ON")
        
    def mask(self, source):
        #if source in self._gated_channels.keys():
        #    self.city.command(f"SGATE {source} OFF")
        self.city.command(f"SGATE {source} OFF")
        
    def burst(self, source):
        #if source in self._gated_channels.keys():
        self.city.command(f"SGATE {source} OFF")
        self.city.command(f"SGATE {source} ON")
        self.city.command(f"SGATE {source} OFF")
            
    def burst_config(self, source, channels):
        for name,channel in self._city_channels_dict.items():
            if channel in channels:
                channel._gate_config("S1")
                channel._burst_on(1)
            else:
                channel._gate_off()
                channel._burst_off()

class CitySynchro:
    def __init__(self, controller):
        self._controller = controller
        
        self._available_mode = "software/xh_diode/streak_ref/laser/shock/synchro_off"
        self._config_cmd = {
            "software": self.config_software,
            "xh_diode": self.config_xh_diode,
            "streak_ref": self.config_streak_ref,
            "laser": self.config_laser,
            "shock": self.config_shock,
            "synchro_off": self.config_synchro_off,
        }
         
    def get_acquisition_master(self, synchro_mode, use_shutter, use_opiom):
        if synchro_mode not in self._available_mode.split("/"):
            raise RuntimeError(f"Synchronization mode {synchro_mode} not in [{self._available_mode}]")
        self._use_target_shutter = use_shutter
        self._use_opiom_hplf = use_opiom
        return CitySynchroAcqMaster(synchro_mode, self)
               
    def prepare_synchro(self, synchro_mode):
        if synchro_mode not in self._available_mode.split("/"):
            raise RuntimeError(f"Synchronization mode {synchro_mode} not in [{self._available_mode}]")
        self.config_mode(synchro_mode)
        if synchro_mode != "software":
            self._controller._oscillo_start()
        if self._use_opiom_hplf:
            # Activate trigger on all camera
            self._controller._opiom_hplf.comm("IM 0x01 0x01")
               
    def post_synchro(self, synchro_mode):
        if self._use_opiom_hplf:
            # Stop trigger on all camera
            print(RED("\n    STOP Triggers on Cameras"))
            self._controller._opiom_hplf.comm("IM 0x00 0x01")
        
    def start_synchro(self, synchro_mode):
        if synchro_mode != "software":
            self._controller.burst("S1")
                
    def config_mode(self, mode):
        self._config_cmd[mode]()
        
    def config_software(self):
        pass
        
    def config_synchro_off(self):
        #                                             channel,      "burst source", Gate,   Burst
        self.config_channel(self._controller.channels.D5,           "I3",           False,  False)
        self.config_channel(self._controller.channels.D25,          "I3",           False,  False)
        self.config_channel(self._controller.channels.DAM,          "I3",           False,  False)
        self.config_channel(self._controller.channels.DAM_Presimmer,"I3",           False,  False)
        self.config_channel(self._controller.channels.early_instru, "S1",           False,  True)
        self.config_channel(self._controller.channels.Instru,       "S1",           False,  True)
        self.config_channel(self._controller.channels.ModBox,       "I3",           False,  False)
        self.config_channel(self._controller.channels.target_fast_shutter,        "S1",           False,  True)
        self.config_channel(self._controller.channels.Pockels,      "S1",           False,  True)
        self.config_channel(self._controller.channels.Quantaray,    "I3",           False,  False)
        self.config_channel(self._controller.channels.Xh_Start,     "S1",           False,  True)
        self.config_channel(self._controller.channels.PCK_ISO_OFF,  "I3",           False,  False)
            
    def config_xh_diode(self):
        #                                             channel,      "burst source", Gate,   Burst
        self.config_channel(self._controller.channels.D5,           "I3",           False,  False)
        self.config_channel(self._controller.channels.D25,          "I3",           False,  False)
        self.config_channel(self._controller.channels.DAM,          "I3",           False,   False)
        self.config_channel(self._controller.channels.DAM_Presimmer,"I3",           False,  False)
        self.config_channel(self._controller.channels.early_instru, "S1",           False,  True)
        self.config_channel(self._controller.channels.Instru,       "S1",           False,  True)
        self.config_channel(self._controller.channels.ModBox,       "I3",           False,  False)
        self.config_channel(self._controller.channels.target_fast_shutter,        "S1",           False,  True)
        self.config_channel(self._controller.channels.Pockels,      "I3",           False,  False)
        self.config_channel(self._controller.channels.Quantaray,    "I3",           False,  False)
        self.config_channel(self._controller.channels.Xh_Start,     "S1",           False,  True)
        self.config_channel(self._controller.channels.PCK_ISO_OFF,  "I3",           False,  False)

        if self._controller._target_shutter is not None:
            if self._use_target_shutter:
                print(RED("SET FAST SHUTTER IN EXTERNAL MODE"))
                self._controller._target_shutter.external()
        
    def config_streak_ref(self):
        #                                             channel,      "burst source", Gate,   Burst
        self.config_channel(self._controller.channels.D5,           "I3",           False,  False)
        self.config_channel(self._controller.channels.D25,          "I3",           False,  False)
        self.config_channel(self._controller.channels.DAM,          "I3",           False,   False)
        self.config_channel(self._controller.channels.DAM_Presimmer,"I3",           False,  False)
        self.config_channel(self._controller.channels.early_instru, "S1",           False,  True)
        self.config_channel(self._controller.channels.Instru,       "S1",           False,  True)
        self.config_channel(self._controller.channels.ModBox,       "I3",           False,  False)
        self.config_channel(self._controller.channels.target_fast_shutter,        "S1",           False,  False)
        self.config_channel(self._controller.channels.Pockels,      "I3",           True,   False)
        self.config_channel(self._controller.channels.Quantaray,    "I3",           False,  False)
        self.config_channel(self._controller.channels.Xh_Start,     "I3",           False,  False)
        self.config_channel(self._controller.channels.PCK_ISO_OFF,  "I3",           False,  False)
        
    def config_laser(self):
        #                                             channel,      "burst source", Gate,   Burst
        self.config_channel(self._controller.channels.D5,           "I3",           False,  False)
        self.config_channel(self._controller.channels.D25,          "I3",           False,  False)
        self.config_channel(self._controller.channels.DAM,          "I3",           False,  False)
        self.config_channel(self._controller.channels.DAM_Presimmer,"I3",           False,  False)
        self.config_channel(self._controller.channels.early_instru, "S1",           False,  True)
        self.config_channel(self._controller.channels.Instru,       "S1",           False,  True)
        self.config_channel(self._controller.channels.ModBox,       "I3",           False,  False)
        self.config_channel(self._controller.channels.target_fast_shutter,        "S1",           False,  False)
        self.config_channel(self._controller.channels.Pockels,      "S1",           False,  True)
        self.config_channel(self._controller.channels.Quantaray,    "I3",           False,  False)
        self.config_channel(self._controller.channels.Xh_Start,     "I3",           False,  False)
        self.config_channel(self._controller.channels.PCK_ISO_OFF,  "I3",           False,  False)
        
    def config_shock(self):
        #                                             channel,      "burst source", Gate,   Burst
        self.config_channel(self._controller.channels.D5,           "I3",           False,  False)
        self.config_channel(self._controller.channels.D25,          "I3",           False,  False)
        self.config_channel(self._controller.channels.DAM,          "I3",           False,  False)
        self.config_channel(self._controller.channels.DAM_Presimmer,"I3",           False,  False)
        self.config_channel(self._controller.channels.early_instru, "S1",           False,  True)
        self.config_channel(self._controller.channels.Instru,       "S1",           False,  True)
        self.config_channel(self._controller.channels.ModBox,       "I3",           False,  False)
        self.config_channel(self._controller.channels.target_fast_shutter,        "S1",           False,  True)
        self.config_channel(self._controller.channels.Pockels,      "S1",           False,  True)
        self.config_channel(self._controller.channels.Quantaray,    "I3",           False,  False)
        self.config_channel(self._controller.channels.Xh_Start,     "S1",           False,  True)
        self.config_channel(self._controller.channels.PCK_ISO_OFF,  "I3",           False,  False)

        if self._controller._target_shutter is not None:
            print(RED("SET FAST SHUTTER IN EXTERNAL MODE"))
            self._controller._target_shutter.external()
    
    def config_channel(self, channel, source, gate_on, burst_on):
        channel._gate_config(source)
        if gate_on:
            channel._gate_on()
        else:
            channel._gate_off()
        if burst_on:
            channel._burst_on(1)
        else:
            channel._burst_off()
 
class CitySynchroAcqMaster(AcquisitionMaster):
    def __init__(self, synchro_mode, synchro_ctrl):
        AcquisitionMaster.__init__(self, None, name="CitySynchroAcqMaster")
        self._synchro_mode = synchro_mode
        self._synchro_ctrl = synchro_ctrl
        
    def prepare(self):
        self._synchro_ctrl.prepare_synchro(self._synchro_mode)
            
    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        self._synchro_ctrl.start_synchro(self._synchro_mode)

    def trigger_ready(self):
        return True

    def wait_ready(self):
        pass

    def stop(self):
        self._synchro_ctrl.post_synchro(self._synchro_mode)
                   
class ID24cityChannelAttrList(ID24attrList):
    
    def __info__(self):
        # µs (keep mu as example)
        lines = [["  ", "", "Channel", "Status", "Burst", "Gate", "Source", "Source State", "Freq", "Abs. Delay", "Rel. Delay"]]
        for name, dev in self._items.items():
            (title, channel, state, burst_state, gate_state, gate_src, gate_src_state, freq, master, rel, delay_unit, abs_delay) = dev._get_condensed_info()
            if master is not None:
                rel_str = f"{rel:_.3f} {delay_unit} / {master._title}"
            else:
                rel_str = ""
            lines.append(["  ", title, channel, state, burst_state, gate_state, gate_src, gate_src_state, freq, f"{abs_delay:_.3f} ns", rel_str])
        mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        print(mystr)
        return ""


class ID24cityChannel:

    def __init__(self, name, config):
    
        self._freq_allowed = [80.0, 10.0, 5.0, 1.0, 0.5, 0.1, 355212, 5683403]
        self._freq_div = [4404604, 35236832, 70473664, 352368320, 704736640,3523683200, 992, 62]
        self._tick_time = 1.0/352371000.0
        self._time = {"s": 1.0, "ms":1e3, "us": 1e6, "ns": 1e9, "ps": 1e12}

        self._name = name
        self._config = config
        
        self._city = config.get("city", None)
        self._title = config.get("title", self._name)
        self._master = config.get("master", None)

        self._config_file = config.get("config", None)
        if self._config_file is None:
            raise RuntimeError(f"No config file defined for channel {self._title}")
        self._read_config_file()
 
        self._channel = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_channel", default_value=self._default_values["channel"])
        self._freq = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_freq", default_value=self._default_values["freq"])
        self._freq_unit = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_freq_unit", default_value=self._default_values["freq_unit"])
        self._freq_lock = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_freq_lock", default_value=self._default_values["freq_lock"])
        self._width = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_width", default_value=self._default_values["width"])
        self._width_unit = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_width_unit", default_value=self._default_values["width_unit"])
        self._default_delay = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_default_delay", default_value=self._default_values["delay"])
        self._extra_delay = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_extra_delay", default_value=0.0)
        self._delay_unit = settings.SimpleSetting(f"CityChannel_{self._city._name}_{name}_delay_unit", default_value=self._default_values["delay_unit"])
                
        self._source = None
            
    ################################################################
    #####
    ##### Default values configuration file
    #####
    def _read_config_file(self):
        if os.path.exists(self._config_file) is None:
            raise RuntimeError(f"Config file \"{self._config_file}\" does not exists for channel {self._name}")
        yaml = YAML(pure=True)
        with open(self._config_file) as file_in:
            self._default_values = yaml.load(file_in)
            if "div" in self._default_values.keys():
                self._default_values["freq"] = self._div2freq(self._default_values["div"])
            if "freq_lock" not in self._default_values.keys():
                self._default_values["freq_lock"] = False

    ################################################################
    #####
    ##### Master channel for relative delay value
    #####
    def _set_master_channel(self, master):
        self._master = master
    
    ################################################################
    #####
    ##### User info
    #####
    def __info__(self):        
        print(self._get_info())
        return ""
        
    def _get_info(self):
        # MASTER
        if self._master is not None:
            master_city_values = self._master._get_city_config()
            master_city_delay = self._s2unit(
                self._unit2s(
                    master_city_values["delay"] + master_city_values["fdelay"],
                    self._master.delay_unit
                ),
                self.delay_unit
            )
        
            master_default_delay_s = self._unit2s(self._master._default_values["delay"], self._master._default_values["delay_unit"])
            master_default_delay = self._s2unit(master_default_delay_s, self._default_values["delay_unit"])
        
        # CITY
        city_values = self._get_city_config()
        city_freq = city_values["freq"]
        city_div = city_values["div"]
        city_width = city_values["width"]
        city_width_unit = city_values["width_unit"]
        city_width_tick = city_values["width_tick"]
        city_delay = city_values["delay"]
        city_fdelay = self._s2unit(self._unit2s(city_values["fdelay"], self.delay_unit), "ns")
        city_delay_tick = city_values["delay_tick"]
        if self._master is not None:
            city_delay_rel = city_delay - master_city_delay
        
        # CURRENT
        current_fdelay = self._s2unit(self._unit2s(self.delay_fine, self.delay_unit), "ns")
        
        # DEFAULT
        default_freq = self._default_values["freq"]
        default_div = self._freq2div(default_freq)
        default_width = self._default_values["width"]
        default_width_unit = self._default_values["width_unit"]
        default_width_tick = self._s2tick(self._unit2s(default_width, default_width_unit))
        default_delay = self._default_values["delay"]
        default_delay_unit = self._default_values["delay_unit"]
        default_delay_tick = self._s2tick(self._unit2s(default_delay, default_delay_unit))
        default_fdelay = self._unit2s(default_delay, default_delay_unit) - self._tick2s(default_delay_tick)
        default_fdelay = self._s2unit(default_fdelay, "ns")
        if self._master is not None:
            default_delay_rel = default_delay - master_default_delay
        
        # EXTRA delay
        extra_delay_tick = self._s2tick(self._unit2s(self.extra_delay, self.delay_unit))
        
        lines = []
        
        mystr = f"  {BOLD(self._title)} - {BOLD(self.channel)} - OUTPUT:{self.state} - BURST: {self.burst_state} GATE:{self.gate_state} SRC:{self.gate_src}={self.gate_src_state}\n"
        
        #
        # FREQUENCY
        #
        lines.append(["  ",BOLD(BLUE("FREQUENCY")),BOLD("Freq"),BOLD("Divider")])
        lines.append(["  ",BOLD("City"),GREEN(f"{city_freq:_.1f} Hz"),GREEN(f"{city_div:,}".replace(",", " "))])
        lines.append(["  ",BOLD("Current"),GREEN(f"{self.freq:_.1f} Hz"),GREEN(f"{self.div:,}".replace(",", " "))])
        lines.append(["  ",BOLD("Default"),GREEN(f"{default_freq:_.1f} Hz"),GREEN(f"{default_div:,}".replace(",", " "))])
        
        lines.append(["     ", "", "", "", "", ""])        
        
        #
        # DELAY
        #
        
        # TITLE        
        line = [
            "  ",
            BOLD(BLUE("DELAY")),
            BOLD("Coarse Delay"),
            BOLD("Ticks"),
            BOLD("Fine delay"),
            BOLD("Abs. Delay"),            
        ]
        if self._master is not None:
            line.append(BOLD(f"Rel/{self._master._title}"))
        lines.append(line)
        
        # CITY values
        res = self._get_city_config()
        abs_delay = self._s2unit(self._unit2s(city_delay, self.delay_unit), "ns")+city_fdelay
        line = [
            "  ",
            BOLD("City"),
            GREEN(f"{city_delay:_.3f} {self.delay_unit}"),
            GREEN(f"{city_delay_tick:,}".replace(",", " ")),
            GREEN(f"{city_fdelay:_.3f} ns"),
            GREEN(f"{abs_delay:_.3f} ns")
        ]
        if self._master is not None:
            line.append(GREEN(f"{city_delay_rel:_.3f} {self.delay_unit}"))
        lines.append(line)
        
        # CURRENT values
        coarse_delay = self._s2unit(self._tick2s(self.delay_tick), self.delay_unit)
        abs_delay = self._s2unit(self._unit2s(self.delay, self.delay_unit), "ns")
        line = [
            "  ",
            BOLD("Current"),
            #GREEN(f"{self.delay:_.3f} {self.delay_unit}"),
            GREEN(f"{coarse_delay:_.3f} {self.delay_unit}"),
            GREEN(f"{self.delay_tick:,}".replace(",", " ")),
            GREEN(f"{current_fdelay:_.3f} ns"),
            GREEN(f"{abs_delay:_.3f} ns")
        ]
        if self._master is not None:
            line.append(GREEN(f"{self.delay_rel:_.3f} {self.delay_unit}"))
        lines.append(line)
        
        # DEFAULT values
        coarse_delay = self._s2unit(self._tick2s(default_delay_tick), self.delay_unit)
        abs_delay = self._s2unit(self._unit2s(self.default_delay, self.delay_unit), "ns")
        line = [
            "  ",
            BOLD("Default"),
            GREEN(f"{coarse_delay:_.3f} {self.delay_unit}"),
            GREEN(f"{default_delay_tick:,}".replace(",", " ")),
            GREEN(f"{default_fdelay:_.3f} ns"),
            GREEN(f"{abs_delay:_.3f} ns")
        ]
        if self._master is not None:
            line.append(GREEN(f"{default_delay_rel:_.3f} {default_delay_unit}"))
        lines.append(line)
        
        # EXTRA delay
        lines.append([
            "  ",
            BOLD("Extra"),
            GREEN(f"{self.extra_delay:_.3f} {self.delay_unit}"),
            GREEN(f"{extra_delay_tick:,}".replace(",", " ")),
            "",
            ""
        ])
        
        lines.append(["     ", "", "", "", "", ""])        
        
        #
        # WIDTH
        #
        lines.append(["  ",BOLD(BLUE("WIDTH")),BOLD("Width"),BOLD("Ticks")])
        lines.append(["  ",BOLD("City"),GREEN(f"{city_width:.0f} {city_width_unit}"),GREEN(f"{city_width_tick:,}".replace(",", " "))])
        lines.append(["  ",BOLD("Current"),GREEN(f"{self.width:.0f} {self.width_unit}"),GREEN(f"{self.width_tick:,}".replace(",", " "))])
        lines.append(["  ",BOLD("Default"),GREEN(f"{default_width:.0f} {default_width_unit}"),GREEN(f"{default_width_tick:,}".replace(",", " "))])

        mystr += "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        mystr += "\n\n"
        
        return mystr
    
    def _get_condensed_info(self):
        # MASTER
        if self._master is not None:
            master = self._master._get_city_config()
            master_city_delay = self._s2unit(self._unit2s(master["delay"], self._master.delay_unit), self.delay_unit)

        # CITY
        res = self._get_city_config()
        city_freq = res["freq"]
        city_div = res["div"]
        city_width = res["width"]
        city_width_unit = res["width_unit"]
        city_width_tick = res["width_tick"]
        city_delay = res["delay"]
        city_delay_tick = res["delay_tick"]
        city_fdelay = self._s2unit(self._unit2s(res["fdelay"], self.delay_unit), "ns")
        abs_delay = self._s2unit(self._unit2s(city_delay, self.delay_unit), "ns")+city_fdelay
        city_delay_rel = 0.0
        if self._master is not None:
            city_delay_rel = city_delay - master_city_delay
        
        title = BOLD(self._title)
        channel = BOLD(self.channel)
        
        return (title, channel, self.state, self.burst_state, self.gate_state, self.gate_src, self.gate_src_state, city_freq, self._master, city_delay_rel, self.delay_unit, abs_delay)
        
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }
        md["_h5_"] = {
            "name": self._title,
            "channel": self.channel,
            "output_state": self.state,
            "burst_state": self.burst_state,
            "gate_state": self.gate_state,
            "gate_src": self.gate_src,
            "gate_src_state": self.gate_src_state,
            "freq": f"{self.freq}Hz - {self.div}",
            "delay": f"{self.delay}{self.delay_unit} - {self.delay_tick}",
            "delay_rel": f"{self.delay_rel}{self.delay_unit} - {self.delay_rel_tick}",
            "width": f"{self.width}{self.width_unit} - {self.width_tick}",
        }
        if self._master is not None:
            md["_h5_"]["delay_rel"] = f"{self.delay_rel}{self.delay_unit} - {self.delay_rel_tick}"
        return md
        
    ################################################################
    #####
    ##### Channel
    #####
    @property
    def state(self):
        rep = self.city.command(f"?OEN {self.channel}")
        return rep.split()[1]
    
    def on(self):
        self.city.command(f"OEN {self.channel} ON")
    
    def off(self):
        self.city.command(f"OEN {self.channel} OFF")
            
    @property
    def channel(self):
        return self._channel.get()        
    
    def _restore_defaults(self):
        self._read_config_file()
        self._channel.set(self._default_values["channel"])
        self._freq.set(self._default_values["freq"])
        self._freq_unit.set(self._default_values["freq_unit"])
        self._width.set(self._default_values["width"])
        self._width_unit.set(self._default_values["width_unit"])
        self._default_delay.set(self._default_values["delay"])
        self._delay_unit.set(self._default_values["delay_unit"])
        self._set_city_config()
        self.on()
        
    ################################################################
    #####
    ##### Gate
    #####
    @property
    def burst_state(self):
        ret = self.city.command(f"?BURST {self.channel}")
        if ret.split()[1] == "OFF":
            return "OFF"
        else:
            return str(ret.split()[2])
        
    @property
    def gate_state(self):
        ret = self.city.command(f"?GATE {self.channel}")
        return ret.split()[1]
        
    @property
    def gate_src(self):
        ret = self.city.command(f"?GATE {self.channel}")
        return ret.split()[4]
        
    @property
    def gate_src_state(self):
        src = self.gate_src
        if src in ["S1", "S2", "S3", "S4"]:
            ret = self.city.command(f"?SGATE {src}")
            return ret.split()[1]
        else:
            return "OFF"
 
    def _gate_config(self, gate_src):
        self.city.command(f"GATE {self.channel} SRCE {gate_src} NORMAL")

    def _gate_on(self):
        self.city.command(f"GATE {self.channel} ON")
        
    def _gate_off(self):
        self.city.command(f"GATE {self.channel} OFF")

    def _burst_on(self, nb_count):
        self.city.command(f"BURST {self.channel} ON {nb_count}")
        
    def _burst_off(self):
        self.city.command(f"BURST {self.channel} OFF")
                    
    ################################################################
    #####
    ##### Freq
    #####
    @property
    def freq(self):
        return self._freq.get()

    @freq.setter
    def freq(self, freq):
        if self._freq_lock.get():
            raise RuntimeError(f"Channel {self._name}: Frequency is locked. Change it using the YML configuration file")
        self._freq.set(freq)
        self._set_city_config()
        
    @property
    def freq_unit(self):
        return self._freq_unit.get()
        
    @property
    def div(self):
        return self._freq2div(self.freq)
    
    ################################################################
    #####
    ##### Width
    #####
    @property
    def width(self):
        return self._width.get()

    @width.setter
    def width(self, width):
        if self.width_unit == "%":
            raise RuntimeError("Cannot change width value on 50% mode")
        self._width.set(width)
        self._set_city_config()
        
    @property
    def width_unit(self):
        return self._width_unit.get()
        
    @property
    def width_tick(self):
        return self._s2tick(self._unit2s(self.width, self.width_unit))
    
    ################################################################
    #####
    ##### Delay
    #####
    @property
    def default_delay(self):
        return self._default_delay.get()
        
    @property
    def extra_delay(self):
        return self._extra_delay.get()
    
    @extra_delay.setter
    def extra_delay(self, extra_delay):
        self.delay = self._default_delay.get() + self._s2unit(self._unit2s(extra_delay, "ns"), self.delay_unit)
        
    @property
    def delay(self):
        return self._default_delay.get()+self._extra_delay.get()

    @delay.setter
    def delay(self, delay):
        self._extra_delay.set(delay - self._default_delay.get())
        self._set_city_config()
        
    @property
    def delay_unit(self):
        return self._delay_unit.get()
        
    @property
    def delay_tick(self):
        return self._s2tick(self._unit2s(self.delay, self.delay_unit))
        
    @property
    def delay_fine(self):
        delay_fine = self._unit2s(self.delay, self.delay_unit) - self._tick2s(self.delay_tick)
        delay_fine = self._s2unit(delay_fine, self.delay_unit)
        return delay_fine

    @property
    def delay_rel(self):
        if self._master is not None:
            master_current_delay = self._s2unit(self._unit2s(self._master.delay, self._master.delay_unit), self.delay_unit)
            current_delay_rel = self.delay - master_current_delay
        else:
            current_delay_rel = 0
        return current_delay_rel
        
    @property
    def delay_rel_tick(self):
        return self._s2tick(self._unit2s(self.delay_rel, self.delay_unit))
        
    def set_delay_rel(self, value):
        old_delay = self.delay
        new_delay = self.delay + value
        self.delay = new_delay
        
        res = self._get_city_config()
        current_delay = res["delay"] + res["fdelay"]
        print(f"\n    Previous Delay: {old_delay:.6f}{self.delay_unit}")
        print(f"    Current  Delay: {current_delay:.6f}{self.delay_unit}\n")
        
    ################################################################
    #####
    ##### Usefull functions
    #####
    def _freq2div(self, freq):
        return self._freq_div[self._freq_allowed.index(freq)]
        
    def _div2freq(self, div):
        return self._freq_allowed[self._freq_div.index(div)]
        
    def _tick2s(self, value):
        return value * self._tick_time
    
    def _s2tick(self, value):
        return int(value / self._tick_time)
        
    def _unit2s(self, value, unit):
        if unit == "%":
            period = 1.0 / self.freq
            return (float(value) / 100.0) * period
        else:
            return float(value / self._time[unit])
        
    def _s2unit(self, value, unit):
        if unit == "%":
            period = 1.0 / self.freq
            return int(0.5 * 100.0 * value / period)
        else:
            return value * self._time[unit]
            
    ################################################################
    #####
    #####  CITY configuration (get/set)
    #####
    def _set_city_config(self):
        delay_fine = 0
        cmd  = f"OCKSET {self.channel} "
        cmd += f"DIV {self.div} "
        if self.width_unit == "%":
            cmd += f"WIDTH {self.width}% "
        else:
            cmd += f"WIDTH {self.width_tick} "
        # delay              
        cmd += f"DELAY {self.delay_tick} "
        if self.channel.find("OA") != -1:
            # Fine delay
            delay_fine = self._s2unit(self._unit2s(self.delay_fine, self.delay_unit), "ps")
            delay_fine = delay_fine / 9.0  
        
        if self._city is not None:
            self._city.command(cmd)
            if self.channel.find("OA") != -1:
                self._city.command(f"FDELAY {self.channel} {delay_fine}")
            #self._city.command(f"OSYNC {self.channel}")
            self._city.command(f"OSYNC")
        else:
            print(f"Send \"{cmd}\"")

    def _get_city_config(self):
        if self._city is not None:
            command  = f"?OCKSET {self.channel} "
            res = self._city.command(command)
            aux = res.split()
            
            read = {}
            # freq
            read["div"] = int(aux[2])
            read["freq"] = self._div2freq(read["div"])
            # width
            if aux[4].find("%") != -1:
                read["width"] = int(aux[4].split("%")[0])
                read["width_unit"] = "%"
                read["width_tick"] = self._s2tick(self._unit2s(read["width"], "%"))
            else:
                read["width"] = self._s2unit(self._tick2s(int(aux[4])), self.width_unit)
                read["width_unit"] = self.width_unit
                read["width_tick"] = int(aux[4])
            # delay
            read["delay"] = self._s2unit(self._tick2s(int(aux[6])), self.delay_unit)
            read["delay_tick"] = int(aux[6])
            # Fine delay
            if self.channel.find("OA") != -1:
                command  = f"?FDELAY {self.channel} "
                res = self._city.command(command)
                read["fdelay"] = self._s2unit(self._unit2s(float(res)*9.0, "ps"), self.delay_unit)
            else:
                read["fdelay"] = 0
            
            return read            
