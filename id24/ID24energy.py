import numpy as np

def change_energy(target_Energy, h, k, l, asym):
    
    asilicon = 5.43

    dspacing = asilicon / np.sqrt(h * h + k * k + l * l)
    constpar=12.39854/(2*dspacing)
    
    theta_target = np.arcsin(constpar / target_Energy) / np.pi * 180.0
    theta_s = 2.0 * theta_target
    theta_m = 2.0 * theta_target
    theta_b = 2.0 * theta_target
 
    #x_target=3400*sin(2*theta_target)

    theta_p = theta_target

    # asym case
    if asym != 0.0:
        theta_p = theta_p - asym

    #----------------------------
    # Computing the gap values
    #-----------------------------

    energy = target_Energy
    lamb = [27, 32]
    alph = [1.0072598, 1.01337772]
    b0 = [2.1041, 1.9103]
    harmstr = ["Fundamental", "Second", "Third", "Fourth", "Fifth", "Sixth"]
    undstr = ["U27", "U32"]
    gapmin = [12.0, 12.0]
    Ea = 6.04
    # array flagi[harmonics][und] with harmonics 0..5 and und 0..3
    flagi = np.zeros((6,2))
    flagi[:][:] = 1.0
    value_gap = np.zeros((6,2))

    #  following will set the array value_gap[harmonics][und] 
    #  if the flagi[harmonics][und] = 1

    #  loop on the harmonics
    for ii in range(6):
        # loop on the undulators
        for jj in range(2):
            # the harmonic 1..6
            nn = ii+1
            valint = ((0.95 * Ea*Ea * 10)/(energy/nn*lamb[jj])) - 1
            if valint > 0:
                value_gap[ii][jj] = (-1)*lamb[jj]/alph[jj]/np.pi * np.log( np.sqrt(2*valint) *10/0.934/lamb[jj]/b0[jj])
            else:
                flagi[ii][jj] = 0

    print("********************************************************")
    print("********************************************************")
    print(f"Positions proposed for Energy      =    {target_Energy:8.3f} KeV")
    print(f"New proposed position for polyth   =    {theta_p:8.3f}")
    print(f"New proposed position for bed2th   =    {theta_b:8.3f}")
    print(f"New proposed position for polyshut =    {theta_s:8.3f}")
    print(f"New proposed position for theta_m  =    {theta_m:8.3f}")
    print("********************************************************")

    print("Gap proposed values")

    # loop on the undulators
    for jj in range(2):
        print(f"On undulator {undstr[jj]}")
        # loop on the harmonics
        for ii in range(6):
            if (flagi[ii][jj] == 1.0) and (value_gap[ii][jj] > gapmin[jj]):
                print(f"{harmstr[ii]} harmonic gap = {value_gap[ii][jj]} mm")
            else:
                print(f"{harmstr[ii]} harmonic gap out of range")
        print("--------------------------------------------------------")

    print("********************************************************")
    print("********************************************************")
    print("Proposed commands are:")
    print(f"umv(polyth, {theta_p:8.3f})")
    print(f"umv(bed2th, {theta_b:8.3f})")
    print(f"umv(polyshut, {theta_s:8.3f})")
    print(f"umv(theta_m, {theta_m:8.3f})")
    print("********************************************************")

    if target_Energy <= 10.0:
        print("Most appropriate stripes on mirrors are Si ones")
    if (target_Energy > 10.0) and (target_Energy < 20.0):
        print("Most appropriate stripes on mirrors are Rh ones")
    if (target_Energy >= 20.0) and (target_Energy < 27.0):
        print("Most appropriate stripes on mirrors are Pt ones")
    if target_Energy > 27.0:
        print("Outside energy range: You can have problems with the reflectivity of the mirrors.\n Use anyway Pt stripes")
    if target_Energy < 4.5:
        print("Outside energy range: Are you sure that the ondulator can give you this energy?")

    print("********************************************************")
    print("********************************************************")

