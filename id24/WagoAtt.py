
import numpy
import tabulate
import itertools
from io import StringIO

from bliss.controllers.bliss_controller import BlissController

class WagoAttenuators(BlissController):

    def __init__(self, config):
        super().__init__(config)


    """
    Load Configuration
    """
    def _load_config(self):
        self._wago = self.config.get("wago", None)
        if self._wago is None:
            raise RuntimeError(f"WagoAttenuators {self.name}: No wago controller given")
        
        self._radix = self.config.get("radix", None)
        if self._radix is None:
            raise RuntimeError(f"WagoAttenuators {self.name}: No wago Attenuators radix given")
        
        self._mono = self.config.get("monochromator", None)
        
        self._attenuators = {}
        att_configs = self.config.get("attenuators", None)
        if att_configs is None:
            raise RuntimeError(f"WagoAttenuators {self.name}: No Attenuators description")
        for att_config in att_configs:
            att_name = att_config.get("att_name")
            self._attenuators[att_name] = WagoAttenuator(att_name, self, att_config)
            
        self._used_attenuators = {}
        used_att = self.config.get("used_att", None)
        if used_att is None:
            self._used_attenuators = self._attenuators
        else:
            for name in used_att:
                self._used_attenuators[name] = self._attenuators[name]
                       
    """
    Initialization
    """
    def _init(self):
        self._attenuation = WagoAttenuation()
        self._nbatt = len(self._used_attenuators.keys())
                
    def __info__(self):
        states = self._channels_state()
        lines = []
        line = [["Name"], ["State"], ["Material"], ["Thickness"], ["Channel"]]
        for name, att in self._used_attenuators.items():
            if len(line[0]) == 8:
                for i in range(5):
                    lines.append(line[i])
                lines.append(["", "", "", "", "", "", "", ""])
                line = [["Name"], ["State"], ["Material"], ["Thickness"], ["Channel"]]
            line[0].append(name)
            line[1].append(states[att.channel])
            line[2].append(att.material)
            line[3].append(att.thickness)
            line[4].append(att.channel)
        for i in range(5):
            lines.append(line[i])
        mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        return mystr
            
    def attenuate(self, attenuation, energy=None, nb_att_max=None):
        if energy is None:
            if self._mono is None:
                raise RuntimeError("No Monochromator specified to get current energy")
            self._mono._motors["bragg_rotation"].sync_hard()
            bragg_offset = self._mono._motors["bragg"].offset
            energy = self._mono.bragg2energy(self._mono._motors["bragg_rotation"].position+bragg_offset)
        if attenuation == 1:
            self._channels_set([])
            print("All attenuators are removed")
        else:
            desired_mu = numpy.log(attenuation)
            if nb_att_max is None:
                nb_att_max = self._nbatt
            elif nb_att_max > self._nbatt:
                
                nb_att_max = self._nbatt

            att_mu = {}
            for name, att in self._used_attenuators.items():
                att_mu[name] = self._used_attenuators[name].attenuation(energy)

            for natt in range(nb_att_max):
                combo_list = list(itertools.combinations(self._used_attenuators.keys(),natt+1))
                sumlist = []
                for combo in combo_list:
                    mu_list = [att_mu[name] for name in combo]
                    sumlist.append(sum(mu_list))
                sumlist = numpy.asarray(sumlist)

                # For a given number of attenuators find a combination with
                # the smallest difference between the actual and the desired mu
                diff = abs(sumlist-desired_mu)
                idx = numpy.abs(diff).argmin()
                
                # Choose the best single attenuator
                if natt == 0:
                    best_mu = sumlist[idx]
                    best_diff=diff[idx]
                    best_combo = combo_list[idx]
                # Increase the number of attenuators and see whether the best
                # combination gives an improvement in mu better than 1% compared to the previous best combination.
                # If yes, declare this combination the best
                else:
                    if (diff[idx]-best_diff)/desired_mu<-0.01:
                        best_mu = sumlist[idx]
                        best_diff = diff[idx]
                        best_combo = combo_list[idx]
                                
            rel_diff = abs((numpy.e**best_mu-attenuation)/attenuation)
            
            print(f"\nAttenuators suggested for attenuation factor {attenuation} at {energy} keV:")
            channel_list = []
            for att_name in best_combo:
                channel_list.append(self._used_attenuators[att_name].channel)
                att_mat = self._used_attenuators[att_name].material
                att_d = self._used_attenuators[att_name].thickness
                att_mu = self._used_attenuators[att_name].attenuation(energy)
                att_chan = self._used_attenuators[att_name].channel
                print(f"    channel {att_chan} - {att_mat} {att_d} mm, mu={att_mu:.2f}")
            print(f"\nResulting total mu           : {best_mu:.2f}")
            print(f"Resulting attenuation factor : {numpy.e**best_mu:.2f}")
            print(f"Relative attenuation error   : {rel_diff*100:.2f}%\n")
            
            
            self._channels_set(channel_list)
    
    @property
    def current_attenuation(self):
        if self._mono is None:
            raise RuntimeError("No Monochromator specified to get current energy")
        self._mono._motors["bragg_rotation"].sync_hard()
        bragg_offset = self._mono._motors["bragg"].offset
        energy = self._mono.bragg2energy(self._mono._motors["bragg_rotation"].position+bragg_offset)
        attenuation = 0.0
        for name, att in self._used_attenuators.items():
            if self._is_in(name):
                attenuation += att.attenuation(energy)
        return numpy.e**attenuation
        
    def _channels_set(self, channel_list):
        set_list = self._wago.get(f"{self._radix}_ctrl")
        for name, att in self._attenuators.items():
            chan = att.channel
            if chan in channel_list:
                set_list[chan] = 1
            else:
                set_list[chan] = 0
        self._wago.set(f"{self._radix}_ctrl", set_list)
    
    def channel_in(self, channel):
        channel_configured = False
        for name, att in self._attenuators.items():
            if channel == att.channel:
                channel_configured = True
        if channel_configured:
            set_list = self._wago.get(f"{self._radix}_ctrl")
            set_list[channel] = 1
            self._wago.set(f"{self._radix}_ctrl", set_list)
    
    def channel_out(self, channel):
        channel_configured = False
        for name, att in self._attenuators.items():
            if channel == att.channel:
                channel_configured = True
        if channel_configured:
            set_list = self._wago.get(f"{self._radix}_ctrl")
            set_list[channel] = 0
            self._wago.set(f"{self._radix}_ctrl", set_list)
    
    def _is_in(self, name):
        channel = self._attenuators[name].channel
        set_list = self._wago.get(f"{self._radix}_ctrl")
        if set_list[channel] == 1:
            return True
        return False
        
    def all_in(self):
        set_list = self._wago.get(f"{self._radix}_ctrl")
        for name, att in self._attenuators.items():
            set_list[att.channel] = 1
        self._wago.set(f"{self._radix}_ctrl", set_list)
        
    def all_out(self):
        set_list = self._wago.get(f"{self._radix}_ctrl")
        for name, att in self._attenuators.items():
            set_list[att.channel] = 0
        self._wago.set(f"{self._radix}_ctrl", set_list)

    def _channels_state(self):
        states = []
        states_in = self._wago.get(f"{self._radix}_in")
        states_out = self._wago.get(f"{self._radix}_out")
        for idx in range(len(states_in)):
            if states_in[idx] == 1:
                if states_out[idx] == 1:
                    states.append("FAULT")
                else:
                    states.append("IN")
            else:
                if states_out[idx] == 1:
                    states.append("OUT")
                else:
                    states.append("MOVING")
        return states

class WagoAttenuator:
    
    def __init__(self, name, controller, config):
        self.name = name
        self._controller = controller
        self.material = config.get("material")
        self.thickness = config.get("thickness")
        self.channel = config.get("channel")
        self.radix = self._controller._radix
        
    def attenuation(self, energy):
        attenuation = self._controller._attenuation.interpolation(self.material, energy)
        return self.thickness * attenuation / 10
        
class WagoAttenuation:
    
    def __init__(self):
        self._load_data()
        
    def interpolation(self, material, energy):
        return numpy.interp(energy, self.data["energy"], self.data[material])
    
    def _load_data(self):
        c = StringIO(WAGO_ATT_TABLE)
        _data = numpy.loadtxt(c).transpose()
        self.data = {
            "energy": _data[0],
            "GlassyC": _data[1],
            "Al": _data[2],
            "Ti": _data[3],
            "PyroC": _data[4],
            "Cu": _data[4], # DUMMY VALUES
        }

# Eneregy GlassyC Al Ti PyroC Cu
# Data are in cm-1 from here : https://physics.nist.gov/PhysRefData/FFast/html/form.html
# Densities used for calcs of linear abs coeffs: PyroC: 2.25, GlassyC 1.5, Al 2.7, Ti 4.5 g/cm3
WAGO_ATT_TABLE = """\
4.06    51.05   905.66  10000.00   7.6579E+01
4.33    41.70   750.79  10000.00   6.2548E+01
4.63    34.08   622.49  10000.00   5.1113E+01
4.95    27.86   516.19  10000.00   4.1791E+01
5.30    22.80   428.09  10000.00   3.4193E+01
5.66    18.58   355.08  10000.00   2.7864E+01
6.05    15.14   294.57  1885.80    2.2716E+01
6.47    12.36   244.42  1591.80    1.8541E+01
6.92    10.10   202.33  1335.10    1.5153E+01
7.39    8.27    166.71  1118.5     1.2404E+01
7.90    6.78    137.39  936.39     1.0173E+01
8.45    5.57    113.27  783.77     8.3612E+00
9.03    4.59    93.25   655.31     6.8814E+00
9.65    3.77    76.29   545.99     5.6569E+00
10.32   3.11    62.45   453.26     4.6679E+00
11.03   2.58    51.15   375.12     3.8687E+00
11.79   2.15    41.92   310.44     3.2220E+00
12.61   1.80    34.38   256.97     2.6989E+00
13.48   1.52    28.23   212.77     2.2752E+00
14.41   1.29    23.19   176.23     1.9319E+00
15.40   1.10    19.08   146.00     1.6534E+00
16.46   0.95    15.71   121.00     1.4273E+00
17.60   0.83    12.96   100.32     1.2434E+00
18.81   0.73    10.71   83.22      1.0937E+00
20.11   0.65    8.87    69.05      9.7157E-01
21.50   0.58    7.36    57.34      8.7176E-01
22.98   0.53    6.13    47.35      7.8995E-01
24.57   0.48    5.11    39.13      7.2270E-01
26.26   0.44    4.28    32.37      6.6722E-01
28.08   0.41    3.59    26.66      6.2078E-01
30.01   0.39    3.02    21.94      5.8210E-01
32.09   0.37    2.55    18.09      5.4979E-01
34.30   0.35    2.17    14.94      5.2265E-01
36.67   0.33    1.85    12.36      4.9963E-01
39.20   0.32    1.60    10.26      4.7993E-01
41.90   0.31    1.39    8.53       4.6289E-01
44.79   0.30    1.21    7.11       4.4798E-01
47.88   0.29    1.07    5.95       4.3481E-01
"""
