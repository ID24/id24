
import os
import tabulate
from ruamel.yaml import YAML
import gevent

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.scanning.chain import AcquisitionMaster

class ID24shutterContainer:
    
    def __init__(self, name, config):
        
        self._name = name
        self._config = config
        
        self._shutters = {}
        shutter_list = config.get("shutters", None)
        if shutter_list is not None:
            for shutter_config in shutter_list:
                name = shutter_config.get("shutter_name", None)
                self._shutters[name] = {"shutter": shutter_config.get("shutter")}
                
        self._exclusions = {}
        exclusion_list = config.get("exclusions", None)
        if exclusion_list is not None:
            for exclusion_config in exclusion_list:
                name = exclusion_config.get("exclusion_name", None)
                self._exclusions[name] = []
                excluded_shutter_list = exclusion_config.get("excluded_shutters", None)
                for excluded_shutter_config in excluded_shutter_list:
                    self._exclusions[name].append(excluded_shutter_config.get("excluded_shutter_name"))
                    
        for shutter in self._shutters.keys():
            self._shutters[shutter]["object"] = ID24shutterObject(self, shutter, self._shutters[shutter]["shutter"])
            setattr(self, shutter, self._shutters[shutter]["object"])  

    def __info__(self):
        mystr = "\nSHUTTERS:\n"
        lines = [["    ", "Name", "Shutter"]]
        for shutter_name in self._shutters.keys():
            shutter = self._shutters[shutter_name]["shutter"]
            lines.append(["    ", shutter_name, shutter._name])
        mystr += tabulate.tabulate(lines, tablefmt="plain")

        mystr += "\n\nEXCLUSIONS:\n"
        lines = [["    ", "Name", "Excluded Shutters"]]
        for exclusion_name in self._exclusions.keys():
            shutter_list = " / ".join(self._exclusions[exclusion_name])
            lines.append(["    ", exclusion_name, shutter_list])
        mystr += tabulate.tabulate(lines, tablefmt="plain")
        return mystr
        
    def _get_exclusion(self, shutter):
        exclusion_list = []
        for exclusion_name in self._exclusions.keys():
            if shutter in self._exclusions[exclusion_name]:
                exclusion_list.append(exclusion_name)
        return exclusion_list
    
class ID24shutterObject:
    def __init__(self, container, name, shutter):
        self._container = container
        self._name = name
        self._shutter = shutter
    
    def _force_open(self):
        self._shutter.open()

    def open(self):
        exclusion_list = self._container._get_exclusion(self._name)
        for exclusion_name in exclusion_list:
            for shutter in self._container._exclusions[exclusion_name]:
                if shutter != self._name:
                    self._container._shutters[shutter]["shutter"].close()
        self._shutter.open()
        
    def close(self):
        self._shutter.close()
        
    def state(self):
        return self._shutter.state
