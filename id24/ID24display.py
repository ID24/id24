
import numpy

from bliss.common import plot

class DataDisplay:
    
    def __init__(self, name, config):
        self._name = name
        self._config = config
        
    def plot_curvestack(self, name, plot_id, data_stack, data_x, label_stack, label_x, title):
        f = plot.get_flint()
        p = f.get_plot(plot_class="curvestack", name=name, unique_name=plot_id)
        p.set_data(curves=data_stack, x=data_x)
        p.xlabel = label_stack
        p.ylabel = label_x
        p.title = title
    
    def plot_curve(self, plot_label, plot_title, plot_id, data_x, data_y, label_x, label_y, names=None):
        f = plot.get_flint()
        p = f.get_plot(plot_class="curve", name=plot_label, unique_name=plot_id)
        p.clear_data()
        
        if names is None:
            legend = ["curve"]
        else:
            legend = names
        # legend is the unique name of a curve
        if data_y.ndim == 1:
            p.add_curve(x=data_x, y=data_y, legend="curve")
        elif data_y.ndim == 2:
            for i in range(data_y.shape[0]):
                leg = legend[i]
                p.add_curve(x=data_x, y=data_y[i], legend=f"{leg}", yaxis='left' if i == 0 else 'right')
        else:
            assert False

        p.xlabel = label_x
        p.ylabel = label_y
        p.title = plot_title
    
    def plot_image(self, plot_label, plot_title, plot_id, data, label_x, label_y):
        f = plot.get_flint()
        p = f.get_plot(plot_class="image", name=plot_label, unique_name=plot_id)
        # legend is the unique name of a curve
        p.set_data(data)
        p.xlabel = label_x
        p.ylabel = label_y
        p.title = plot_title
        
    def test_plot_curve(self):
        a = numpy.linspace(0, 10, 11)
        b = numpy.sin(a)
        self.plot_curve("test_curve", "CURVE", "MYCURVE", a, b, "a", "b")
        
    def test_plot_image(self):
        a = numpy.arange(10000)
        a.shape = 100, 100
        self.plot_image("test_image", "IMAGE", "MYIMAGE", a, "a_image", "b_image")
    
