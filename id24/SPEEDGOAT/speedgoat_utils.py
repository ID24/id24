import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import psd, csd, cohere
from datetime import datetime
from scipy.io import savemat

"""
SPEEDGOAT UTILS
"""
class SpeedgoatUtils:
    def __init__(self, speedgoat):
        self._speedgoat = speedgoat
        self._fastdaq = self._speedgoat.fastdaq.fastdaq
        self._Fs = 10000 # TODO - Should be in Speedgoat object

    def _spectral_analysis(self, counters, duration=10, time_averaging=1, directory=None, file_name=None):
        """ Computes the Power Spectral Density of Speedgoat counters. """
        assert duration > time_averaging, "duration should be larger than time_averaging"

        # Configure FastDAQ
        self._fastdaq.prepare_time(duration, counters)

        # Start the FastDAQ
        self._fastdaq.start(silent=True, wait=True)

        # Get FastDAQ Data
        data = self._fastdaq.get_data()

        # Plot the identified transfer function
        win = np.hanning(int(time_averaging*self._Fs))
        fig = plt.figure(dpi=150)
        ax = fig.add_subplot(1, 1, 1)

        for counter in counters:
            [Pxx, f] = psd(data[counter.name], window=win, NFFT=len(win), Fs=int(self._Fs), noverlap=int(len(win)/2), detrend="mean")
            ax.plot(f, np.sqrt(np.abs(Pxx)), '-', label=f"{counter.name}: {np.std(data[counter.name]):.2e} {counter.unit}rms")

        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.grid(True, which="both", axis="both")
        ax.set_xlabel("Frequency [Hz]")
        ax.set_ylabel("Amplitude Spectral Density [unit/sqrt(Hz)")
        ax.set_xlim(1, 1e3)
        ax.legend()
    
        if directory is not None and file_name is not None:
            now = datetime.now().strftime("%d-%m-%Y_%H-%M")
            savemat("%s/%s_%s.mat" % (directory, now, file_name), data)

            fig.savefig("%s/%s_%s.pdf" % (directory, now, file_name))
            fig.savefig("%s/%s_%s.png" % (directory, now, file_name))
            plt.close(fig)


    def _identify_plant(self, generator, counter_in, counters_out, directory=None, file_name=None):
        """ Computes the transfer function from between counter_in and counters_out using given generator.
        Save Data and figure in the specified directory with given filename.
        The duration of the identification is equal to the duration of the generator. """

        # Configure FastDAQ
        self._fastdaq.prepare_time(generator.duration, [counter_in] + counters_out)
    
        # Start the FastDAQ
        self._fastdaq.start(silent=True, wait=False)

        # Start the test signal
        generator.start()

        # Get FastDAQ Data
        self._fastdaq.wait_finished()
        data = self._fastdaq.get_data()

        # Plot the identified transfer function
        win = np.hanning(self._Fs)
        fig, axs = plt.subplots(2, 1, dpi=150, sharex=True)

        for counter_out in counters_out:
            self._tfestimate(data[counter_in.name], data[counter_out.name], win=win, Fs=int(self._Fs), plot=True, axs=axs, legend=f"{counter_in.name} to {counter_out.name}")

        axs[0].set_xlim(1, 1e3)
        axs[0].legend()
        axs[1].set_ylim(-180, 180)

        if directory is not None and file_name is not None:
            now = datetime.now().strftime("%d-%m-%Y_%H-%M")
            savemat("%s/%s_%s.mat" % (directory, now, file_name), data)
            
            fig.savefig("%s/%s_%s.pdf" % (directory, now, file_name))
            fig.savefig("%s/%s_%s.png" % (directory, now, file_name))
            plt.close(fig)

    def _tfestimate(self, x, y, win, Fs, plot=False, axs=None, legend=""):
        """ Computes the transfer function from x to y.
        win is a windowing function.
        Fs is the sampling frequency in [Hz] """
        nfft = len(win)

        [Pyx, f] = csd(x, y, window=win, NFFT=nfft, Fs=int(Fs), noverlap=int(nfft/2), detrend="mean")
        Pxx      = psd(x,    window=win, NFFT=nfft, Fs=int(Fs), noverlap=int(nfft/2), detrend="mean")[0]

        G = Pyx/Pxx

        if plot:
            if axs is None:
                axs = plt.subplots(2, 1, dpi=150, sharex=True)[1]
            axs[0].plot(f, np.abs(G), '-', label=legend)
            axs[0].set_yscale('log')
            axs[0].grid(True, which="both", axis="both")
            axs[0].set_ylabel("Amplitude")

            axs[1].plot(f, 180/np.pi*np.angle(G), '-')
            axs[1].set_xscale('log')
            axs[1].set_ylim(-180, 180)
            axs[1].grid(True, which="both", axis="both")
            axs[1].set_yticks(np.arange(-180, 180.1, 15))
            axs[1].set_xlabel("Frequency [Hz]")
            axs[1].set_ylabel("Phase [deg]")

        return G, f

    def _pwelch(x, win, Fs, ax=None, plot=False, label=''):
        nfft = len(win)
        [Pxx, f] = psd(x, window=win, NFFT=nfft, Fs=int(Fs), noverlap=int(nfft/2), detrend="mean")

        if plot and ax is not None:
            ax.plot(f, np.sqrt(np.abs(Pxx)), '-', label=label)

        return Pxx, f
