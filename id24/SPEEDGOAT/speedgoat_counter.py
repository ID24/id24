import numpy as np
import weakref
import tabulate

"""
SPEEDGOAT COUNTERS
"""
class SpeedgoatHdwCounterController:
    def __init__(self, speedgoat):
        self._speedgoat = speedgoat
        self._counters = None
        self._load()

    def __info__(self):
        if self._counters is None:
            return "    No Counter in the model"
        lines = [["    ", "Name"]]
        for counter_name, counter in self._counters.items():
            lines.append(["    ", counter.name])
        mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        return mystr

    def _load(self, force=False):
        if self._counters is None or force:
            counter_tree = self._speedgoat.parameter.tree.subtree("counters")
            self._counters = {}
            for node in counter_tree.children("counters"):
                sp_counter =  SpeedgoatHdwCounter(self._speedgoat, node.tag)
                setattr(self, node.tag, sp_counter)
                self._counters[node.tag] = sp_counter
        return self._counters

    def setFilteredTime(self, avg_time):
        fir_length = len(self._speedgoat.parameter.fir_coef_lpf)
        avg_cnt = int(avg_time*self._speedgoat._Fs)
        if avg_cnt > fir_length:
            raise ValueError(f"avg_time must be less than {fir_length/self._speedgoat._Fs}")
        else:
            fir_coef = np.zeros(fir_length)
            if avg_cnt > 0:
                fir_coef[0:avg_cnt] = 1.0/avg_cnt
            self._speedgoat.parameter.fir_coef_lpf = fir_coef

class SpeedgoatHdwCounter:

    def __init__(self, speedgoat, counter_name):
        self._speedgoat = speedgoat
        self.name = counter_name

        param = f"counters/{counter_name}/counter_description/String"
        self.description = self._speedgoat.parameter.get(param)

        param = f"counters/{counter_name}/counter_unit/String"
        self.unit = self._speedgoat.parameter.get(param)

        param = f"counters/{counter_name}/counter_index/Value"
        self.index = int(self._speedgoat.parameter.get(param))

    def __info__(self):
        lines = []
        lines.append(["Name", self.name])
        lines.append(["Description", self.description])
        lines.append(["Unit", self.unit])
        lines.append(["Index", self.index])
        lines.append(["", ""])
        lines.append(["Counter Value", self.value])
        mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        return mystr

    def tree(self):
        param = f"counters/{self.name}"
        self._speedgoat.parameter._cache["param_tree"].subtree(param).show()

    @property
    def value(self):
        signal = f"counters/{self.name}/counter_value"
        return float(self._speedgoat.signal.get(signal))
