# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""ESRF - Simulink controller

Example YAML_ configuration:

.. code-block:: yaml

    plugin: bliss      # (1)
    class: simulink    # (2)
    name: goat1        # (3)
    url: pcmel1        # (4)


1. use the *bliss* plugin (mandatory, inherited)
2. simulink class (mandatory, inherited)
3. name (mandatory)
4. URL: the 0RPC address (ex: tcp://pcmel1:8200)
  * if simple host name or IP is given, it assumes 'tcp://' protocol and
    port 8001 (in the above example the string 'pcmel1' is interpreted
    as 'tcp://pcmel1:8200')

Usage::
started

    >>> from bliss.config.static import get_config()
    >>> config = get_config()

    >>> goat = config.get('goat1')
"""

# import logging
import functools
import collections
import numpy as np
import gevent
import treelib
import json
import enum
import weakref

from bliss.comm import rpc

from id24.SPEEDGOAT.speedgoat_parameter import SpeedgoatHdwParameterController
from id24.SPEEDGOAT.speedgoat_signal import SpeedgoatHdwSignalController
from id24.SPEEDGOAT.speedgoat_scope import SpeedgoatHdwScopeController, SpeedgoatHdwFastdaqController, SpeedgoatHdwRingBufferController, SpeedgoatHdwDisplScopeController
from id24.SPEEDGOAT.speedgoat_motor import SpeedgoatHdwMotorController
from id24.SPEEDGOAT.speedgoat_counter import SpeedgoatHdwCounterController
from id24.SPEEDGOAT.speedgoat_regul import SpeedgoatHdwRegulController
from id24.SPEEDGOAT.speedgoat_generator import SpeedgoatHdwGeneratorController
from id24.SPEEDGOAT.speedgoat_filter import SpeedgoatHdwFilterController
from id24.SPEEDGOAT.speedgoat_trigger import SpeedgoatHdwTriggerController
from id24.SPEEDGOAT.speedgoat_lut import SpeedgoatHdwLutController
from id24.SPEEDGOAT.speedgoat_utils import SpeedgoatUtils

class SpeedgoatHdwController:

    def __init__(self, name, config):
        # Set the Speedgoat Name
        self.name = name

        # self._log = logging.getLogger(f"{__name__}.{type(self).__name__}.{name}")

        # Get the RPC client object
        url = self._to_zerorpc_url(config["url"], default_port=8200)
        self._conn = rpc.Client(url)

        self._Fs = 1e4

        self.parameter  = SpeedgoatHdwParameterController(self)
        self.signal     = SpeedgoatHdwSignalController(self)
        self.scope      = SpeedgoatHdwScopeController(self)
        self.tgscope    = SpeedgoatHdwDisplScopeController(self)
        self.fastdaq    = SpeedgoatHdwFastdaqController(self)
        self.ringbuffer = SpeedgoatHdwRingBufferController(self)
        self.motor      = SpeedgoatHdwMotorController(self)
        self.counter    = SpeedgoatHdwCounterController(self)
        self.regul      = SpeedgoatHdwRegulController(self)
        self.generator  = SpeedgoatHdwGeneratorController(self)
        self.filter     = SpeedgoatHdwFilterController(self)
        self.trigger    = SpeedgoatHdwTriggerController(self)
        self.lut        = SpeedgoatHdwLutController(self)
        self.utils      = SpeedgoatUtils(self)

    @property
    def app_name(self):
        return self._conn.get_app_name()

    @property
    def is_app_running(self):
        return self._conn.is_app_running()

    @property
    def is_overloaded(self):
        return self._conn.is_overloaded()

    @property
    def sample_time(self):
        return self._conn.get_sample_time()

    def reload_model_objects(self):
        self.parameter._load(force=True)
        self.signal._load(force=True)
        self.scope._load(force=True)
        self.motor._load(force=True)
        self.counter._load(force=True)
        self.fastdaq._load(force=True)
        self.regul._load(force=True)
        self.generator._load(force=True)

    def __getattr__(self, name):
        server_call = getattr(self._conn, name)

        def func(*args):
            return server_call(*args)

        func.__name__ = name
        setattr(self, name, func)
        return func

    def _to_zerorpc_url(self, url, default_port=None):
        """Get the correctly formated string used to connect to host computer

        :url: URL used to connect to the Host Machine (e.g. PCSPEEDGOAT:8200)
        :default_port: If the port is not present in the url, a default value can be used
        :returns: Returns a correctly formated string to connect to the host machine, for instance 'tcp://PCSPEEDGOAT:8200'
        """
        # Get host and port
        pars = url.rsplit(":", 1) if isinstance(url, str) else url
        host = pars[0]
        port = int(pars[1]) if len(pars) > 1 else default_port

        # Add TCP if not already present in host
        if "://" not in host:
            host = "tcp://" + host

        # Returns correctly formated string
        return "{}:{}".format(host, port)

    """
    Speedgoat Tree Tools
    """
    def _get_all_objects_from_key(self, key):
        objs = []
        for param in self.parameter._params:
            if param["block"].rfind(key) != -1:
                objs.append(param["block"][0:param["block"].rindex(key)-1])
        return objs

    def _create_block_dict(self, infos):
        """
        A dictionary of blocks. Key is block name and value is a parameter/signal
        dictionary.
        """
        blocks = collections.defaultdict(dict)
        for info in infos:
            blocks[info["block"]][info["name"]] = info
        return blocks

    def _create_tree(self, infos, param_leaf=True):
        """infos: obtained from speedgoat.get_param_infos()
                  or speedgoat.get_signal_infos()
        """
        tree = treelib.Tree()
        root = tree.create_node("", "")
        for block, params in self._create_block_dict(infos).items():
            parent, block_path = root, []
            for item in block.split("/"):
                if item:
                    block_path.append(item)
                block_name = "/".join(block_path)
                node = tree.get_node(block_name)
                if node is None:
                    node = tree.create_node(item, block_name, parent)
                parent = node
            for param_name, param_info in params.items():
                puid = "{}/{}".format(param_info["block"], param_info["name"])
                param_node = tree.create_node(param_name, puid, parent, param_info)
                if not param_leaf:
                    for pname, pvalue in param_info.items():
                        piuid = puid + "." + pname
                        pilabel = "{} = {}".format(pname, pvalue)
                        tree.create_node(pilabel, piuid, param_node, pvalue)
        return tree

class SpeedgoatHdwParameterInit:
    def __init__(self, name, config):
        self._config = config
        self._name = name
        self._goat_ctl = self._config.get("goat_ctl")
        
        for parameter in self._config.get("parameters"):
            for par, value in par.items():
                self._goat_ctl.set(par, value)
