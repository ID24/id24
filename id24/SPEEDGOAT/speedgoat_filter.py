import tabulate
import enum

"""
SPEEDGOAT Signal filters
"""
class SpeedgoatHdwFilterController:
    def __init__(self, speedgoat):
        self._speedgoat = speedgoat
        self._filters = None
        self._load()

    def __info__(self):
        if self._filters is None:
            return "    No Filter in the model"

        lines = [["    ", "Name", "Unique Name"]]
        for filter_name, _filter in self._filters.items():
            lines.append(["    ", _filter.name, _filter._unique_name])

        mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        return mystr

    def _load(self, force=False):
        if self._filters is None or force:
            filters = self._speedgoat._get_all_objects_from_key("bliss_filter")
            if len(filters) > 0:
                self._filters = {}
                for _filter in filters:
                    sp_filter = SpeedgoatHdwFilter(self._speedgoat, _filter)
                    sp_filter_name = sp_filter.name
                    setattr(self, sp_filter.name, sp_filter)
                    self._filters[sp_filter.name] = sp_filter

        return self._filters

class FilterType(enum.IntEnum):
    first_order_lpf = 1
    second_order_lpf = 2
    notch = 3
    remove_dc = 4
    moving_average = 5
    general_fir = 6
    general_iir = 7

class SpeedgoatHdwFilter:

    def __init__(self, speedgoat, unique_name):
        self._speedgoat = speedgoat
        self._unique_name = unique_name
        self.name = self._speedgoat.parameter.get(f"{unique_name}/bliss_filter/String")

        self._type_param = {
            "first_order_lpf": ["cut_off"],
            "second_order_lpf": ["cut_off"],
            "notch": ["freq", "gmin", "damp"],
            "remove_dc": [],
            "moving_average": ["avg_time"],
            "general_fir": ["num_coef"],
            "general_iir": ["num_coef", "den_coef"],
        }

    def __info__(self):
        lines = []
        lines.append(["Name", self.name])
        lines.append(["Unique Name", self._unique_name])
        lines.append(["Enabled", self.enabled])
        lines.append(["", ""])
        type_str = self._get_type_str()
        lines.append(["Type", type_str])
        lines.append(["", ""])
        for param in self._type_param[type_str]:
           lines.append([param, getattr(self, param)])
        mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        return mystr

    def tree(self):
        self._speedgoat.parameter._load()["param_tree"].subtree(self._unique_name).show()

    def _get_type_str(self):
        return self.type().name

    def type(self):
        return FilterType(int(self._speedgoat.parameter.get(f"{self._unique_name}/filter_type")))

    def enable(self):
        self._speedgoat.parameter.set(f"{self._unique_name}/enable", 1)

    def disable(self):
        self._speedgoat.parameter.set(f"{self._unique_name}/enable", 0)

    def trigger(self):
        trigger_value = int(self._speedgoat.parameter.get(f"{self._unique_name}/trigger"))
        self._speedgoat.parameter.set(f"{self._unique_name}/trigger", trigger_value+1)

    @property
    def enabled(self):
        return bool(self._speedgoat.parameter.get(f"{self._unique_name}/enable"))

    @property
    def cut_off(self):
        return self._speedgoat.parameter.get(f"{self._unique_name}/cut_off")

    @cut_off.setter
    def cut_off(self, value):
        self._speedgoat.parameter.set(f"{self._unique_name}/cut_off", value)

    @property
    def gmin(self):
        return self._speedgoat.parameter.get(f"{self._unique_name}/gmin")

    @gmin.setter
    def gmin(self, value):
        self._speedgoat.parameter.set(f"{self._unique_name}/gmin", value)

    @property
    def damp(self):
        return self._speedgoat.parameter.get(f"{self._unique_name}/damp")

    @damp.setter
    def damp(self, value):
        self._speedgoat.parameter.set(f"{self._unique_name}/damp", value)

    @property
    def avg_time(self):
        return self._speedgoat.parameter.get(f"{self._unique_name}/avg_time")

    @avg_time.setter
    def avg_time(self, value):
        self._speedgoat.parameter.set(f"{self._unique_name}/avg_time", value)

    @property
    def num_coef(self):
        return self._speedgoat.parameter.get(f"{self._unique_name}/num_coef")

    @num_coef.setter
    def num_coef(self, value):
        self._speedgoat.parameter.set(f"{self._unique_name}/num_coef", value)

    @property
    def den_coef(self):
        return self._speedgoat.parameter.get(f"{self._unique_name}/den_coef")

    @den_coef.setter
    def den_coef(self, value):
        self._speedgoat.parameter.set(f"{self._unique_name}/den_coef", value)

    @property
    def input(self):
        return self._speedgoat.signal.get(f"{self._unique_name}/input")

    @property
    def output(self):
        return self._speedgoat.signal.get(f"{self._unique_name}/output")


class SpeedgoatfilterParam:
    def __init__(self, param_ctl, unique_name, param_name):
        self._param_ctl = param_ctl
        self._param_name = param_name
        self._unique_name = unique_name

    def __get__(self, obj, objtype):
        return self._param_ctl.get(f"{self._unique_name}/{self._param_name}")

    def __set__(self, obj, value):
        self._param_ctl.set(f"{self._unique_name}/{self._param_name}", value)
