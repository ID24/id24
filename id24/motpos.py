
import os
import glob
import shutil
import tabulate
import gevent
import numpy
from ruamel.yaml import YAML
from ruamel.yaml.compat import StringIO

from bliss import current_session, setup_globals
from bliss.common.utils import ColorTags, BOLD, BLUE, GREEN, ORANGE, RED
from bliss.shell.standard import umv

"""
YAML Configuration:

- plugin: bliss
  package: id24.motpos
  class: MotPosContainer
  name: motpos

"""
class MotPosContainer:

    def __init__(self, name, config):
        
        self._name = name
        self._config = config

        self._motpos_list = {}
        self._create_all_objects_from_file()
        
        self._approximation = 2.0e-3

    """
    Show MotPos object loaded in memory
    """
    def __info__(self):
        mystr = BOLD("\n    Created MotPos object:\n")
        for name in self._motpos_list:
            mystr += f"        {BLUE(name)}\n" 
        print(mystr)
        return ""        
        
    """
    Create a new MotPos object.
        - Check with the name that it does not exist
        - Create it with *motors if given
    """
    def new(self, new_name, *motors):
        if new_name in self._motpos_list:
            raise RuntimeError(f"MotPos \"{new_name}\" already exists")
            
        if new_name in current_session.env_dict:
            raise RuntimeError(f"Name \"{new_name}\" already exists")
            
        self._motpos_list[new_name] = MotPos(self, new_name, *motors)
        current_session.env_dict[new_name] = self._motpos_list[new_name]
        
    def remove(self, name, remove_file=True):
        if name in self._motpos_list:
            self._motpos_list[name].delete(remove_file=remove_file)
        
    """
    Remove all MotPos object from memory
    """
    def clean(self, remove_file=True):
        motpos_list = []
        for motpos in self._motpos_list:
            motpos_list.append(self._motpos_list[motpos])
        for motpos in motpos_list:
            motpos.delete(remove_file=remove_file)
            
        
    """
    Reload all MotPos object from file
    """
    def reload(self):
        self._create_all_objects_from_file()
                   
    """
    Get current directory to save MotPos objects
    """
    def _get_dir(self):
        SCAN_SAVING = current_session.scan_saving
        motpos_dir = os.path.join(
            SCAN_SAVING.base_path,
            SCAN_SAVING.proposal_name,
            SCAN_SAVING.beamline,
            "motpos"
        )
        return motpos_dir
    
    """
    Read directory motpos_dir and build dictionnary with key=name, value=filename
    """
    def _read_dir(self, motpos_dir):
        set_file_list = {}
        expr = f"{motpos_dir}/*.yml"
        file_list = glob.glob(expr)
        for path_radix_name_ext in file_list:
            radix_name_ext = path_radix_name_ext.split("/")[-1]
            name =radix_name_ext.split(".")[0]
            set_file_list[name] = path_radix_name_ext
            
        return set_file_list
        
    def _create_object_from_file(self, name):
        self._motpos_list[name] = MotPos(self, name, load=True)
        current_session.env_dict[name] = self._motpos_list[name]
        
    def _create_all_objects_from_file(self):
        motpos_dir = self._get_dir()
        motpos_list = self._read_dir(motpos_dir)
        for name in motpos_list:
            self._create_object_from_file(name)

    def copy_from_visitor(self, proposal_name):
        SCAN_SAVING = current_session.scan_saving
        from_dir = os.path.join(
            "/data/visitor",
            proposal_name,
            SCAN_SAVING.beamline,
            "motpos"
        )
        to_dir = self._get_dir()
        
        self._copy_from(from_dir, to_dir)

    def copy_from_inhouse(self, proposal_name):
        SCAN_SAVING = current_session.scan_saving
        from_dir = os.path.join(
            f"/data/{SCAN_SAVING.beamline}/inhouse",
            proposal_name,
            SCAN_SAVING.beamline,
            "motpos"
        )
        to_dir = self._get_dir()
        
        self._copy_from(from_dir, to_dir)
    
    def _copy_from(self, from_dir, to_dir):
        for f in glob.glob(f"{from_dir}/*"):
            print(f"Copy {f} in {to_dir}")
            shutil.copy(f, to_dir)
            
        self.reload()

class MotPos:
    
    def __init__(self, container, name, *motors, load=False):
        
        self._container = container
        self._name = name
        self._data_dir = self._container._get_dir()
        self._data_file = f"{self._data_dir}/{self._name}.yml"
        
        if load:
            self._load_from_file()
        else:
            self._motors = []
            self._positions = {}
            self.add_motor(*motors)
                                        
    def __info__(self):
        lines = []
        if self._motors is not None:
            line = ["", ""]
            for motor in self._motors:
                line.append(BOLD(motor["name"]))
            lines.append(line)
        
        for name, position in self._positions.items():
            line = ["", BOLD(position._name)]
            for motor in position._positions:
                pos = motor["position"]
                line.append(GREEN(f"{pos:.3f}"))
            lines.append(line)
        
        mystr = "\n"
        mystr += tabulate.tabulate(lines, tablefmt="plain")
        mystr += "\n"
        #return mystr
        print(mystr)
        return ""
        
    ##############################################################
    #####
    ##### User methods
    #####
        
    def add_position(self, name):
        position = self._get_position_by_name(name)
        if position is not None:
            raise RuntimeError(f"Position \"{name}\" already exists")
        self._positions[name] = MotPosPosition(self, name)
        self._positions[name].update()
        self._set_position_attr(self._positions[name])
        self._save_to_file()      
        
    def rem_position(self, name):
        position = self._get_position_by_name(name)
#        if position is not None:
        if position is None:
            raise RuntimeError(f"Position \"{name}\" Not in {self._name} object")
        self._positions[name].delete()
        
    def add_motor(self, *motors):
        for motor in motors:
            if self._get_motor_index(motor.name) == -1:
                self._motors.append({"name": motor.name})
                for name, position in self._positions.items():
                    position._positions.append({"motor": motor.name, "position": motor.position})
        self._save_to_file()      
        
    def rem_motor(self, motor):
        ind = self._get_motor_index(motor.name)
        if ind != -1:
            self._motors.pop(ind)
            for name, position in self._positions.items():
                position._positions.pop(ind)
        self._save_to_file()
            
    
    ##############################################################
    #####
    ##### Positions/Motors method utils
    #####
    """
    get current position of MotPos motors
    Return: a list of dictionary "motor_name", "current_position"
    """
    def _get_current_positions(self):
        mot_pos_list = []
        for motor in self._motors:
            motname = motor["name"]
            motpos = current_session.env_dict[motname].position
            mot_pos_list.append({"motor": motname, "position": motpos})
        return mot_pos_list

    """
    Get position by name
    """
    def _get_position_by_name(self, name):
        for posname, position in self._positions.items():
            if posname == name:
                return position
        return None
    """
    Check if motor is in the object
    """
    def _get_motor_index(self, motname):
        for i in range(len(self._motors)):
            if self._motors[i]["name"] == motname:
                return i
        return -1
        
    ##############################################################
    #####
    ##### File Management
    #####
    """
    Save motpos in file
    """
    def _save_to_file(self):
        param_dict = {"motors": self._motors, "positions": []}
        for name, position in self._positions.items():
            param_dict["positions"].append(
                {
                    "name": name,
                    "motors": position._positions
                }
            )
        yaml = YAML(pure=True)
        yaml.default_flow_style = False
        stream = StringIO()
        yaml.dump(param_dict, stream=stream)
        with open(self._data_file, "w") as file_out:
            file_out.write(stream.getvalue())
    """
    load parameters from file
    """
    def _load_from_file(self):
        if os.path.exists(self._data_file) is None:
            raise RuntimeError(f"MotPos file \"{self._name}.yml\" does not exists")
        yaml = YAML(pure=True)
        with open(self._data_file) as file_in:
            param_list = yaml.load(file_in)
            self._motors = param_list["motors"]
            self._positions = {}
            for position in param_list["positions"]:
                self._positions[position["name"]] = MotPosPosition(self, position["name"])
                self._positions[position["name"]]._positions = position["motors"]
                self._set_position_attr(self._positions[position["name"]])

    """
    delete motopos object file+memory
    """
    def delete(self, remove_file=True):
        if remove_file:
            os.remove(self._data_file)
        self._container._motpos_list.pop(self._name)
        current_session.env_dict.pop(self._name)
        
    """
    Attributes management
    """
    def _set_position_attr(self, position):
        setattr(self, position._name, position)  
        
    def _del_position_attr(self, posname):
        delattr(self, posname)  

class MotPosPosition:
    def __init__(self, container, name, *motor):
        self._name = name
        self._container = container
        
        self._positions = []
        
    def update(self):
        self._positions = self._container._get_current_positions()  
        self._container._save_to_file()      
        
    def delete(self):
        self._container._del_position_attr(self._name)
        self._container._positions.pop(self._name)
        self._container._save_to_file()
        
    def _motors_end(self):
        for axis in self._moving_axis:
            if axis.is_moving:
                return False
        return True
    
    def _get_formated_motor_positions(self, last=False):
        title_str = [""]
        target_str = [BOLD("Target")]
        current_str = [BOLD("Current")] 
        for i in range(len(self._moving_axis)):
            mot = self._moving_axis[i]
            target = GREEN(f"{self._moving_target[i]:.3f}")
            if last:
                current = GREEN(f"{mot.position:.3f}")
            else:
                current = ORANGE(f"{mot.position:.3f}")
            title_str.append(mot.name)
            target_str.append(target)
            current_str.append(current)
        lines = [title_str, target_str, current_str]
        return tabulate.tabulate(lines, tablefmt="plain")
        
    def _move_once(self):
        magic_char = "\033[F"  # "back to previous line" character
        self._moving_axis = []
        self._moving_target = []
        
        for position in self._positions:
            mot = current_session.env_dict[position["motor"]]
            mot.move(position["position"], wait=False)
            self._moving_axis.append(mot)
            self._moving_target.append(position["position"])
        
        print("\n")
        while not self._motors_end():
            mystr = self._get_formated_motor_positions()
            print(mystr)
            print(f"{magic_char*4}")
            gevent.sleep(0.1)
            
        mystr = self._get_formated_motor_positions(last=True)
        print(mystr)
        print("\n")
        
    def move(self):
        approx = self._container._container._approximation
        
        self._move_once()
                
        redo = False
        for position in self._positions:
            mot = current_session.env_dict[position["motor"]]
            if not numpy.isclose(mot.position, position["position"], atol=approx):
                redo = True

        if redo:
            print(RED(f"    All motors did not reached their position, Retry...\n"))
            self._move_once()
               
            
