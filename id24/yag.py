from bliss.common.soft_axis import SoftAxis as _SoftAxis

def create_yag(name,config):
    laser = config.get('laser')
    return _SoftAxis(name,laser,position='power',move='power', export_to_session=False)
