
from bliss.controllers.tango_shutter import TangoShutterState

class BackgroundShutter:
    def __init__(self, name, config):
        self.__name = name
        self.__config = config
        
        self.shutter = self.config.get("shutter", None)
        
        if self.shutter is None:
            raise RuntimeError("No Shutter defined")

    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    def __info__(self):
        return f"{self.name}: {self.state}"
    
    @property
    def state(self):
        shut_state = self.shutter.state
        if  shut_state == TangoShutterState.CLOSED:
            return "CLOSED"
        elif shut_state == TangoShutterState.OPEN:
            return "OPEN"
        else:
            return "SOMETHINGELSE"
            
    def open(self):
        self.shutter.open()
            
    def close(self):
        if self.state != "CLOSED":
            self.shutter.close()
