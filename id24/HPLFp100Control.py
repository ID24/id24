
import time
import datetime
import gevent
import sys
import tabulate
import numpy
import os

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE

from id24.ID24city import ID24cityChannelAttrList
from id24.HPLFp100Tango import HPLFtangoDeviceAttrList, HPLFtangoCameraAttrList
from id24.ID24utils import ID24profile

class HPLFp100Control:

    def __init__(self, name, config):
        self._ampli_init_state = [
            "Disconnected",
            "Connecting...",
            "Connected - No Image",
            "Connected"
        ]
        
        self._name = name
        self._config = config
        
        #
        # City Channels
        #
        self._city_control = self._config.get("city")
        self._city_channels = {}
        city_channels = self._config.get("city_channels")
        for city_channel in city_channels:
            name = city_channel.get("city_name")
            device = self._city_control._get_channel_object(name)
            self._city_channels[name] = device
        self.city = ID24cityChannelAttrList(self._city_channels)
        
        #
        # Tango Devices
        #
        self._device = {}
        devices = self._config.get("tango_devices")
        for device in devices:
            name = device.get("device_name")
            dev = device.get("device")
            self._device[name] = dev
        self.device = HPLFtangoDeviceAttrList(self._device)

        #
        # Tango CCDs
        #
        self._camera = {}
        cameras = self._config.get("tango_ccds")
        for camera in cameras:
            name = camera.get("device_name")
            device = camera.get("device")
            self._camera[name] = device
        self.camera = HPLFtangoCameraAttrList(self._camera)

        #
        # Streak Cameras
        #
        self._camera = {}
        cameras = self._config.get("tango_ccds")
        for camera in cameras:
            name = camera.get("device_name")
            device = camera.get("device")
            self._camera[name] = device
        self.camera = HPLFtangoCameraAttrList(self._camera)
            
    def __info__(self):
        self.device.att1.__info__()
        self.device.att2.__info__()
        self.device.intrepid.__info__()
        self.device.modbox.__info__()
        self.device.ampli.__info__()
        self.device.synchro.__info__()
        
        return ""
                
    ###############################################################
    #####
    ##### PLOTS
    #####
    def plot_modbox_profile(self):
        data = self.get_modbox_profile()
        setup_globals.display.plot_curve(
            "ModBox",
            "Modebox Profile",
            "HPLF Curve",
            data["time"],
            data["level"],
            "Time (ns)",
            "Level"
        )
        
    ###############################################################
    #####
    ##### DATA
    #####
    def _get_metadata(self):
        
        md = {}
        
        # Modbox
        data = self.get_modbox_profile()
        modbox = ID24profile()
        modbox.set_data("time", data["time"], ["level",], [data["level"],])
        md = {}
        md["_ascii_"] = {
            "modbox_profile": {
                "type": "p100",
                "data": modbox,
            }
        }
        
        # tango devices
        md["p100_tango"] = {}
        for key,val in self._device.items():
            md["p100_tango"][key] = val._get_metadata()
        
        # city channels
        md["p100_city_channels"] = {}
        for key,val in self._city_channels.items():
            md["p100_city_channels"][key] = val._get_metadata()
        
        # cameras
        md["p100_cameras"] = {}
        for key,val in self._camera.items():
            md["p100_cameras"][key] = val._get_metadata()
        
        return md
        
    def get_modbox_profile(self):
        data = {}
        data["level"] = self._device["modbox"]._device.LoadedProfile
        data["time"] = numpy.linspace(1, len(data["level"]), len(data["level"])) * 125.0 / 1000.0
        return data
        
    ###############################################################
    #####
    ##### COMMANDS
    #####
    def reset_errors(self):
        self._device["ampli"]._device.Reset_Errors_command = True
        
    def set_low_energy(self):
        if not self._device["ampli"]._device.Energy_State:
            print("P100 already in Energy Low")
            return
            
        print("\n")
        print("  Switch_To_Low_Energy_Command\n")
        self._device["ampli"]._device.Switch_To_Low_Energy_Command = True
        
        print("  Wait_State_Low\n")
        with gevent.timeout.Timeout(3000):
            while not self._device["ampli"]._device.Wait_State_Low:
                gevent.sleep(0.01)
                
        print("  Set D25 delay to 5000600 us\n")
        self.city.D25.delay = 5000600 # us
        gevent.sleep(0.1)
        
        print("  Sync_Modified_Low_Energy\n")
        self._device["ampli"]._device.Sync_Modified_For_Low_Energy = True
        
        print("  Waiting P100 re-synchro (20s)\n")
        gevent.sleep(20)
        
        energy_state = self._device["ampli"]._device.Energy_State
        if energy_state:
            mystr = BOLD(RED("MOVE TO LOW ENERGY DID NOT SUCCEED\n"))
        else:
            mystr = BOLD(GREEN("Move to LOW Energy OK\n"))
        print(f"{mystr}\n")
    
    def set_high_energy(self):
        if self._device["ampli"]._device.Energy_State:
            print("P100 already in Energy High")
            return
            
        print("\n")
        print("  Switch_To_High_Energy_Command\n")
        self._device["ampli"]._device.Switch_To_High_Energy_Command = True
        
        print("  Wait_State_High\n")
        with gevent.timeout.Timeout(3000):
            while not self._device["ampli"]._device.Wait_State_High:
                gevent.sleep(0.01)
                
        print("  Set D25 delay to +100 us / D5\n")
        self.city.D25.delay = self.city.D5.delay + 100 # us
        gevent.sleep(0.1)
        
        print("  Sync_Modified_High_Energy\n")
        self._device["ampli"]._device.Sync_Modified_For_High_Energy = True
        
        print("  Waiting P100 re-synchro (20s)\n")
        time_start = time.time()
        while time.time() - time_start < 20:
            print(self._device["ampli"]._device.Errors)
            gevent.sleep(0.5)
        
        energy_state = self._device["ampli"]._device.Energy_State
        if not energy_state:
            mystr = BOLD(RED("MOVE TO HIGH ENERGY DID NOT SUCCEED\n"))
        else:
            mystr = BOLD(GREEN("Move to HIGH Energy OK\n"))
        print(f"{mystr}\n")
