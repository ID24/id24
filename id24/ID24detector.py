
import numpy
import click
import time

from bliss.common import plot
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
from bliss.controllers.lima.limatools import _limatake_parse_args
import bliss.common.plot as plot_module

from id24.ID24menu import menu_list, menu_number, menu_string, menu_choice


#
# METHOD TO BE IMPLEMENTED WHEN SUR CLASSED
#
#   - _get_acq_paramaters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq)
#   - _get_acq_data(self, nbframe, nbacq)
#   - _get_detector_width(self)
#   - _get_detector_height(self)

class ID24detectorBase:
    
    def __init__(self, name, config):  
        self._name = name
        self._config = config
              
        self.lima = config.get("_par_lima", None)
        if self.lima is None:
            raise RuntimeError("No Lima device specified")
        
        self._rotate = config.get("_par_rotate", 0)
        
        # Parameters
        self._default_param = {
            # Title
            "title": "Spectrum",
            
            # detector
            "first_pixel": 0,
            "last_pixel": self._width-1,
            "nb_pixel": self._width,
            "time_unit": "s",
            "latency_time": 0,
        
            # I
            "I_group": 1,
            "I_frame": 1,
            "I_acq": 10,
            "I_time": 0.001,
            "I_bck_acq": 10,
            "I_bck_time": 0.001,
        
            # I0
            "I0_mode": "after",
            "I0_acq": 10,
            "I0_time": 0.001,
            "I0_bck_acq": 10,
            "I0_bck_time": 0.001,
        
            # energy = A+B*pixel+C*pixel^2
            "energy_use": True,
            "energy_A": 0.0,
            "energy_B": 1.0,
            "energy_C": 0.0,
        }
                
    def __info__(self):
        mystr =  f"Detector        : {self._name}\n"
        mystr += f"    Live        : {self.lima.proxy.video_live}\n"
        mystr += f"    First pixel : {self._param.first_pixel}\n"
        mystr += f"    Last pixel  : {self._param.last_pixel}\n"
        
        mystr += "\nI0\n"
        mystr += f"    Mode        : {self._param.I0_mode}\n"
        mystr += f"    Bckgnd acq. : {self._param.I0_bck_acq}\n"
        mystr += f"    Bckgnd time : {self._param.I0_bck_time} ({self._param.time_unit})\n"
        mystr += f"    Acq.        : {self._param.I0_acq}\n"
        mystr += f"    Time        : {self._param.I0_time} ({self._param.time_unit})\n"
                
        mystr += "\nEnergy Calib. (E = A + B*pix + C*pix^2)\n"
        mystr += f"    Use         : {self._param.energy_use}\n"
        mystr += f"    A           : {self._param.energy_A}\n"
        mystr += f"    B           : {self._param.energy_B}\n"
        mystr += f"    C           : {self._param.energy_C}\n"
        
        return mystr

    ##################################################################
    #
    # Acquisition Methods
    #
    def _get_energy_data(self):
        # energy = A+B*pixel+C*pixel^2
        A = self._param.energy_A
        B = self._param.energy_B
        C = self._param.energy_C
        nbpixel = self._param.nb_pixel
        data_energy = numpy.zeros((1, nbpixel))
        for npixel in range(nbpixel):
            pixel = self._param.first_pixel + npixel
            data_energy[0][npixel] = A + B * pixel + C * pixel * pixel
        return data_energy
    
    def _get_detector_parameters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq):
        par_dict = self._get_acq_parameters(synchro_mode, acq_mode, inttime, nbframe, nbacq)
        det_dict = {
            "ctrl_params" : {"saving_format": "EDF"},
            "acq_params": par_dict
        }
        #(det_dict, other) = _limatake_parse_args(par_dict)
        return det_dict
    
    def _get_acq_paramaters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq):
        raise NotImplementedError
    
    def _param2dict(self):
        param_dict = {}
        param_dict["I"] = {
            "nbframe": self._param.I_frame,
            "nbacq": self._param.I_acq,
            "expotime": self._param.I_time,
        }
        
        param_dict["I_bck"] = {
            "nbframe": 1,
            "nbacq": self._param.I_bck_acq,
            "expotime": self._param.I_time,
        }
        
        param_dict["I0"] = {
            "nbframe": 1,
            "nbacq": self._param.I0_acq,
            "expotime": self._param.I0_time,
        }
        
        param_dict["I0_bck"] = {
            "nbframe": 1,
            "nbacq": self._param.I0_bck_acq,
            "expotime": self._param.I0_time,
        }
        
        return param_dict
        
    def _get_acq_param_value(self, param, acq_mode):
        param_dict = self._param2dict()
        return param_dict[acq_mode][param]
                
    def _get_acq_data(self, nbframe, nbacq):
        raise NotImplementedError
        
    def _acq_prepare(self):
        pass
        
    def _acq_end(self):
        pass
                
    ##################################################################
    #
    # Acquisition Parameters
    #
    def parameters(self):
        param = ["Detector", "I", "I0", "Energy"]
        while True:
            (rep, ind) = menu_list(f"Parameters [{self._name}]", param)
            if rep == "Detector":
                self._parameters_detector()
            if rep == "I":
                self._parameters_I()
            if rep == "I0":
                self._parameters_I0()
            if rep == "Energy":
                self._parameters_energy()
            if rep == "q":
                return
        
    def _parameters_detector(self):
        param = ["First Pixel", "Last Pixel"]
        title = ["Parameter", "Value"]
        while True:
            value = [self._param.first_pixel, self._param.last_pixel]
            (rep, ind) = menu_list("Detector", param, values=value, title=title)
            if rep == "First Pixel":
                minmax = [0, self._param.last_pixel-1]
                self._param.first_pixel = menu_number(
                    "First Pixel",
                    minmax=minmax,
                    default=self._param.first_pixel,
                    integer=True
                )
            if rep == "Last Pixel":
                minmax = [self._param.first_pixel+1, self._width-1]
                self._param.last_pixel = menu_number(
                    "Last Pixel",
                    minmax=minmax,
                    default=self._param.last_pixel,
                    integer=True
                )
            self._param.nb_pixel = self._param.last_pixel - self._param.first_pixel + 1
            self._configure()
            self._configure_lima_roi()
            if rep == "q":
                return rep
                
    def _parameters_I(self):
        param = ["I background acq"]
        title = ["Parameter", "Value"]
        while True:
            value = [self._param.I_bck_acq]
            (rep, ind) = menu_list("I", param, values=value, title=title)
            if rep == param[0]:
                minmax = [1, 10000]
                self._param.I_bck_acq = menu_number(
                    param[0],
                    default=self._param.I_bck_acq,
                    integer=True
                )
            if rep == "q":
                return rep
                
    def _parameters_I0(self):
        param = [
            "I0 mode",
            "I0 background acq", 
            f"I0 background time ({self._param.time_unit})", 
            "I0 acq",
            f"I0 time({self._param.time_unit})"
        ]
        title = ["Parameter", "Value"]
        while True:
            value = [
                self._param.I0_mode,
                self._param.I0_bck_acq,
                self._param.I0_bck_time,
                self._param.I0_acq,
                self._param.I0_time,
            ]
            (rep, ind) = menu_list("I0", param, values=value, title=title)
            if rep == param[0]:
                values = ["before", "after", "each_spectrum", "last_aquired"]
                self._param.I0_mode = menu_string(
                    param[0],
                    choice=values,
                    default=self._param.I0_mode,
                )
            if rep == param[1]:
                minmax = [1, 10000]
                self._param.I0_bck_acq = menu_number(
                    param[1],
                    #minmax=minmax,
                    default=self._param.I0_bck_acq,
                    integer=True
                )
            if rep == param[2]:
                minmax = [0.00005, 1000000]
                self._param.I0_bck_time = menu_number(
                    param[2],
                    #minmax=minmax,
                    default=self._param.I0_bck_time,
                    integer=False
                )
            if rep == param[3]:
                minmax = [1, 10000]
                self._param.I0_acq = menu_number(
                    param[3],
                    #minmax=minmax,
                    default=self._param.I0_acq,
                    integer=True
                )
            if rep == param[4]:
                minmax = [0.00005, 10]
                self._param.I0_time = menu_number(
                    param[4],
                    #minmax=minmax,
                    default=self._param.I0_time,
                    integer=False
                )
            if rep == "q":
                return rep
                
    def _parameters_energy(self):
        param = ["Energy use", "A", "B", "C"]
        title = ["Parameter", "Value"]
        while True:
            value = [
                self._param.energy_use,
                self._param.energy_A,
                self._param.energy_B,
                self._param.energy_C,
            ]
            (rep, ind) = menu_list("Energy", param, values=value, title=title)
            if rep == param[0]:
                self._param.energy_use = click.confirm(
                    "Use Energy Calibration",
                    default=self._param.energy_use
                )
            if rep == param[1]:
                self._param.energy_A = menu_number(
                    param[1],
                    default=self._param.energy_A,
                    integer=False
                )
            if rep == param[2]:
                self._param.energy_B = menu_number(
                    param[2],
                    default=self._param.energy_B,
                    integer=False
                )
            if rep == param[3]:
                self._param.energy_C = menu_number(
                    param[3],
                    default=self._param.energy_C,
                    integer=False
                )
            if rep == "q":
                return rep
        
    def _get_meta_data(self):
        scan_info = {}
        scan_info["Detector name"] = self._name
        scan_info["Detector first pixel"] = self._param.first_pixel
        scan_info["Detector last pixel"] = self._param.last_pixel
        scan_info["Detector latency time"] = self._param.latency_time
        scan_info["Time units"] = self._param.time_unit
        scan_info["I groups"] = self._param.I_group
        scan_info["I frames"] = self._param.I_frame
        scan_info["I acq."] = self._param.I_acq
        scan_info["I time"] = self._param.I_time
        scan_info["I background acq."] = self._param.I_bck_acq
        scan_info["I background time (s)"] = self._param.I_bck_time
        scan_info["I0 mode"] = self._param.I0_mode
        scan_info["I0 lines"] = self._param.I0_acq
        scan_info["I0 time"] = self._param.I0_time
        scan_info["I0 background lines"] = self._param.I0_bck_acq
        scan_info["I0 background time"] = self._param.I0_bck_time
        scan_info["Energy used"] = self._param.energy_use
        scan_info["Energy A"] = self._param.energy_A
        scan_info["Energy B"] = self._param.energy_B
        scan_info["Energy C"] = self._param.energy_C
        return scan_info

    ##################################################################
    #
    # ROI's
    #
    def _configure_lima_roi(self):
        first_pixel = self._param.first_pixel
        last_pixel = self._param.last_pixel
        roi_width = int((last_pixel - first_pixel) / 4)
        if self._rotate == 0:
            self.lima.roi_counters["I1"] = [0, 0, roi_width, 1]
            self.lima.roi_counters["I2"] = [roi_width, 0, roi_width, 1]
            self.lima.roi_counters["I3"] = [2*roi_width, 0, roi_width, 1]
        else:
            self.lima.roi_counters["I1"] = [0, 0, 1, roi_width]
            self.lima.roi_counters["I2"] = [0, roi_width, 1, roi_width]
            self.lima.roi_counters["I3"] = [0, 2*roi_width, 1, roi_width]
        width = roi_width
        if 3*roi_width+roi_width >= self._param.nb_pixel:
            width=self._param.nb_pixel - (3*roi_width)
        if self._rotate == 0:
            self.lima.roi_counters["I4"] = [3*roi_width, 0, width, 1]
            self.lima.roi_counters["Isum"] = [0, 0, self._param.nb_pixel, 1]
        else:
            self.lima.roi_counters["I4"] = [0, 3*roi_width, 1, width]
            self.lima.roi_counters["Isum"] = [0, 0, 1, self._param.nb_pixel]
        
    def _configure(self):
        pass

    ##################################################################
    #
    # Lima helper
    #
    @property
    def _width(self):
        return self._get_detector_width()

    @property
    def _height(self):
        return self._get_detector_height()
        
    def _get_detector_width(self):
        raise NotImplementedError
        
    def _get_detector_height(self):
        raise NotImplementedError
        
    def start_live(self, inttime=0.001):
        self.lima.start_live(inttime)      
            
    def stop_live(self):
        self.lima.stop_live()
