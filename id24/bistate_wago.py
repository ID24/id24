# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import tabulate

from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED

from id24.bistate import BistateBase

"""
WagoBistate Class allow to configure and run a 2 states devices (shutter, valve, actuator...)
which are controlled by a Wago.

The device to control need a name and a reference to the wago controlling it.
Thoses informations are given in the YAML file:
    plugin: bliss
    module: wago_bistate
    class: WagoBistate
    name: mvalve
    wago: $wcd23a

In the YAML file, as define by the BistateBase class, the polling time and timeout
May be defined.
    
    polling: True           (False if not defined)
    polling_time: 0.2       (0.1s if not defined)
    polling_timeout: 3.0    (2.0s if not defined)

Depending of the type of device you want to control, the command and state 
name are defined in the yml config file. If nothing is set, the WagoBistate
object is considered to be a Valve/Shutter
    Examples:
        Generic Shutter/Valve:
            <Empty YAML file for thoses parameters>
        Beamline Specific Shutter/Valve:
            state1_name: A
            state1_cmd_name: set_A
            state2_name: B
            state2_cmd_name: set_B
        Generic Actuator:
            state1_name: IN
            state1_cmd_name: set_in
            state2_name: OUT
            state2_cmd_name: set_out
        Beamline specific Actuator:
            state1_name: "DetCoverIn"
            state1_cmd_name: set_in
            state2_name: "DetCoverOut"
            state2_cmd_name: set_out
            
Wago keys and values for state and command are also defined in the YAML file.
    state1_cmd_key: <wago channel to set state1>
    state1_cmd_val: <value>
    state1_state_key: <wago channel to read state1> 
    state1_state_val: <value>
    state2_cmd_key: <wago channel to set state2>
    state2_cmd_val: <value> 
    state2_state_key: <wago channel to read state2>
    state2_state_val: <value> 

For Wago keys and values, defaults are provided in the way that the minimum
parameters should be given for the simplest wago configuration.
    Example:
        A valve is open or close by only 1 wago output without wago channel to
        read the states, the YAML will look like:
            state1_cmd_key: shut_cmd
            state1_cmd_val: 1
        In this case, the Close (state2) command key will be the same as the
        Open (state1) one with the opposite value (0 to close in our case).
        The values for the open and closed states will be the reading of the
        wago channel used for the command which keep the last value set: in
        our example: OPEN=1/CLOSED=0
    
"""

class WagoBistate(BistateBase):
    def __init__(self, name, config):

        BistateBase.__init__(self, name, config)
        
        # Wago controller
        self._wago = config.get("_wago")

        # state1 Command
        self._state1_cmd_key = config.get("state1_cmd_key")
        self._state1_cmd_val = 1.0
        if "state1_cmd_val" in config.keys():
            self._state1_cmd_val = float(config.get("state1_cmd_val"))

        # state1 State
        self._state1_state_key = self._state1_cmd_key
        if "state1_state_key" in config.keys():
            self._state1_state_key = config.get("state1_state_key")
        self._state1_state_val = self._state1_cmd_val
        if "state1_state_val" in config.keys():
            self._state1_state_val = config.get("state1_state_val")

        # state2 command
        self._state2_cmd_key = self._state1_cmd_key
        if "state2_cmd_key" in config.keys():
            self._state2_cmd_key = config.get("state2_cmd_key")
        self._state2_cmd_val = 0.0 if self._state1_cmd_val == 1.0 else 1.0
        if "state2_cmd_val" in config.keys():
            self._state2_cmd_val = config.get("state2_cmd_val")

        # state2 State
        self._state2_state_key = self._state2_cmd_key
        if "state2_state_key" in config.keys():
            self._state2_state_key = config.get("state2_state_key")
        self._state2_state_val = self._state2_cmd_val
        if "state2_state_val" in config.keys():
            self._state2_state_val = config.get("state2_state_val")
            
        self._configure(
            self._wago_set_state1,
            self._wago_is_state1,
            self._wago_set_state2,
            self._wago_is_state2
        )

    def _get_condensed_info(self):
        state = self.state
        return (self._name, self._get_color_state(state)(state))
        
    def report(self):
        print("\n")
        state_str = self._get_color_state(self.state)(self.state)
        lines = [[f"  {BOLD(self._name)} - {state_str}", BOLD("state1"), BOLD("state2")]]
        lines.append([BOLD("name"), self._state_names["state1"], self._state_names["state2"]])
        lines.append([BOLD("cmd name"), self._state_cmd_names["state1"], self._state_cmd_names["state2"]])
        lines.append([BOLD("cmd key"), self._state1_cmd_key, self._state2_cmd_key])
        lines.append([BOLD("cmd val"), self._state1_cmd_val, self._state2_cmd_val])
        lines.append([BOLD("state key"), self._state1_state_key, self._state2_state_key])
        lines.append([BOLD("state val"), self._state1_state_val, self._state2_state_val])
        lines.append([BOLD("state current val"), self._wago.get(self._state1_state_key), self._wago.get(self._state2_state_key)])
        print(tabulate.tabulate(lines, tablefmt="plain", stralign="right"))
        print("\n")
        
    def _wago_set_state1(self):
        self._wago.set(self._state1_cmd_key, self._state1_cmd_val)

    def _wago_set_state2(self):
        self._wago.set(self._state2_cmd_key, self._state2_cmd_val)

    def _wago_is_state1(self):
        is_state1 = self._wago.get(self._state1_state_key)
        if is_state1 == self._state1_state_val:
            return True
        else:
            return False

    def _wago_is_state2(self):
        is_state2 = self._wago.get(self._state2_state_key)
        if is_state2 == self._state2_state_val:
            return True
        else:
            return False
