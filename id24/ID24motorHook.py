
import click
import gevent
import time

from bliss.common.hook import MotionHook
from bliss.shell.standard import umv
from bliss.common.utils import ColorTags, BOLD
from bliss import setup_globals
from bliss.controllers.motor import CalcController
from bliss.config.settings import QueueSetting, ParametersWardrobe
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
from bliss import setup_globals, current_session

from id24.ID24utils import motor_esync

class DedHook(MotionHook):

    def __init__(self, name, config):

        self.config = config
        self.name = name

        self.wago_name = config.get("wago_name", None)
        if self.wago_name is None:
            raise RuntimeError("DedHook: wago_name not defined")

        self.wago_channel = config.get("wago_channel", None)
        if self.wago_name is None:
            raise RuntimeError("DedHook: wago_channel not defined")

        super().__init__()
        
        self.in_scan = False

    def pre_move(self, motion_list):
        if not self.in_scan:
            self.release_brakes()

    def post_move(self, motion_list):
        if not self.in_scan:
            self.engage_brakes()

    def pre_scan(self, motion_list):
        self.release_brakes()
        self.in_scan = True

    def post_scan(self, motion_list):
        self.in_scan = False
        self.engage_brakes()

    def release_brakes(self):
        print(f"RELEASE ded Brakes .....", end="")
        self.wago_name.set(self.wago_channel, 1)
        print("DONE")
    
    def engage_brakes(self):
        print(f"\n\n\nENGAGE ded Brakes .....", end="")
        self.wago_name.set(self.wago_channel, 0)
        print("DONE")

class AirHook(MotionHook):

    def __init__(self, name, config):

        self.config = config
        self.name = name

        self.wago_name = config.get("wago_name", None)
        if self.wago_name is None:
            raise RuntimeError("DedHook: wago_name not defined")

        self.wago_channel = config.get("wago_channel", None)
        if self.wago_name is None:
            raise RuntimeError("DedHook: wago_channel not defined")
            
        self.waiting_time = config.get("waiting_time", 1)

        super().__init__()
        
        self.in_scan = False

    def pre_move(self, motion_list):
        if not self.in_scan:
            self.release_brakes()
        for motion in motion_list:
            axis = motion.axis
            cont = axis.controller
            if isinstance(axis.controller, CalcController):
                for raxis in axis.controller.reals:
                    self._wait_state(raxis)
            else:
                self._wait_state(axis)
        
    def post_move(self, motion_list):
        if not self.in_scan:
            self.engage_brakes()

    def pre_scan(self, motion_list):
        print(motion_list)
        self.release_brakes()
        self._motion_list = motion_list
        self.in_scan = True

    def post_scan(self, motion_list):
        self.in_scan = False
        self.engage_brakes()

    def release_brakes(self):
        print(f"Set air ON .....", end="")
        self.wago_name.set(self.wago_channel, 1) 
        gevent.sleep(2)
        print("DONE")

    def _wait_state(self, axis):
        state = axis.hw_state
        state_list = state.current_states_names
        start_time = time.time()
        while ("LIMPOS" in state_list) and ("LIMNEG" in state_list):
            if time.time()-start_time > 10:
                raise RuntimeError("Motor \"axis.name\": Timeout setting air ON")
            gevent.sleep(0.1)
            state = axis.hw_state
            state_list = state.current_states_names
    
    def engage_brakes(self):
        print(f"\n\n\nSet air OFF .....", end="")
        self.wago_name.set(self.wago_channel, 0)
        gevent.sleep(self.waiting_time)
        print("DONE")

class AttHook(MotionHook):

    def __init__(self, name, config):

        self.config = config
        self.name = name

        self.shutter = config.get("shutter", None)
        if self.shutter is None:
            raise RuntimeError("AttHook: shutter not defined")
            
        self.passwd = config.get("password", None)

        super().__init__()
        
    def pre_move(self, motion_list):
        if self.passwd is not None:
            if not self.ask_passwd():
                raise RuntimeError("AttHook: Wrong Password")
        
        self.shutter.close()

    def post_move(self, motion_list):
        pass
        
    def ask_passwd(self):
        mystr = BOLD("Password")
        rep = click.prompt(f"\n\n    {mystr}", default="")
        if rep == self.passwd:
            return True
        return False

class IcHook(MotionHook):

    def __init__(self, name, config):
        super().__init__()
        
    def pre_move(self, motion_list):
        for motion in motion_list:
            #if motion.axis.name in ["lvisarty"]:
            if motion.axis.name in ["lvisarty", "uscope_ds_ty"]:
                try:
                    ldrivety_at_limneg = setup_globals.ldrivety.state.LIMNEG
                except:
                    raise RuntimeError(f"ldrivety state is not available, cannot move Axis {motion.axis.name}")
                if not ldrivety_at_limneg:
                    raise RuntimeError(f"Axis {motion.axis.name} may move only if ldrivety is at its NEGATIVE Limit")
                    
            if motion.axis.name == "ldrivety":
                try:
                    uscope_ds_ty_at_limpos = setup_globals.uscope_ds_ty.state.LIMPOS  # NSR 2022-11-19: uscope_ds v2 has its limits following ESRF conventions (was not true for v1)
                    lvisarty_at_limneg = setup_globals.lvisarty.state.LIMNEG
                except:
                    raise RuntimeError(f"lvisarty OR uscope_ds_ty states are not available, cannot move Axis \"{motion.axis.name}\"")
                if not lvisarty_at_limneg or not uscope_ds_ty_at_limpos:
                #if not lvisarty_at_limneg:
                    raise RuntimeError(f"\nAxis \"{motion.axis.name}\" may move only if:\n\t- lvisarty is at its NEGATIVE limit: {lvisarty_at_limneg}\n\tAND\n\t- uscope_ds_ty is at it POSITIVE limit: {uscope_ds_ty_at_limpos}\n")
                    
    def post_move(self, motion_list):
        pass

class OnoffHook(MotionHook):

    def __init__(self, name, config):

        self.config = config
        self.name = name
        self._in_scan = False
        super().__init__()

        self._disable_motors = QueueSetting(f"OnoffHook_{name}")
        self._enable_motors = QueueSetting(f"OnoffHook_enable_{name}")
        
    def __info__(self):
        print("")
        disable = RED("DISABLE")
        enable = GREEN("ENABLE")
        for axis_name in self._disable_motors:
            print(f"    {axis_name}: {disable}")
        for axis_name in self._enable_motors:
            print(f"    {axis_name}: {enable}")
            
        print("")
        return ""
    
    def disable_axis(self, axis):
        if axis.name in self._enable_motors:
            self._enable_motors.remove(axis.name)
        if axis.name not in self._disable_motors:
            self._disable_motors.append(axis.name)
        axis.on()
        motor_esync(axis)
        
    def enable_axis(self, axis):
        if axis.name in self._disable_motors:
            self._disable_motors.remove(axis.name)
        if axis.name not in self._enable_motors:
            self._enable_motors.append(axis.name)
        motor_esync(axis)
        axis.off()
            
    def pre_move(self, motion_list):
        if not self._in_scan:
            axis_list = []
            for motion in motion_list:
                axis_list.append(motion.axis)
            self.on(axis_list)
            
    def post_move(self, motion_list):
        if not self._in_scan:
            axis_list = []
            for motion in motion_list:
                axis_list.append(motion.axis)
            self.off(axis_list)
            
    def pre_scan(self, axis_list):
        self._in_scan = True
        self.on(axis_list)

    def post_scan(self, axis_list):
        self._in_scan = False
        self.off(axis_list)
    
    def pre_on(self, axis_list):
        self._in_scan = True
        self.on(axis_list)
    
    def pre_off(self, axis_list):
        self._in_scan = False
        self.off(axis_list)
        
    def on(self, axis_list):
        for axis in axis_list:
            if axis.name not in self._disable_motors:
                axis.on()
                motor_esync(axis)
    
    def off(self, axis_list):
        for axis in axis_list:
            if axis.name not in self._disable_motors:
                motor_esync(axis)
                axis.off()


        
