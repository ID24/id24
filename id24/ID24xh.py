import matplotlib.pyplot as plt
import numpy
import gevent
import os
import glob

from bliss import setup_globals
from bliss.config import settings
from bliss.config.settings import ParametersWardrobe
from bliss.common import plot
from bliss.common.cleanup import cleanup
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
from bliss.shell.standard import umv
from bliss.scanning.group import Sequence
from bliss.scanning.chain import AcquisitionChannel

from id24.ID24detector import ID24detectorBase
from id24.ID24menu import menu_list, menu_number, menu_string, menu_choice

#####################################################################
#####
##### Hamamatsu Detector
#####
#####################################################################
class ID24xhDetector(ID24detectorBase):
    
    def __init__(self, name, config):
        super().__init__(name, config)
                        
        # Add Xh specific default parameters and create ParametersWardrobe
        self._default_param["bunch"] = 7000
        self._default_param["unit"] = "bunch"
        self._param = ParametersWardrobe(f"scan_parameters_{self._name}", default_values=self._default_param)
        self._param.time_unit = "bunch"
        
        self.controller = config.get("_par_controller")
        
        self._configure()
        self._configure_lima_roi()
                
        self._xh_shutter = config.get("_par_xh_shutter", None)
        if self._xh_shutter is not None:
            try:
                self._xh_shutter.mode = self._xh_shutter.MANUAL
            except:
                pass
        
    ##################################################################
    #
    # Acquisition Methods
    #
    
    # Parameters
    def _get_acq_parameters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq):
        acq_par = {
            "acq_mode": "SINGLE",
            "prepare_once": False,
            "start_once": False,
            #"saving_format": "EDF",
            "acq_expo_time": 1e-9,
            "acq_trigger_mode": "INTERNAL_TRIGGER",
        }
                    
        if acq_mode == "I":
            self._param.I_frame = nbframe
            self._param.I_acq = nbacq
            self._param.I_time = inttime
            if synchro_mode == "software":
                self.controller.param.trig_channel = 9
                self.controller.xh_set_trigger(0, 0, 2)
                self.controller.xh_config_timing(self._param.I_time, 1, self._param.I_frame, self._param.I_acq)
                acq_par["acq_nb_frames"] = self._param.I_frame
            elif synchro_mode == "scan_orbit":
                self.controller.xh_timing_orbit(1 , 1, inttime)
                self.controller.param.trig_channel = 9
                self.controller.xh_set_trigger(0, 2, 0)
                self.controller.xh_config_timing(inttime, 1, 1, 1)
                acq_par["acq_nb_frames"] = 1
            else:
                self.controller.param.trig_channel = 0
                self.controller.xh_set_trigger(1, 0, 2)
                self.controller.xh_config_timing(self._param.I_time, 1, self._param.I_frame, self._param.I_acq)
                acq_par["acq_nb_frames"] = self._param.I_frame
        elif acq_mode == "I_bck":
            self._param.I_time = inttime
            self.controller.param.trig_channel = 9
            self.controller.xh_set_trigger(0, 0, 2)
            if synchro_mode == "scan_orbit":
                self.controller.xh_config_timing(inttime, 1, 1, 1000)
            else:
                self.controller.xh_config_timing(self._param.I_time, 1, 1, self._param.I_bck_acq)
            acq_par["acq_nb_frames"] = 1
        elif acq_mode == "I0":
            self.controller.param.trig_channel = 9
            self.controller.xh_set_trigger(0, 0, 2)
            self.controller.xh_config_timing(self._param.I0_time, 1, 1, self._param.I0_bck_acq)
            acq_par["acq_nb_frames"] = 1
        elif acq_mode == "I0_bck":
            self.controller.param.trig_channel = 9
            self.controller.xh_set_trigger(0, 0, 2)
            self.controller.xh_config_timing(self._param.I0_time, 1, 1, self._param.I0_acq)
            acq_par["acq_nb_frames"] = 1
        else:
            raise RuntimeError(f"Xh Acq: acquisition mode {acq_mode} not implemented (I/I0/I_bck/I0_bck)")
            
        return acq_par

    # Data
    def _get_acq_data(self, scan, nbframe, nbacq):
        node = scan.get_data()[f"{self.lima.name}:image"]
        data = numpy.zeros((nbframe, self._param.nb_pixel))
        for nframe in range(nbframe):
            #data[nframe] = node.get_image(nframe).byteswap() / nbacq
            data[nframe] = node.get_image(nframe)
        return data
        #return data[:][self._param.first_pixel:self._param.last_pixel]
            
    def _get_acq_roi(self, scan, nbframe, nbacq):
        data = self._get_acq_data(scan, nbframe, nbacq)
        data_roi = {
            "roi_I1": numpy.zeros((nbframe,)),
            "roi_I2": numpy.zeros((nbframe,)),
            "roi_I3": numpy.zeros((nbframe,)),
            "roi_I4": numpy.zeros((nbframe,)),
            "roi_Isum": numpy.zeros((nbframe,)),
        }
        rois = self.lima.roi_counters.get_rois()
        for nframe in range(nbframe):
            data_roi["roi_I1"][nframe] = data[nframe][rois[0].x:rois[0].x+rois[0].width].sum() / rois[0].width
            data_roi["roi_I2"][nframe] = data[nframe][rois[1].x:rois[1].x+rois[1].width].sum() / rois[1].width
            data_roi["roi_I3"][nframe] = data[nframe][rois[2].x:rois[2].x+rois[2].width].sum() / rois[2].width
            data_roi["roi_I4"][nframe] = data[nframe][rois[3].x:rois[3].x+rois[3].width].sum() / rois[3].width
            data_roi["roi_Isum"][nframe] = data[nframe].sum() / self._param.nb_pixel
        return data_roi
            
    def _get_acq_roi_lima(self, scan, nbframe, nbacq):
        scan_data = scan.get_data()
        data_roi = {
            "roi_I1": numpy.zeros((nbframe,)),
            "roi_I2": numpy.zeros((nbframe,)),
            "roi_I3": numpy.zeros((nbframe,)),
            "roi_I4": numpy.zeros((nbframe,)),
            "roi_Isum": numpy.zeros((nbframe,)),
        }
        for nframe in range(nbframe):
            for nacq in range(nbacq):
                data_roi["roi_I1"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:I1_sum"][nframe*nbacq+nacq]/nbacq)
                data_roi["roi_I2"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:I2_sum"][nframe*nbacq+nacq]/nbacq)
                data_roi["roi_I3"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:I3_sum"][nframe*nbacq+nacq]/nbacq)
                data_roi["roi_I4"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:I4_sum"][nframe*nbacq+nacq]/nbacq)
                data_roi["roi_Isum"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:Isum_sum"][nframe*nbacq+nacq]/nbacq)
        return data_roi
        
        #scan_data = scan.get_data()
        #data_roi = {
        #    "roi_I1": numpy.zeros((nbframe,)),
        #    "roi_I2": numpy.zeros((nbframe,)),
        #    "roi_I3": numpy.zeros((nbframe,)),
        #    "roi_I4": numpy.zeros((nbframe,)),
        #    "roi_Isum": numpy.zeros((nbframe,)),
        #}
        #for nframe in range(nbframe):
        #    data_roi["roi_I1"][nframe] = (scan_data[f"{self.lima.name}:roi_counters:I1_sum"][nframe]/nbacq)
        #    data_roi["roi_I2"][nframe] = (scan_data[f"{self.lima.name}:roi_counters:I2_sum"][nframe]/nbacq)
        #    data_roi["roi_I3"][nframe] = (scan_data[f"{self.lima.name}:roi_counters:I3_sum"][nframe]/nbacq)
        #    data_roi["roi_I4"][nframe] = (scan_data[f"{self.lima.name}:roi_counters:I4_sum"][nframe]/nbacq)
        #    data_roi["roi_Isum"][nframe] = (scan_data[f"{self.lima.name}:roi_counters:Isum_sum"][nframe]/nbacq)
        #return data_roi
        
    # Shutter Management
    def _acq_prepare(self):
        if self._xh_shutter is not None:
            self._xh_shutter.open()
        
    def _acq_end(self):
        if self._xh_shutter is not None:
            self._xh_shutter.close()

    ##################################################################
    #
    # Lima helper
    #
    def _get_detector_width(self):
        return self.lima.proxy.image_max_dim[0]

    def _get_detector_height(self):
        return self.lima.proxy.image_max_dim[1]
        
    def _configure(self):
        self.lima.image.roi = [self._param.first_pixel, 0, self._param.nb_pixel, 1]

#####################################################################
#####
##### XH controller
#####
#####################################################################
class ID24xhControl:
    
    def __init__(self, name, config):

        self.lima = config.get("lima", None)
        if self.lima is None:
            raise RuntimeError("ID24xhControl: No lima DS specified")
                    
        self.lima.image.flip = [True, False]
        
        self.default_param = {}
        self.default_param["xh_timing_script"] = 0
        self.default_param["xh_capa"] = 0
        self.default_param["xh_orbit_delay"] = 0
        self.default_param["xh_voltage"] = 0
        self.default_param["cycles"] = 1
        self.default_param["trig_channel"] = 9 # hdw=0/1 soft=9
        self.default_param["trig_group"] = 0
        self.default_param["trig_frame"] = 0
        self.default_param["trig_acq"] = 0
        self.default_param["latency_time"] = 0
        self.param = ParametersWardrobe(f"xh_config", default_values=self.default_param)

        self._XH_TIMING_SCRIPT = [
            "config_timing_1turn",
            "config_timing_2turn",
            "config_timing_3turn",
            "config_timing_4turn",
            
            "config_timing_3turn_no_overlap",
            "config_timing_4turn_no_overlap",
            
            "config_timing_2turn_4bunch",
            "config_timing_2turn_16bunch",
            "config_timing_2turn_4bunch_no",
            "config_timing_2turn_16bunch_no",

            "config_timing_3turn_4bunch",
            "config_timing_3turn_16bunch",

            "config_timing_4turn_4bunch",
            "config_timing_4turn_16bunch",

            "config_timing_5turn_4bunch",
            "config_timing_5turn_16bunch"
        ]

        self._XH_CAPA = [2,5,7,10,15,20,32,40]
        
        self._trigger_group = settings.SimpleSetting(f"XH_trigger_group", default_value=0)
        self._trigger_frame = settings.SimpleSetting(f"XH_trigger_frame", default_value=0)
        self._trigger_acq = settings.SimpleSetting(f"XH_trigger_acq", default_value=2)
        self._orbit_delay = settings.SimpleSetting(f"XH_orbit_delay", default_value=0)
        self.xh_set_orbit_delay(self._orbit_delay.get())

    ##################################################################
    #####
    ##### USER INFO
    #####    
    def _get_metadata(self):
        det_config = {}
        det_config["xh_orbit_delay"] = self.param.xh_orbit_delay
        det_config["xh_orbit_delay_unit"] = "Xh clock unit: 22ns"
        det_config["xh_capa"] = self._XH_CAPA[self.param.xh_capa]
        det_config["xh_capa_unit"] = "pF"
        det_config["xh_voltage"] = self.param.xh_voltage
        det_config["xh_voltage_unit"] = "V"
        det_config["xh_timing_script"] = self._XH_TIMING_SCRIPT[self.param.xh_timing_script]
        #(t1, t2, t3, t4) = self.xh_get_logs_temp()
        #det_config["xh_temperature"] = f"{t1} {t2} {t3} {t4}"
        return det_config
        
    ##################################################################
    #####
    ##### Communication
    #####
    def xh_send(self, cmd):
        self.lima._get_proxy("Xh").sendCommand(cmd)

    ##################################################################
    #####
    ##### TRIGGERS
    #####    
    @property
    def trigger_group(self):
        return self._trigger_group.get()
        
    @trigger_group.setter
    def trigger_group(self, val):
        self._trigger_group.set(val)
    
    @property
    def trigger_frame(self):
        return self._trigger_frame.get()
        
    @trigger_frame.setter
    def trigger_frame(self, val):
        self._trigger_frame.set(val)
    
    @property
    def trigger_acq(self):
        return self._trigger_acq.get()
        
    @trigger_acq.setter
    def trigger_acq(self, val):
        self._trigger_acq.set(val)
        
    def xh_set_trigger(self, group, frame, acq):
        # 0=notrig 1=external trig 2=orbit internal trig
        self.param.trig_group = group
        self.param.trig_frame = frame
        self.param.trig_acq = acq
        
    ##################################################################
    #####
    ##### Timing Configuration for xh acquisition
    #####    
    def xh_config_timing(self, inttime, nbgroup, nbframe, nbacq):
        bunch = int(inttime)
        self.xh_send("xstrip timing ext-output \'xh0\' -1 integration")
        for group in range(nbgroup):
            # Output integration time on all frames in the group
            cycles_time = bunch
            quarter = 0
            if bunch != inttime:
                cycles_time = bunch
                quarter = int(0.5+(inttime - bunch) / 0.25)
                if quarter == 4:
                    quarter = 0
                    bunch = bumch + 1
            cmd = f"xstrip timing setup-group \'xh0\' {group} {nbframe} {nbacq} {cycles_time} s2-delay {quarter}"
            # group trigger
            if self.param.trig_group == 1:
                cmd += f" ext-trig-group trig-mux {self.param.trig_channel}"
            if self.param.trig_group == 2:
                cmd += f" orbit-group orbit-mux 3"
            # frame trigger
            if self.param.trig_frame == 1:
                cmd += f" ext-trig-frame trig-mux {self.param.trig_channel}"
            if self.param.trig_frame == 2:
                cmd += f" orbit-frame orbit-mux 3"
            # acq trigger
            if self.param.trig_acq == 1:
                cmd += f" ext-trig-scan trig-mux {self.param.trig_channel}"
            if self.param.trig_acq == 2:
                cmd += f" orbit-scan orbit-mux 3"
            # latency time
            cmd += f" group-delay {self.param.latency_time}"
                
            cmd += " lemo-out 65535"
            
            #if self.param.trig_group == 1 and self.param.trig_channel != 9:
            #    cmd += f" cycles-start {nbacq} cycles-end"
            # !!!! ASK WILLIAM WHAT IS cycles-start %d cycles-end
            #if ((SYNC_mode == 2) && (sttrilizg != 9)) {
            #    cmd = sprintf("%s cycles-start %d cycles-end", cmd, stscans)
            #
           
            if group == nbgroup-1:
                cmd += " last" 

        self.xh_send(cmd)
        
    ##################################################################
    #####
    ##### Timing Scripts
    #####    
    def xh_set_timing_script(self, ind=None):
        if ind is None:
            (rep, self.param.xh_timing_script) = menu_choice(
                f"XH Config File",
                self._XH_TIMING_SCRIPT,
                self.param.xh_timing_script
            ) 
        else:
            self.param.xh_timing_script = ind
        selected_script = self._XH_TIMING_SCRIPT[self.param.xh_timing_script]
        self.xh_send(f"~{selected_script}")
    
    def xh_get_timing_script(self):
        try:
            self.xh_send("%xstrip_timing_file")
        except:
            pass
            
    ##################################################################
    #####
    ##### Capa
    #####    
    def xh_set_capa(self, capa=None):
        if capa is None:
            (rep, self.param.xh_capa) = menu_choice(
                f"XH CAPA",
                self._XH_CAPA,
                self.param.xh_capa
            ) 
        else:
            self.param.xh_capa = capa
        selected_capa = self._XH_CAPA[self.param.xh_capa]
        #self.lima.proxy.setHeadCaps([selected_capa, selected_capa])
        self.lima._get_proxy("Xh").setHeadCaps([selected_capa, selected_capa])
        
    def xh_get_capa(self):
        try:
            self.xh_send("xstrip head get-xchip3-settings \"xh0\" 0")
        except:
            pass
        
    ##################################################################
    #####
    ##### Voltage
    #####    
    def xh_set_voltage(self, v):
        nsteps = 20
        if v != 0:
            self.xh_send("xstrip hv enable \'xh0\' auto")
        try:
            self.xh_send("set-func \'v_curr\' \'xstrip hv get-adc \"xh0\"\' local hide")
        except:
            pass
        try:
            self.xh_send(f"set-func \'vstep\' \'({v}-v_curr)/{nsteps}\' local")
        except:
            pass
        for i in range(nsteps):
            try:
                self.xh_send("set-func \'v_curr\' \'%%v_curr+vstep\' local")
            except:
                pass
            try:
                self.xh_send("xstrip hv set-dac \"xh0\" %%v_curr")
            except:
                pass
        try:
            self.xh_send(f"xstrip hv set-dac \"xh0\" {v}")
        except:
            pass
        self.param.xh_voltage = v
    
    def xh_get_voltage(self):
        try:
            self.xh_send("xstrip hv get-adc \"xh0\"")
        except:
            pass

    ##################################################################
    #####
    ##### High Voltage
    #####    
    def xh_set_hv_on(self):
        self.xh_send("xstrip hv enable \'xh0\' auto")
        gevent.sleep(5)
        self.xh_send("xstrip hv set-dac \'xh0\' -90")
        gevent.sleep(5)

    def xh_set_hv_off(self):
        self.xh_send("xstrip hv enable \'xh0\' off")
        gevent.sleep(5)

    ##################################################################
    #####
    ##### Temperature
    #####    
    def xh_readtemp(self):
        for ch in range(4):
            try:
                self.xh_send(f"xstrip tc get \"xh0\" ch {ch} t")
            except:
                pass
                
    def xh_get_logs_temp(self):
        saving_dir = "/users/blissadm/local/beamline_configuration/detector/xh_data"
        remote_dir = "/xh-data/mirion/logs/xhEsrfSys5"
        remote_file = "ibias.log"
        os.system(f"scp blissadm@lid243:{remote_dir}/{remote_file} {saving_dir}/{remote_file}")
        data = numpy.loadtxt(f"{saving_dir}/{remote_file}", dtype=numpy.float, comments="#").transpose()
        return (data[7][-1], data[8][-1], data[9][-1], data[10][-1])
                
    ##################################################################
    #####
    ##### Switch ON/OFF
    #####    
    def xh_powerdown(self):
        self.xh_send("~head_powerdown")

    def xh_cooldown(self):
        self.xh_send("~cooldown_xh")

    ##################################################################
    #####
    ##### Orbit Delay
    #####    
    def xh_set_orbit_delay(self, orbit_delay=None):
    
        if orbit_delay is not None:
            self.param.xh_orbit_delay = orbit_delay
        else:
            #minmax = [0, 1000]
            self.param.xh_orbit_delay = menu_number(
                "Orbit Scan Delay",
                #minmax=minmax,
                default=self.param.xh_orbit_delay,
                integer=True
            )
    
        self._orbit_delay.set(self.param.xh_orbit_delay)
        self.xh_send(f"xstrip timing setup-orbit \'xh0\' {self.param.xh_orbit_delay}")

    ##################################################################
    #####
    ##### Timing Orbit
    #####    
    def xh_timing_orbit(self, nframe, nacq, bunch):
        self.xh_send("xstrip timing ext-output \'xh0\' -1 integration")
        cmd = f"xstrip timing setup-group \'xh0\' 0 {nframe} {nacq} {bunch} last orbit-scan orbit-mux 3 lemo-out 65535"
        self.xh_send(cmd)

class ID24xhTransfert:
    
    def __init__(self, name, config):
        self._name = name
        self._config = config
        self._dir_remote_base = "/xh-data/mirion"
        #self._saving_dir = "/users/blissadm/local/beamline_configuration/detectors/xh_data/"
        self._saving_dir = "/data/id24/inhouse/ED/XH/"
        #self._remote_dir = "/xh-data/mirion/20210428/"
        self._remote_dir = "/xh-data/mirion/20210928/"
        self._iv_og_file = "_iv_og.asc"
        self._xh_iv_file = "_xh_iv.asc"
    
    def __info__(self):
        mystr = ""
        mystr += f"Remote Directory  : {self._dir_remote_base}{self._remote_dir}\n"
        mystr += f"Remote iv/og File : {self._iv_og_file}\n"
        mystr += f"Remote xh/iv File : {self._xh_iv_file}\n\n"
        mystr += f"Saving Directory  : {self._saving_dir}\n"
        return mystr
        
    def get_iv_og(self, num):
        iv_og_file = f"{num}{self._iv_og_file}"
        os.system(f"scp blissadm@lid243:{self._remote_dir}{iv_og_file} {self._saving_dir}{iv_og_file}")
        return(numpy.loadtxt(f"{self._saving_dir}{iv_og_file}", dtype=numpy.float, comments="#").transpose())
        
    def plot_iv_og(self, nums):
        data = []
        for ind in nums:
            data.append(self.get_iv_og(ind))
        display = setup_globals.display
        y_data = numpy.zeros((len(nums), len(data[0][1])))
        header = []
        for i in range(len(nums)):
            y_data[i] = data[i][3]
        display.plot_curve("iv_og", "iv_og", "iv_og",data[0][1],  y_data[:], "V", "uA", names=nums)

    def get_temperature(self):
        temp_dir = f"{self._dir_remote_base}/logs/xhEsrfSys6"
        temp_file = "ibias.log"
        os.system(f"rsync blissadm@lid243:{temp_dir}/{temp_file} {self._saving_dir}{temp_file}")
        self._data = numpy.loadtxt(f"{self._saving_dir}{temp_file}", dtype=numpy.float64, comments="#").transpose()
        return (self._data[7][-1], self._data[8][-1], self._data[9][-1], self._data[10][-1])

    def get_voltage_from_file(self):
        temp_dir = f"{self._dir_remote_base}/logs/xhEsrfSys6"
        temp_file = "ibias.log"
        os.system(f"rsync blissadm@lid243:{temp_dir}/{temp_file} {self._saving_dir}{temp_file}")
        self._data = numpy.loadtxt(f"{self._saving_dir}{temp_file}", dtype=numpy.float64, comments="#").transpose()
        return (self._data[3][-1])

    def synchronize_ibias_file(self):
        temp_dir = f"{self._dir_remote_base}/logs/xhEsrfSys6"
        temp_file = "ibias.log"
        os.system(f"rsync -v blissadm@lid243:{temp_dir}/{temp_file} {self._saving_dir}{temp_file}")

    def synchronize_iv_files(self):
        os.system(f"rsync -v blissadm@lid243:{self._dir_remote_base}/*/*iv_og.asc {self._saving_dir}/")

    def plot_last_3_iv_curves(self):
        os.chdir(self._saving_dir)
        
        fig, ax = plt.subplots(1, 1, figsize=[10, 8])
        
        flist = os.listdir()
        #print(sorted(flist))
        flist.remove("ibias.log")
        
        for f in sorted(flist)[-4:-1]:  # take the 3 last ones
            if f.endswith("_iv_og.asc"):
                with open(f) as input_file:
                    head = [next(input_file) for _ in range(2)]
                    comment = head[-1]
                    #print(comment)
                data = numpy.loadtxt(f, delimiter="\t", skiprows=2)
                voltage = data[:,2]
                current = data[:,3]
                ax.plot(voltage, current, label=f"{comment.strip().replace('# ', '')}")
        plt.xlabel("Vbias [V]")
        plt.ylabel("Leakage current [mA]")
        plt.legend()
        plt.show()
