
from bliss import setup_globals
from bliss.scanning.chain import ChainPreset
from bliss.config.settings import SimpleSetting

class ScanEdshut:
    def __init__(self, name, config):
        self._name = name
        self._preset_name = "ScanEdshut_{name}"
        self._state = SimpleSetting(self._preset_name, default_value="Off")
        self._preset = EdshutScanPreset()
    
    def __info__(self):
        return f"\n    State: {self.state}\n"
        
    def on(self):
        setup_globals.DEFAULT_CHAIN.add_preset(preset=self._preset, name=self._preset_name)
        self._state.set("On")
        
    def off(self):
        setup_globals.DEFAULT_CHAIN.remove_preset(preset=self._preset, name=self._preset_name)
        self._state.set("Off")
        
    @property
    def state(self):
        return self._state.get()
        
class EdshutScanPreset(ChainPreset):
    """
    This class interface will be called by the chain object
    at the beginning and at the end of a chain iteration.

    A typical usage of this class is to manage the opening/closing
    by software or to control beamline multiplexer(s)
    """

    def get_iterator(self, chain):
        """Yield ChainIterationPreset instances, if needed"""
        pass

    def prepare(self, chain):
        """
        Called on the preparation phase of the chain iteration.
        """
        setup_globals.edshut.open()

    def start(self, chain):
        """
        Called on the starting phase of the chain iteration.
        """
        pass
        
    def before_stop(self, chain):
        """
        Called at the end of the scan just before calling **stop** on detectors
        """
        pass

    def stop(self, chain):
        """
        Called at the end of the chain iteration.
        """
        setup_globals.edshut.close()
