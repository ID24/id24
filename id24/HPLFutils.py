from bliss.config import settings

class HPLFutils:
    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self._default_delay = config.get("_par_default_delay", None)
        if self._default_delay is None:
            raise RuntimeError("_par_default_delay is not defined in the yml")
            
        self.__drive_max_energy = settings.SimpleSetting(f"HPLFutils_{name}_drive_max_energy", default_value=40)  # J
        self.__drive_requested_energy = settings.SimpleSetting(f"HPLFutils_{name}_drive_requested_energy", default_value=40)  # J
        self.__drive_requested_delay = settings.SimpleSetting(f"HPLFutils_{name}_drive_requested_delay", default_value=0)
        self.__laserprobe_requested_delay = settings.SimpleSetting(f"HPLFutils_{name}_laserprobe_requested_delay", default_value=0)
        
    @property
    def _drive_max_energy(self):
        return self.__drive_max_energy.get()
        
    @_drive_max_energy.setter
    def _drive_max_energy(self, value):
        """
        pour pouvoir faire blabla._drive_max_energy = value
        """
        self.__drive_max_energy.set(value)
    
    @property
    def _drive_requested_energy(self):
        return self.__drive_requested_energy.get()
    
    @_drive_requested_energy.setter
    def _drive_requested_energy(self, value):
        self.__drive_requested_energy.set(value)
        
    @property
    def _drive_requested_delay(self):
        return self.__drive_requested_delay.get()
    
    @_drive_requested_delay.setter
    def _drive_requested_delay(self, value):
        self.__drive_requested_delay.set(value)

    @property
    def _laserprobe_requested_delay(self):
        return self.__laserprobe_requested_delay.get()
    
    @_laserprobe_requested_delay.setter
    def _laserprobe_requested_delay(self, value):
        self.__laserprobe_requested_delay.set(value)
