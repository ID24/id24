import os
import tabulate
from ruamel.yaml import YAML
import gevent

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.scanning.chain import AcquisitionMaster

from id24.ID24utils import ID24attrList, ID24profile
from id24.ID24menu import menu_list, menu_number, menu_string, menu_choice

class ID24opiomControl:
    
    def __init__(self, name, config):
        
        #
        # Config
        #
        self._name = name
        self._config = config
        
        self.opiom = config.get("opiom")
        
        # Synchronisation object
        self._synchro = OpiomSynchro(self)
        
        #
        # Init. Variables
        #
        
        # pulse length at TTL ON in s
        self._pulse_length = 0.1
        # number of pulse counter to be use (1/2)
        self._pulse_counter = 1
        # Delay between pulse 1 and 2 if 2 pulses in s
        self._pulse_delay = 1
        # Time to wait before pulse 1
        self._pulse_wait = 0
        
    ################################################################
    #####
    ##### User info
    #####
    def __info__(self):
        return self.opiom.__info__()
        
    def _get_info(self):
        return ""
        
    def _get_metadata(self):
        md = {}            
        return md
    
    ################################################################
    #####
    ##### Pulse Command
    #####
    def pulse_prepare(self):
        opiom_clock = 2000000
        high_time = int(self._pulse_length * opiom_clock)
        if self._pulse_wait <= 0:
            low_time = 1
        else:
            low_time = int(self._pulse_wait*opiom_clock)
        self.opiom.comm("CNT 1 RESET")
        self.opiom.comm(f"CNT 1 CLK2 PULSE {high_time} {low_time} 1")
        if self._pulse_counter == 2:
            hight_time = self._pulse_length * opiom_clock
            low_time = low_time + high_time + self._pulse_delay*opiom_clock
            self.opiom.comm("CNT 2 RESET")
            self.opiom.comm(f"CNT 2 CLK2 START RISE SP1 PULSE {high_time} {low_time} 1")
                    
    def pulse_start(self):
        if self._pulse_counter == 2:
            self.opiom.comm("CNT 2 START")
        self.opiom.comm("CNT 1 START")
        
    ################################################################
    #####
    ##### Pulses Parameters
    #####
    def parameters(self):
        param = ["Pulse Length (s)", "Pulse Number [1/2]", "Time between 2 Pulses (s)", "Waiting time before 1st pulse (s)"]
        title = ["Parameter", "Value"]
        while True:
            value = [self._pulse_length, self._pulse_counter, self._pulse_delay, self._pulse_wait]
            (rep, ind) = menu_list("Opiom", param, values=value, title=title)
            if rep == "Pulse Length (s)":
                minmax = [0, 10]
                self._pulse_length = menu_number(
                    "Pulse Length (s)",
                    minmax=minmax,
                    default=self._pulse_length,
                    integer=False
                )
            if rep == "Pulse Number [1/2]":
                minmax = [1, 2]
                self._pulse_counter = menu_number(
                    "Pulse Number [1/2]",
                    minmax=minmax,
                    default=self._pulse_counter,
                    integer=True
                )
            if rep == "Time between 2 Pulses (s)":
                minmax = [0, 10]
                self._pulse_delay = menu_number(
                    "Time between 2 Pulses (s)",
                    minmax=minmax,
                    default=self._pulse_delay,
                    integer=False
                )
            if rep == "Waiting time before 1st pulse (s)":
                minmax = [0, 10]
                self._pulse_wait = menu_number(
                    "Waiting time before 1st pulse (s)",
                    minmax=minmax,
                    default=self._pulse_wait,
                    integer=False
                )
            if rep == "q":
                return

class OpiomSynchro:
    def __init__(self, controller):
        self._controller = controller
        
        self._available_mode = "intern/extern"
         
    def get_acquisition_master(self, synchro_mode):
        if synchro_mode not in self._available_mode.split("/"):
            raise RuntimeError(f"Synchronization mode {synchro_mode} not in [{self._available_mode}]")
        return OpiomSynchroAcqMaster(synchro_mode, self)
               
    def prepare_synchro(self, synchro_mode):
        if synchro_mode not in self._available_mode.split("/"):
            raise RuntimeError(f"Synchronization mode {synchro_mode} not in [{self._available_mode}]")
        if synchro_mode == "intern":
            self._controller.pulse_prepare()
  
    def start_synchro(self, synchro_mode):
        if synchro_mode == "intern":
            self._controller.pulse_start()
                
 
class OpiomSynchroAcqMaster(AcquisitionMaster):
    def __init__(self, synchro_mode, synchro_ctrl):
        AcquisitionMaster.__init__(self, None, name="OpiomSynchroAcqMaster")
        self._synchro_mode = synchro_mode
        self._synchro_ctrl = synchro_ctrl
        
    def prepare(self):
        self._synchro_ctrl.prepare_synchro(self._synchro_mode)
            
    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        self._synchro_ctrl.start_synchro(self._synchro_mode)

    def trigger_ready(self):
        return True

    def wait_ready(self):
        pass

    def stop(self):
        pass
