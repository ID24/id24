

from bliss.common.cleanup import cleanup
from bliss.common.utils import ColorTags, BOLD, RED
from bliss.config import settings
from bliss.scanning.scan import Scan
from bliss.controllers.mca.handel.error import HandelError
from bliss import current_session
from bliss.config.static import get_config

from id24.SCANS.scan_tools import (
    scantool_set_scan_parameters,
    scantool_contscan_get_acquisition_chain,
    scantool_contscan_get_acquisition_master,
    ScanOutput,
    ScanWatchdog
)
from id24.ID24utils import save_contscan_ascii

class ContScan:

    def __init__(self, name, config):

        # internal parameter
        self._name = name
        self._config = config

        # shutter
        self._shutter = config.get("_par_shutter", None)

        # Get multiplexer and its parameters to select triggers signals
        self._opiom_mux = None
        opiom_mux = config.get("_par_multiplexer", None)
        if opiom_mux is not None:
            self._opiom_mux = {}
            for mux in opiom_mux:
                for key, val in mux.items():
                    self._opiom_mux[key] = val

        # Metadata
        self._metadata = config.get("par_metadata", None)

        # RockingCurve Method
        self._rocking_curve = config.get("_par_rocking_curve", None)

        # MUSST card for synchronization
        self._musst = config.get("_par_musst", None)
        if self._musst is None:
            raise RuntimeError("ContScan: No musst device defined")

        # Get Musst Counters for Continuous scans
        musst_config = self._config.get("musst_counters", None)
        if musst_config is not None:
            self._musst_counter = {}
            for musst_cnt in musst_config:
                counter = musst_cnt.get("counter", None)
                if counter is not None:
                    self._musst_counter[counter.name] = self._musst.counters[counter.name]

        # Get Musst Calculated Counters for Continuous scans
        musst_calc_config = self._config.get("musst_calc_counters", None)
        if musst_calc_config is not None:
            self._musst_calc_counter = {}
            for musst_calc in musst_calc_config:
                counter = musst_calc.get("counter", None)
                if counter is not None:
                    self._musst_calc_counter[counter.name] = counter
        else:
            self._musst_calc_counter = None

        # Variables
        #self._shutter = None
        self._scan_state = None
        self._scan_name = None
        self._scan_par = None
        self._scan_par_back = None
        self._builder = None
        self._builder_back = None
        self._chain = None
        self._chain_back = None
        self._master = None
        self._master_back = None
        self._timeout = 10
        self._expo_time = -1

        # Settings
        self._start_trig_mode = settings.SimpleSetting(f"ContScan_{self._name}_start_trig_mode", default_value="POSITION")
        self._point_trig_mode = settings.SimpleSetting(f"ContScan_{self._name}_point_trig_mode", default_value="TIME")

    """
    Continuous scan in time
    No Motor Movement
    """
    def time(self, nb_points, time_per_points, *counters, comment=None):
        self._scan("time", None, 0, 0, nb_points, time_per_points, "TIME", "TIME", *counters, comment=comment)
        
    def xas(self, E_start, E_end, n_points, time, *counters):
        dcmfshut = get_config().get("dcmfshut")
        mono = get_config().get("dcm")
        undu = mono.trajectory._undulator_master
        scan_saving = current_session.scan_saving
        undutrajund = get_config().get("undutrajund")
        
        dcmfshut.open()
        print(f"\nScan directoy is: {scan_saving.get_path()}")
        self.motor(undutrajund, undu.tracking.energy2tracker(E_start), undu.tracking.energy2tracker(E_end), n_points, time, *counters, save_ascii = False)
        dcmfshut.close()
    
    """
    Continuous scan with motor
    """
    def motor(
        self,
        motor,
        start,
        stop,
        nb_points,
        time_per_points,
        *counters,
        nbscan=1,
        backNforth=False,
        comment=None,
        save_ascii=False
    ):
        
        # CHECKS
        if start == stop:
            raise RuntimeError("ZAP: Start and Stop positions are the same")

        # INIT VARIABLES
        if start > stop:
            way = "down"
            way_back = "up"         
        else:
            way = "up"
            way_back = "down"            
             
        for nscan in range(nbscan):
            
            # SCAN PARAMETERS
            if not backNforth or (backNforth and nscan%2==0):
                start_pos = start
                stop_pos = stop
                s_way = way
            else:
                start_pos = stop
                stop_pos = start
                s_way = way_back
                                
            print(f"\nSCAN #{nscan+1}(/{nbscan}) - {s_way.upper()} direction")
            self._scan(
                "motor",
                motor,
                start_pos,
                stop_pos,
                nb_points,
                time_per_points,
                self._start_trig_mode.get(),
                self._point_trig_mode.get(),
                *counters,
                comment=comment,
                save_ascii=save_ascii,
            )

    """
    Scan devices
    """
    def set_shutter(self, shutter):
        self._shutter = shutter

    def set_exposure_time(self, expo_time):
        self._expo_time = expo_time

    """
    General Scan Macros
    """
    def get_undershoot(self, motor, start, stop, nb_points, time_per_points):
        # set scan parameters
        scan_par = scantool_set_scan_parameters(
            motor=motor,
            start=start,
            stop=stop,
            points=nb_points,
            time=time_per_points,
            trig_start=self._start_trig_mode.get(),
            trig_point=self._point_trig_mode.get()
        )

        # MOTOR MASTER
        master = scantool_contscan_get_acquisition_master(motor, scan_par)

        return master.undershoot

    def _scan_end(self):
        """
        Method called at the end of the scan
        """
        # Close beamshutter
        if self._shutter is not None:
            self._shutter.close()

        if self._opiom_mux is not None:
            if self._opiom_mux["value_out"] == "LAST":
                value = self._opiom_mux["value_saved"]
            else:
                value = self._opiom_mux["value_out"]
            self._opiom_mux["opiom_mux"].switch(self._opiom_mux["selector"], value)

        if self._scan_state != "FINISHED":
            self._scan_state = "ABORTED"

        self._scan_name = None

    def _scan(
        self,
        scan_type,
        motor,
        start,
        stop,
        nb_points,
        time_per_points,
        start_trig_mode,
        point_trig_mode,
        *counters,
        comment=None,
        save_ascii=False,
    ):

        # SCAN STATE
        self._scan_state = "PREPARING"
        self._scan_name = scan_type

        with cleanup(self._scan_end):

            # OPIOM multiplexer to select trig source.
            if self._opiom_mux is not None:
                opiom_mux = self._opiom_mux["opiom_mux"]
                selector = self._opiom_mux["selector"]
                if self._opiom_mux["value_out"] == "LAST":
                    self._opiom_mux["value_saved"] = opiom_mux.getOutputStat(selector)
                opiom_mux.switch(selector, self._opiom_mux["value_in"])

            # Add musst counters to user counters
            musst_counters = tuple(self._musst_counter.values())
            counter_list = counters + musst_counters

            # set scan parameters
            self._scan_par = scantool_set_scan_parameters(
                motor=motor,
                start=start,
                stop=stop,
                points=nb_points,
                time=time_per_points,
                trig_start=start_trig_mode,
                trig_point=point_trig_mode
            )
            if self._expo_time != -1:
                self._scan_par["expo_time"] = self._expo_time

            # MOTOR MASTER
            self._master = scantool_contscan_get_acquisition_master(motor, self._scan_par)

            # ACQUISITION CHAIN
            (self._builder, self._chain) = scantool_contscan_get_acquisition_chain(
                self._master,
                self._musst,
                self._scan_par,
                self._musst_calc_counter,
                counter_list
            )
            print(self._chain._tree)

            # SCAN DISPLAY
            self._display = ScanOutput(self._musst,
                self._scan_par["points"],
                self._scan_par["motor"],
                self._builder,
            )

            # SCAN WATCHDOG
            timeout = self._timeout
            if self._master is not None:
                timeout = 2.0 * self._master.undershoot / self._master.velocity + self._timeout
            self._watchdog = ScanWatchdog(self._musst,
                self._scan_par["points"],
                self._scan_par["motor"],
                self._builder,
                timeout=timeout,
            )

            # META DATA
            instrument = {}
            if self._metadata is not None:
                instrument = metadata.get_meta_data()

            # SCAN INFO
            sc_title = f"{self._name}.{scan_type}"
            if motor is not None:
                sc_title += f" {motor.name} {start} {stop}"
            sc_title += f" {nb_points} {time_per_points}"
            if comment is not None:
                 sc_title += f" {comment}"
            scan_info = {
                "title": sc_title,
                "type": f"{self._name}.exafs_cont",
                "points": nb_points,
                "instrument": instrument
            }

            # SCAN
            self._scan_obj = Scan(
                self._chain,
                name=f"{self._name}.{scan_type}",
                scan_info=scan_info,
                data_watch_callback=self._display,
                watchdog_callback=self._watchdog
            )
            self._scan_state = "RUNNING"

            # Open beamshutter
            if self._shutter is not None:
                self._shutter.open()

            try:
                self._scan_obj.run()
                #self.plotter.run(self._scan_obj)
            except KeyboardInterrupt:
                print("KeyboardInterrup, exit...\n\n")
                return
            except HandelError:
                print(RED("fx Error, continue...\n\n"))
            except TimeoutError:
                print(RED("\Missing points on last scan, continue....\n\n"))
                
            if save_ascii:
                uxassamty = get_config().get("uxassamty")
                uxassamtz = get_config().get("uxassamtz")
                ascii_header = f"uxassamty: {uxassamty.position} - uxassamtz: {uxassamtz.position}"
                save_contscan_ascii(self._scan_obj, comment=ascii_header)

