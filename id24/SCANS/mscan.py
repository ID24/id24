
import gevent

from bliss.shell.standard import umv

class MonochromatorScans:
   
    def __init__(self, name, config):

        # internal parameter
        self._name = name
        self._config = config

        # Monochromator
        self._mono = config.get("_par_mono", None)

        # Energy traj Undulator motor - Energy constant speed
        self._energy_traj_und = config.get("_par_energy_traj_und")

        # Energy traj Undulator motor - Undulator constant speed
        self._undulator_traj_und = config.get("_par_undulator_traj_und")
        
        # Continuous scans
        self._contscan = config.get("_par_contscan")
        
        # shutter
        self._shutter = config.get("_par_shutter", None)
        self._contscan.set_shutter(self._shutter)
        
        # Metadata
        self._metadata = config.get("par_metadata", None)

    """
    Continuous scan in time
    No Motor Movement
    """
    def time(self, nb_points, time_per_points, *counters, comment=None):
        self._contscan._scan("time", None, 0, 0, nb_points, time_per_points, "TIME", "TIME", *counters, comment=comment)
        
    """
    Continuous scan Energy cst speed
    """
    def energy_cst(self, Estart, Estop, nb_points, time_per_points, *counters, nbscan=1, backNforth=False, comment=None):
        
        if Estart == Estop:
            raise RuntimeError("MonochromatorScans.energy_cst: start == stop")
            
        # Load Trajectory
        undershoot = self._contscan.get_undershoot(
            self._energy_traj_und,
            Estart,
            Estop,
            nb_points,
            time_per_points
        )
        if Estart < Estop:
            traj_start = Estart - 2 * undershoot
            traj_stop = Estop + 2 * undershoot
        else:
            traj_start = Estop - 2 * undershoot
            traj_stop = Estart + 2 * undershoot
        
        self._mono.trajectory.load_energy(traj_start, traj_stop, 4000, use_lut=True)
        umv(self._energy_traj_und, Estart)
        self._mono.regul.on()
        self._mono.regul.on()
        gevent.sleep(0.1)
        # Scan
        self._motor(
            self._energy_traj_und,
            Estart,
            Estop,
            nb_points,
            time_per_points,
            self._contscan._start_trig_mode.get(),
            self._contscan._point_trig_mode.get(),
            *counters,
            nbscan=1,
            backNforth=False,
            comment=None
        )

        
    """
    Continuous scan Undulator cst speed
    """
    def undulator_cst(self, undulator, Estart, Estop, nb_points, time_per_points, *counters, nbscan=1, backNforth=False, comment=None):
        
        if Estart == Estop:
            raise RuntimeError("MonochromatorScans.energy_cst: start == stop")
            
        # Find undulator start/stop position
        Ustart = undulator.tracking.energy2tracker(Estart)
        Ustop = undulator.tracking.energy2tracker(Estop)
        
        # Load Trajectory
        undershoot = self._contscan.get_undershoot(
            self._undulator_traj_und,
            Ustart,
            Ustop,
            nb_points,
            time_per_points
        )
        if Ustart < Ustop:
            traj_ustart = Ustart - 0.1
            traj_ustop = Ustop + 0.1
        else:
            traj_ustart = Ustop - 0.1
            traj_ustop = Ustart + 0.1
        traj_estart = undulator.tracking.tracker2energy(traj_ustart)
        traj_estop = undulator.tracking.tracker2energy(traj_ustop)
        self._mono.trajectory.load_undulator(undulator, traj_estart, traj_estop, 4000, use_lut=True)
        
        umv(self._undulator_traj_und, Ustart-undershoot)
        self._mono.regul.on()
        self._mono.regul.on()
        print(self._mono.regul)
        # Scan
        self._motor(
            self._undulator_traj_und,
            Ustart,
            Ustop,
            nb_points,
            time_per_points,
            self._contscan._start_trig_mode.get(),
            self._contscan._point_trig_mode.get(),
            *counters,
            nbscan=1,
            backNforth=False,
            comment=None
        )
    
    """
    Continuous scan with motor
    """
    def _motor(self, motor, start, stop, nb_points, time_per_points, *counters, nbscan=1, backNforth=False, comment=None):
        
        # CHECKS
        if start == stop:
            raise RuntimeError("ZAP: Start and Stop positions are the same")

        # INIT VARIABLES
        if start > stop:
            way = "down"
            way_back = "up"         
        else:
            way = "up"
            way_back = "down"            
             
        for nscan in range(nbscan):
            
            # SCAN PARAMETERS
            if not backNforth or (backNforth and nscan%2==0):
                start_pos = start
                stop_pos = stop
                s_way = way
            else:
                start_pos = stop
                stop_pos = start
                s_way = way_back
                                
            print(f"\nSCAN #{nscan+1}(/{nbscan}) - {s_way.upper()} direction")
            self._contscan._scan(
                "motor",
                motor,
                start_pos,
                stop_pos,
                nb_points,
                time_per_points,
                *counters,
                comment=comment
            )
