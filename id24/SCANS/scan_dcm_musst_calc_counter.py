
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave

from id24.SCANS.scan_tools_musst import MusstBaseCalc

#######################################################################
#####
##### MUSST helper for calculated counters configuration
#####
class MusstMonochromatorEncCalcChannelBase:
    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.mono = config.get("mono", None)
        self.musst = config.get("musst", None)
        if self.musst is None:
            raise RuntimeError(f"MusstMonochromatorEncCalcChannel ({self.name}): No musst defined")
        self.musst_ch_name = config.get("musst_ch_name", None)
        if self.musst_ch_name is None:
            raise RuntimeError(f"MusstMonochromatorEncCalcChannel ({self.name}): No musst channel name defined")
        self.steps_per_unit = config.get("steps_per_unit", 1)
        self.offset = config.get("offset", 0)
                
    def get_acquisition_slave(self, musst_acq, scan_par):
        self.scan_par = scan_par
        hook = MusstBaseCalc(
            self.musst,
            self.musst_ch_name,
            self.name,
            self.calculate,
        )
        slave = CalcChannelAcquisitionSlave(
            hook.dest_name,
            (musst_acq,),
            hook,
            hook.acquisition_channels
        )
        return slave
        
    def calculate(self, data):
        raise NotImplemented("Implement calculate method in {0}".format(self.__class__))

class MusstDCMEnergyTraj(MusstMonochromatorEncCalcChannelBase):
    def calculate(self, data, abs_data):
        pos_step = abs_data + self.offset
        angle = pos_step / float(self.steps_per_unit)
        return angle

class MusstDCMEnergyBragg(MusstMonochromatorEncCalcChannelBase):
    def calculate(self, data, abs_data):
        pos_step = abs_data + self.offset
        angle = pos_step / float(self.steps_per_unit)
        ene_data = self.mono.bragg2energy(angle)
        return ene_data

class MusstDCMBragg(MusstMonochromatorEncCalcChannelBase):
    def calculate(self, data, abs_data):
        pos_step = abs_data + self.offset
        angle = pos_step / float(self.steps_per_unit)
        return angle
