
import numpy

from bliss.config import settings
from bliss.scanning.chain import AcquisitionChannel
from bliss.scanning.acquisition.calc import CalcHook
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.controllers.motors.icepap.trajectory import TrajectoryAxis

#######################################################################
#####
##### MUSST helper to get channel configured type
#####
def musst_get_channel_type(musst, ch_num):
    cmd = f"?CHCFG CH{ch_num}"
    return musst.putget(cmd)

#######################################################################
#####
##### MUSST helper to get AcquisitionMaster
#####
def musst_get_master(musst, scan_par, channel_list):
    """
    Input Params.
        - musst: Bliss object which synchronize the scan
        - scan_par = {}
            "start"        : start position of the scan
            "stop"         : stop position of the scan
            "points"       : Number of points in the scan
            "time"         : Integration time(s) pwer points
            "trig_start"   : type of synchronization ("TIME"/"POSITION")
                             for start point
            "trig_point"   : type of synchronization ("TIME"/"POSITION")
                             for all other points
        - channel_list     : MUSST channel number to be saved
    """
    
    if isinstance(scan_par["motor"], TrajectoryAxis):
        motor = scan_par["motor"].controller.axes["mtraj"]
    else:
        motor = scan_par["motor"]
    
    # MUSST: TO BE REMOVED (WORKAROUND)
    musst.ABORT
    musst.CLEAR
    
    # MUSST: memory configuration
    musst.set_histogram_buffer_size(2048, 1)  # Histogram (MCA)
    musst.set_event_buffer_size(int(524288 / 16), 1)  # Buffers
    
    # MUSST: channel(s) to be read
    store_mask = 0
    for chan in channel_list:
        store_mask |= 1 << int(chan)
    
    # zap time
    if motor is None:
        schan = 0
        sdata = int(0.001 * musst.get_timer_factor())
        pchan = 0
        pdata = int(scan_par["time"] * musst.get_timer_factor())
        pscandir = 0
        sscandir = 0
    # zap with movement
    else:
        steps_per_unit = motor.steps_per_unit
        channel = int(musst.counters[motor.name].channel[-1])
        
        # Intitialze trigged channel to current position
        motpos = motor.position * motor.steps_per_unit
        musst.putget(f"CH {musst.counters[motor.name].channel} {motpos}")
        
        # Start tigged on time
        if scan_par["trig_start"] == "TIME":
            schan = 0
            sdata = int(scan_par["time"] * musst.get_timer_factor())
            sscandir = 0
        # start trigged on position
        else:
            schan = channel
            sdata = int(scan_par["start"] * steps_per_unit)
            if scan_par["stop"] > scan_par["start"]:
                sscandir = 0
            else:
                sscandir = 1
        # point trigged on time
        if scan_par["trig_point"] == "TIME":
            pchan = 0
            pdata = int(scan_par["time"] * musst.get_timer_factor())
            pscandir = 0
        # point trigged on position
        else:
            pchan = channel
            delta = abs(scan_par["stop"] - scan_par["start"]) / scan_par["points"]
            pdata = int(delta * steps_per_unit)
            if scan_par["stop"] > scan_par["start"]:
                pscandir = 0
            else:
                pscandir = 1
                
    # MUSST master parameters
    musst_vars = {
        "SMODE": 1,  # 0=external trigger / 1=internal channel
        "SCHAN": schan,  # if SMODE=1 0=time 1=ch1 2=ch2 3=ch3 4=ch4 5=ch5 6=ch6
        "SDATA": sdata,
        "PMODE": 1,
        "PCHAN": pchan,
        "PDATA": pdata,
        "NPOINT": int(scan_par["points"]),
        "PSCANDIR": pscandir,  # 0=positive direction 1=negative direction
        "SSCANDIR": sscandir,  # 0=positive direction 1=negative direction
        "STOREMSK": store_mask,
    }
    
    # MUSST: acquisition master
    musst_master = MusstAcquisitionMaster(
        musst,
        program="zapintgen.mprg",
        program_start_name="HOOK",
        vars=musst_vars,
    )

    return musst_master
        
#######################################################################
#####
##### MUSST helper for calculated counters during continuous scans
#####
class MusstBaseCalc(CalcHook):
    def __init__(
        self,
        master,
        src_channel_name,
        dest_channel_name,
        calculate_func = None,
        data_per_point=1
    ):
        self.master = master
        self.name = master.name
        self.src_channel_name = src_channel_name
        self.dest_channel_name = dest_channel_name
        self.data_per_point = data_per_point
        if calculate_func is None:
            raise RuntimeError("No calculate function")
        self.calculate_func = calculate_func
        self.__overflow = 0
        self.__last_data = None

    @property
    def dest_name(self):
        return f"{self.name}:{self.dest_channel_name}"

    def compute(self, sender, data_dict):
        data = data_dict.get(self.src_channel_name)
        
        if data is None:
            return dict()

        calc_data = self.calculate(data)
        if calc_data is not None:
            return { self.dest_name: calc_data }
        else:
            return dict()

    def absolute(self, data):
        if self.__last_data is None:
            self.__last_data = data[0]
        raw_data = numpy.append(self.__last_data, data)
        abs_data = raw_data.astype(numpy.float)
        if len(raw_data) >= 2:
            over_idxs = numpy.where(numpy.diff(abs_data) != numpy.diff(raw_data))[0]
            abs_data += self.__overflow * 2**32
            for idx in over_idxs:
                ovr_sign = raw_data[idx] > 0 and 1. or -1.
                abs_data[idx+1:] += ovr_sign * 2**32
                self.__overflow += ovr_sign
        self.__last_data = raw_data[-1:]
        return abs_data[len(self.__last_data):]
       
    def calculate(self, data):
        abs_data = self.absolute(data)
        return self.calculate_func(data, abs_data)

    @property
    def acquisition_channels(self):
        return [AcquisitionChannel(self.dest_name, numpy.float, ())]
