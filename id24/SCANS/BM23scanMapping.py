
from bliss.scanning.scan import ScanPreset
from bliss.common.utils import ColorTags, BOLD, RED
from bliss.common.cleanup import cleanup
from bliss.shell.standard import umv

from fscan.fscantools import (
    FScanMode,
    FScanAxisMode,
    FScanTrigMode,
    FScanCamSignal,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
)

class BM23mappingScan:
   
    def __init__(self, name, config):
    
        self._name = name
        self._config = config

        # fscan
        self._fscan = self._config.get("_par_fscan", None)
        if self._fscan is None:
            raise RuntimeError("BM23mappingScan: No fscan defined")

        # OPIOM multiplexer
        self._multpx = self._config.get("_par_multiplexer", None)
        if self._multpx is None:
            raise RuntimeError("BM23mappingScan: No multiplexer defined")

        # shutter
        self._shutter = self._config.get("_par_shutter", None)
        
        # Scan Preset
        self._scan_preset = MappingScanPreset(self._multpx, self._shutter)
                
        # Install scan preset
        self._fscan.add_scan_preset(self._scan_preset, "mapping_scan_preset")
        
        # Fixed motors
        self._slow_axis = self._config.get("_par_slow_axis", None)
        self._fast_axis = self._config.get("_par_fast_axis", None)
    
    def dmap(
        self,
        slow_rel_start, slow_step_size, slow_nb_intervals,
        fast_rel_start, fast_step_size, fast_nb_intervals,
        acq_time, save=True
    ):
        
        self._scan_type = "dmap"
        self._slow_old_pos = self._slow_axis.position
        self._fast_old_pos = self._fast_axis.position
        
        with cleanup(self._scan_end):
            sstart = self._slow_old_pos + slow_rel_start
            fstart = self._fast_old_pos + fast_rel_start
            self._map(
                sstart, slow_step_size, slow_nb_intervals,
                fstart, fast_step_size, fast_nb_intervals,
                acq_time, save=save
            )
                      
    def amap(
        self,
        slow_start_pos, slow_step_size, slow_nb_intervals,
        fast_start_pos, fast_step_size, fast_nb_intervals,
        acq_time, save=True
    ):
        self._scan_type = "amap"
        self._map(
            slow_start_pos, slow_step_size, slow_nb_intervals,
            fast_start_pos, fast_step_size, fast_nb_intervals,
            acq_time, save=save
        )
    
    def _scan_end(self):
        if self._scan_type == "dmap":
            umv(
                self._slow_axis, self._slow_old_pos,
                self._fast_axis, self._fast_old_pos
            )           
                   
    def _map(self, slow_start_pos, slow_step_size, slow_nb_points,
                  fast_start_pos, fast_step_size, fast_nb_points,
                  acq_time, save=True):
                      
        # Set scan title
        title = f"{self._scan_type} "
        title += f"{slow_start_pos} {slow_step_size} {slow_nb_points}"
        title += f"{fast_start_pos} {fast_step_size} {fast_nb_points}"
        title += f"{acq_time} {acq_time+0.0001}"
        scan_info = {"title": title}
        
        # Set fscan Parameters
        self._fscan.fscan2d.pars.slow_motor      = self._slow_axis
        self._fscan.fscan2d.pars.slow_start_pos  = slow_start_pos
        self._fscan.fscan2d.pars.slow_step_size  = slow_step_size
        self._fscan.fscan2d.pars.slow_npoints    = slow_nb_points
        self._fscan.fscan2d.pars.fast_motor      = self._fast_axis
        self._fscan.fscan2d.pars.fast_start_pos  = fast_start_pos
        self._fscan.fscan2d.pars.fast_step_size  = fast_step_size
        self._fscan.fscan2d.pars.fast_npoints    = fast_nb_points
        self._fscan.fscan2d.pars.fast_step_time  = acq_time+0.0001
        self._fscan.fscan2d.pars.acq_time        = acq_time
        self._fscan.fscan2d.pars.fast_motor_mode = FScanAxisMode.REWIND
        self._fscan.fscan2d.pars.fast_acc_margin = 0.0
        self._fscan.fscan2d.pars.scan_mode       = FScanMode.POSITION
        self._fscan.fscan2d.pars.gate_mode       = FScanMode.TIME
        self._fscan.fscan2d.pars.camera_signal   = FScanCamSignal.LEVEL
        self._fscan.fscan2d.pars.save_flag       = save
        self._fscan.fscan2d.pars.display_flag    = True
        self._fscan.fscan2d.pars.sync_encoder    = True
        self._fscan.fscan2d.pars.home_rotation   = False
        self._fscan.fscan2d.pars.sampling_time   = 0.5
        self._fscan.fscan2d.pars.latency_time    = 0.0
        self._fscan.fscan2d.pars.scatter_plot   = True
        
        self._fscan.fscan2d.run(scan_info=scan_info)   
                
class MappingScanPreset(ScanPreset):
    
    def __init__(self, multiplexer, shutter):
        self.multiplexer = multiplexer
        self.shutter = shutter
            
    def prepare(self, scan):
        self.multiplexer.switch("SEL_TRIG", "FSCAN")
        if self.shutter is not None:
            self.shutter.open()    
        
    def start(self, scan):
        pass     
           
    def stop(self, scan):
        self.multiplexer.switch("SEL_TRIG", "STEP")
        if self.shutter is not None:
            self.shutter.close()    


