# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import functools
import gevent
import tabulate

from bliss.common.event import dispatcher
from bliss.shell.standard import prdef
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED, PURPLE

"""
BistateBase is the base class to configure objects with 2 differentes states and
the way to go from one state to the other.

The name of states as well as the name of the command to change from on to the other
may be configured in their YAML file usint the parameters:
    - state1_name
    - state1_cmd_name
    - state2_name
    - state2_cmd_name

A polling is foreseen:
    - the YAML parameter "polling" (True/False) allows you to use it or not.
    - the YAML parameter "polling_time" (in s) control the frequency to check the state
    - the YAML parameter "polling_timeout" (in s) allows to get out of the functions when problem occured

The command to go in state 1 and 2 which names are defined in the YAML file can be
call with the 2 keywords arguments polling=True/False to overwritte locally what
is define in the yml file and silent=True/False, default=False, to execute the commands
silently (when used in another macros for example).

To create your own Bistate object, Inherit from this class and call the "configure" method
to register your specific functions to make the state of your object changeable. 4 functions need to be specified:
    - set_state1: change the state object to state1
    - is_state1: True if the state of the object is state1, False otherwise
    - set_state2: change the state object to state2
    - is_state2: True if the state of the object is state2, False otherwise
A method is implemented in the BistateBase class to give the state of the object: It
returns "state1_name" if the object is in state1 and not in state2, "state2_name"
if the obejct is in state2 and not in state1, "MOVING" if the obejct is neither 
in state1 nor in state2 and "UNKNOWN" if the obejct is in state1 AND in state2.
    

"""

####################################################################
#####                                                          #####
#####                 BISTATE BASE                             #####
#####                                                          #####
####################################################################

class BistateBase(object):

    def __init__(self, name, config):
        
        self._name = name
        self._config = config
        
        self._polltime = self._config.get("polling_time", 0.1)
        self._polltimeout = self._config.get("polling_timeout", 3)
        self._poll = self._config.get("polling", False)
        
        self.__state1 = True
        self.__state2 = False
        self._old_state = None
        
        self._state_names = {}
        self._state_cmd_names = {}
        
        self._state_colors = {
            "state1": GREEN,
            "state2": RED,
            "UNKNOWN": BLUE,
            "MOVING": YELLOW
        }
        
        self._state_names["state1"] = self._config.get("state1_name", "OPEN")
        self._state_cmd_names["state1"] = self._config.get("state1_cmd_name", None)
        if self._state_cmd_names["state1"] is not None:
            setattr(self, self._state_cmd_names["state1"], functools.partial(self._set_state1))
        self._set_state1_cmd = None
        self._is_state1_cmd = None
       
        self._state_names["state2"] = self._config.get("state2_name", "CLOSED")
        self._state_cmd_names["state2"] = self._config.get("state2_cmd_name", None)
        if self._state_cmd_names["state2"] is not None:
            setattr(self, self._state_cmd_names["state2"], functools.partial(self._set_state2))
        self._set_state2_cmd = None
        self._is_state2_cmd = None

    def __info__(self):
        print(self._get_info())
        return ""
        
    def _get_info(self):
        state_str = self._get_color_state(self.state)(self.state)
        mystr = f"  {BOLD(self._name)}\t- {state_str}"
        return mystr 
        
    def _configure(self, set_state1=None, is_state1=None, set_state2=None, is_state2=None):
        self._set_state1_cmd = set_state1
        self._is_state1_cmd = is_state1
        self._set_state2_cmd = set_state2
        self._is_state2_cmd = is_state2

    """
    Generic state management
    """
    def toggle(self):
        if self._is_state("state1"):
            self._set_state2()
        elif self._is_state("state2"):
            self._set_state1()

    def _set_state1(self, polling=None, silent=False):
        if self._set_state1_cmd is not None:
            self._old_state = self.state
            self.__state1 = True
            self.__state2 = False
            self._set_state1_cmd()
            self._poll_state("state1", polling=polling, silent=silent)
        else:
            print(f"\"{self._name}\" is not able to change its state")

    def _set_state2(self, polling=None, silent=False):
        if self._set_state2_cmd is not None:
            self._old_state = self.state
            self.__state1 = False
            self.__state2 = True
            self._set_state2_cmd()
            self._poll_state("state2", polling=polling, silent=silent)
        else:
            print(f"\"{self._name}\" is not able to change its state")
    
    def _get_color_state(self, state):
        if self._state_names["state1"] == state:
            return self._state_colors["state1"]
        if self._state_names["state2"] == state:
            return self._state_colors["state2"]
        return self._state_colors[state]
         
    def _poll_state(self, state, polling=None, silent=False):
        if self._old_state is None:
            self._old_state = self.state
        
        do_poll = self._poll
        if polling is not None:
            do_poll = polling
        
        if not silent:
            old_state_str = self._get_color_state(self._old_state)(self._old_state)
            state_str = self._get_color_state(self.state)(self.state)
            print(f"    {self._name} was {old_state_str} is now {state_str}", end="\r")
            
        if do_poll:
            try:
                state_str = self._state_names[state]
                with gevent.Timeout(self._polltimeout, f"Timeout while setting {state_str}    "):
                    while True:
                        if not silent:
                            old_state_str = self._get_color_state(self._old_state)(self._old_state)
                            state_str = self._get_color_state(self.state)(self.state)
                            print(f"    {self._name} was {old_state_str} is now {state_str}    ", end="\r")
                        if self._is_state(state):
                            old_state_str = self._get_color_state(self._old_state)(self._old_state)
                            state_str = self._get_color_state(self.state)(self.state)
                            print(f"    {self._name} was {old_state_str} is now {state_str}    ", end="\r")
                            break
                        else:
                            gevent.sleep(self._polltime)
            finally:
                dispatcher.send("state", self, self.state)
        if not silent:
            print("\n")
        self._old_state = None

    def _is_state(self, state):
        if state == "state1":
            func = self._is_state1_cmd
            def_state = self.__state1
        else:
            func = self._is_state2_cmd
            def_state = self.__state2
            
        if func is not None:
            ret = func()
            if ret is not None:
                return ret
        return def_state

    @property
    def state(self):
        if self._is_state("state1"):
            if self._is_state("state2"):
                return "UNKNOWN"
            else:
                return self._state_names["state1"]
        else:
            if self._is_state("state2"):
                return self._state_names["state2"]
            else:
                return "MOVING"

    """
    Context manager methods
    """
    def __enter__(self):
        self._set_state1()

    def __exit__(self, *args):
        self._set_state2()

####################################################################
#####                                                          #####
#####                 BISTATE MULTI                            #####
#####                                                          #####
####################################################################

"""
- This class allow to move synchronously bistates.
- All bistates included in a MultiBistate object MUSS have 
  the same set_state1 and set_state2 command name
- The state is an AND of the individual states
"""
class MultiBistate:
    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self._state_colors = {
            "state1": GREEN,
            "state2": RED,
            "UNKNOWN": BLUE,
            "MOVING": YELLOW,
            "MISMATCH": PURPLE
        }
        
        self.__state1 = True
        self.__state2 = False
        self._old_state = None
        
        self._polltime = 0.1
        self._polltimeout = 3
        self._poll = False
        
        self._state_names = {"state1": None, "state2": None}
        self._state_cmd_names = {"state1": None, "state2": None}
                
        self._bistates = {}
        bistates = self._config.get("bistates")
        for bistate in bistates:
            device = bistate.get("bistate")
            # Check set state1 command
            if self._state_cmd_names["state1"] is None:
                self._state_cmd_names["state1"] = device._state_cmd_names["state1"]
            else:
                if self._state_cmd_names["state1"] != device._state_cmd_names["state1"]:
                    raise RuntimeError("All bistates do not have the same set_state1 command")
            # Check set state2 command
            if self._state_cmd_names["state2"] is None:
                self._state_cmd_names["state2"] = device._state_cmd_names["state2"]
            else:
                if self._state_cmd_names["state2"] != device._state_cmd_names["state2"]:
                    raise RuntimeError("All bistates do not have the same set_state2 command")
            # Check state1 name
            if self._state_names["state1"] is None:
                self._state_names["state1"] = device._state_names["state1"]
            else:
                if self._state_names["state1"] != device._state_names["state1"]:
                    raise RuntimeError("All bistates do not have the same state1 name")
            # Check state2 name
            if self._state_names["state2"] is None:
                self._state_names["state2"] = device._state_names["state2"]
            else:
                if self._state_names["state2"] != device._state_names["state2"]:
                    raise RuntimeError("All bistates do not have the same state2 name")
            if self._polltime < device._polltime:
                self._polltime = device._polltime
            if self._polltimeout < device._polltimeout:
                self._polltimeout = device._polltimeout
            if device._poll:
                self._poll = True
            self._bistates[device._name] = device
            
        if self._state_cmd_names["state1"] is not None:
            setattr(self, self._state_cmd_names["state1"], functools.partial(self._set_state1))
        if self._state_cmd_names["state2"] is not None:
            setattr(self, self._state_cmd_names["state2"], functools.partial(self._set_state2))

    def __info__(self):
        print(self._get_info())
        for name,bistate in self._bistates.items():
            bistate.__info__()
        return ""
        
    def _get_info(self):
        state_str = self._get_color_state(self.state)(self.state)
        mystr = f"  {BOLD(self._name)}\t- {state_str}"
        return mystr 

    def _get_condensed_info(self):
        state = self.state
        return (self._name, self._get_color_state(state)(state))
                    
    """
    Generic state management
    """
    def toggle(self):
        if self._is_state("state1"):
            self._set_state2()
        elif self._is_state("state2"):
            self._set_state1()

    def _set_state1(self, polling=None, silent=False):
        if self._state_cmd_names["state1"] is not None:
            self._old_state = self.state
            self.__state1 = True
            self.__state2 = False
            for name, bistate in self._bistates.items():
                bistate._set_state1(polling=False, silent=True)
            self._poll_state("state1", polling=polling, silent=silent)
        else:
            print(f"\"{self._name}\" is not able to change its state")

    def _set_state2(self, polling=None, silent=False):
        if self._state_cmd_names["state2"] is not None:
            self._old_state = self.state
            self.__state1 = False
            self.__state2 = True
            for name, bistate in self._bistates.items():
                bistate._set_state2(polling=False, silent=True)
            self._poll_state("state2", polling=polling, silent=silent)
        else:
            print(f"\"{self._name}\" is not able to change its state")
    
    def _get_color_state(self, state):
        if self._state_names["state1"] == state:
            return self._state_colors["state1"]
        if self._state_names["state2"] == state:
            return self._state_colors["state2"]
        return self._state_colors[state]
         
    def _poll_state(self, state, polling=None, silent=False):
        if self._old_state is None:
            self._old_state = self.state
        
        do_poll = self._poll
        if polling is not None:
            do_poll = polling
        
        if not silent:
            old_state_str = self._get_color_state(self._old_state)(self._old_state)
            state_str = self._get_color_state(self.state)(self.state)
            print(f"    {self._name} was {old_state_str} is now {state_str}", end="\r")
            
        if do_poll:
            try:
                state_str = self._state_names[state]
                with gevent.Timeout(self._polltimeout, f"Timeout while setting {state_str}    "):
                    while True:
                        if not silent:
                            old_state_str = self._get_color_state(self._old_state)(self._old_state)
                            state_str = self._get_color_state(self.state)(self.state)
                            print(f"    {self._name} was {old_state_str} is now {state_str}    ", end="\r")
                        if self._is_state(state):
                            old_state_str = self._get_color_state(self._old_state)(self._old_state)
                            state_str = self._get_color_state(self.state)(self.state)
                            print(f"    {self._name} was {old_state_str} is now {state_str}    ", end="\r")
                            break
                        else:
                            gevent.sleep(self._polltime)
            finally:
                dispatcher.send("state", self, self.state)
        if not silent:
            print("\n")
        self._old_state = None

    def _is_state(self, state):
        if state == "state1":
            if self.state == self._state_names["state1"]:
                return True
            else:
                return False
        else:
            if self.state == self._state_names["state2"]:
                return True
            else:
                return False

    @property
    def state(self):
        state = None
        for name,bistate in self._bistates.items():
            if state is None:
                state = bistate.state
            else:
                if state != "UNKNOWN":
                    if bistate.state == "UNKNOWN":
                        state = UNKNOWN
                    else:
                        if state != "MOVING":
                            if bistate.state == "MOVING":
                                state = "MOVING"
                            else:
                                if bistate.state != state:
                                    state = "MISMATCH"
                                    
        return state
    
        
####################################################################
#####                                                          #####
#####                 BISTATE WAGO                             #####
#####                                                          #####
####################################################################
class WagoBistate(BistateBase):
    def __init__(self, name, config):

        BistateBase.__init__(self, name, config)
        
        # Wago controller
        self._wago = config.get("_wago")

        # state1 Command
        self._state1_cmd_key = config.get("state1_cmd_key")
        self._state1_cmd_val = 1.0
        if "state1_cmd_val" in config.keys():
            self._state1_cmd_val = float(config.get("state1_cmd_val"))

        # state1 State
        self._state1_state_key = self._state1_cmd_key
        if "state1_state_key" in config.keys():
            self._state1_state_key = config.get("state1_state_key")
        self._state1_state_val = self._state1_cmd_val
        if "state1_state_val" in config.keys():
            self._state1_state_val = config.get("state1_state_val")

        # state2 command
        self._state2_cmd_key = self._state1_cmd_key
        if "state2_cmd_key" in config.keys():
            self._state2_cmd_key = config.get("state2_cmd_key")
        self._state2_cmd_val = 0.0 if self._state1_cmd_val == 1.0 else 1.0
        if "state2_cmd_val" in config.keys():
            self._state2_cmd_val = config.get("state2_cmd_val")

        # state2 State
        self._state2_state_key = self._state2_cmd_key
        if "state2_state_key" in config.keys():
            self._state2_state_key = config.get("state2_state_key")
        self._state2_state_val = self._state2_cmd_val
        if "state2_state_val" in config.keys():
            self._state2_state_val = config.get("state2_state_val")
            
        self._configure(
            self._wago_set_state1,
            self._wago_is_state1,
            self._wago_set_state2,
            self._wago_is_state2
        )

    def _get_condensed_info(self):
        state = self.state
        return (self._name, self._get_color_state(state)(state))
        
    def report(self):
        print("\n")
        state_str = self._get_color_state(self.state)(self.state)
        lines = [[f"  {BOLD(self._name)} - {state_str}", BOLD("state1"), BOLD("state2")]]
        lines.append([BOLD("name"), self._state_names["state1"], self._state_names["state2"]])
        lines.append([BOLD("cmd name"), self._state_cmd_names["state1"], self._state_cmd_names["state2"]])
        lines.append([BOLD("cmd key"), self._state1_cmd_key, self._state2_cmd_key])
        lines.append([BOLD("cmd val"), self._state1_cmd_val, self._state2_cmd_val])
        lines.append([BOLD("state key"), self._state1_state_key, self._state2_state_key])
        lines.append([BOLD("state val"), self._state1_state_val, self._state2_state_val])
        lines.append([BOLD("state current val"), self._wago.get(self._state1_state_key), self._wago.get(self._state2_state_key)])
        print(tabulate.tabulate(lines, tablefmt="plain", stralign="right"))
        print("\n")
        
    def _wago_set_state1(self):
        self._wago.set(self._state1_cmd_key, self._state1_cmd_val)

    def _wago_set_state2(self):
        self._wago.set(self._state2_cmd_key, self._state2_cmd_val)

    def _wago_is_state1(self):
        is_state1 = self._wago.get(self._state1_state_key)
        if is_state1 == self._state1_state_val:
            return True
        else:
            return False

    def _wago_is_state2(self):
        is_state2 = self._wago.get(self._state2_state_key)
        if is_state2 == self._state2_state_val:
            return True
        else:
            return False


        
####################################################################
#####                                                          #####
#####                 BISTATE MULTI CMD WAGO                   #####
#####                                                          #####
####################################################################
class WagoMultCmdBistate(WagoBistate):
    def __init__(self, name, config):

        self._temporisation = config.get("tempo", 0)
        
        WagoBistate.__init__(self, name, config)
        
    def _wago_set_state1(self):
        self._wago.set(self._state1_cmd_key, self._state1_cmd_val)
        gevent.sleep(self._temporisation)
        self._wago.set(self._state1_cmd_key, abs(1-self._state1_cmd_val))

    def _wago_set_state2(self):
        self._wago.set(self._state2_cmd_key, self._state2_cmd_val)
        gevent.sleep(self._temporisation)
        self._wago.set(self._state2_cmd_key, abs(1-self._state2_cmd_val))
