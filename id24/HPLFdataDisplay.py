
import os
import tabulate
from ruamel.yaml import YAML
import gevent
import numpy

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.common.cleanup import cleanup

from bliss.common import plot

class HPLFdataDisplayStop:
    def __init__(self, name, config):
        self._name = name
        self._config = config
        self._stop_acq = settings.SimpleSetting("HPLFdataDisplay_stop_acq", default_value=False)
        self._state = settings.SimpleSetting("HPLFdataDisplay_state", default_value="Idle")
        
    def stop(self):
        self._stop_acq.set(True)
    
    def state(self):
        return self._state.get()
        
    def stop_acq_state(self):
        return self._stop_acq.get()
        
class HPLFdataDisplay:
    
    def __init__(self, name, config):
        
        self._name = name
        self._config = config

        self._city = config.get("city", None)
        if self._city is None:
            raise RuntimeError("No city Controller")
        
        self._enmeter_eh1 = config.get("enmeter_eh1", None)

        self._p100 = config.get("p100", None)
        if self._p100 is None:
            raise RuntimeError("No P100 controller")
            
        self._state = settings.SimpleSetting("HPLFdataDisplay_state", default_value="Idle")
        self._stop_acq = settings.SimpleSetting("HPLFdataDisplay_stop_acq", default_value=False)
        
        self._symbols = ["|", "/", "-", "\\"]
        self._next_symbols = 0

        f = plot.get_flint()
        container = f.get_plot("grid", name="HPLF Data", unique_name="HPLF Data", selected=True)
        self.plot2d = {
            "container": container,
            "ccd_ff_amp": container.get_plot("image", row=0, col=0),
            "ccd_ff_fe": container.get_plot("image", row=0, col=1),
            "ccd_nf_amp": container.get_plot("image", row=0, col=2),
            "ccd_nf_fe": container.get_plot("image", row=0, col=3),
            "ccd_nf_regen": container.get_plot("image", row=0, col=4),
            "bas_nf": container.get_plot("image", row=0, col=5),
            "bas_ff": container.get_plot("image", row=0, col=6),
            "tektro64": container.get_plot("curve", row=1, col=0, col_span=2),
            "tektro54": container.get_plot("curve", row=1, col=2, col_span=2),
            "energy": container.get_plot("curve", row=1, col=4, col_span=2),
        }
        self._oscillo_channels = ["CH1", "CH2", "CH3", "CH4"]
        for key in self._p100.camera._items.keys():
            self.plot2d[key].side_histogram_displayed = False
            self.plot2d[key].title = key
        self.plot2d["bas_nf"].side_histogram_displayed = False
        self.plot2d["bas_nf"].title = "bas_nf"
        self.plot2d["bas_ff"].side_histogram_displayed = False
        self.plot2d["bas_ff"].title = "bas_ff"
            
    def start(self, sleep):
        self._state.set("Running")
        self.acquire(sleep)
        
    def stop(self):
        self._stop_acq.set(True)
        
    def state(self):
        return self._state.get()

    def acquire_stop(self):
        print("\n")
        self._stop_acq.set(False)
        self._state.set("Idle")
        
    def acquire(self, sleep):
        with cleanup(self.acquire_stop):
            self._npoint = 0
            self._enmeter_eh1_data = None
            while not self._stop_acq.get():

                self.plot_p100_camera()
                self.plot_oscillo()
                self.plot_energy()
                                
                # return_lines = 1

                # (mystr, nblines) = self.get_symbol()
                # print(f"    {mystr}\n")
                # return_lines = return_lines + 1 + nblines
                
                # (mystr, nblines) = self.get_energy()
                # print(f"    {mystr}\n")
                gevent.sleep(sleep)
                #return_lines = return_lines + 1 + nblines
                
                #ret_lines = "\x1B[1A"*(return_lines)
                #print(ret_lines)
                
            self._stop_acq.set(False)
                
    def plot_oscillo(self):
        for key, val in self._city._oscillos.items():
            data = self._city.get_oscillo_profile(key, self._oscillo_channels, poll=False,silent=True)
            for channel in self._oscillo_channels:
                self.plot2d[key].add_curve(data["time"], data[channel]["data"], legend=channel)        
        
    def plot_p100_camera(self):
        for key, val in self._p100.camera._items.items():
            self.plot2d[key].set_data(val.Image)        

    def plot_energy(self):
        self._npoint = self._npoint + 1
        data_point = numpy.linspace(1, self._npoint, self._npoint)
        
        # enmeter_eh1
        energy = self._enmeter_eh1.energy
        if self._enmeter_eh1_data is None:
            self._enmeter_eh1_data = numpy.zeros((1))
            self._enmeter_eh1_data[0] = energy
        else:
            self._enmeter_eh1_data = numpy.append(self._enmeter_eh1_data, energy)
        self.plot2d["energy"].add_curve(data_point, self._enmeter_eh1_data, legend="enmeter_eh1")      
          
    def get_energy(self):
        lines = [[BOLD("Name"), BOLD("Energy"), BOLD("Energy Raw"), BOLD("Convertion Factor")]]
        if self._enmeter_eh1 is not None:
            energy = self._enmeter_eh1.energy
            energy_raw = self._enmeter_eh1.energy_raw
            conv_factor = self._enmeter_eh1._conv_factor
            lines.append(["enmeter_eh1", GREEN(energy), GREEN(energy_raw), GREEN(conv_factor)])
            
        mystr = tabulate.tabulate(lines, tablefmt="plain")
        return (mystr, len(lines))
        
    def get_symbol(self):
        mystr = BLUE(self._symbols[self._next_symbols%4])
        self._next_symbols = (self._next_symbols+1)%4
        return (mystr, 1)
        
