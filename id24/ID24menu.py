import click
import tabulate

from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.shell.standard import clear

def menu_list(subject, elements, values=None, title=None):
    qu4 = BOLD("    q") 
    qu = BOLD("q") 
    value = ""    
    if values is not None:
        value = "Value"
    
    nbelem = len(elements)
    while True:
        clear()
        lines = []
        print(f"\n  {GREEN(subject)}:\n")
        if title is not None:
            line = [""]
            for col_title in title:
                line.append(BOLD(col_title))
            lines.append(line)
        for nelem in range(nbelem):
            line = []
            line.append(BOLD(f"    {nelem+1}"))
            line.append(f"{elements[nelem]}")
            if values is not None:
                line.append(values[nelem])
            else:
                line.append("")
            lines.append(line)
        lines.append(["", "", ""]) 
        lines.append([f"{qu4}", "Quit", ""]) 
        mystr = tabulate.tabulate(lines, tablefmt="plain")
        print(mystr)
        rep = click.prompt(f"\n  Your choice ([1..{nbelem}] or {qu}=quit)", default="q")
        print("")
        if rep == "q":
            return (rep, -1)
        elif int(rep) >= 1 and int(rep) <= nbelem:
            return (elements[int(rep)-1], int(rep)-1)
        
def menu_number(subject, minmax=None, default=None, integer=False):
    mystr = f"    {BLUE(subject)}"
    if minmax is not None:
        mystr += f" [{minmax[0]}-{minmax[1]}]"
    while True:
        rep = click.prompt(mystr, default=default)
        print("")
        if minmax is not None:
            if float(rep) >= minmax[0] and float(rep) <= minmax[1]:
                if integer:
                    return int(rep)
                else:
                    return float(rep)
        else:
            if integer:
                return int(rep)
            else:
                return float(rep)
           
        
def menu_string(subject, choice=None, default=None):
    if choice is None :
        return
    mystr = f"    {BLUE(subject)}"
    for i in range(len(choice)):
        ch = choice[i]
        if i == 0:
            mystr += f" [{ch}"
        else:
            mystr += f"/{ch}"
    mystr += "]"
    while True:
        rep = click.prompt(mystr, default=default)
        print("")
        if rep in choice:
            return(rep)

def menu_choice(subject, elements, selected):
    qu4 = BOLD("    q") 
    qu = BOLD("q") 
    
    nbelem = len(elements)
    while True:
        clear()
        lines = []
        print(f"\n  {GREEN(subject)}:\n")
        for nelem in range(nbelem):
            line = []
            line.append(BOLD(f"    {nelem+1}"))
            line.append(f"{elements[nelem]}")
            lines.append(line)
        mystr = tabulate.tabulate(lines, tablefmt="plain")
        print(mystr)
        rep = click.prompt(f"\n  Your choice ([1..{nbelem}]", default=selected+1)
        print("")
        if int(rep) >= 1 and int(rep) <= nbelem:
            return (elements[int(rep)-1], int(rep)-1)

