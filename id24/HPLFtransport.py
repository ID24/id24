
import time
import gevent
import sys
import tabulate

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED

from id24.ID24utils import ID24attrList

class HPLFtransport:

    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self._devices = {}
        devices = self._config.get("devices")
        for device in devices:
            dev = device.get("device")
            self._devices[dev._name] = dev
        self.devices = HPLFcrossShutterAttrList(self._devices)
        
        
        #
        # ENERGY METERS
        #
        self._enmeter = self._config.get("enmeters", None)
        if self._enmeter is None:
            raise RuntimeError("HPLFsequence: No Energy Meter object defined")
        
    def __info__(self):
        self.devices.__info__()
        return ""
        
    def _get_metadata(self):
        return self._enmeter._get_metadata()

class HPLFcrossShutterAttrList(ID24attrList):
    
    def __info__(self):
        lines = []
        for name, dev in self._items.items():
            (name, state) = dev._get_condensed_info()
            lines.append(["  ", name, state])
        mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        print(mystr)
        return ""
