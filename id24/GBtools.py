
import cv2
import numpy
import os

from silx import io as silx_io
from silx.math import colormap
from PIL import Image

from bliss.common.plot import plot

class MovieMaker:

    def __init__(self, name, config):
        self._name = name
        self._config = config

        self.data = None
        self.input_file = None
        self.output_path = None
        
    def read_file(self, input_file):
        self.input_file = input_file
        h5data = silx_io.open(self.input_file)
        self.data = h5data["scan_0"]["measurement"]["image_0"]["data"]
        self.nb_images = len(self.data)
        print(f"\n    Get {len(self.nb_images)} images from {self.input_file}\n")
        
    def display(self, image_num):
        print(f"\n    Display image #{image_num} (/ {len(self.data)} images from {self.input_file}\n")
        plot(self.data[image_num])
    
    def generate_png(self, vmin, vmax, output_dir):
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)
    def edf2mp4(self, vmin, vmax, freq):
        # Get images
        self.read()
        if not os.path.isdir(self.output_path):
            os.makedirs(self.output_path)
        for i in range(len(self.data)):
            print(f"Image - {i} ({len(self.data)})", end="\r")
            rgb = colormap.apply_colormap(
                self.data[i], 
                colormap="viridis",
                norm="linear",
                autoscale="none",
                vmin=vmin,
                vmax=vmax
            )
            image = Image.fromarray(rgb)
            image.save(f"{self.output_path}/image_{i:04d}.png")
        print("\n\n")

        os.system(f"cd {self.output_path} ; ffmpeg -r {freq} -i image_%04d.png -vcodec mpeg4 -y movie.mp4")
        
        print(f"\n\n  Movie saved in {self.output_path}/movie.mp4\n")
        
       
