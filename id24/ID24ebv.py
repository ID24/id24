
from bliss.controllers.ebv import EBV
from bliss.shell.standard import umv

class ProtectedEBV(EBV):

    def __init__(self, name, config_node):
    
        super().__init__(name, config_node)
        
        self._protection_type = config_node.get("protection_type", None)
        
        if self._protection_type == "motor":
            self._protection_motor = config_node.get("protection_motor",None)
            self._protection_inpos = config_node.get("protection_inpos", None)
            self._protection_outpos = config_node.get("protection_outpos", None)
            
            if self._protection_motor is None:
                raise RuntimeError("Protection motor not set")
            
            if self._protection_inpos is None or self._protection_outpos is None:
                raise RuntimeError("Out or In secured positions not set")
                
    #def __info__(self):
    #    pass
        
                
    def screen_in(self):
        if self._protection_type == "motor":
            umv(self._protection_motor, self._protection_inpos)
        super().screen_in()
        
    def screen_out(self):
        super().screen_out()
        if self._protection_type == "motor":
            umv(self._protection_motor, self._protection_outpos)
        
