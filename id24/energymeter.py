# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2021 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss import global_map
from bliss.comm.util import get_comm, TCP
from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.controllers.counter import SamplingCounterController

""" Config example, YML file:
# Energymeter
- plugin: bliss
  class: EnergyMeter
  name: enmeter
  tcp:
    url: enmeterid241
  counters:
    - counter_name: laser_energy
      role: income_energy
"""


class EnergyMeter:
    def __init__(self, name, config_tree):

        self.comm = get_comm(config_tree, ctype=TCP, port=5000)

        # self.comm.write("start \n".encode())

        self.name = name
        self._conv_factor = config_tree.get("convertion_factor", 1.0)

        self.values = {"income_energy": 0.0}
        
        self._mode2str = ["W", "J"]
        
        self._scale2str = [
            "1p", "3p", "10p", "30p", "100p", "300p",
            "1n", "3n", "10n", "30n", "100n", "300n",
            "1u", "3u", "10u", "30u", "100u", "300u",
            "1m", "3m", "10m", "30m", "100m", "300m",
            "1", "3", "10", "30", "100", "300",
            "1k", "3k", "10k", "30k", "100k", "300k",
            "1M", "3M", "10M", "30M", "100M", "300M",
        ]
        
        # Counters
        self.counters_controller = EnergyMeterController(self)
        counter_node = config_tree.get("counters")
        for config_dict in counter_node:
            if self.counters_controller is not None:
                counter_name = config_dict.get("counter_name")
                EnergyMeterCounter(counter_name, config_dict, self.counters_controller)

    def raw_write(self, message):
        ans = self.comm.write_readline(message.encode(), eol="\r").decode()
        return ans

    def __info__(self):
        info_str = f"EnergyMeter \nName    : {self.name}\nComm.   : {self.comm}\n"
        return info_str
        
    @property
    def counters(self):
        return self.counters_controller.counters

    """ EnergyMeter Command """

    @property
    def energy_raw(self):
        return float(self.comm.write_readline("*CVU\r".encode(), eol="\r").decode())

    @property
    def energy(self):
        return float(self.comm.write_readline("*CVU\r".encode(), eol="\r").decode()) * self._conv_factor

    @property
    def mode(self):
        rep = int(self.raw_write("*GMD").split(":")[1])
        return self._mode2str[rep]

    @property
    def scale(self):
        rep = int(self.raw_write("*GCR").split(":")[1])
        return f"{self._scale2str[rep]}{self.mode}"
        
    def new_value(self):
        mystr = self.comm.write_readline("*NVU\r".encode(), eol="\r").decode()
        return mystr

    def enmeter_read_counters(self):
        current_value = self.energy
        self.values["income_energy"] = current_value


class EnergyMeterController(SamplingCounterController):
    def __init__(self, enmeter):
        self.enmeter = enmeter

        super().__init__(self.enmeter.name, register_counters=False)

        global_map.register(enmeter, parents_list=["counters"])

    def read_all(self, *counters):
        self.enmeter.enmeter_read_counters()

        values = []

        for cnt in counters:
            values.append(self.enmeter.values[cnt.role])

        return values


class EnergyMeterCounter(SamplingCounter):
    def __init__(self, name, config, controller):

        self.role = config["role"]

        if self.role not in controller.enmeter.values.keys():
            raise RuntimeError(
                f"enmeter: counter {self.name} role {self.role} does not exists"
            )

        SamplingCounter.__init__(self, name, controller, mode=SamplingMode.LAST)

