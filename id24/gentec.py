# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2021 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.
import gevent

from bliss import global_map
from bliss.comm.util import get_comm, TCP
from bliss.common.counter import SamplingCounter, SamplingMode
from bliss.controllers.counter import SamplingCounterController

""" Config example, YML file:
# Gentec
- plugin: bliss
  class: EnergyMeter
  name: enmeter
  tcp:
    url: enmeterid241
  counters:
    - counter_name: laser_energy
      role: income_energy
"""

class GentecSlink:
    def __init__(self, name, config_tree):

        self.comm = get_comm(config_tree, ctype=TCP, port=5000)
        self.name = name
        
        self._chan_conf = {
            1: {
                "conv_factor": 1,
                "scale": 22,
            },
            2: {
                "conv_factor": 1,
                "scale": 22,
            }
        }
        channels_conf = config_tree.get("channels", None)
        if channels_conf is not None:
            for channel_conf in channels_conf:
                channel = channel_conf["channel"]
                self._chan_conf[channel]["conv_factor"] = channel_conf["conversion_factor"]
                self._chan_conf[channel]["scale"] = channel_conf["scale"]

    def __info__(self):
        info_str =  f"Gentec S-Link \n"
        info_str += f"Name   : {self.name}\n"
        info_str += f"Comm.  : {self.comm}\n\n"
        return info_str

    ###############################################################
    #
    # Communication
    #
    def raw_write(self, message):
        #ans = self.comm.write_readline(message.encode()).decode()
        ans = self.comm.write_readline(message)
        return ans
    
    def energy_setup(self, channel, scale):
        self.comm.write_readline("*RST".encode(), timeout=1)
        gevent.sleep(0.1)
        self.comm.write(f"*CS{channel}".encode())
        gevent.sleep(0.1)
        self.comm.write(f"*SS{channel}001".encode())
        gevent.sleep(0.1)
        self.comm.write(f"*SC{channel}{scale:02d}".encode())
        gevent.sleep(0.1)
        self.comm.write("*VNM".encode())
        gevent.sleep(0.1)
        self.comm.write(f"*CA{channel}".encode())
        
    def energy_raw(self, channel):
        rep = self.comm.write_readline(f"*CV{channel}".encode(), timeout=1)
        value = float(str(rep).split(":")[-1][0:-3])
        return value
        
    def energy(self, channel):
        rep = self.comm.write_readline(f"*CV{channel}".encode(), timeout=1)
        value = float(str(rep).split(":")[-1][0:-3]) * self._chan_conf[channel]["conv_factor"]
        return value
        
    def command(self, command):
        self.comm.write(command.encode())
        gevent.sleep(0.1)
        print(self.comm.readline(timeout=5).decode())

class EnergyMeter:
    def __init__(self, name, config_tree):

        self.comm = get_comm(config_tree, ctype=TCP, port=5000)

        # self.comm.write("start \n".encode())

        self.name = name

        self.values = {"income_energy": 0.0}

        # Counters
        self.counters_controller = EnergyMeterController(self)
        counter_node = config_tree.get("counters")
        for config_dict in counter_node:
            if self.counters_controller is not None:
                counter_name = config_dict.get("counter_name")
                EnergyMeterCounter(counter_name, config_dict, self.counters_controller)

    def raw_write(self, message):
        ans = self.comm.write_readline(message.encode()).decode()
        return ans

    def __info__(self):
        info_str = f"EnergyMeter \nName    : {self.name}\nComm.   : {self.comm}\n\n"
        return info_str

    @property
    def counters(self):
        return self.counters_controller.counters

    """ EnergyMeter Command """

    @property
    def energy(self):
        return float(self.comm.write_readline("*CVU\r".encode(), eol="\r").decode())

    def enmeter_read_counters(self):
        current_value = self.energy
        self.values["income_energy"] = current_value


class EnergyMeterController(SamplingCounterController):
    def __init__(self, enmeter):
        self.enmeter = enmeter

        super().__init__(self.enmeter.name, register_counters=False)

        global_map.register(enmeter, parents_list=["counters"])

    def read_all(self, *counters):
        self.enmeter.enmeter_read_counters()

        values = []

        for cnt in counters:
            values.append(self.enmeter.values[cnt.role])

        return values


class EnergyMeterCounter(SamplingCounter):
    def __init__(self, name, config, controller):

        self.role = config["role"]

        if self.role not in controller.enmeter.values.keys():
            raise RuntimeError(
                f"enmeter: counter {self.name} role {self.role} does not exists"
            )

        SamplingCounter.__init__(self, name, controller, mode=SamplingMode.LAST)

