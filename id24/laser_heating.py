import os
import glob
import h5py
import gevent
import numpy
from tango import DeviceProxy
from bliss.common import protocols
from bliss.common.utils import autocomplete_property
from bliss.comm.util import get_comm
from bliss.config import settings
from bliss.config.beacon_object import BeaconObject
from bliss.controllers.counter import CounterController
from bliss.common.counter import Counter
from bliss.scanning.chain import AcquisitionSlave

class LaserHeating(BeaconObject):
    wavelength = BeaconObject.property_setting('wavelength',default=(None,None))
    fit_wavelength = BeaconObject.property_setting('fit_wavelength',default=(400,900))
    current_calibration = BeaconObject.property_setting('current_calibration')
    exposure_time = BeaconObject.property_setting('exposure_time',default=1.)
    calibration_path = BeaconObject.config_getter('calibration_path')
    epsilon = BeaconObject.property_setting('epsilon',default=1.)
    background_mode = BeaconObject.property_setting('background_mode',default=False)
    
    def __init__(self,name,config):
        super().__init__(config,name,share_hardware=False)
        self._counters_container = _LaserHeatingCounters(self)
        self.spectro = _LaserSpectro(self,config)
        self._background_invalid = True
        
    @property
    def calibration_list(self):
        path = os.path.expanduser(self.calibration_path)
        return [os.path.basename(x) for x in glob.glob(f'{path}/*')]

    @current_calibration.setter
    def current_calibration(self,cal_name):
        if cal_name is not None and cal_name not in self.calibration_list:
            raise RuntimeError("Calibration not found")
        
    def has_capability(self, capability_type):
        """
        Simple check that the device has this capability.
        So return True if he can provide it.
        """
        if capability_type == protocols.CAPABILITY_TYPE.COUNTER_CONTAINER:
            return True
        return False

    def get_capability(self, capability_type):
        """
        Return a known bliss protocol interface
        """
        if capability_type == protocols.CAPABILITY_TYPE.COUNTER_CONTAINER:
            return self._counters_container

    @property
    def counters(self):
        return self._counters_container.counters

    @background_mode.setter
    def background_mode(self,value):
        self._background_invalid = True
        return bool(value)

    @exposure_time.setter
    def exposure_time(self,value):
        self._background_invalid = True
        return value
class _LaserSpectro:
    def __init__(self,laser_heating,config):
        cfg = config.get('spectro',dict())
        self._cnx = get_comm(cfg,eol=b'\r\n')

    @autocomplete_property
    def wavelength(self):
        return float(self._cnx.write_readline(b"?NM\r\n").split()[1])

    @wavelength.setter
    def wavelength(self,value):
        return self._cnx.write_readline(b"%.3f GOTO\r\n" % value)

class _oneDCounter(Counter):
    def __init__(self,*args,**kargs):
        super().__init__(*args,**kargs)
        self._shape = 1340
    @property
    def shape(self):
        return (self._shape,)
        
class _LaserHeatingCounters(CounterController):
    def __init__(self,laser_heating):
        super().__init__(laser_heating.name,register_counters=True)
        
        for cnt_name in ['T_wien','T_wien_error','wien_emissivity','wien_emissivity_error',
                         'T_planck','T_planck_error','planck_emissivity','planck_emissivity_error',
                         'planck_emissivity','planck_emissivity_error']:
            Counter(cnt_name,self)
        for cnt_name in ['raw_data','spectrum_lambdas',
                         'wien_data','wien_fit',
                         'planck_data','planck_fit']:
            _oneDCounter(cnt_name,self)
        self._laser_heating = laser_heating
        
    def get_acquisition_object(self,acq_params,ctrl_params,parent_acq_params):
        return _LaserAcquisitonSlave(self._laser_heating)

    def get_default_chain_parameters(self,*args):
        return {}


class _LaserAcquisitonSlave(AcquisitionSlave):
    def __init__(self,laser_heating,npoints=1):
        self._laser_heating = laser_heating
        super().__init__(laser_heating._counters_container,npoints=npoints,
                         trigger_type=AcquisitionSlave.SOFTWARE,
                         prepare_once=False,start_once=False)

        proxy = self._laser_heating.camera.proxy
        tango_addr = proxy.getplugindevicenamefromtype('blackbodyfitting')
        self._blackbody_fitting = DeviceProxy(tango_addr)
        self._init_blackbody = False
        #resize gradient
        lambdas = numpy.linspace(laser_heating.wavelength[0],
                                 laser_heating.wavelength[1],1340,
                                 endpoint=True)

        lambda_mask = numpy.ma.masked_inside(lambdas,laser_heating.fit_wavelength[0],
                                                laser_heating.fit_wavelength[1])

        self._lambdas = lambdas


    def prepare(self):
        blackbody_fitting = self._blackbody_fitting
        camera = self._laser_heating.camera
        camera.acquisition.expo_time = self._laser_heating.exposure_time
        camera.acquisition.nb_frames = 1

        if not self._init_blackbody:
            if self._laser_heating.background_mode:
                if self._laser_heating._background_invalid:
                    blackbody_fitting.Stop()
                    try:
                        camera.proxy.set_timeout_millis(int(1000 * (self._laser_heating.exposure_time + 2)))
                        blackbody_fitting.takeBackground()
                    finally:
                        camera.proxy.set_timeout_millis(int(1000 * 3.))
                    self._laser_heating._background_invalid=False
            else:
                blackbody_fitting.clearBackground()
                     
                
            blackbody_fitting.Start()
            blackbody_fitting.lambda_ranges = self._laser_heating.wavelength
            blackbody_fitting.fit_lambda_ranges = self._laser_heating.fit_wavelength
            blackbody_fitting.epsilon = self._laser_heating.epsilon


            calibration_path = os.path.expanduser(self._laser_heating.calibration_path)
            current_calibration = self._laser_heating.current_calibration
            if current_calibration is None:
                raise RuntimeError("Please select a valid calibration")
            
            calib_file_path = os.path.join(calibration_path,current_calibration,'temp_calibration.h5')

            with h5py.File(calib_file_path,'r') as f:
                dark = f['dark']
                data = f['data']
                temperature = float(f['temperature'].value)
                epsilon = float(f['epsilon'].value)
                exposure_time = float(f['exposure_time'].value)
                
                blackbody_fitting.reference_temperature = temperature
                blackbody_fitting.reference_epsilon = epsilon
                image_roi = camera.image.roi
                y_start = image_roi[1]
                y_end = image_roi[1]+image_roi[3]

                x_start = image_roi[0]
                x_end = x_start+image_roi[2]
                
                dark_croped = dark[x_start:x_end][y_start:y_end].astype(float).sum(axis=0)
                data_croped = data[x_start:x_end][y_start:y_end].astype(float).sum(axis=0)

                ff = data_croped - dark_croped
                calc_ff = blackbody_fitting.setFlatField(ff/exposure_time)
                self._calc_ff = calc_ff
                

            self._init_blackbody = True
            
        camera.prepareAcq()
        
    def start(self):
        pass

    def stop(self):
        pass
    
    def trigger(self):
        camera = self._laser_heating.camera
        camera.startAcq()
        while camera.acquisition.status == 'Running':
            gevent.sleep(0.1)

        temperature_data = self._blackbody_fitting.readCounters(0)
        with gevent.Timeout(1.,"Error can't get datas from blackbody fitting"):
            while not len(temperature_data):
                gevent.sleep(0.1)
                temperature_data = self._blackbody_fitting.readCounters(0)
        nb_val = 11
        nb_measure,frame_id,wien,wien_error,wien_emissivity,wien_emissivity_error,\
        planck,planck_error,planck_emissivity,planck_emissivity_error,\
        raw_data_len = temperature_data[:nb_val]
        
        raw_data_len = int(raw_data_len)
        #wien data
        first_index = nb_val
        next_index = first_index + raw_data_len
        wien_data = temperature_data[first_index:next_index]

        #wien fit
        first_index = next_index
        next_index = first_index + raw_data_len
        wien_fit = temperature_data[first_index:next_index]

        #planck data
        first_index = next_index
        next_index = first_index + raw_data_len
        planck_data = temperature_data[first_index:next_index]

        #planck fit
        first_index = next_index
        next_index = first_index + raw_data_len
        planck_fit = temperature_data[first_index:next_index]

        #raw data
        first_index = next_index
        next_index = first_index + raw_data_len
        raw_data = temperature_data[first_index:next_index]

        """
        from matplotlib import pyplot as plt
        plt.figure()
        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        ax1.plot(self._calc_ff)
        ax2.plot(spectrum)
        plt.show()
        """
        self.channels.update({
            #Wien
            'T_wien': wien,
            'T_wien_error' : wien_error,
            'wien_emissivity' : wien_emissivity,
            'wien_emissivity_error' : wien_emissivity_error,
            'wien_data' : wien_data,
            'wien_fit' : wien_fit,
            #PLanck
            'T_planck': planck,
            'T_planck_error' : planck_error,
            'planck_emissivity' : planck_emissivity,
            'planck_emissivity_error' : planck_emissivity_error,
            'planck_data' : planck_data,
            'planck_fit' : planck_fit,
            'raw_data': raw_data,
            'spectrum_lambdas' : self._lambdas,
        })       
