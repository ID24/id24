import numpy as np

from bliss.controllers.motor import CalcController
from bliss.config import settings

class AngleCalcMotor(CalcController):

    
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        
        self._angle = self.config.config_dict.get("angle", 0.0)
        
        self._x_origin = settings.SimpleSetting(f"blade_{self.name}_x_origin", default_value=0.0)
        self._y_origin = settings.SimpleSetting(f"blade_{self.name}_y_origin", default_value=0.0)
        self._updated = False
    
    def update_origin(self):
        self._x_origin.set(self._tagged["x"][0].position)
        self._y_origin.set(self._tagged["y"][0].position)
        self._updated = True
        self.pseudos[0].sync_hard()

    def calc_from_real(self, reals_dict):
        pseudos_dict = {}
        if self._updated:
            pseudos_dict["blade"] = 0.0
            self._updated = False
        else:
            val_cos = (reals_dict["x"]-self._x_origin.get())/np.cos(np.radians(self._angle))
            val_sin = (reals_dict["y"]-self._y_origin.get())/np.sin(np.radians(self._angle))
            print(f"COS {val_cos} SIN {val_sin}")
            if np.isclose(val_cos, val_sin, 0.001):
                pseudos_dict["blade"] = val_cos
            else:
                pseudos_dict["blade"] = np.nan
        return pseudos_dict

    def calc_to_real(self, pseudos_dict):
        reals_dict = {}
        if not np.isnan(pseudos_dict["blade"]):
            reals_dict["x"] = self._x_origin.get() + pseudos_dict["blade"] * np.cos(np.radians(self._angle))
            reals_dict["y"] = self._y_origin.get() + pseudos_dict["blade"] * np.sin(np.radians(self._angle))
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position
        return reals_dict

class VisarCalcMotor(CalcController):
    
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

    def calc_from_real(self, reals_dict):
        return {"trans": 100.0 * reals_dict["rot"] / 45.0}

    def calc_to_real(self, pseudos_dict):
        return {"rot": 0.45 * pseudos_dict["trans"] / 100.0}

        
