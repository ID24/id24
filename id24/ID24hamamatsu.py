
import numpy
import struct

from bliss.config.settings import ParametersWardrobe
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE

from id24.ID24detector import ID24detectorBase

#####################################################################
#####
##### Hamamatsu Detector
#####
#####################################################################
class ID24hamamatsuDetector(ID24detectorBase):
    
    def __init__(self, name, config):
        super().__init__(name, config)
        
        self._default_param["unit"] = "s"
        self._param = ParametersWardrobe(f"detector_parameters_{self._name}", default_values=self._default_param)
        
        self._configure()
        self._configure_lima_roi()
        
        # Hamamatsu specific
        self._param.time_unit = "s"
        self._fmt = '<IHHIIHHHHHHHHIIIIIIII'
        self._header_len = struct.calcsize(self._fmt)

    ##################################################################
    #
    # Acquisition Methods
    #
    
    # Parameters
    def _get_acq_parameters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq):
        acq_par = {
            "acq_mode": "SINGLE",
            "prepare_once": False,
            "start_once": False,
#            "latency_time": self.lima.camera.readout_time,
#            "acq_mode": "CONCATENATION",
#            "saving_format": "EDF"
        }
        
        # acq_trigger_mode
        if synchro_mode == "software":
            acq_par["acq_trigger_mode"] = "INTERNAL_TRIGGER"
        else:
            acq_par["acq_trigger_mode"] = "EXTERNAL_TRIGGER"
            
        if acq_mode == "I":
            self._param.I_frame = nbframe
            self._param.I_acq = nbacq
            self._param.I_time = inttime
            acq_par["acq_nb_frames"] = self._param.I_frame * self._param.I_acq
            acq_par["acq_expo_time"] = self._param.I_time
#            acq_par["concat_nb_frames"] = self._param.I_acq
        elif acq_mode == "I_bck":
            self._param.I_time = inttime
            acq_par["acq_nb_frames"] = 1 * self._param.I_bck_acq
            acq_par["acq_expo_time"] = self._param.I_time
#            acq_par["concat_nb_frames"] = self._param.I_bck_acq
        elif acq_mode == "I0":
            acq_par["acq_nb_frames"] = 1 * self._param.I0_acq
            acq_par["acq_expo_time"] = self._param.I0_time
#            acq_par["concat_nb_frames"] = self._param.I0_acq
        elif acq_mode == "I0_bck":
            acq_par["acq_nb_frames"] = 1 * self._param.I0_bck_acq
            acq_par["acq_expo_time"] = self._param.I0_time
#            acq_par["concat_nb_frames"] = self._param.I0_bck_acq
        else:
            raise RuntimeError(f"Hamamatsu Acq: acquisition mode {acq_mode} not implemented (I/I0/I_bck/I0_bck)")
            
        return acq_par

    # Data
    def _get_acq_data(self, scan, nbframe, nbacq):
        #if nbacq > 1:
        #    data_aver = self._read_data_concat(nbframe, nbacq)
        #else:
        #    data_aver = self._read_data_bliss(scan, nbframe, nbacq)
        data_aver = self._read_data_bliss(scan, nbframe, nbacq)
        return data_aver
        
    def _decode_data(self, acq_start, acq_end):
        data_encoded = self.lima.proxy.readImageSeq([acq_start, acq_end])
        header_data = struct.unpack(self._fmt, data_encoded[1][:self._header_len])
        real_header_len = header_data[2]
        data_raw = numpy.fromstring(data_encoded[1][real_header_len:], dtype='uint16')
        width, height, nbacq = header_data[7:10]
        data_raw.shape = (nbacq, height, width)
        data = numpy.average(data_raw, axis=0)
        return data
        
    def _read_data_concat(self, nbframe, nbacq):
        data = numpy.zeros((nbframe, self._param.nb_pixel))
        for frame in range(nbframe):
            acq_start = frame * nbacq
            acq_end = acq_start + nbacq -1
            data[frame] = self._decode_data(acq_start, acq_end)
        return data
        
    def _read_data_bliss(self, scan, nbframe, nbacq):
        node = scan.get_data()[f"{self.lima.name}:image"]
        data = numpy.zeros((nbframe, self._param.nb_pixel))
        for nframe in range(nbframe):
            for nacq in range(nbacq):
                data[nframe] += (node.get_image(nframe*nbacq+nacq)[0]/nbacq)
        return data
            
    def _get_acq_roi(self, scan, nbframe, nbacq):
        scan_data = scan.get_data()
        data_roi = {
            "roi_I1": numpy.zeros((nbframe,)),
            "roi_I2": numpy.zeros((nbframe,)),
            "roi_I3": numpy.zeros((nbframe,)),
            "roi_I4": numpy.zeros((nbframe,)),
            "roi_Isum": numpy.zeros((nbframe,)),
        }
        for nframe in range(nbframe):
            for nacq in range(nbacq):
                data_roi["roi_I1"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:I1_sum"][nframe*nbacq+nacq]/nbacq)
                data_roi["roi_I2"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:I2_sum"][nframe*nbacq+nacq]/nbacq)
                data_roi["roi_I3"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:I3_sum"][nframe*nbacq+nacq]/nbacq)
                data_roi["roi_I4"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:I4_sum"][nframe*nbacq+nacq]/nbacq)
                data_roi["roi_Isum"][nframe] += (scan_data[f"{self.lima.name}:roi_counters:Isum_sum"][nframe*nbacq+nacq]/nbacq)
        return data_roi

    ##################################################################
    #
    # Lima helper
    #
    def _get_detector_width(self):
        return self.lima.proxy.image_max_dim[0]

    def _get_detector_height(self):
        return self.lima.proxy.image_max_dim[1]
        
    def _configure(self):
        self.lima.image.roi = [self._param.first_pixel, 0, self._param.nb_pixel, 1]

"""
BERRU [27]: x = frelon_l.proxy.readImageSeq([0, 9])
BERRU [28]: x[0]
  Out [28]: 'DATA_ARRAY'

BERRU [29]: fmt = '<IHHIIHHHHHHHHIIIIIIII'
BERRU [30]: import struct
BERRU [31]: import numpy as np
BERRU [32]: header_len = struct.calcsize(fmt)
BERRU [33]: header_len
  Out [33]: 64

BERRU [34]: header_data = struct.unpack(fmt, x[1][:header_len])
BERRU [35]: header_data
  Out [35]: (1146372441, 2, 64, 4, 1, 0, 3, 901, 1, 9, 0, 0, 0, 2, 1802, 1802, 0, 0, 0, 0, 0)

BERRU [36]: real_header_len = header_data[2]
BERRU [37]: real_header_len
  Out [37]: 64

BERRU [38]: np.fromstring(x[1][real_header_len:], dtype='int16')
  Out [38]: array([112, 111, 112, ..., 110, 106, 113], dtype=int16)

BERRU [39]: d = np.fromstring(x[1][real_header_len:], dtype='int16')
BERRU [40]: d.shape
  Out [40]: (8109,)

BERRU [41]: x = frelon_l.proxy.readImageSeq([0, 10])
BERRU [42]: header_data = struct.unpack(fmt, x[1][:header_len])
BERRU [43]: real_header_len = header_data[2]
BERRU [44]: d = np.fromstring(x[1][real_header_len:], dtype='int16')
BERRU [45]: d.shape
  Out [45]: (9010,)

BERRU [46]: frelon_l.proxy.valid_ranges
  Out [46]: array([1.0000000e-06, 6.5535000e+01, 3.1000000e-05, 6.5535031e+01])

BERRU [47]: frelon_l._get_proxy('Frelon').execSerLineCmd
!!! === AttributeError: execSerLineCmd === !!! ( for more details type cmd 'last_error' )
BERRU [48]: frelon_l._get_proxy('Frelon').execSerialCmd
!!! === AttributeError: execSerialCmd === !!! ( for more details type cmd 'last_error' )
BERRU [49]: frelon_l._get_proxy('Frelon').execSerialCommand
  Out [49]: functools.partial(<function logging_call at 0x7ff67ed80a70>, name='execSerialCommand', tango_func=<function __get_command_func.<locals>.f at 0x7ff66a801440>, logger=<bound method Logger.debug of <BlissLogger id24/limafrelon/frelon_l (WARNING)>>)

BERRU [50]: frelon_l._get_proxy('Frelon').execSerialCommand('Z?')
  Out [50]: '!OK:0\r\n'

BERRU [51]: width, height, nb_frames = header_data[7:10]
BERRU [52]: d.shape = (nb_frames, height, width)
BERRU [53]: d.shape
  Out [53]: (10, 1, 901)

BERRU [54]:


"""
