
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy as np
import tabulate
import gevent
import time
from scipy import signal
import glob
import numpy as np

from bliss import setup_globals
from bliss.config import settings
from bliss.shell.standard import umv
from bliss.common.utils import ColorTags, BOLD 
from bliss.common.plot import plot

from id24.DCM.DCM_V0.monochromator import MonochromatorFixExitBase

from id24.DCM.DCM_V0.esrf_dcm_restart import EsrfDCMRestart
from id24.DCM.DCM_V0.esrf_dcm_lut import EsrfDCMcalib

class EsrfDCM(MonochromatorFixExitBase):
    
    def __init__(self, name, config):
      
        self.change_motor = config.get("change_motor", None)
        if self.change_motor is None:
            raise RuntimeError("EsrfDCM: No Change_motor defined to cahnge xtals")
            
        MonochromatorFixExitBase.__init__(self, name, config)
        
        self._error_lf = False
        
        # FastJack Steppers offset
        self.fjs_cont = self.motors["bragg_fe"].controller._tagged["xtal"][0].controller
        def_val = {"fjs_offset_rx": 0.0, "fjs_offset_ry": 0.0, "fjs_offset_use": False}
        self.dcm_setting_name = f"{self.name}_mono_fe"
        self.dcm_setting = settings.HashObjSetting(self.dcm_setting_name, default_values=def_val)
         
        # Get Speedgoat object
        try:
            self.goat = self.config.get("speedgoat")
        except Exception as dcm_undu_track_exc:
            print(BOLD("DCM")+": Speedgoat not available")
            self.goat= None
            self.speedgoat = None
        
        # Xtal specific parameters and motor
        self._xtal_dxtal0 = self.xtals.get_xtals_config("dxtal0")        
        self._xtal_ty_pos = self.xtals.get_xtals_config("ty_position")
        try:
            self.motors["dxtal"] = self.config.get("dxtal_motor", None)
            self.motors["dxtal"].controller.set_mono(self)
        except Exception as dcm_undu_track_exc:
            print(BOLD("DCM")+": No \"dxtal\" motor Define")
            self.motors["dxtal"]= None
            self.dxtal = None

        # get real brag motor for regulation reset
        self.motors["real_bragg"] = self.config.get("real_bragg_motor", None)
       
        # get thtraj motor
        self.motors["thtraj"] = self.config.get("thtraj_motor", None)
       
        # get thtraj+undulator motor
        try:
            self.motors["thtraj_und"] = self.config.get("thtraj_undulator_motor", None)
        except Exception as dcm_undu_track_exc:
            print(BOLD("DCM")+": thtraj_und not available")
            self.motors["thtraj_und"]= None
            self.thtraj_undulator_motor = None
        if self.motors["thtraj_und"] is not None:
            self.motors["thtraj_und"].controller.set_mono(self)
       
        # get enetraj motor
        self.motors["enetraj"] = self.config.get("enetraj_motor", None)
       
        # get enetraj+undulator motor
        try:
            self.motors["enetraj_und"] = self.config.get("enetraj_undulator_motor", None)
        except Exception as dcm_undu_track_exc:
            print(BOLD("DCM")+": enetraj_und not available")
            self.motors["enetraj_und"]= None
            self.enetraj_undulator_motor = None
        if self.motors["enetraj_und"] is not None:
            self.motors["enetraj_und"].controller.set_mono(self)
        
        # get scan object
        try:
            self.zapdcm = self.config.get("zap", None)
            if self.zapdcm is not None:
                self.zapdcm.set_mono(self)
        except Exception as dcm_zap_exc:
            print(BOLD("DCM")+": zapdcm not available")
            self.zapdcm = None
            self.zap = None
        
        # get trajectory object
        self.traj = self.config.get("trajectory", None)
        if self.traj is not None:
            self.traj.set_mono(self)
        
        # Get Lookup Table(lut)
        #self.lut = self.config.get("lut", None)
        #if self.lut is not None and self.goat is not None:
        #    self.lut.set_mono(self)
        self.calib = EsrfDCMcalib(self)
            

        # Initialyze Speedgoat regulation object
        self.regul = EsrfDCMRegul(self)
         
        # Initialyse Init procedures object
        self.restart = EsrfDCMRestart(self.goat, self)

    def initialize(self):
        
        self.low_pos = {}
        self.high_pos = {}
        self.low_pos = self.xtals.get_xtals_config("low_pos")
        self.high_pos = self.xtals.get_xtals_config("high_pos")
        
    #################################################################
    ### 
    ### INFO
    ###
    def __info__(self):
        info_str = self.info_mono()
        info_str += self.info_xtals()
        info_str += self.info_fix_exit()
        #info_str += self.info_fastjacks()
        info_str += self.info_motor()
        return info_str

    def info_fix_exit(self):
        info_str = f"fix_exit_offset  : {self.fix_exit_offset}\n"
        info_str += f"dxtals_at_xtal_0 : {self.dxtals_at_xtal0}\n\n"
        return info_str
    
    def info_fastjacks(self):
        cont = self.motors["bragg_fe"].controller._tagged["xtal"][0].controller
        #
        # TITLE
        #
        title = [""]
        for axis in cont.pseudos:
            title.append("{0}".format(BOLD(axis.name)))
        for axis in cont.reals:
            title.append("{0}".format(BOLD(axis.name)))

        #
        # CURRENT POSITION ROW
        #
        current = ["Current"]
        for axis in cont.pseudos:
            current.append("{axis.position:.3f}")
        for axis in cont.reals:
            current.append("{axis.position:.3f}")
            
        mystr = tabulate.tabulate([current,], headers=title, tablefmt="plain")
        mystr += "\n"
        
    def info_bragg(self):
        pass
        
    def info_undulator(self):
        pass
        
    def info_motor(self):
        bragg = self.motors["bragg"].position
        energy = self.bragg2energy(bragg)

        #
        # TITLE
        #
        title = [""]
        title.append(self.motors["bragg"].name)
        title.append(self.motors["xtal"].name)
        title.append(self.motors["dxtal"].name)
        title.append(self.motors["bragg_fe"].name)
        title.append(self.motors["ene"].name)
        if self.motors["ene_und"] is not None:
            cont = self.motors["ene_und"].controller
            for axis in cont.reals:
                tag = cont._axis_tag(axis)
                if tag != "energy":
                    title.append(axis.name)
            title.append(self.motors["ene_und"].name)

        #
        # CALCULATED POSITION ROW
        #
        calculated = ["Calculated"]
        # bragg
        val = self.motors["bragg"].position
        valu = self.motors["bragg"].unit
        calculated.append(f"{val:.3f} {valu}")
        # xtal
        val = self.motors["xtal"].position
        valu = self.motors["xtal"].unit
        calculated.append(f"{val:.3f} {valu}")
        # dxtal
        val = self.motors["dxtal"].position
        valu = self.motors["dxtal"].unit
        calculated.append(f"{val:.3f} {valu}")
        # bragg_fe
        val = self.motors["bragg"].position
        valu = self.motors["bragg_fe"].unit
        calculated.append(f"{val:.3f} {valu}")
        # energy
        valu = self.motors["ene"].unit
        calculated.append(f"{energy:.3f} {valu}")
        # Undulator
        if self.motors["ene_und"] is not None:
            cont = self.motors["ene_und"].controller
            for axis in cont.reals:
                tag = cont._axis_tag(axis)
                if tag != "energy":
                    undu = cont.energy2undulator(axis, energy)
                    calculated.append(f"{undu:.3f} {axis.unit}")
            valu = self.motors["ene_und"].unit
            calculated.append(f"{energy:.3f} {valu}")

        #
        # CURRENT POSITION ROW
        #
        current = ["Current"]
        # bragg
        val = self.motors["bragg"].position
        valu = self.motors["bragg"].unit
        current.append(f"{val:.3f} {valu}")
        # xtal
        val = self.motors["xtal"].position
        valu = self.motors["xtal"].unit
        current.append(f"{val:.3f} {valu}")
        # dxtal
        val = self.motors["dxtal"].position
        valu = self.motors["dxtal"].unit
        current.append(f"{val:.3f} {valu}")
        # bragg_fe
        val = self.motors["bragg_fe"].position
        valu = self.motors["bragg_fe"].unit
        current.append(f"{val:.3f} {valu}")
        # energy
        valu = self.motors["ene"].unit
        current.append(f"{energy:.3f} {valu}")
        # Undulator
        if self.motors["ene_und"] is not None:
            cont = self.motors["ene_und"].controller
            for axis in cont.reals:
                tag = cont._axis_tag(axis)
                if tag != "energy":
                    current.append(f"{axis.position:.3f} {axis.unit}")
            val = self.motors["ene_und"].position
            valu = self.motors["ene_und"].unit
            current.append(f"{val:.3f} {valu}")
        
        space = ["", "", "", "", "", "", "", "", ""]
        
        mystr = tabulate.tabulate([calculated, current], headers=title, tablefmt="plain")
        
        mystr += "\n"

        #
        # TITLE
        #
        title = [""]
        title.append(self.motors["thtraj"].name)
        if self.motors["thtraj_und"] is not None:
            title.append(self.motors["thtraj_und"].name)

        #
        # CALCULATED POSITION ROW
        #
        calculated = ["Calculated"]
        # thtraj
        val = self.motors["thtraj"].position
        valu = self.motors["thtraj"].unit
        calculated.append(f"{val:.3f} {valu}")
        # thtraj_und
        if self.motors["thtraj_und"] is not None:
            val = self.motors["thtraj_und"].position
            valu = self.motors["thtraj_und"].unit
            calculated.append(f"{val:.3f} {valu}")
 
        #
        # CURRENT POSITION ROW
        #
        current = ["Current"]
        # thtraj
        val = self.motors["thtraj"].position
        valu = self.motors["thtraj"].unit
        current.append(f"{val:.3f} {valu}")
        # thtraj_und
        if self.motors["thtraj_und"] is not None:
            val = self.motors["thtraj_und"].position
            valu = self.motors["thtraj_und"].unit
            current.append(f"{val:.3f} {valu}")
        
        space = ["", "", "", "", "", "", "", ""]
        
        mystr += "\n"
        mystr += tabulate.tabulate([calculated, current], headers=title, tablefmt="plain")
       
        
        return mystr
        
    #################################################################
    ### 
    ### Speedgoat fix exit offset management
    ###
    @property
    def goat_fix_exit(self):
        return self.goat.get_param("setPoint/beam_offset(mm)/Value")
        
    @goat_fix_exit.setter
    def goat_fix_exit(self, value):
        self.goat.set_param("setPoint/beam_offset(mm)/Value", value)

    #################################################################
    ### 
    ### ESRF DCM manage distance between crystals with a motor (xtal) moving 
    ### second crystals perpendicular to the crystal plane.
    ### dxtals_at_xtal0 is the distance between first and second crystal when
    ### the motor (xtal) is at zero position
    ### Positive motion is intended to get both crystals closer.
    ### It will be taken into account if present in the yml file
    @property
    def dxtals_at_xtal0(self):
        if self._xtal_dxtal0 is None:
            return None
        try:
            dxtal0 = self._xtal_dxtal0[self.xtals.xtal_sel]
            return dxtal0
        except:
            return None        
    
    """
    This method needs to be overwrite to reflect the monochromator behaviour
    ID21 DCM:
        - move ty to selected position given by self._xtal_ty_pos[selected_xtal]
        - synchronize dxtal motor
    """
    def _xtal_change(self, xtal):
        pass

    #################################################################
    ### 
    ### OFFSET
    ### 
    def fjs_offset(self, rx=None, ry=None):
        if rx is not None:
            self.dcm_setting["fjs_offset_rx"] = rx
        if ry is not None:
            self.dcm_setting["fjs_offset_ry"] = ry
        return (self.dcm_setting["fjs_offset_rx"], self.dcm_setting["fjs_offset_ry"])
        
    def fjs_offset_use(self, state=None):
        if state is not None:
            self.dcm_setting["fjs_offset_use"] = state
        return self.dcm_setting["fjs_offset_use"]
    

    #################################################################
    ### 
    ### Add Calculation method
    ### return the position of xtal motor according to distance 
    ### between crystal
    ###
    def dxtal2xtal(self, dxtal):
        if self.dxtals_at_xtal0 is not None:
            xtal = self.dxtals_at_xtal0 - dxtal
            return xtal
        raise RuntimeError(f"No dxtals_at_xtal0 defined")            

    def xtal2dxtal(self, xtal):
        if self.dxtals_at_xtal0 is not None:
            dxtal = self.dxtals_at_xtal0 - xtal
            return dxtal
        raise RuntimeError(f"No dxtals_at_xtal0 defined")            

    def xyz2fjs(self, rx, ry, tz):

        tab3pos = {}
        tab3pos["rx"] = rx
        tab3pos["ry"] = ry
        tab3pos["tz"] = tz

        leg = self.fjs_cont.calc_to_real(tab3pos)

        fjsur = leg["lega"]
        fjsuh = leg["legb"]
        fjsd  = leg["legc"]

        return (fjsur, fjsuh, fjsd)

    def fjs2xyz(self, fjsur, fjsuh, fjsd):

        leg = {}
        leg["lega"] = fjsur
        leg["legb"] = fjsuh
        leg["legc"]  = fjsd

        tab3pos = self.fjs_cont.calc_from_real(leg)

        rx = tab3pos["rx"]
        ry = tab3pos["ry"]
        tz = tab3pos["tz"]

        return (rx, ry, tz)

    #################################################################
    ### 
    ### Change xtals
    ###
    def _xtal_is_in(self, xtal):
        low_pos = self.low_pos[xtal]
        high_pos = self.high_pos[xtal]
        current_pos = self.change_motor.position
        if current_pos > self.low_pos[xtal] and current_pos < self.high_pos[xtal]:
            return True
        return False
        
    # Usefull methods
    def get_goat_xtal_name(self):
        if self.xtal_sel == "Si311":
            return "311"
        else:
            return "111"
            
    #def _xtal_is_in(self, xtal):
    #    if xtal == "Si111" or "Si333":
    #        return True
    #    return False
            
    def traj_master(self, mot, start_pos, end_pos, nbp, int_time):
        
        from id21.DCMunduMaster import DCMTrajMaster
        
        if mot.name == self.motors["thtraj"].name:
            self.trajectory.load_bragg(start_pos-2, end_pos+2, 1000)
        elif mot.name == self.motors["enetraj"].name:
            self.trajectory.load_energy(start_pos-0.03, end_pos+0.03, 1000)
        else:
            raise RuntimeError(f"Motor must be thtraj or enetraj")

        self.trajmaster = DCMTrajMaster(self, mot, start_pos, end_pos, nbp, int_time)
    
    def traj_set_undu(self, mot):
        param = self.trajmaster.movables_params.get(mot.name, None)
        if param is None:
            print("wrong undulator")
        else:
            print(f"{mot.name} - pos: {mot.position}")
            print(f"OLD - vel: {mot.velocity} - acc: {mot.acceleration}")
            vel = param["vel"]
            mot.velocity = vel
            acc = param["acc"]
            mot.acceleration = acc
            mytime = time.time()
            #while mot.acceleration != pytest.approx(acc, abs=0.01) and \
            #      mot.velocity != pytest.approx(vel, abs=0.01):
            while np.isclose(mot.acceleration, acc, atol=0.01) and \
                  np.isclose(mot.velocity, vel, atol=0.01):
                gevent.sleep(0.01)
                myt = time.time()-mytime
                print(f"waiting... {myt}\r", end = "")
                
            print(f"\nNEW - vel: {mot.velocity} ({vel})  - acc: {mot.acceleration} ({acc})")
    """
    TO BE REMOVED
    """
    def traj_unset_undu(self, mot):
        param = self.trajmaster.movables_params.get(mot.name, None)
        if param is None:
            print("wrong undulator")
        else:
            print(f"{mot.name} - pos: {mot.position}")
            print(f"NEW - vel: {mot.velocity} - acc: {mot.acceleration}")
            vel = param["vel_old"]
            mot.velocity = vel
            acc = param["acc_old"]
            mot.acceleration = acc 
            mytime = time.time()
            #while mot.acceleration != pytest.approx(acc, abs=0.01) and \
            #      mot.velocity != pytest.approx(vel, abs=0.01):
            while np.isclose(mot.acceleration, acc, atol=0.01) and \
                  np.isclose(mot.velocity, vel, atol=0.01):
                gevent.sleep(0.01)
                myt = time.time()-mytime
                print(f"waiting... {myt}\r", end = "")
            print(f"\nOLD - vel: {mot.velocity} ({vel})  - acc: {mot.acceleration} ({acc})")
            
    def traj_move_undu(self, axis):
        param = self.trajmaster.movables_params.get(axis.name, None)
        if param is None:
            print("wrong undulator")
        else:
            
            if param["start"] < param["end"]:
                undu_start = 200 - (param["end"] - param["start"])
                undu_end = 200
            else:
                undu_start = 200
                undu_end = 200 - (param["start"] - param["end"])
            undu_time = 2*param["acct"]+self.trajmaster.tot_time
            
            print(f"{axis.name} move to START position ({undu_start})")
            umv(axis, undu_start)
            gevent.sleep(1)
            print(f"{axis.name} at {axis.position}\n\n")
            
            self.traj_set_undu(axis)
            print("\n\n")
           
            
            print(f"{axis.name} move to END position ({undu_end})")
            myt = time.time()
            umv(axis, undu_end)
            mvt = time.time() - myt
            print(f"Time scan [{undu_time}] - Undu time [{mvt}]")
            gevent.sleep(1)
            print(f"{axis.name} at {axis.position}\n\n")
        
            self.traj_unset_undu(axis)
        
"""

  ESRF DCM regulation object
  
"""
class EsrfDCMRegul(object):
    
    def __init__(self, mono):
        self.mono = mono
        self.goat = mono.goat
        
        regul_data_dir = self.mono.config.get("regul_data_dir", None)
        if regul_data_dir is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No data directory given for FJS calibration")
        self.data_dir = regul_data_dir
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_regulmodel_{self.mono.name}", default_value="regul_default")

    @property
    def radix(self):
        return self._radix_setting.get()
        
    @radix.setter
    def radix(self, value):
        self._radix_setting.set(value)

    def load(self, radix=None):        
        if radix is not None:
            self.radix = radix
        filename = f"{self.data_dir}/{self.radix}.dat"
        
        print(f"Load Regul Parameters from :")
        print(f"    - {filename}")
        
        # Make sure regul is off (Mode A)
        self.off()

        # Load controller
        K = np.loadtxt(filename, dtype=np.float, delimiter=" ")
        
        self.goat.set_param("Regulator/Num/Value", K[0,:])
        self.goat.set_param("Regulator/Den/Value", K[1,:])

    def on(self):
        self.goat.regul.setOn()

    def off(self):
        self.goat.regul.setOff()

    def state(self):
        return(self.goat.regul.state())

