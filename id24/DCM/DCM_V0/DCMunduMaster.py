# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy as np
import time
import gevent

from bliss.scanning.chain import AcquisitionMaster
from bliss.scanning.channel import AcquisitionChannel
from bliss.common.motor_group import Group

class DCMTrajMaster(AcquisitionMaster):
    
    def __init__(
        self,
        mono,
        axis,
        start,
        end,
        npoints,
        int_time,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys
    ):
        AcquisitionMaster.__init__(
            self,
            axis,
            trigger_type=trigger_type,
            npoints=npoints,
            **keys)

        self.mono = mono
        self.dcm_mot = axis
        if self.dcm_mot == self.mono.motors["thtraj"]:
            self.dcm_mot_type = "bragg"
        else:
            self.dcm_mot_type = "energy"
        self.start_pos = start
        self.end_pos = end
        self.int_time = int_time
        self.tot_time = float(self.npoints * self.int_time)

        self.movables = [axis]
        self.movables_params = {axis.name: {"axis": axis}}
        self.tracked_list = self.mono.tracked_axes()
        if self.tracked_list is not None:
            self.dcm_cont = self.mono.motors["ene_und"].controller
            self.movables += self.tracked_list
            for mot in self.tracked_list:
                self.movables_params[mot.name] = {"axis": mot}
        self._mot_group = Group(*self.movables)
        
        # to add a channel
        self.channels.extend(
            (
                AcquisitionChannel(f"axis:{mot.name}", np.double, (), unit=mot.unit)
                for mot in self.movables
            )
        )
        
        # Calculate velocity, start and stop position for all motors
        self.position_list = {}
        self.calc_param()
        
    def prepare(self):
        
        # Move to start position
        movement = []
        for name in self.movables_params.keys():
            movement.append(self.movables_params[name]["axis"])
            movement.append(self.movables_params[name]["start"])
        self._mot_group.move(*movement)

    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        try:
            # Emit theoretical position for all motors
            for channel in self.channels:
                #print(f"\n EMIT {channel.short_name} DATA")
                channel.emit(self.position_list[channel.short_name])
        
            # Set speed and acceleration for concerned motors
            self.set_speed_acc()
        
            # Move to end position
            movement = []
            for name in self.movables_params.keys():
                movement.append(self.movables_params[name]["axis"])
                movement.append(self.movables_params[name]["end"])
            self._mot_group.move(*movement)
        finally:
            self.unset_speed_acc()
            
    def stop(self):
        self._mot_group.stop()
        
    def calc_param(self):
        dcm_mot_vel = abs(self.end_pos - self.start_pos) / self.tot_time
        dcm_mot_acct = dcm_mot_vel / self.dcm_mot.acceleration
        dcm_mot_accd = dcm_mot_vel * dcm_mot_acct / 2.0

        # mono motor        
        self.position_list[self.dcm_mot.name] = np.linspace(self.start_pos, self.end_pos, self.npoints+1)
        self.movables_params[self.dcm_mot.name]["vel_old"] = self.dcm_mot.velocity
        self.movables_params[self.dcm_mot.name]["acc_old"] = self.dcm_mot.acceleration
        self.movables_params[self.dcm_mot.name]["vel"] = dcm_mot_vel
        self.movables_params[self.dcm_mot.name]["acc"] = self.dcm_mot.acceleration
        self.movables_params[self.dcm_mot.name]["acct"] = dcm_mot_acct
        self.movables_params[self.dcm_mot.name]["accd"] = dcm_mot_accd
        if self.start_pos < self.end_pos:
            self.movables_params[self.dcm_mot.name]["start"] = self.start_pos - dcm_mot_accd
            self.movables_params[self.dcm_mot.name]["end"] = self.end_pos + dcm_mot_accd
        else:
            self.movables_params[self.dcm_mot.name]["start"] = self.start_pos + dcm_mot_accd
            self.movables_params[self.dcm_mot.name]["end"] = self.end_pos - dcm_mot_accd
        
        # Tracked undulators
        if self.tracked_list is not None:
            energy_start = self.start_pos
            energy_end = self.end_pos
            if self.dcm_mot_type == "bragg":
                energy_start = self.mono.bragg2energy(self.start_pos)
                energy_end = self.mono.bragg2energy(self.end_pos)
            
            for axis in self.tracked_list:
                start_pos = self.dcm_cont.energy2undulator(axis, energy_start)
                end_pos = self.dcm_cont.energy2undulator(axis, energy_end)
                vel = abs(end_pos - start_pos) / self.tot_time
                accd = vel * dcm_mot_acct / 2.0
                self.position_list[axis.name] = np.linspace(start_pos, end_pos, self.npoints+1)
                #print(f"------------------- {axis.name}")
                self.movables_params[axis.name]["vel_old"] = axis.controller.read_velocity(axis)
                self.movables_params[axis.name]["acc_old"] = axis.controller.read_acceleration(axis)
                self.movables_params[axis.name]["vel"] = vel
                self.movables_params[axis.name]["acc"] = vel / dcm_mot_acct
                if self.movables_params[axis.name]["acc"] < 0.5:
                    self.movables_params[axis.name]["acc"] = 0.5
                self.movables_params[axis.name]["acct"] = dcm_mot_acct
                self.movables_params[axis.name]["accd"] = accd
                if start_pos < end_pos:
                    self.movables_params[axis.name]["start"] = start_pos - accd
                    self.movables_params[axis.name]["end"] = end_pos + accd
                else:
                    self.movables_params[axis.name]["start"] = start_pos + accd
                    self.movables_params[axis.name]["end"] = end_pos - accd
        
        #print("------------  calc_param   -------------")
        #print(self.movables_params)
                    
    def set_speed_acc(self):
        for key in self.movables_params:
            axis = self.movables_params[key]["axis"]
            vel = self.movables_params[key]["vel"]
            acc = self.movables_params[key]["acc"]
            axis.velocity = vel
            axis.acceleration = acc
            #print(f"set_speed_acc: axis({axis.name}) vel({vel}) acc({acc})")
            
        #mytime = time.time()
        #while not self.check_speed_acc_is_ok("new"):
        #        gevent.sleep(0.01)
        #        myt = time.time()-mytime
        #        if myt > 10:
        #            raise RuntimeError(f"Cannot set speed/acc for undulators") 
                #print(f"waiting for New... {myt:3f}\r", end = "")
        #print("\n")
        
        #print("------------  set_speed_acc   -------------")
        #print(self.movables_params)
                    
    def unset_speed_acc(self):
        for key in self.movables_params:
            axis = self.movables_params[key]["axis"]
            vel = self.movables_params[key]["vel_old"]
            acc = self.movables_params[key]["acc_old"]
            axis.velocity = vel
            axis.acceleration = acc
            #print(f"set_speunset_speed_acced_acc: axis({axis.name}) vel({vel}) acc({acc})")
            #print(self.movables_params)
            
        #mytime = time.time()
        #while not self.check_speed_acc_is_ok("old"):
        #        gevent.sleep(0.01)
        #        myt = time.time()-mytime
        #        print(f"waiting for OLD ... {myt:3f}\r", end = "")
        #print("\n")
        
    def check_speed_acc_is_ok(self, state):
        for axis in self.tracked_list:
            name = axis.name
            v_vel = self.movables_params[name]["vel"]
            v_acc = self.movables_params[name]["acc"]
            v_vel_old = self.movables_params[name]["vel_old"]
            v_acc_old = self.movables_params[name]["acc_old"]                
            if state == "old":
                vel = self.movables_params[name]["vel"]
                acc = self.movables_params[name]["acc"]
            else:
                vel = self.movables_params[name]["vel_old"]
                acc = self.movables_params[name]["acc_old"]
            if np.isclose(axis.velocity, vel, atol=0.01):
                #print(f"{axis.name} - {state} - vel {vel}")
                #print(f"NEW vel [{v_vel}] - acc [{v_acc}]")
                #print(f"CUR vel [{axis.velocity}] - acc [{axis.acceleration}]")
                #print(f"OLD vel [{v_vel}] - acc [{v_acc}]")
                return False
            if np.isclose(axis.acceleration, acc, atol=0.01):
                #print(f"{axis.name} - {state} - acc = {acc}")
                #print(f"NEW vel [{v_vel}] - acc [{v_acc}]")
                #print(f"CUR vel [{axis.velocity}] - acc [{axis.acceleration}]")
                #print(f"OLD vel [{v_vel}] - acc [{v_acc}]")
                return False
                
        return True

            
