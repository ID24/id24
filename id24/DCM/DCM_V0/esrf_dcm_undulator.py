import numpy
import xcalibu

from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.physics.units import ur, units
from bliss.common.utils import add_object_method
from bliss.common.utils import ColorTags, BOLD 

from id24.DCM.DCM_V0.monochromator import MonochromatorCalcMotorBase

class UndulatorTrackingObject(object):
    
    def __init__(self, name, config):
        
        self.__setting = None
        self.__config = config
        self.__name = self.config.get("name")
        self._mono = self.config.get("mono")
        self.motors = {}
        self.harmonics = {}
        def_val = {}
        
        self.undu_config = self.config.get("undulator")
        for undu in self.undu_config:
            axis = undu.get("motor")
            name = axis.name
            self.motors[name] = axis
            if "harmonics" in undu.keys():
                # NEED TO CHECK IF "Ei" or "table" is present in the yaml file
                harm_sel = None
                self.harmonics[name] = {}
                harm_node = undu.get("harmonics")
                for harm_config in harm_node:
                    harm_name = harm_config.get("value")
                    if harm_sel is None:
                        harm_sel = harm_name
                    self.harmonics[name][harm_name] = {
                        "E6": float(harm_config.get("E6")), 
                        "E5": float(harm_config.get("E5")), 
                        "E4": float(harm_config.get("E4")), 
                        "E3": float(harm_config.get("E3")), 
                        "E2": float(harm_config.get("E2")), 
                        "E1": float(harm_config.get("E1")), 
                        "E0": float(harm_config.get("E0"))
                    }
                    self.harmonics[name][harm_name]["calib_file"] = harm_config.get("table")
                    self.read_calib(name, harm_name)
                    
                    theory = harm_config.get("theory", None)
                    self.harmonics[name][harm_name]["theory"] = {}
                    if theory is not None:
                        for elem in theory:
                            for key,val in elem.items():
                                self.harmonics[name][harm_name]["theory"][key] = val
                def_val[name] = {"harmonic": harm_sel, "track": True, "mode": "polynom"}
        self.__settings_name = f"{self.name}_track"
        self.__settings = settings.HashObjSetting(self.__settings_name, default_values=def_val)
        
        for name in self.motors:
            axis = self.motors[name]
            add_object_method(axis, self.track, None, types_info=("bool", "bool"))
            add_object_method(axis, self.harmonic, None, types_info=("int", "int"))
            add_object_method(axis, self.track_mode, None, types_info=("str", "str"))
        
    @property
    def name(self):
        return self.__name

    @property
    def config(self):
        return self.__config

    @property
    def settings(self):
        return self.__settings

    def track(self, axis, state=None):
        if state is not None:
            setting = self.settings[axis.name]
            setting["track"] = state
            self.settings[axis.name] = setting
        return self.settings[axis.name]["track"]

    def harmonic(self, axis, harm=None):
        if harm is not None:
            setting = self.settings[axis.name]
            setting["harmonic"] = harm
            self.settings[axis.name] = setting
        return self.settings[axis.name]["harmonic"]

    def track_mode(self, axis, mode=None):
        if mode is not None:
            if mode in ("polynom", "table", "theory"):
                setting = self.settings[axis.name]
                setting["mode"] = mode
                self.settings[axis.name] = setting
            else:
                raise ValueError(f"mode {mode} does not exists (polynom, table, theory)")
        return self.settings[axis.name]["mode"]

    def read_calib(self, axis_name, harm):
        
        self.harmonics[axis_name][harm]["calib"] = xcalibu.Xcalibu()
        self.harmonics[axis_name][harm]["calib"].set_calib_name(axis_name)
        self.harmonics[axis_name][harm]["calib"].set_calib_time(0)
        self.harmonics[axis_name][harm]["calib"].set_calib_type("TABLE")
        self.harmonics[axis_name][harm]["calib"].set_reconstruction_method("INTERPOLATION")
        
        data = numpy.loadtxt(self.harmonics[axis_name][harm]["calib_file"], dtype=numpy.float).transpose()
        
        self.harmonics[axis_name][harm]["calib"].set_raw_x(numpy.copy(data[0]))
        self.harmonics[axis_name][harm]["calib"].set_raw_y(numpy.copy(data[1]))
    
    def get_undu_from_polynom(self, axis_name, harm, energy):
        und = self.harmonics[axis_name][harm]["E6"] * numpy.power(energy, 6) + \
            self.harmonics[axis_name][harm]["E5"] * numpy.power(energy, 5) + \
            self.harmonics[axis_name][harm]["E4"] * numpy.power(energy, 4) + \
            self.harmonics[axis_name][harm]["E3"] * numpy.power(energy, 3) + \
            self.harmonics[axis_name][harm]["E2"] * numpy.power(energy, 2) + \
            self.harmonics[axis_name][harm]["E1"] * numpy.power(energy, 1) + \
            self.harmonics[axis_name][harm]["E0"]
        return und
            
    def get_undu_from_table(self, axis_name, harm, energy):
        e_min = self.harmonics[axis_name][harm]["calib"].min_x()
        e_max = self.harmonics[axis_name][harm]["calib"].max_x()
        
        if isinstance(energy, numpy.ndarray):
            ene = numpy.copy(energy)
        else:
            ene = numpy.array([energy], dtype=float)
        
        undu = numpy.copy(ene)
        for i in range(ene.size):
            if ene[i] >= e_min and ene[i] <= e_max:
                undu[i] = self.harmonics[axis_name][harm]["calib"].get_y(ene[i])
            else:
                undu[i] = self.get_undu_from_polynom(axis_name, harm, ene[i])
                
        if undu.size == 1:
            return undu[0]
        
        return undu
            
    def _get_undu_from_theory(self, axis_name, harm, energy):
        if self.harmonics[axis_name][harm]["theory"] is not None:
            func_name = self.harmonics[axis_name][harm]["theory"]["method"]
            meth = getattr(self, func_name)
            if isinstance(energy, numpy.ndarray):
                ene = numpy.copy(energy)
            else:
                ene = numpy.array([energy], dtype=float)
        
            #undu = numpy.copy(ene)
            #for i in range(ene.size):
            #    undu[i] = meth(energy, harm, self.harmonics[axis_name][harm]["theory"])
            undu = meth(energy, harm, self.harmonics[axis_name][harm]["theory"])
                
            #if len(undu).size == 1:
            #    return undu[0]
    
            return undu
        else:
            raise RuntimeError("No method given to calculate tracker")
            
    def energy2undulator(self, axis, energy):
        
        harm_sel = self.settings[axis.name]["harmonic"]

        if harm_sel is None:
            raise RuntimeError(f"{axis.name}: No harmonic selected")
        if harm_sel not in self.harmonics[axis.name].keys():
            raise RuntimeError(f"Harmonic {harm} does not exist for {axis.name}")

        und = numpy.nan
        mode = self.settings[axis.name]["mode"]
        if mode == "polynom":
            und = self.get_undu_from_polynom(axis.name, harm_sel, energy)
        if mode == "table":
            und = self.get_undu_from_table(axis.name, harm_sel, energy)
        if mode == "theory":
            und = self._get_undu_from_theory(axis.name, harm_sel, energy)
        
        return und
        
class ID24undulatorTrackingObject(UndulatorTrackingObject):
    
    def ene2gap(self, energy, harm, config):
        Uperiod = float(config["Uperiod"])
        
        #Constants
        me = 9.1093836e-31  # kg
        qe = 1.6021766e-19  
        hbar = 1.054572e-34 # m^2 kg/s
        c = 299792458 # m/s

        ### Source parameters
        Ering = 6.0 #GeV
        a_un = 1.909 ### added in the formulas as aBr #####TO BE CHECKED####
        und_step = 0.05001 ### time quant of acceleration of undulator gap, seconds

        ### Calculated constants
        gamma = Ering / 0.511e-3 # Ering is in GeV
        c1 = qe / (2.0 * numpy.pi * me * c) / 1e3 # To have E in keV and gap in mm
        c2 = 8.0 * numpy.pi * hbar * c * gamma**2 / qe # To have E in keV and gap in mm

        gap = Uperiod / numpy.pi * numpy.log(c1 * a_un * Uperiod / (numpy.sqrt(c2 * harm /(Uperiod * energy) - 2.0)))
        
        return gap
        
        
        
"""
Energy + undulator Calculated motor
"""
class EnergyUndulatorCalcMotor(MonochromatorCalcMotorBase):

    def __init__(self, *args, **kwargs):
        MonochromatorCalcMotorBase.__init__(self, *args, **kwargs)
        self.undu = self.config.config_dict.get("undu_track")
        
    def energy2undulator(self, axis, energy):
        return self.undu.energy2undulator(axis, energy)
        
    def calc_from_real(self, reals_dict):

        pseudos_dict = {}
              
        energy = reals_dict["energy"]
        
        in_pos = True
        for axis in self.reals:
            tag = self._axis_tag(axis)
            if tag != "energy":
                if axis.track():
                    und = self.undu.energy2undulator(axis, energy)
                    rund = reals_dict[tag]
                    #print(f"\n{axis.name} - {und} - {rund}")
                    if not numpy.isclose(und, reals_dict[tag], self.approx):
                        in_pos = False
        
        if in_pos or self.pseudos_are_moving():
            pseudos_dict["energy_undulator"] = energy
        else:
            pseudos_dict["energy_undulator"] = numpy.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}
        
        energy = pseudos_dict["energy_undulator"]
        if not isinstance(energy, numpy.ndarray):
            energy = numpy.array([energy], dtype=float)

        if not numpy.isnan(energy).any():
            reals_dict["energy"] = energy
            for axis in self.reals:
                tag = self._axis_tag(axis)
                if tag != "energy":
                    if axis.track():
                        reals_dict[tag] = self.undu.energy2undulator(axis, energy)
                    else:
                        reals_dict[tag] = axis.position
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position
                
        return reals_dict

class BraggUndulatorTrajCalcMotor(MonochromatorCalcMotorBase):

    def __init__(self, *args, **kwargs):
        MonochromatorCalcMotorBase.__init__(self, *args, **kwargs)
        try:
            self.undu = self.config.config_dict.get("undu_track")
        except Exception as undu_track_exc:
            print(BOLD("BraggUndulatorTrajCalcMotor")+": No Undulator Tracking Available")
            self.undu = None
        
    def energy2undulator(self, axis, energy):
        return self.undu.energy2undulator(axis, energy)
    
    def read_velocity(self, axis):
        # MasterMotor will get the speed of the axis via this method
        # axis is the calculated motor normally (TO BE CHECKED)
        pass
    
    def set_velocity(self, axis, new_velocity):
        # MasterMotor will set the speed of the axis via this method
        # axis is the calculated motor normally (TO BE CHECKED)
        pass
        
    def calc_from_real(self, reals_dict):

        pseudos_dict = {}
              
        thtraj = reals_dict["thtraj"]
        
        energy = self.mono.bragg2energy(thtraj)
        
        in_pos = True
        for axis in self.reals:
            tag = self._axis_tag(axis)
            if tag != "thtraj":
                if axis.track():
                    und = self.undu.energy2undulator(axis, energy)
                    rund = reals_dict[tag]
                    if not numpy.isclose(und, reals_dict[tag], self.approx):
                        in_pos = False
        
        if in_pos or self.pseudos_are_moving():
            pseudos_dict["thtraj_undulator"] = thtraj
        else:
            pseudos_dict["thtraj_undulator"] = numpy.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}
        
        thtraj = pseudos_dict["thtraj_undulator"]
        
        if not numpy.isnan(thtraj).any():
            reals_dict["thtraj"] = thtraj
            energy = self.mono.bragg2energy(thtraj)
            for axis in self.reals:
                tag = self._axis_tag(axis)
                if tag != "thtraj":
                    if axis.track():
                        reals_dict[tag] = self.undu.energy2undulator(axis, energy)
                    else:
                        reals_dict[tag] = axis.position
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position
                
        return reals_dict

class EnergyUndulatorTrajCalcMotor(MonochromatorCalcMotorBase):

    def __init__(self, *args, **kwargs):
        MonochromatorCalcMotorBase.__init__(self, *args, **kwargs)
        try:
            self.undu = self.config.config_dict.get("undu_track")
        except Exception as undu_track_exc:
            print(BOLD("EnergyUndulatorTrajCalcMotor")+": No Undulator Tracking Available")
            self.undu = None
        
    def energy2undulator(self, axis, energy):
        return self.undu.energy2undulator(axis, energy)
    
    def read_velocity(self, axis):
        # MasterMotor will get the speed of the axis via this method
        # axis is the calculated motor normally (TO BE CHECKED)
        pass
    
    def set_velocity(self, axis, new_velocity):
        # MasterMotor will set the speed of the axis via this method
        # axis is the calculated motor normally (TO BE CHECKED)
        pass
        
    def calc_from_real(self, reals_dict):

        pseudos_dict = {}
              
        energy = reals_dict["enetraj"]
        
        if not numpy.isnan(energy).any():
            in_pos = True
            for axis in self.reals:
                tag = self._axis_tag(axis)
                if tag != "enetraj":
                    if axis.track():
                        und = self.undu.energy2undulator(axis, energy)
                        rund = reals_dict[tag]
                        if not numpy.isclose(und, reals_dict[tag], self.approx):
                            in_pos = False
        
            if in_pos or self.pseudos_are_moving():
                pseudos_dict["enetraj_undulator"] = energy
            else:
                pseudos_dict["enetraj_undulator"] = numpy.nan
        else:
            pseudos_dict["enetraj_undulator"] = numpy.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}
        
        energy = pseudos_dict["enetraj_undulator"]
        
        if not numpy.isnan(energy).any():
            reals_dict["enetraj"] = energy
            for axis in self.reals:
                tag = self._axis_tag(axis)
                if tag != "enetraj":
                    if axis.track():
                        reals_dict[tag] = self.undu.energy2undulator(axis, energy)
                    else:
                        reals_dict[tag] = axis.position
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position
                
        return reals_dict
