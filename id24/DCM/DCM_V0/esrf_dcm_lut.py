import numpy as np
import xcalibu
import glob
import time
import gevent
import os
import warnings
from scipy.signal import kaiserord, lfilter, firwin, freqz, firls

from bliss.common.logtools import log_debug, log_info
from bliss.common import plot
from bliss.shell.standard import umv
from bliss.common.cleanup import cleanup
from bliss.config import settings
from bliss.config.static import get_config

class EsrfDCMcalib:
    
    def __init__(self, mono):
        
        self.fjs = EsrfDCMfjsCalib(mono)
        self.interf = EsrfDCMinterfCalib(mono)
        
class EsrfDCMfjsCalib:
    def __init__(self, mono):
        self.mono = mono
        
        fjs_data_dir = self.mono.config.get("calib_fjs_data_dir", None)
        if fjs_data_dir is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No data directory given for FJS calibration")
        self.data_dir = fjs_data_dir
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self.mono.name}", default_value="fjs_default")
        
        bragg_interval = self.mono.config.get("calib_fjs_interval", None)
        if bragg_interval is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No Bragg intervals given for FJS calibration")
        bragg_interval = list(bragg_interval)
        bragg_interval.sort()
        self._bragg_interval =  bragg_interval[::-1]
        self._max_coil_range = 16
        #
        # TODO: Check if rotation < 16 degree if not raise error
        #
        
        self._thtraj = get_config().get("thtraj")
        self._fjstraj = get_config().get("fjstraj")
        self._mcoil = get_config().get("mcoil")
        self._fjsz = get_config().get("fjsz")
        
        self._fjs_margin = 0.005
        self._fjs_default_speed = 0.125
        
        fjs_min, fjs_max = self._fjsz.limits
        #self._fjsz_min = int(fjs_min)
        #self._fjsz_max = int(fjs_max)+1
        self._fjsz_min = 0
        self._fjsz_max = 7

        self.fjs_calib = {}
        for name in ("fjsur", "fjsuh", "fjsd"):
            self.fjs_calib[name] = xcalibu.Xcalibu()
            self.fjs_calib[name].set_calib_name(name)
            self.fjs_calib[name].set_calib_time(0)
            self.fjs_calib[name].set_calib_type("TABLE")
            self.fjs_calib[name].set_reconstruction_method("INTERPOLATION")
            
        # Load last LUT by default
        # Uncomment when default file is there
        #self.load()

    @property
    def radix(self):
        return self._radix_setting.get()
        
    @radix.setter
    def radix(self, value):
        self._radix_setting.set(value)

    def load(self, radix=None):        
        if radix is not None:
            self.radix = radix
        filename = f"{self.data_dir}/{self.radix}_{self.mono.xtal_sel}"
        
        print(f"Load LUT Table from :")
        print(f"    - {filename}_ur.dat")
        print(f"    - {filename}_uh.dat")
        print(f"    - {filename}_d.dat")
        
        self._lut_table_ur = np.loadtxt(f"{filename}_ur.dat", dtype=np.float, delimiter=" ").transpose()
        self._lut_table_uh = np.loadtxt(f"{filename}_uh.dat", dtype=np.float, delimiter=" ").transpose()
        self._lut_table_d = np.loadtxt(f"{filename}_d.dat", dtype=np.float, delimiter=" ").transpose()
        
    def _check_file(self, radix, remove=False):
        
        path_radix = f"{self.data_dir}/{radix}_{self.mono.xtal_sel}_*.dat"
        file_list = glob.glob(path_radix)
        if len(file_list) != 0:
            if remove:
                for f in file_list:
                    os.remove(f)
            else:
                msg_str = f"Radix {radix} already exists in directory {self._data_dir}"
                raise RuntimeError(msg_str)

    def list(self):
        file_list = glob.glob(f"{self.data_dir}/*")
        print("\n")
        for fullname in file_list:
            filename = fullname.split("/")[-1]
            print(f"    {filename}")
        print("\n")

    def save(self):
        data_ur = np.transpose(self._lut_table_ur)
        data_uh = np.transpose(self._lut_table_uh)
        data_d = np.transpose(self._lut_table_d)
        filename = f"{self.data_dir}/{self.radix}_{self.mono.xtal_sel}"
        fn_ur = f"{filename}_ur.dat"
        fn_uh = f"{filename}_uh.dat"
        fn_d = f"{filename}_d.dat"
        print(f"Save LUT Table in :")
        print(f"    - {filename}_ur.dat")
        print(f"    - {filename}_uh.dat")
        print(f"    - {filename}_d.dat")
        np.savetxt(fn_ur, data_ur)
        np.savetxt(fn_uh, data_uh)
        np.savetxt(fn_d, data_d)
        
    def build(self, radix, remove=False, fjs_speed=None):
        """ Make Several Scans to build a LUT over the Full Stroke.
        Scans are performed from high bragg angles to low bragg angles.

        Args:
            lut_radix: name of the LUT files
            remove: Should remove LUT with same radix?
        """
        
        # First check if LUT can be saved.
        self._check_file(radix, remove=remove)
        self.radix = radix
        
        if fjs_speed is None:
            used_fjs_speed = self._fjs_default_speed
        else:
            used_fjs_speed = fjs_speed
            
        # Initialize LUT Data
        lut_nb_interval = len(self._bragg_interval)-1
        lut_ur_full = np.zeros((2,1))
        lut_uh_full = np.zeros((2,1))
        lut_d_full  = np.zeros((2,1))

        # Scan Bragg angle
        for idx in range(lut_nb_interval):
            fjs1 = self.mono.bragg2xtal(self._bragg_interval[idx+1])
            fjs2 = self.mono.bragg2xtal(self._bragg_interval[idx])
            data = self._acq_zone_fjs(fjs1, fjs2, speed=used_fjs_speed)

            # Calculate/Save LUT:
            lut_ur, lut_uh, lut_d = self._calc_lut(data)            

            # Concatenate the LUT for UR
            merge_ur = (lut_ur_full[0,-1] + lut_ur[0,0])/2
            lut_ur_full = np.concatenate((
                lut_ur_full[:,lut_ur_full[0,:] < merge_ur],
                lut_ur[     :,lut_ur[0,:]      > merge_ur]
            ), axis=1)

            # Concatenate the LUT for UH
            merge_uh = (lut_uh_full[0,-1] + lut_uh[0,0])/2
            lut_uh_full = np.concatenate((
                lut_uh_full[:,lut_uh_full[0,:] < merge_uh],
                lut_uh[:,lut_uh[0,:] > merge_uh]
                ), axis=1)
                
            # Concatenate the LUT for D
            merge_d = (lut_d_full[0,-1] + lut_d[0,0])/2
            lut_d_full = np.concatenate((
                lut_d_full[:,lut_d_full[0,:] < merge_d],
                lut_d[:,lut_d[0,:] > merge_d]
                ), axis=1)

        # Remove dummy first point of the lut
        lut_ur_full = lut_ur_full[:, 1:]
        lut_uh_full = lut_uh_full[:,1:]
        lut_d_full = lut_d_full[:,1:]
        # Add dummy points
        lut_ur, lut_uh, lut_d = self.add_dummy_points_lut(lut_ur_full, lut_uh_full, lut_d_full)
        
        # Save the LUT
        self._lut_table_ur = lut_ur
        self._lut_table_uh = lut_uh
        self._lut_table_d = lut_d
        self.save()

    def _acq_zone_fjs(self, fj1, fj2, speed):
        """ Make fjstraj scan that will be used for making the LUT.
        The scan is made for increasing energies, i.e. for small to large fast jack positions

        Args:
            fj1: Fast Jack position 1 [mm]
            fj2: Fast Jack position 2 [mm]
            speed: Maximum Fast Jack Velocity [mm/s]

        Returns:
            Numpy Array containing the raw measurements acquired by the FastDAQ

        """
        # Make sure fj2 > fj1
        if (fj1 > fj2):
            fj1b = fj1
            fj1 = fj2
            fj2 = fj1b
        assert fj2 > fj1, "fj2 should be greater than fj1"

        # Make sure that mcoil can be disabled
        th1 = self.mono.xtal2bragg(fj1-self._fjs_margin)
        th2 = self.mono.xtal2bragg(fj2+self._fjs_margin)
        if np.abs(th1-th2) > self._max_coil_range:
            raise ValueError("Stroke ({np.abs(th1-th2)}) is too large to disable mcoil (Max: {self._mac_coil_range}).")

        fast_daq_counters = [
            {"name": "bragg",  "cnt": self.mono.goat.counters.FPGA2_SSIM6},
            {"name": "ur_e",   "cnt": self.mono.goat.counters.error_ur},
            {"name": "uh_e",   "cnt": self.mono.goat.counters.error_uh},
            {"name": "d_e",    "cnt": self.mono.goat.counters.error_d},
            {"name": "fjs_ur", "cnt": self.mono.goat.counters.FPGA2_SSIM8},
            {"name": "fjs_uh", "cnt": self.mono.goat.counters.FPGA2_SSIM9},
            {"name": "fjs_d",  "cnt": self.mono.goat.counters.FPGA2_SSIM10}
        ]

        # Load Trajectory larger than built LUT
        # self.mono.trajectory.load_fjs(fj1-self.fj_traj_offset, fj2+self.fj_traj_offset, 1000, use_lut=False)
        # Thomas - Changed this otherwise there is some velocity issues (too small)
        self.mono.trajectory.load_fjs(fj1-1.1*self._fjs_margin, fj2+1.1*self._fjs_margin, 5000, use_lut=False)

        # Make sure regul is off (Mode A)
        self.mono.regul.off()

        # Go to Start position
        umv(self._fjstraj, fj1-self._fjs_margin)
        # If scan without mcoil, put mcoil at middle position
        self._fjstraj.disable_axis(axis=self._mcoil)
        print("MCOIL is disabled. Send it at mean position: %.1f [deg]" % ((th1+th2)/2.0))
        umv(self._mcoil, (th1+th2)/2.0)

        # Set maximum velocity
        max_fjs_vel = self._fjstraj._get_max_velocity()
        if (speed < max_fjs_vel):
            self._fjstraj.velocity = speed
        else:
            self._fjstraj.velocity = 0.95 * max_fjs_vel
        print("Fast Jack velocity is set to: %.2f [mm/s]" % self._fjstraj.velocity)

        # Compute approximate time of scan [s]
        scan_time = 2+2*self._fjstraj.acctime+(fj2-fj1)/self._fjstraj.velocity
        print("Scan will last ~ %.1f [s]" % scan_time)

        # Run fast DAC
        self.fastdaq_start(10000, 10000*np.ceil(scan_time).astype(int), fast_daq_counters)

        # Make the scan
        umv(self._fjstraj, fj2+self._fjs_margin)

        # Wait end of fast DAC
        self.fastdaq_poll()

        # If disable mcoil, put it back at end position and re-enable it
        print("Position the MCOIL motor to end position: %.1f [deg]" % th2)
        self._fjstraj.enable_axis(axis=self._mcoil)
        umv(self._fjstraj, fj2)

        # Get Data from Fast DAQ
        daq_data = self.fastdaq_read(fast_daq_counters)

        return np.transpose(daq_data)

    def _calc_lut(self, data, lut_inc=100e-9):
        """ Computes the LUT for ur/uh/d

        Args:
            data: Acquired data during the LUT scan
            lut_inc: Distance between everypoint of the LUT

        """
        # Get all data in SI units
        bragg = np.pi/180*data[:,0] # Bragg Angle [rad]
        fjur = 1e-8*data[:,4] # ur Fast Jack Step in [m]
        fjuh = 1e-8*data[:,5] # uh Fast Jack Step in [m]
        fjd = 1e-8*data[:,6] # d Fast Jack Step in [m]
        time = 1e-4*np.arange(0, np.size(bragg), 1) # Time vector [s]

        # Computation of the position errors of the FJ as measured by the interferometers
        # error = self.lut_a_p2r @ [ddz, dry, drx]
        fjur_e = -1e-9*data[:,1] # [m]
        fjuh_e = -1e-9*data[:,2] # [m]
        fjd_e  = -1e-9*data[:,3] # [m]

        # Generate Low pass FIR Filter
        sample_rate = 10000.0 # Sample Rate [Hz]
        nyq_rate = sample_rate / 2.0 # Nyquist Rate [Hz]
        cutoff_hz = 27 # The cutoff frequency of the filter [Hz]

        # Window with specific ripple [dB] and width [Nyquist Fraction]
        N, beta = kaiserord(60, 4/nyq_rate)
        N_delay = int((N-1)/2) # Delay in ticks

        # Fitler generation
        taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))

        # Filter the Data and compensation of the delay introduced by the FIR filter
        fjur_e_filt = lfilter(taps, 1.0, fjur_e)[N:]
        fjuh_e_filt = lfilter(taps, 1.0, fjuh_e)[N:]
        fjd_e_filt  = lfilter(taps, 1.0, fjd_e)[N:]
        time_filt   = time[N_delay+1:-N_delay]

        # Get Only Interesting Data
        filt_array = np.where(np.logical_or(fjd[N_delay+1:-N_delay] > fjd[0] + self._fjs_margin, fjd[N_delay+1:-N_delay] < fjd[-1] - self._fjs_margin))

        fjur_e_filt = np.delete(fjur_e_filt, filt_array)
        fjuh_e_filt = np.delete(fjuh_e_filt, filt_array)
        fjd_e_filt  = np.delete(fjd_e_filt,  filt_array)
        time_filt   = np.delete(time_filt, filt_array)

        fjur_filt = np.delete(fjur[N_delay+1:-N_delay], filt_array)
        fjuh_filt = np.delete(fjuh[N_delay+1:-N_delay], filt_array)
        fjd_filt  = np.delete(fjd[ N_delay+1:-N_delay], filt_array)

        # Build individual LUT
        u,c = np.unique(fjur_filt, return_index=True)
        lut_ur = np.stack((u+fjur_e_filt[c], u))
        u,c = np.unique(fjuh_filt, return_index=True)
        lut_uh = np.stack((u+fjuh_e_filt[c], u))
        u,c = np.unique(fjd_filt, return_index=True)
        lut_d = np.stack((u+fjd_e_filt[c], u))

        return 1e3*lut_ur, 1e3*lut_uh, 1e3*lut_d
        
    def add_dummy_points_lut(self, lut_ur, lut_uh, lut_d):
        """ Add (theoretical) points at beginning and ending of LUT.
            These points are here only to make sure that there is no problem when loading a trajectory.
        """

        lut_ur = np.concatenate(
            (
                np.concatenate((np.arange(self._fjsz_min, lut_ur[0][0], 1)[np.newaxis,:], lut_ur[1][0] - lut_ur[0][0] + np.arange(self._fjsz_min, lut_ur[0][0], 1)[np.newaxis,:])),
                lut_ur,
                np.concatenate((np.arange(lut_ur[0][-1]+1, self._fjsz_max, 1)[np.newaxis,:], lut_ur[1][-1] - lut_ur[0][-1] + np.arange(lut_ur[0][-1]+1, self._fjsz_max, 1)[np.newaxis,:])),
            ),
            axis=1
        )
        lut_uh = np.concatenate(
            (
                np.concatenate((np.arange(self._fjsz_min, lut_uh[0][0], 1)[np.newaxis,:], lut_uh[1][0] - lut_uh[0][0] + np.arange(self._fjsz_min, lut_uh[0][0], 1)[np.newaxis,:])),
                lut_uh,
                np.concatenate((np.arange(lut_uh[0][-1]+1, self._fjsz_max, 1)[np.newaxis,:], lut_uh[1][-1] - lut_uh[0][-1] + np.arange(lut_uh[0][-1]+1, self._fjsz_max, 1)[np.newaxis,:])),
            ),
            axis=1
        )
        lut_d = np.concatenate(
            (
                np.concatenate((np.arange(self._fjsz_min, lut_d[0][0], 1)[np.newaxis,:], lut_d[1][0] - lut_d[0][0] + np.arange(self._fjsz_min, lut_d[0][0], 1)[np.newaxis,:])),
                lut_d,
                np.concatenate((np.arange(lut_d[0][-1]+1, self._fjsz_max, 1)[np.newaxis,:], lut_d[1][-1] - lut_d[0][-1] + np.arange(lut_d[0][-1]+1, self._fjsz_max, 1)[np.newaxis,:])),
            ),
            axis=1
        )
        
        return lut_ur, lut_uh, lut_d

    #
    # Calibration are used to calculate trajectories
    # This function load the last table in the Fastjack stepper Calibration objects
    #
    def lut_to_calib(self):
        self.fjs_calib["fjsur"].set_raw_x(np.copy(self._lut_table_ur[0]))
        self.fjs_calib["fjsur"].set_raw_y(np.copy(self._lut_table_ur[1]))

        self.fjs_calib["fjsuh"].set_raw_x(np.copy(self._lut_table_uh[0]))
        self.fjs_calib["fjsuh"].set_raw_y(np.copy(self._lut_table_uh[1]))

        self.fjs_calib["fjsd"].set_raw_x(np.copy(self._lut_table_d[0]))
        self.fjs_calib["fjsd"].set_raw_y(np.copy(self._lut_table_d[1]))

    #
    # Speedgoat Fastdaq
    #
    def fastdaq_start(self, frequency, nbp, goat_counters):

        self.lut_nbp = nbp

        # Configure and start Speedgoat Fast Scope
        print("\n Configure and start Speedgoat fast acquisition scope ...")
        self.lut_fdaq = self.mono.goat.get_fastdaq()

        # or goat1.counters._asdict()['xtal1_111_ry'] ...
        cnt_list = []
        for cnt in goat_counters:
            cnt_list.append(cnt["cnt"])

        self.lut_fdaq.start_fast_acquisition(frequency, self.lut_nbp, cnt_list)
        self.lut_fastdaq_start_time = time.time()
        self.lut_fastdaq_total_time = float(nbp)/float(frequency)

        gevent.sleep(1)

    def fastdaq_poll(self):
        while not self.lut_fdaq.scope_read_is_finish():
            time_left = self.lut_fastdaq_total_time - (time.time() - self.lut_fastdaq_start_time)
            print(f" Waiting Speedgoat fastdaq to terminate %.2fs (/{self.lut_fastdaq_total_time}s)"%(time_left,), end="\r")
            gevent.sleep(0.2)

    def fastdaq_read(self, goat_counters):

        data = self.lut_fdaq.scope_get_data()
        nb_counters = len(goat_counters)

        self.lut_data = {}
        lut_data = np.zeros([nb_counters, len(data[0][1])])

        for index in range(nb_counters):
            name = goat_counters[index]["name"]
            self.lut_data[name] = np.array(data[index][1])
            lut_data[index] = np.array(data[index][1])

        return lut_data
                
class EsrfDCMinterfCalib:
    def __init__(self, mono):
        self.mono = mono
        interf_data_dir = self.mono.config.get("calib_interf_data_dir", None)
        if interf_data_dir is None:
            raise RuntimeError("esrf_dcm/InterfCalib: No data directory given for INTERF calibration")
        self.data_dir = interf_data_dir
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_interfcalib_{self.mono.name}", default_value="interf_default")

    @property
    def radix(self):
        return self._radix_setting.get()
        
    @radix.setter
    def radix(self, value):
        self._radix_setting.set(value)

    def load(self, radix=None):        
        if radix is not None:
            self.radix = radix
        xtal = self.mono.xtal_sel
        filename = f"{self.data_dir}/{self.radix}_{xtal}.dat"
        
        # TODO: Load the file
        
        # TODO: Load data on Speedgoat
    
    def state(self):
        return self.mono.goat.get_param("plant/counters/counter_xtal_111_drx/correction_LUT_OnOff/Value")
        
    def off(self):
        self.mono.goat.set_param("plant/counters/counter_xtal_111_drx/correction_LUT_OnOff/Value", 0)
        self.mono.goat.set_param("plant/counters/counter_xtal_111_dry/correction_LUT_OnOff/Value", 0)
        self.mono.goat.set_param("plant/counters/counter_xtal_111_dz/correction_LUT_OnOff/Value", 0)

        self.mono.goat.set_param("plant/counters/counter_xtal_311_drx/correction_LUT_OnOff/Value", 0)
        self.mono.goat.set_param("plant/counters/counter_xtal_311_dry/correction_LUT_OnOff/Value", 0)
        self.mono.goat.set_param("plant/counters/counter_xtal_311_dz/correction_LUT_OnOff/Value", 0)

    def on(self):
        self.mono.goat.set_param("plant/counters/counter_xtal_111_drx/correction_LUT_OnOff/Value", 1)
        self.mono.goat.set_param("plant/counters/counter_xtal_111_dry/correction_LUT_OnOff/Value", 1)
        self.mono.goat.set_param("plant/counters/counter_xtal_111_dz/correction_LUT_OnOff/Value", 1)

        self.mono.goat.set_param("plant/counters/counter_xtal_311_drx/correction_LUT_OnOff/Value", 1)
        self.mono.goat.set_param("plant/counters/counter_xtal_311_dry/correction_LUT_OnOff/Value", 1)
        self.mono.goat.set_param("plant/counters/counter_xtal_311_dz/correction_LUT_OnOff/Value", 1)
        
