
class UnduMaster(AcquisitionMaster, UndershootMixin):
    def __init__(
        self,
        axis,
        start,
        end,
        time=0,
        undershoot=None,
        undershoot_start_margin=0,
        undershoot_end_margin=0,
        trigger_type=AcquisitionMaster.SOFTWARE,
        backnforth=False,
        **keys,
    ):
        AcquisitionMaster.__init__(self, axis, trigger_type=trigger_type, **keys)
        UndershootMixin.__init__(
            self, undershoot, undershoot_start_margin, undershoot_end_margin
        )

        self.movable = axis
        self.start_pos = start
        self.end_pos = end
        self.time = time

        self.backnforth = backnforth
        if isinstance(self.start_pos, list):
            self.velocity = (
                abs(self.start_pos[1] - self.start_pos[0]) / float(self.time)
                if self.time > 0
                else self.movable.velocity
            )
        else:
            self.velocity = (
                abs(self.end_pos - self.start_pos) / float(self.time)
                if self.time > 0
                else self.movable.velocity
            )

    def __iter__(self):
        self._iter_index = 0
        if isinstance(self.start_pos, list):
            iter_pos = iter(self.start_pos)
            niter = len(self.start_pos)
            self.start_pos = next(iter_pos)
            last_end_pos = self.end_pos
            while self._iter_index < niter:
                if self._iter_index < niter - 1:
                    self.end_pos = next(iter_pos)
                else:
                    self.end_pos = last_end_pos
                yield self
                self.start_pos = self.end_pos
                self._iter_index += 1
        else:
            while True:
                yield self
                self._iter_index += 1
                if not self.parent:
                    break

    def prepare(self):
        start = self._calculate_undershoot(self.start_pos)
        self.movable.move(start)

    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        return self._start_move()

    def _start_move(self):
        self.initial_velocity = self.movable.velocity
        try:
            self.movable.velocity = self.velocity
            end = self._calculate_undershoot(self.end_pos, end=True)
            self.movable.move(end)
            if self.backnforth:
                self.start_pos, self.end_pos = self.end_pos, self.start_pos
        finally:
            self.movable.velocity = self.initial_velocity

    def trigger_ready(self):
        return not self.movable.is_moving

    def wait_ready(self):
        self.movable.wait_move()

    def stop(self):
        self.movable.stop()

