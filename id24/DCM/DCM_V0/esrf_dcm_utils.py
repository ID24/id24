import inspect
import time
from tabulate import tabulate

from bliss import setup_globals

from bliss.controllers import pepu
from bliss.shell.standard import umv, mv, wm
from bliss.common.scans import ascan
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED



##############################################################
#####
##### LUT (IS IT STILL USEFULL???? GB 09/07/2021)
#####
def dcm_lut(dcm, thtraj, mg1):
    """
    ??? Fine lut draft ???
    """

    dcm.trajectory_load_bragg(39, 51, 1000)
    thtraj.velocity = 1.0
    umv(thtraj, 43)
    ascan(thtraj, 44, 46, 500, 0.1, mg1)

    dcm.lut_reset("/tmp/dcm_40_50_250919_1.dat")
    dcm.lut_scan(40, 50, 10000, 120000)
    dcm.lut_calculate(41)
    dcm.trajectory_load_bragg(39, 51, 1000, use_lut=True)
    thtraj.velocity = 1.0
    umv(thtraj, 43)
    ascan(thtraj, 44, 46, 500, 0.1, mg1)

    dcm.lut_scan(40, 50, 10000, 120000)
    dcm.lut_calculate(41)
    dcm.trajectory_load_bragg(39, 51, 1000, use_lut=True)
    thtraj.velocity = 1.5
    umv(thtraj, 43)
    ascan(thtraj, 44, 46, 500, 0.1, mg1)

    dcm.lut_scan(40, 50, 10000, 120000)
    dcm.lut_calculate(21)
    dcm.trajectory_load_bragg(39, 51, 1000, use_lut=True)
    thtraj.velocity = 1.0
    umv(thtraj, 43)
    ascan(thtraj, 44, 46, 500, 0.1, mg1)

    dcm.lut_scan(40, 50, 10000, 120000)
    dcm.lut_calculate(11)
    dcm.trajectory_load_bragg(39, 51, 1000, use_lut=True)
    thtraj.velocity = 1.0
    umv(thtraj, 43)
    ascan(thtraj, 44, 46, 500, 0.1, mg1)

##############################################################
#####
##### DCM Helpers
#####
def dcmhelper():
    print("\n    "+BOLD("DCM blocks motor positions"))
    print("        "+GREEN("wbragg"))
    print("        "+GREEN("wty"))
    print("        "+GREEN("wtz"))
    print("        "+GREEN("wtfjs"))
    print("        "+GREEN("wtfjp"))
    print("\n    "+BOLD("DCM calculations methods"))
    print("        "+GREEN("dcm.bragg2energy")+" - "+GREEN("dcm.energy2bragg"))
    print("        "+GREEN("dcm.energy2dxtal")+" - "+GREEN("dcm.dxtal2energy"))
    print("        "+GREEN("dcm.bragg2dxtal")+" - "+GREEN("dcm.dxtal2bragg"))
    print("\n    "+BOLD("DCM Undulator Tracking"))
    print("        "+GREEN("<undu_motor>.track"))
    print("        "+GREEN("<undu_motor>.track_mode"))
    print("        "+GREEN("<undu_motor>.harmonic"))
    print("\n    "+BOLD("DCM Control"))
    print("        "+BLUE("Restart"))
    print("        "+BLUE("Fast Jack Offset"))
    print("            "+GREEN("dcm.fjs_offset"))
    print("            "+GREEN("dcm.fjs_offset_use"))
    print("        "+BLUE("Polynomial Interferometer Correction"))
    print("            "+GREEN("dcm.polynom")+" - Show polynom factors")
    print("            "+GREEN("dcm.polynom.state"))
    print("            "+GREEN("dcm.polynom.on"))
    print("            "+GREEN("dcm.polynom.off"))
    print("            "+GREEN("dcm.polynom.list"))
    print("            "+GREEN("dcm.polynom.load"))
    print("        "+BLUE("Interferometer LUT"))
    print("            "+GREEN("dcm.lut")+" - Show current LUT file")
    print("            "+GREEN("dcm.lut.build"))
    print("            "+GREEN("dcm.lut.list"))
    print("            "+GREEN("dcm.lut.load"))
    print("        "+BLUE("Regulation"))
    print("            "+GREEN("dcm.regul.state"))
    print("            "+GREEN("dcm.regul.off"))
    print("            "+GREEN("dcm.regul.on"))
    print("        "+BLUE("Fine LUT"))
    print("        "+BLUE("Trajectories"))
    print("            "+GREEN("dcm.traj.load_bragg"))
    print("            "+GREEN("dcm.traj.load_energy"))
    print("        "+BLUE("Scans"))
    print("            "+GREEN("zapdcm.trajund"))

    print("\n")
    
##############################################################
#####
##### Motor tool functions
#####
def dcmwm(*motors):
    """
    Print name and position of a group of motors.
    A bare version of wm()

    example:
       DEMO [8]: dcmwm(bad, roby, custom_axis)
       Name  bad(None)  roby(None)  custom_axis(None)
       Pos.  0.000      3.300       0.000
    """
    print("")
    lines = []
    line1 = [BOLD("    Name")]
    line2 = [BOLD("    Pos.")]
    for mot in motors:
        line1.append(f"{GREEN(mot.name)}({mot.unit})")
        line2.append(f"{mot.position:.3f}")
    lines.append(line1)
    lines.append(line2)
    mystr = tabulate(lines, tablefmt="plain")
    print(mystr)
    print("")

#def motor_esync(mot):
#    """
#    Encoder synchronization: Set axis position to encoder position.
#    Done twice in case power was not ON because doing ON can move a bit the axis.
#    Then synchronize BLISS position.
#    """
#    cont = mot.controller
#    addr = mot.address
#    cont.raw_write(f"{addr}:esync")
#    cont.raw_write(f"{addr}:power on")
#    time.sleep(1)
#    cont.raw_write(f"{addr}:esync")
#    mot.sync_hard()

def motor_info(mot_list):
    """
    Print position  dial  offset  lim-  lim+  state of a list of motors.
    A compact variation on wm().

    Example:
    TEST_SESSION [12]: print(motor_info([bad, roby, custom_axis]))
                 position  dial  offset  lim-  lim+  state
    bad          0         0.0   0.0     -inf  inf   ['READY']
    roby         3.3       3.3   0.0     -inf  inf   ['READY']
    custom_axis  0         0.0   0.0     -inf  inf   ['READY']
    """
    headers_list = ["", "position", "dial", "offset", "lim-", "lim+", "state"]
    table = []

    for mot in mot_list:
        mot_state = mot.state.current_states_names
        mot_info = (mot.name,
                    f"{mot.position:g}",
                    f"{mot.dial:g}",
                    f"{mot.offset:g}",
                    f"{mot.low_limit:g}",
                    f"{mot.high_limit:g}",
                    mot_state)
        table.append(mot_info)

    info_str = tabulate(tuple(table),  tablefmt="plain", headers=headers_list)

    return(info_str)

##############################################################
#####
##### Where All Motor Groups
#####
def wty():
    dcmwm(setup_globals.tyy, setup_globals.tyrz)
    dcmwm(setup_globals.mtyu, setup_globals.mtyd)
    dcmwm(setup_globals.mtyu_brake, setup_globals.mtyd_brake)
    
def wtz():
    dcmwm(setup_globals.tzz, setup_globals.tzrx, setup_globals.tzry)
    dcmwm(setup_globals.mtzur, setup_globals.mtzdr, setup_globals.mtzh)

def wbragg():
    dcmwm(setup_globals.bragg_rot)
    dcmwm(setup_globals.mbrag, setup_globals.msafe, setup_globals.mcoil)

def wfjs():
    dcmwm(setup_globals.fjsz, setup_globals.fjsrx, setup_globals.fjsry)
    dcmwm(setup_globals.mfjsuh, setup_globals.mfjsur, setup_globals.mfjsd)

def wfjp():
    dcmwm(setup_globals.fjpz, setup_globals.fjpry, setup_globals.fjprx)
    dcmwm(setup_globals.fjpur, setup_globals.fjpuh, setup_globals.fjpd)

def wdcm():
    dcmwm(setup_globals.enetraj, setup_globals.thtraj, setup_globals.mtraj)
    dcmwm(setup_globals.dcmene, setup_globals.bragg_fe, setup_globals.bragg_rot)
    dcmwm(setup_globals.mbrag, setup_globals.msafe, setup_globals.mcoil)
    wfjs()

    print("\nEnergy at bragg position (%g) : %g"%(
            setup_globals.mbrag.position,
            setup_globals.dcm.bragg2energy(setup_globals.mbrag.position)
        )
    )
    print("Fjs     at bragg position (%g)  : %g"%(
            setup_globals.mbrag.position, 
            setup_globals.dcm.bragg2xtal(setup_globals.mbrag.position)
        )
    )

def wtraj():
    wm(setup_globals.enetraj, setup_globals.thtraj, setup_globals.mtraj)


##############################################################
#####
##### DCM Motors Synchronization
#####
def fjs_sync():
    setup_globals.mfjsuh.sync_hard()
    setup_globals.mfjsur.sync_hard()
    setup_globals.mfjsd.sync_hard()

def fjp_sync():
    setup_globals.fjpur.sync_hard()
    setup_globals.fjpuh.sync_hard()
    setup_globals.fjpd.sync_hard()
    setup_globals.fjpz.sync_hard()
    setup_globals.fjprx.sync_hard()
    setup_globals.fjpry.sync_hard()

def thesync():
    print("Bragg  esync")
    motor_esync(setup_globals.mbrag)
    print("safety esync")
    motor_esync(setup_globals.msafe)
    print("Coil   esync")
    motor_esync(setup_globals.mcoil)
    print("Fjs    esync")
    fjs_sync()
    print("Fjp    esync")
    fjp_sync()

##############################################################
#####
##### DCM Motors Set 0 Position
#####
def fjs_set_zero():
    mot = setup_globals.mfjsd
    mot.position(0.0)
    mot.dial(0)

    mot = setup_globals.mfjsur
    mot.position(0.0)
    mot.dial(0)

    mot = setup_globals.mfjsuh
    mot.position(0.0)
    mot.dial(0)
