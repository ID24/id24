import numpy as np
import xcalibu

from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.physics.units import ur, units
from bliss.common.utils import add_object_method

from bliss.common.logtools import log_debug
from bliss import global_map

from id24.DCM.DCM_V0.monochromator import MonochromatorCalcMotorBase

"""
Energy + undulator Calculated motor
"""
class EnergyUndulatorCalcMotor(MonochromatorCalcMotorBase):

    def __init__(self, *args, **kwargs):
        MonochromatorCalcMotorBase.__init__(self, *args, **kwargs)

        self.harmonics = {}
        def_val = {}
        for name in self._axes_config:
            axis = self._axes_config[name][1]
            if "harmonics" in axis.keys():
                # NEED TO CHECK IF "Ei" or "table" is present in the yaml file
                harm_sel = None
                self.harmonics[name] = {}
                harm_node = axis.get("harmonics")
                for harm_config in harm_node:
                    harm_name = harm_config.get("value")
                    if harm_sel is None:
                        harm_sel = harm_name
                    self.harmonics[name][harm_name] = {
                        "E6": float(harm_config.get("E6")),
                        "E5": float(harm_config.get("E5")),
                        "E4": float(harm_config.get("E4")),
                        "E3": float(harm_config.get("E3")),
                        "E2": float(harm_config.get("E2")),
                        "E1": float(harm_config.get("E1")),
                        "E0": float(harm_config.get("E0"))
                    }
                    self.harmonics[name][harm_name]["calib_file"] = harm_config.get("table")
                    self.read_calib(name, harm_name)
                def_val[name] = {"harmonic": harm_sel, "track": True, "mode": "polynom"}
        self.__settings_name = f"{self.name}_track"
        self.__settings = settings.HashObjSetting(self.__settings_name, default_values=def_val)

    def initialize(self):
        CalcController.initialize(self)

        for axis in self.reals:
            add_object_method(axis, self.track, None, types_info=("bool", "bool"))
            add_object_method(axis, self.harmonic, None, types_info=("int", "int"))
            add_object_method(axis, self.track_mode, None, types_info=("str", "str"))

    def track(self, axis, state=None):
        if state is not None:
            setting = self.settings[axis.name]
            setting["track"] = state
            self.settings[axis.name] = setting
        return self.settings[axis.name]["track"]

    def harmonic(self, axis, harm=None):
        if harm is not None:
            setting = self.settings[axis.name]
            setting["harmonic"] = harm
            self.settings[axis.name] = setting
        return self.settings[axis.name]["harmonic"]

    def track_mode(self, axis, mode=None):
        if mode is not None:
            if mode in ("polynom", "table"):
                setting = self.settings[axis.name]
                setting["mode"] = mode
                self.settings[axis.name] = setting
            else:
                raise ValueError(f"mode {mode} does not exists (polynom, table)")
        return self.settings[axis.name]["mode"]

    @property
    def settings(self):
        return self.__settings

    def read_calib(self, axis_name, harm):

        self.harmonics[axis_name][harm]["calib"] = xcalibu.Xcalibu()
        self.harmonics[axis_name][harm]["calib"].set_calib_name(axis_name)
        self.harmonics[axis_name][harm]["calib"].set_calib_time(0)
        self.harmonics[axis_name][harm]["calib"].set_calib_type("TABLE")
        self.harmonics[axis_name][harm]["calib"].set_reconstruction_method("INTERPOLATION")

        data = np.loadtxt(self.harmonics[axis_name][harm]["calib_file"], dtype=np.float).transpose()

        self.harmonics[axis_name][harm]["calib"].set_raw_x(np.copy(data[0]))
        self.harmonics[axis_name][harm]["calib"].set_raw_y(np.copy(data[1]))

    def get_undu_from_polynom(self, axis_name, harm, energy):
        und = self.harmonics[axis_name][harm]["E6"] * np.power(energy, 6) + \
            self.harmonics[axis_name][harm]["E5"] * np.power(energy, 5) + \
            self.harmonics[axis_name][harm]["E4"] * np.power(energy, 4) + \
            self.harmonics[axis_name][harm]["E3"] * np.power(energy, 3) + \
            self.harmonics[axis_name][harm]["E2"] * np.power(energy, 2) + \
            self.harmonics[axis_name][harm]["E1"] * np.power(energy, 1) + \
            self.harmonics[axis_name][harm]["E0"]
        return und

    def get_undu_from_table(self, axis_name, harm, energy):
        e_min = self.harmonics[axis_name][harm]["calib"].min_x()
        e_max = self.harmonics[axis_name][harm]["calib"].max_x()

        if energy>=e_min and energy<=e_max:
            return self.harmonics[axis_name][harm]["calib"].get_y(energy)
        else:
            return self.get_undu_from_polynom(axis_name, harm, energy)

    def energy2undulator(self, axis, energy):

        harm_sel = self.settings[axis.name]["harmonic"]

        if harm_sel is None:
            raise RuntimeError(f"{axis.name}: No harmonic selected")
        if harm_sel not in self.harmonics[axis.name].keys():
            raise RuntimeError(f"Harmonic {harm} does not exist for {axis.name}")

        und = np.nan
        mode = self.settings[axis.name]["mode"]
        if mode == "polynom":
            und = self.get_undu_from_polynom(axis.name, harm_sel, energy)
        if mode == "table":
            und = self.get_undu_from_table(axis.name, harm_sel, energy)

        return und

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        energy = reals_dict["energy"]

        in_pos = True
        for axis in self.reals:
            tag = self._axis_tag(axis)
            if tag != "energy":
                if axis.track():
                    und = self.energy2undulator(axis, energy)
                    rund = reals_dict[tag]
                    #print(f"\n{axis.name} - {und} - {rund}")
                    if np.isclose(und, reals_dict[tag], atol=self.approx):
                        in_pos = False

        if in_pos or self.pseudos_are_moving():
            pseudos_dict["energy_undulator"] = energy
        else:
            pseudos_dict["energy_undulator"] = np.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        energy = pseudos_dict["energy_undulator"]

        if not np.isnan(energy).any():
            reals_dict["energy"] = energy
            for axis in self.reals:
                tag = self._axis_tag(axis)
                if tag != "energy":
                    if axis.track():
                        reals_dict[tag] = self.energy2undulator(axis, energy)
                    else:
                        reals_dict[tag] = axis.position
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict

"""
Energy Calculated motor
"""
class EnergyCalcMotor(MonochromatorCalcMotorBase):

    def __init__(self, *args, **kwargs):
        MonochromatorCalcMotorBase.__init__(self, *args, **kwargs)

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        if self.mono is not None and self.mono.xtal_sel is not None:
            ene = self.mono.bragg2energy(reals_dict["bragg"])
            if not np.isnan(ene).any():
                unit = self.pseudos[0].unit
                ene = (ene*ur.keV).to(unit).magnitude
            pseudos_dict["energy"] = ene
        else:
            pseudos_dict["energy"] = np.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}
        if self.mono is not None and self.mono.xtal_sel is not None and not np.isnan(pseudos_dict["energy"]).any():
            unit = self.pseudos[0].unit
            ene = (pseudos_dict["energy"]*ur.parse_units(unit)).to("keV").magnitude
            bragg = self.mono.energy2bragg(ene)
            reals_dict["bragg"] = bragg
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict

"""

# bragg fix exit (bragg rotaions + xtal motor)
- plugin: emotion
  package: id21.esrf_dcm_calcmotor
  class: BraggFixExitCalcMotor
  fix_exit_offset: 10.5
  dxtals_at_xtal0: 30.29429878
  approximation: 0.01

  axes:
    - name: $bragg_rot
      tag: real bragg
    - name: $sim_fjsz
      tag real xtal

    - name: bragg_fe
      tag: bragg_fix_exit

"""
class BraggFixExitCalcMotor(MonochromatorCalcMotorBase):

    def __init__(self, *args, **kwargs):
        MonochromatorCalcMotorBase.__init__(self, *args, **kwargs)

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        bragg = 0.0
        rbragg = 0.0
        if self.mono is not None:

            try:
                bragg = self.mono.xtal2bragg(reals_dict["xtal"])
            except:
                pseudos_dict["bragg_fix_exit"] = np.nan
                return pseudos_dict

            rbragg = reals_dict["bragg"]

            if (np.isclose(bragg, rbragg, atol=self.approx)) or \
               self.pseudos_are_moving():
                pseudos_dict["bragg_fix_exit"] = reals_dict["bragg"]
            else:
                pseudos_dict["bragg_fix_exit"] = np.nan
        else:
            pseudos_dict["bragg_fix_exit"] = np.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        if self.mono is not None and not np.isnan(pseudos_dict["bragg_fix_exit"]).any():
            reals_dict["bragg"] = pseudos_dict["bragg_fix_exit"]
            reals_dict["xtal"] = self.mono.bragg2xtal(pseudos_dict["bragg_fix_exit"])
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict

"""
controller:
    class: BraggRotationCalcMotor
    approximation: 0.01
    axes:
        - name: $mbrag
          tags: real bragg
        - name: $msafe
          tags: real safety
        - name: $mcoil
          tags: real coil

        - name: bragg_rot
          tags: bragg_rotation
"""

class BraggRotationCalcMotor(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.approx = float(self.config.get("approximation"))

        global_map.register(self)

    def pseudos_are_moving(self):
        for axis in self.pseudos:
            if axis.is_moving:
                return True
        return False

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        if (np.isclose(reals_dict["bragg"] , reals_dict["safety"], atol=self.approx) and \
           np.isclose(reals_dict["bragg"], reals_dict["coil"], atol=self.approx)) or \
           self.pseudos_are_moving():
            pseudos_dict["bragg_rotation"] = reals_dict["bragg"]
        else:
            pseudos_dict["bragg_rotation"] = np.nan

        _b = reals_dict["bragg"]
        _s = reals_dict["safety"]
        _c = reals_dict["coil"]
        _br = pseudos_dict["bragg_rotation"]
        log_debug(self, "calc_from_real(): %g %g %g -> %g", _b, _s, _c, _br)

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        if not np.isnan(pseudos_dict["bragg_rotation"]).any():
            reals_dict["bragg"]  = pseudos_dict["bragg_rotation"]
            reals_dict["safety"] = pseudos_dict["bragg_rotation"]
            reals_dict["coil"]   = pseudos_dict["bragg_rotation"]
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        _br = pseudos_dict["bragg_rotation"]
        _b = reals_dict["bragg"]
        _s = reals_dict["safety"]
        _c = reals_dict["coil"]
        log_debug(self, "calc_to_real(): %g -> %g %g %g", _br, _b, _s, _c)

        return reals_dict

class SafetyCoilCalcMotor(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.approx = float(self.config.get("approximation"))

    def pseudos_are_moving(self):
        for axis in self.pseudos:
            if axis.is_moving:
                return True
        return False

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        # Return nan if real motors are not close enough.
        if np.isclose(reals_dict["coil"], reals_dict["safety"], atol=self.approx) or \
           self.pseudos_are_moving():
            pseudos_dict["safe_coil"] = reals_dict["safety"]
        else:
            pseudos_dict["safe_coil"] = np.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        if not np.isnan(pseudos_dict["safe_coil"]).any():
            reals_dict["safety"] = pseudos_dict["safe_coil"]
            reals_dict["coil"]   = pseudos_dict["safe_coil"]
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict


class DxtalCalcMotor(MonochromatorCalcMotorBase):
    """
    Calculation of distance between Xtals:
    *  from fjsz calc motor

    """
    def __init__(self, *args, **kwargs):
        MonochromatorCalcMotorBase.__init__(self, *args, **kwargs)

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        if self.mono is not None:
            pseudos_dict["dxtal"] = self.mono.xtal2dxtal(reals_dict["xtal"])
        else:
            pseudos_dict["dxtal"] = np.nan
        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        if not np.isnan(pseudos_dict["dxtal"]).any():
            reals_dict["xtal"]  = self.mono.dxtal2xtal(pseudos_dict["dxtal"])
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict
