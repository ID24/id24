
from bliss.controllers.counter import CalcCounterController

class SpeedgoatBraggEnergyCalcCounter(CalcCounterController):

    def __init__(self, name, config):

        self.mono = config.get("mono", None)

        # CalcCounterController.__init__(self, name, config)
        super().__init__(name, config)


    def calc_function(self, input_dict):

        bragg_goat = input_dict["bragg_goat"]

        output_dict = {}
        output_dict["ene_goat"] = self.mono.bragg2energy(bragg_goat)

        return output_dict
