import gevent
import tabulate

from bliss import setup_globals
from bliss.shell.standard import umv, umvr
from bliss.config.static import get_config

from id24.DCM.esrf_dcm_utils import fjs_sync


class EsrfDCMRestart(object):
    
    def __init__(self, goat, mono):
        self.goat = goat
        self.mono = mono
        
    def restart_all(self):
        print("Remove All offset")
        self.mono.regul.off()
        self.mono.fjs_offset_use(False)
        
        print("Desacivate LUT on Interferometers")
        self.mono.calib.interf.off()

        print("Homing of Fast Jacks")
        self.fjs_home()

        print("Synchronize FastJack Position with Speedgoat")
        self.fjs_reset_offset_speedgoat()

        print("Go at 10 degrees for reference position")
        umv(get_config().get("braggfev0"), 10)
        (rx, ry) = self.mono.fjs_offset()
        umv(get_config().get("fjsrx"), rx, get_config().get("fjsry"), ry)

        # TODO: RC to optimize the position / flux => fjs_offset
        
        print("Reset Interferometers and Photodiodes")
        self.realign_interf()

    def realign_interf(self):
        """
            Function to reset the interferometers to zeros.
            After realign, xtal1 = 0, metro_frame=0 and xtal2 corresponds to the wanted fjsz
        """
        # Make sure regul is off and piezo at middle position
        self.mono.regul.off()

        self.goat.set_param("plant/actors/actor_FPGA1_SSIS1/Offset/Value", 5000)
        self.goat.set_param("plant/actors/actor_FPGA1_SSIS2/Offset/Value", 5000)
        self.goat.set_param("plant/actors/actor_FPGA1_SSIS3/Offset/Value", 5000)

        # Reset all the interferometers
        self.goat.set_param("plant/counters/counter_metroFrame_z/offset(cnt)/Bias",   self.goat.get_param("plant/counters/counter_metroFrame_z/offset(cnt)/Bias" )-self.goat.get_signal("plant/counters/counter_metroFrame_z/Unit" ))
        self.goat.set_param("plant/counters/counter_metroFrame_rx/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_metroFrame_rx/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_metroFrame_rx/Unit"))
        self.goat.set_param("plant/counters/counter_metroFrame_ry/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_metroFrame_ry/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_metroFrame_ry/Unit"))
        
        gevent.sleep(0.5)
        # Xtal1
        self.goat.set_param("plant/counters/counter_xtal1_111_z/offset(cnt)/Bias",   self.goat.get_param("plant/counters/counter_xtal1_111_z/offset(cnt)/Bias" )-self.goat.get_signal("plant/counters/counter_xtal1_111_z/Unit" ))
        self.goat.set_param("plant/counters/counter_xtal1_111_rx/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_xtal1_111_rx/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_xtal1_111_rx/Unit"))
        self.goat.set_param("plant/counters/counter_xtal1_111_ry/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_xtal1_111_ry/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_xtal1_111_ry/Unit"))
        self.goat.set_param("plant/counters/counter_xtal1_311_z/offset(cnt)/Bias",   self.goat.get_param("plant/counters/counter_xtal1_311_z/offset(cnt)/Bias" )-self.goat.get_signal("plant/counters/counter_xtal1_311_z/Unit" ))
        self.goat.set_param("plant/counters/counter_xtal1_311_rx/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_xtal1_311_rx/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_xtal1_311_rx/Unit"))
        self.goat.set_param("plant/counters/counter_xtal1_311_ry/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_xtal1_311_ry/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_xtal1_311_ry/Unit"))
        
        gevent.sleep(0.5)
        # Xtal2
        self.goat.set_param("plant/counters/counter_xtal2_111_z/offset(cnt)/Bias",   1e6*self.mono.motors["xtal"].position+self.goat.get_param("plant/counters/counter_xtal2_111_z/offset(cnt)/Bias" )-self.goat.get_signal("plant/counters/counter_xtal2_111_z/Unit" ))
        self.goat.set_param("plant/counters/counter_xtal2_111_rx/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_xtal2_111_rx/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_xtal2_111_rx/Unit"))
        self.goat.set_param("plant/counters/counter_xtal2_111_ry/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_xtal2_111_ry/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_xtal2_111_ry/Unit"))
        self.goat.set_param("plant/counters/counter_xtal2_311_z/offset(cnt)/Bias",   1e6*self.mono.motors["xtal"].position+self.goat.get_param("plant/counters/counter_xtal2_311_z/offset(cnt)/Bias" )-self.goat.get_signal("plant/counters/counter_xtal2_311_z/Unit" ))
        self.goat.set_param("plant/counters/counter_xtal2_311_rx/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_xtal2_311_rx/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_xtal2_311_rx/Unit"))
        self.goat.set_param("plant/counters/counter_xtal2_311_ry/offset(cnt)/Bias",  self.goat.get_param("plant/counters/counter_xtal2_311_ry/offset(cnt)/Bias")-self.goat.get_signal("plant/counters/counter_xtal2_311_ry/Unit"))

        gevent.sleep(1.0)
        if self.mono.xtal_sel == "Si111":
            print("Xtal Si111")
            goat_xtal = "111"
        else:
            print("Xtal Si311")
            goat_xtal = "311"
        goat_sig = f"plant/counters/counter_xtal_{goat_xtal}_dz_filter/Unit"
        fjsz_pos = 1e6*self.mono.motors["xtal"].position
        dz_val = self.goat.get_signal(goat_sig)
        print(f"  interf ddz {dz_val-fjsz_pos} - interf dz  ({dz_val}) - fjsz ({fjsz_pos})")
        goat_sig = f"plant/counters/counter_xtal_{goat_xtal}_dry_filter/Unit"
        print(f"  interf dry ({self.goat.get_signal(goat_sig)})")
        goat_sig = f"plant/counters/counter_xtal_{goat_xtal}_drx_filter/Unit"
        print(f"  interf drx ({self.goat.get_signal(goat_sig)})")
        
    def fjs_home(self):
        """ Homing of all three fast jacks """
        fjsz =get_config().get("fjsz") 
        mfjsur =get_config().get("mfjsur") 
        mfjsuh =get_config().get("mfjsuh") 
        mfjsd =get_config().get("mfjsd") 
        
        umv(fjsz, 0)
        gevent.sleep(0.2)
        
        umvr(fjsz, 0.01)
        gevent.sleep(0.2)
        
        mfjsur.home()
        gevent.sleep(0.2)
        mfjsuh.home()
        gevent.sleep(0.2)
        mfjsd.home()
        gevent.sleep(0.2)

        fjs_sync()
        
    def fjs_reset_offset_speedgoat(self):
        mfjsur =get_config().get("mfjsur") 
        mfjsuh =get_config().get("mfjsuh") 
        mfjsd =get_config().get("mfjsd") 

        self.goat.set_param("plant/counters/counter_FPGA2_SSIM8/offset(cnt)/Bias",  0)
        self.goat.set_param("plant/counters/counter_FPGA2_SSIM9/offset(cnt)/Bias",  0)
        self.goat.set_param("plant/counters/counter_FPGA2_SSIM10/offset(cnt)/Bias", 0)

        fjs_sync()

        self.goat.set_param("plant/counters/counter_FPGA2_SSIM8/offset(cnt)/Bias",  int(1e5*mfjsur.position - self.goat.counters.FPGA2_SSIM8.read()))
        self.goat.set_param("plant/counters/counter_FPGA2_SSIM9/offset(cnt)/Bias",  int(1e5*mfjsuh.position - self.goat.counters.FPGA2_SSIM9.read()))
        self.goat.set_param("plant/counters/counter_FPGA2_SSIM10/offset(cnt)/Bias", int(1e5*mfjsd.position  - self.goat.counters.FPGA2_SSIM10.read()))
        
        
