import numpy as np

from bliss.common.logtools import log_debug, log_info

class EsrfDCMTrajectory(object):
    
    def __init__(self, name, config):
        
        self.name = name
        self.mono = None
        self.mbrag = config.get("bragg")
        self.msafe = config.get("safety")
        self.mcoil = config.get("coil")
        self.mfjsur = config.get("fjsur")
        self.mfjsuh = config.get("fjsuh")
        self.mfjsd = config.get("fjsd")
        self.mtraj = config.get("traj")
        self.enetraj = config.get("enetraj")
        self.thtraj = config.get("thtraj")
        self.fjstraj = config.get("fjstraj")
        
        self.approx = 0.5
        
        self.fjs_name    = ("fjsur", "fjsuh", "fjsd")

    def set_mono(self, mono):
        self.mono = mono
        
    ##########################################################################
    #
    # trajectory
    #
    def is_trajectory_ok(self, pos_start, pos_stop, traj_type="bragg"):
        """
        Check that start and end position of all trajectories are
        coherent with the start and end values asked by user.
        """
        if traj_type == "bragg":
            angle_start = float(pos_start)
            angle_stop  = float(pos_stop)

            fjs_start = self.mono.bragg2xtal(angle_start)
            fjs_stop  = self.mono.bragg2xtal(angle_stop)

        if traj_type == "fjs":
            fjs_start = float(pos_start)
            fjs_stop  = float(pos_stop)
            
            angle_start = self.mono.xtal2bragg(fjs_start)
            angle_stop  = self.mono.xtal2bragg(fjs_stop)

        # Check Bragg angle
        if traj_type != "fjs":
            angle_traj_start = self.traj_data["brag"][0] / self.mbrag.steps_per_unit
            if not np.isclose(angle_traj_start, angle_start, self.approx):
                return False
            angle_traj_stop  = self.traj_data["brag"][-1] / self.mbrag.steps_per_unit
            if not np.isclose(angle_traj_stop, angle_stop, self.approx):
                return False

        # fjsur
        fjs_traj_start = self.traj_data["fjsur"][0] / self.mfjsur.steps_per_unit
        if not np.isclose(fjs_traj_start, fjs_start, self.approx):
            print("Error on fjsur start value")
            return False
        fjs_traj_stop  = self.traj_data["fjsur"][-1] / self.mfjsur.steps_per_unit
        if not np.isclose(fjs_traj_stop, fjs_stop, self.approx):
            print("Error on fjsur stop value")
            return False

        # fjsuh
        fjs_traj_start = self.traj_data["fjsuh"][0] / self.mfjsuh.steps_per_unit
        if not np.isclose(fjs_traj_start, fjs_start, self.approx):
            print("Error on fjsuh start value")
            return False
        fjs_traj_stop  = self.traj_data["fjsuh"][-1] / self.mfjsuh.steps_per_unit
        if not np.isclose(fjs_traj_stop, fjs_stop, self.approx):
            print("Error on fjsuh stop value")
            return False

        # fjsd
        fjs_traj_start = self.traj_data["fjsd"][0] / self.mfjsd.steps_per_unit
        if not np.isclose(fjs_traj_start, fjs_start, self.approx):
            print("Error on fjsd start value")
            return False
        fjs_traj_stop  = self.traj_data["fjsd"][-1] / self.mfjsd.steps_per_unit
        if not np.isclose(fjs_traj_stop, fjs_stop, self.approx):
            print("Error on fjsd stop value")
            return False

        return True

    def trajectory_check(self, traj_type="bragg", silent=False):
        """
        Print start and end positions of all trajectories for visual
        verification.
        """
        if not silent:
            print("    ", end="")
        f_val = self.traj_data["fjsur"][0] / self.mfjsur.steps_per_unit
        t_val = self.traj_data["fjsur"][-1] / self.mfjsur.steps_per_unit
        if not silent:
            print("fjsur: [%g - %g] "%(f_val, t_val), end=" ")

        f_val = self.traj_data["fjsuh"][0] / self.mfjsuh.steps_per_unit
        t_val = self.traj_data["fjsuh"][-1] / self.mfjsuh.steps_per_unit
        if not silent:
            print("fjsuh: [%g - %g]"%(f_val, t_val), end=" ")

        f_val = self.traj_data["fjsd"][0] / self.mfjsd.steps_per_unit
        t_val = self.traj_data["fjsd"][-1] / self.mfjsd.steps_per_unit
        if not silent:
            print("fjsd: [%g - %g]"%(f_val, t_val), end="")

        if traj_type == "bragg":
            f_val = self.traj_data["brag"][0] / self.mbrag.steps_per_unit
            t_val = self.traj_data["brag"][-1] / self.mbrag.steps_per_unit
            if not silent:
                print(" bragg: [%g - %g]"%(f_val, t_val), end="")

        if not silent:
            print("")

    
    ############################################################################
    #
    # Trajectory General
    #
    def fjs_do_correction(self, use_lut, silent):
        
        # Add Offset on fjsrx, fjsry if configured
        if self.mono.fjs_offset_use():
            if not silent:
                print("Add Offset")
            (rx_off, ry_off) = self.mono.fjs_offset()
            for i in range(len(self.fjs_data)):
                (fjsur, fjsuh, fjsd) = self.mono.xyz2fjs(rx_off, ry_off, self.fjs_data[i])
                self.traj_data["fjsur"][i] = fjsur
                self.traj_data["fjsuh"][i] = fjsuh
                self.traj_data["fjsd"][i]  = fjsd

        # Use LUT if configured
        if use_lut:
            if not silent:
                print("Use LUT")
            self.mono.calib.fjs.lut_to_calib()
            for i in range(len(self.fjs_data)):
                for name in self.fjs_name:
                    # add 3 LUT correction to 3 FJS trajectories.
                    calib = self.mono.calib.fjs.fjs_calib[name]
                    self.traj_data[name][i] = calib.get_y(self.traj_data[name][i])
                    
    def print_check_trajectory(self,from_pos,to_pos,traj_type="bragg",silent=False,ask_check=False):

        # Check if trajectories are valid
        log_debug(self, f"trajectory_check('{traj_type}')")
        self.trajectory_check(traj_type, silent=silent)
        traj_ok = self.is_trajectory_ok(from_pos, to_pos, traj_type=traj_type)
        if (traj_ok == 0) or ask_check:
            if traj_ok == 0:
                print("\n!!!! Error on trajectory !!!!")
            rep = input('\ncontinue (y/n) ?')
            if rep != "y":
                raise RuntimeError("Invalid Trajectory or Interruption")
        else:
            if not silent:
                print("traj is ok.")
        
    ############################################################################
    #
    # Bragg Trajectory
    #
    def load_bragg(self, from_th, to_th, nbp_per_mm, use_lut=False, ask_check=False, silent=False):
        """
        Used to perform angle-traj.
        * <nbp_per_mm>: relative to fast jack stepper ! (not angle)
        * <use_lut>: bennnn
        * <ask_check>: tete de noeud tu veux vraiment charger ?
        """

        if not silent:
            print(f"Load trajectory for thtraj from {from_th}(deg) to {to_th}(deg)")

        from_fjs = self.mono.bragg2xtal(from_th)
        to_fjs   = self.mono.bragg2xtal(to_th)
        nbp      = int( abs(from_fjs - to_fjs) * float(nbp_per_mm) ) + 1
        
        if not silent:
            print(f"Need {nbp} points")

        self.fjs_data   = np.linspace(from_fjs, to_fjs, nbp)
        angle_data = np.copy(self.fjs_data)
        for ind in range(len(self.fjs_data)):
            angle_data[ind] = self.mono.xtal2bragg(self.fjs_data[ind])

        self.traj_data = {}
        self.traj_data["x"]     = angle_data
        self.traj_data["mtraj"] = np.copy(angle_data)
        self.traj_data["brag"]  = np.copy(angle_data)
        self.traj_data["safe"]  = np.copy(angle_data)
        self.traj_data["coil"]  = np.copy(angle_data)

        self.traj_data["fjsur"] = np.copy(self.fjs_data)
        self.traj_data["fjsuh"] = np.copy(self.fjs_data)
        self.traj_data["fjsd"]  = np.copy(self.fjs_data)
        
        # FJS correction (offset + LUT)
        self.fjs_do_correction(use_lut, silent)

        # Load trajectories on Icepap axes
        mdict = {
            self.mbrag.name:  self.traj_data["brag"],
            self.msafe.name:  self.traj_data["safe"],
            self.mcoil.name:  self.traj_data["coil"],
            self.mfjsur.name: self.traj_data["fjsur"],
            self.mfjsuh.name: self.traj_data["fjsuh"],
            self.mfjsd.name:  self.traj_data["fjsd"],
            self.mtraj.name:  self.traj_data["mtraj"]
        }

        self.thtraj.set_positions(angle_data,
                             mdict,
                             self.thtraj.LINEAR)

        self.print_check_trajectory(from_th,to_th,traj_type="bragg",silent=silent,ask_check=ask_check)
        
    ############################################################################
    #
    # Fjs Trajectory
    #
    def load_fjs(self, from_fjs, to_fjs, nbp_per_mm, use_lut=False, ask_check=False, silent=False):
        """
        Used to perform fjs-traj.
        * <nbp_per_mm>: relative to fast jack stepper ! (not angle)
        * <use_lut>: bennnn
        * <ask_check>: tete de noeud tu veux vraiment charger ?
        """

        if not silent:
            print(f"Load trajectory for fjs from {from_fjs}(mm) to {to_fjs}(mm)")

        nbp = int( abs(from_fjs - to_fjs) * float(nbp_per_mm) ) + 1
        
        if not silent:
            print(f"Need {nbp} points")

        self.fjs_data   = np.linspace(from_fjs, to_fjs, nbp)
        angle_data = np.copy(self.fjs_data)
        for ind in range(len(self.fjs_data)):
            angle_data[ind] = self.mono.xtal2bragg(self.fjs_data[ind])

        self.traj_data = {}
        self.traj_data["x"]     = self.fjs_data
        self.traj_data["mtraj"] = np.copy(self.fjs_data)
        self.traj_data["brag"]  = np.copy(angle_data)
        self.traj_data["safe"]  = np.copy(angle_data)
        self.traj_data["coil"]  = np.copy(angle_data)

        self.traj_data["fjsur"] = np.copy(self.fjs_data)
        self.traj_data["fjsuh"] = np.copy(self.fjs_data)
        self.traj_data["fjsd"]  = np.copy(self.fjs_data)
        
        # FJS correction (offset + LUT)
        self.fjs_do_correction(use_lut, silent)

        # Load trajectories on Icepap axes
        mdict = {
            self.mbrag.name:  self.traj_data["brag"],
            self.msafe.name:  self.traj_data["safe"],
            self.mcoil.name:  self.traj_data["coil"],
            self.mfjsur.name: self.traj_data["fjsur"],
            self.mfjsuh.name: self.traj_data["fjsuh"],
            self.mfjsd.name:  self.traj_data["fjsd"],
            self.mtraj.name:  self.traj_data["mtraj"]
        }

        self.fjstraj.set_positions(self.fjs_data,
                             mdict,
                             self.fjstraj.LINEAR)

        self.print_check_trajectory(from_fjs,to_fjs,traj_type="fjs",silent=silent,ask_check=ask_check)

    ############################################################################
    #
    # Energy Trajectory
    #
    def load_energy(self, from_ene, to_ene, nbp_per_mm, use_lut=False, ask_check=False, silent=False):
        """
        Used to perform energy-traj.
        """
        
        if not silent:
            print(f"Load trajectory for enetraj from {from_ene} (KeV) to {to_ene} (KeV)")
     
        from_fjs = self.mono.energy2xtal(from_ene)
        to_fjs   = self.mono.energy2xtal(to_ene)
        nbp      = int( abs(from_fjs - to_fjs) * float(nbp_per_mm) ) + 1
        
        if not silent:
            print(f"Need {nbp} points")

        self.fjs_data    = np.linspace(from_fjs, to_fjs, nbp)
        angle_data  = np.copy(self.fjs_data)
        energy_data = np.copy(self.fjs_data)
        for ind in range(len(self.fjs_data)):
            angle_data[ind]  = self.mono.xtal2bragg(self.fjs_data[ind])
            energy_data[ind] = self.mono.bragg2energy(angle_data[ind])

        self.traj_data = {}
        self.traj_data["x"]     = energy_data
        self.traj_data["mtraj"] = np.copy(energy_data)
        self.traj_data["brag"]  = np.copy(angle_data)
        self.traj_data["safe"]  = np.copy(angle_data)
        self.traj_data["coil"]  = np.copy(angle_data)

        self.traj_data["fjsur"] = np.copy(self.fjs_data)
        self.traj_data["fjsuh"] = np.copy(self.fjs_data)
        self.traj_data["fjsd"]  = np.copy(self.fjs_data)
        
        # FJS correction (offset + LUT)
        self.fjs_do_correction(use_lut, silent)
        
        mdict = {
            self.mbrag.name:  self.traj_data["brag"],
            self.msafe.name:  self.traj_data["safe"],
            self.mcoil.name:  self.traj_data["coil"],
            self.mfjsur.name: self.traj_data["fjsur"],
            self.mfjsuh.name: self.traj_data["fjsuh"],
            self.mfjsd.name:  self.traj_data["fjsd"],
            self.mtraj.name:  self.traj_data["mtraj"]
        }

        self.enetraj.set_positions(
            energy_data,
            mdict,
            self.enetraj.LINEAR
        )

        from_pos = self.mono.energy2bragg(from_ene)
        to_pos = self.mono.energy2bragg(to_ene)
        self.print_check_trajectory(from_pos,to_pos,traj_type="bragg",silent=silent,ask_check=ask_check)
