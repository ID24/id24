
from bliss.common.hook import MotionHook
from bliss.shell.standard import umv


class TyHook(MotionHook):

    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.mbu = config['mbu']
        self.mbd = config['mbd']
        self.mode = config["mode"]
        super().__init__()
        self.in_scan = False

    def pre_move(self, motion_list):
        if not self.in_scan:
            self.ty_release_brakes()

    def post_move(self, motion_list):
        if not self.in_scan:
            self.ty_engage_brakes()

    def pre_scan(self, motion_list):
        self.ty_release_brakes()
        self.in_scan = True

    def post_scan(self, motion_list):
        self.in_scan = False
        self.ty_engage_brakes()

    def ty_release_brakes(self):
        print("RELEASE TY Brakes .....")
        self.mbu.hw_limit(-1, wait=False)
        self.mbd.hw_limit(-1, wait=False)
        self.mbu.wait_move()
        self.mbd.wait_move()
        self.mbu.sync_hard()
        self.mbd.sync_hard()
        print("TY Brakes RELEASED")
    
    def ty_engage_brakes(self):
        print("ENGAGE TY Brakes .....")
        if self.mode == "ESRF":
            umv(self.mbu, -5.751, self.mbd, -4.632)
        else:
            self.mbu.hw_limit(1, wait=False)
            self.mbd.hw_limit(1, wait=False)
            self.mbu.wait_move()
            self.mbd.wait_move()
            self.mbu.sync_hard()
            self.mbd.sync_hard()
        print("TY Brakes ENGAGED")
