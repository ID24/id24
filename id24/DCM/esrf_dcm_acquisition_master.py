# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
import time
import gevent

from bliss.scanning.chain import AcquisitionMaster
from bliss.scanning.channel import AcquisitionChannel
from bliss.common.motor_group import Group
from bliss.common.axis import AxisState
from bliss.controllers.motor import CalcController
from bliss.common.logtools import user_print, disable_user_output

# class TrajectoryEnergyConstantUndulatorMaster(AcquisitionMaster):
    
    # def __init__(
        # self,
        # axis,
        # start,
        # end,
        # npoints,
        # int_time,
        # trigger_type=AcquisitionMaster.SOFTWARE,
        # **keys
    # ):

        # self.calc_axis = axis
        
        # if not isinstance(axis.controller, CalcController):
            # raise RuntimeError("Wrong motor type: Not a calculation motor")
                        
        # # Get motor list of tracked motors
        # self.tracked_list = []
        # self.dcm_mot = None
        # self.dcm_mot_type = None
        # for mot in axis.controller.reals:
            # tag = axis.controller._axis_tag(mot)
            # if tag.find("tracker") != -1:
                # if mot.tracking.state:
                    # self.tracked_list.append(mot)
            # if tag == "energy":
                # self.dcm_mot = mot
                # self.dcm_mot_type = "energy"
            # if tag == "bragg":
                # self.dcm_mot = mot
                # self.dcm_mot_type = "bragg"
               
        # if len(self.tracked_list) <= 0:
            # raise RuntimeError("No tracked motors")
        # if self.dcm_mot_type is None:
            # raise RuntimeError("Wrong motor type: No energy or bragg tagged motors")

        # AcquisitionMaster.__init__(
            # self,
            # self.dcm_mot,
            # trigger_type=trigger_type,
            # npoints=npoints,
            # **keys)
                    
        # self.start_pos = start
        # self.end_pos = end
        # self.int_time = int_time
        # self.tot_time = float(self.npoints * self.int_time)

        # self.movables = [self.dcm_mot]
        # self.movables += self.tracked_list
        # self._mot_group = Group(*self.movables)

        # self.movables_params = {self.dcm_mot.name: {"axis": self.dcm_mot}}
        # for mot in self.tracked_list:
            # self.movables_params[mot.name] = {"axis": mot}
                
        # # to add a channel
        # self.channels.extend(
            # (
                # AcquisitionChannel(f"axis:{mot.name}", numpy.double, (), unit=mot.unit)
                # for mot in self.movables
            # )
        # )
        
        # # Calculate velocity, start and stop position for all motors
        # self.position_list = {}
        # self.calc_param()
        
    # def prepare(self):
        # # Move to start position
        # movement = []
        # for name in self.movables_params.keys():
            # movement.append(self.movables_params[name]["axis"])
            # movement.append(self.movables_params[name]["start"])
        # self._mot_group.move(*movement)

    # def start(self):
        # if self.parent:
            # return
        # self.trigger()

    # def trigger(self):
        # try:
            # # Emit theoretical position for all motors
            # for channel in self.channels:
                # #print(f"\n EMIT {channel.short_name} DATA")
                # channel.emit(self.position_list[channel.short_name])
        
            # # Set speed and acceleration for concerned motors
            # self.set_speed_acc()
        
            # # Move to end position
            # movement = []
            # for name in self.movables_params.keys():
                # movement.append(self.movables_params[name]["axis"])
                # movement.append(self.movables_params[name]["end"])
            # self._mot_group.move(*movement)
        # finally:
            # self.unset_speed_acc()
            
    # def stop(self):
        # self._mot_group.stop()
        
    # def calc_param(self):
        # dcm_mot_vel = abs(self.end_pos - self.start_pos) / self.tot_time
        # dcm_mot_acct = dcm_mot_vel / self.dcm_mot.acceleration
        # dcm_mot_accd = dcm_mot_vel * dcm_mot_acct / 2.0
        # self.undershoot = dcm_mot_accd
        # self.velocity = dcm_mot_vel

        # # mono motor        
        # self.position_list[self.dcm_mot.name] = numpy.linspace(self.start_pos, self.end_pos, self.npoints+1)
        # self.movables_params[self.dcm_mot.name]["vel_old"] = self.dcm_mot.velocity
        # self.movables_params[self.dcm_mot.name]["acc_old"] = self.dcm_mot.acceleration
        # self.movables_params[self.dcm_mot.name]["vel"] = dcm_mot_vel
        # self.movables_params[self.dcm_mot.name]["acc"] = self.dcm_mot.acceleration
        # self.movables_params[self.dcm_mot.name]["acct"] = dcm_mot_acct
        # self.movables_params[self.dcm_mot.name]["accd"] = dcm_mot_accd
        # if self.start_pos < self.end_pos:
            # self.movables_params[self.dcm_mot.name]["start"] = self.start_pos - dcm_mot_accd
            # self.movables_params[self.dcm_mot.name]["end"] = self.end_pos + dcm_mot_accd
        # else:
            # self.movables_params[self.dcm_mot.name]["start"] = self.start_pos + dcm_mot_accd
            # self.movables_params[self.dcm_mot.name]["end"] = self.end_pos - dcm_mot_accd
        
        # # Tracked undulators
        # if self.tracked_list is not None:
            # energy_start = self.start_pos
            # energy_end = self.end_pos
            # if self.dcm_mot_type == "bragg":
                # energy_start = self.dcm_mot.controller._mono.bragg2energy(self.start_pos)
                # energy_end = self.dcm_mot.controller._mono.bragg2energy(self.end_pos)
            
            # for axis in self.tracked_list:
                # start_pos = axis.tracking.energy2tracker(energy_start)
                # end_pos = axis.tracking.energy2tracker(energy_end)
                # vel = abs(end_pos - start_pos) / self.tot_time
                # accd = vel * dcm_mot_acct / 2.0
                # self.position_list[axis.name] = numpy.linspace(start_pos, end_pos, self.npoints+1)
                # self.movables_params[axis.name]["vel_old"] = axis.velocity
                # self.movables_params[axis.name]["acc_old"] = axis.acceleration
                # self.movables_params[axis.name]["vel"] = vel
                # self.movables_params[axis.name]["acc"] = vel / dcm_mot_acct
                # if self.movables_params[axis.name]["acc"] < 0.5:
                    # self.movables_params[axis.name]["acc"] = 0.5
                # self.movables_params[axis.name]["acct"] = dcm_mot_acct
                # self.movables_params[axis.name]["accd"] = accd
                # if start_pos < end_pos:
                    # self.movables_params[axis.name]["start"] = start_pos - accd
                    # self.movables_params[axis.name]["end"] = end_pos + accd
                # else:
                    # self.movables_params[axis.name]["start"] = start_pos + accd
                    # self.movables_params[axis.name]["end"] = end_pos - accd
                            
    # def set_speed_acc(self):
        # for key in self.movables_params:
            # axis = self.movables_params[key]["axis"]
            # vel = self.movables_params[key]["vel"]
            # acc = self.movables_params[key]["acc"]
            # axis.velocity = vel
            # axis.acceleration = acc
                    
    # def unset_speed_acc(self):
        # for key in self.movables_params:
            # axis = self.movables_params[key]["axis"]
            # vel = self.movables_params[key]["vel_old"]
            # acc = self.movables_params[key]["acc_old"]
            # axis.velocity = vel
            # axis.acceleration = acc
        
    # def check_speed_acc_is_ok(self, state):
        # for axis in self.tracked_list:
            # name = axis.name
            # v_vel = self.movables_params[name]["vel"]
            # v_acc = self.movables_params[name]["acc"]
            # v_vel_old = self.movables_params[name]["vel_old"]
            # v_acc_old = self.movables_params[name]["acc_old"]                
            # if state == "old":
                # vel = self.movables_params[name]["vel"]
                # acc = self.movables_params[name]["acc"]
            # else:
                # vel = self.movables_params[name]["vel_old"]
                # acc = self.movables_params[name]["acc_old"]
            # if numpy.isclose(axis.velocity, vel, atol=0.01):
                # return False
            # if numpy.isclose(axis.acceleration, acc, atol=0.01):
                # return False
                
        # return True

            

class TrajectoryEnergyTrackerMaster(AcquisitionMaster):
    
    def __init__(
        self,
        axis,
        start,
        end,
        npoints,
        int_time,
        trigger_type=AcquisitionMaster.SOFTWARE,
        **keys
    ):

        self.calc_axis = axis
        
        if not isinstance(axis.controller, CalcController):
            raise RuntimeError("Wrong motor type: Not a calculation motor")
                        
        # Get motor list of tracked motors
        self.tracked_list = []
        self.scanned_axis = self.calc_axis.controller.get_master_motor()
        for mot in self.calc_axis.controller.reals:
            if mot != self.scanned_axis:
                tag = self.calc_axis.controller._axis_tag(mot)
                if tag.find("tracker") != -1:
                    if mot.tracking.state:
                        self.tracked_list.append(mot)
               
        AcquisitionMaster.__init__(
            self,
            self.scanned_axis,
            trigger_type=trigger_type,
            npoints=npoints,
            **keys)
                    
        self.start_pos = start
        self.end_pos = end
        self.int_time = int_time
        self.tot_time = float(self.npoints * self.int_time)

        self.movables = [self.scanned_axis]
        self.movables += self.tracked_list
        self._mot_group = Group(*self.movables)

        self.movables_params = {self.scanned_axis.name: {"axis": self.scanned_axis}}
        for mot in self.tracked_list:
            self.movables_params[mot.name] = {"axis": mot}
                
        # to add a channel
        self.channels.extend(
            (
                AcquisitionChannel(f"axis:{mot.name}", numpy.double, (), unit=mot.unit)
                for mot in self.movables
            )
        )
        
        # Calculate velocity, start and stop position for all motors
        self.position_list = {}
        self.calc_param()
        
    def prepare(self):
        # Move to start position
        movement = []
        for name in self.movables_params.keys():
            movement.append(self.movables_params[name]["axis"])
            movement.append(self.movables_params[name]["start"])
        self._mot_group.move(*movement)

    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        try:
            # Emit theoretical position for all motors
            for channel in self.channels:
                channel.emit(self.position_list[channel.short_name])
        
            # Set speed and acceleration for concerned motors
            self.set_speed_acc()
        
            # Move to end position
            movement = []
            for name in self.movables_params.keys():
                movement.append(self.movables_params[name]["axis"])
                movement.append(self.movables_params[name]["end"])
            self._mot_group.move(*movement)
        finally:
            self.unset_speed_acc()
            
    def stop(self):
        self._mot_group.stop()
        
    def calc_param(self):
        dcm_mot_vel = abs(self.end_pos - self.start_pos) / self.tot_time
        dcm_mot_acct = dcm_mot_vel / self.scanned_axis.acceleration
        dcm_mot_accd = dcm_mot_vel * dcm_mot_acct / 2.0
        self.undershoot = dcm_mot_accd
        self.velocity = dcm_mot_vel

        # Scanned Motor        
        self.position_list[self.scanned_axis.name] = numpy.linspace(self.start_pos, self.end_pos, self.npoints+1)
        self.movables_params[self.scanned_axis.name]["vel_old"] = self.scanned_axis.velocity
        self.movables_params[self.scanned_axis.name]["acc_old"] = self.scanned_axis.acceleration
        self.movables_params[self.scanned_axis.name]["vel"] = dcm_mot_vel
        self.movables_params[self.scanned_axis.name]["acc"] = self.scanned_axis.acceleration
        self.movables_params[self.scanned_axis.name]["acct"] = dcm_mot_acct
        self.movables_params[self.scanned_axis.name]["accd"] = dcm_mot_accd
        if self.start_pos < self.end_pos:
            self.movables_params[self.scanned_axis.name]["start"] = self.start_pos - dcm_mot_accd
            self.movables_params[self.scanned_axis.name]["end"] = self.end_pos + dcm_mot_accd
        else:
            self.movables_params[self.scanned_axis.name]["start"] = self.start_pos + dcm_mot_accd
            self.movables_params[self.scanned_axis.name]["end"] = self.end_pos - dcm_mot_accd
        
        # Tracked undulators
        if self.tracked_list is not None:
            energy_start = self.calc_axis.controller.master2energy_dial(self.start_pos)
            energy_end = self.calc_axis.controller.master2energy_dial(self.end_pos)
            
            for axis in self.tracked_list:
                start_pos = axis.tracking.energy2tracker(energy_start)
                end_pos = axis.tracking.energy2tracker(energy_end)
                vel = abs(end_pos - start_pos) / self.tot_time
                accd = vel * dcm_mot_acct / 2.0
                self.position_list[axis.name] = numpy.linspace(start_pos, end_pos, self.npoints+1)
                self.movables_params[axis.name]["vel_old"] = axis.velocity
                self.movables_params[axis.name]["acc_old"] = axis.acceleration
                self.movables_params[axis.name]["vel"] = vel
                self.movables_params[axis.name]["acc"] = vel / dcm_mot_acct
                if self.movables_params[axis.name]["acc"] < 0.5:
                    self.movables_params[axis.name]["acc"] = 0.5
                self.movables_params[axis.name]["acct"] = dcm_mot_acct
                self.movables_params[axis.name]["accd"] = accd
                if start_pos < end_pos:
                    self.movables_params[axis.name]["start"] = start_pos - accd
                    self.movables_params[axis.name]["end"] = end_pos + accd
                else:
                    self.movables_params[axis.name]["start"] = start_pos + accd
                    self.movables_params[axis.name]["end"] = end_pos - accd
        
    def set_speed_acc(self):
        #with disable_user_output():
        for key in self.movables_params:
            axis = self.movables_params[key]["axis"]
            vel = self.movables_params[key]["vel"]
            acc = self.movables_params[key]["acc"]
 
            axis.wait_move()
 
            #print(f"velocity {axis.name} - {vel}")
            axis.velocity = vel
            #print("DONE")
            #print(f"acceleration {axis.name} - {acc}")
            #axis.acceleration = acc
            #print("DONE")
            #print(f"Set variables - {axis.acceleration} - {axis.velocity}")
            self.movables_params[key]["acc_set"] = axis.acceleration
            self.movables_params[key]["vel_set"] = axis.velocity
            #print("DONE")
                    
    def unset_speed_acc(self):
        with disable_user_output():
            for key in self.movables_params:
                axis = self.movables_params[key]["axis"]
                vel = self.movables_params[key]["vel_old"]
                acc = self.movables_params[key]["acc_old"]
                #start_time = time.time()
                #print(f"\nWait {axis.name} end of move ... ", end="")
                axis.wait_move()
                #print(f"Done ({time.time()-start_time:.1f})")
                #start_time = time.time()

                while axis.state.MOVING:
                    gevent.sleep(0.1)
                
                #while AxisState.READY not in axis.state:
                #    gevent.sleep(0.1)
                    #print(f"\nWait {axis.name} ready ... ({time.time()-start_time:.1f})", end="\r")
                #print(" Done")
                axis.acceleration = acc
                axis.velocity = vel
        
    def check_speed_acc_is_ok(self, state):
        for axis in self.tracked_list:
            name = axis.name
            v_vel = self.movables_params[name]["vel"]
            v_acc = self.movables_params[name]["acc"]
            v_vel_old = self.movables_params[name]["vel_old"]
            v_acc_old = self.movables_params[name]["acc_old"]                
            if state == "old":
                vel = self.movables_params[name]["vel"]
                acc = self.movables_params[name]["acc"]
            else:
                vel = self.movables_params[name]["vel_old"]
                acc = self.movables_params[name]["acc_old"]
            if numpy.isclose(axis.velocity, vel, atol=0.01):
                return False
            if numpy.isclose(axis.acceleration, acc, atol=0.01):
                return False
                
        return True

            
