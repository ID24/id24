import numpy as np
from bliss.config import settings

class EsrfDCMregul:
    
    def __init__(self, mono):
        self._mono = mono
        self._goat_ctl = self._mono._goat._hwc
        self._fastdaq = self._goat_ctl.fastdaq.fastdaq
        self._results_dir = "/data/id24/inhouse/DCM" # Where to store identification results
   
        regul_data_dir = self._mono.config.get("regul_data_dir", None)
        if regul_data_dir is None:
            raise RuntimeError("esrf_dcm_regul: No data directory given for REGUL")
        self._data_dir = regul_data_dir
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_regul_{self._mono.name}_radix", default_value="regul_default")
        
        # Load Filters
        self.load()

    def __info__(self):
        return self._goat_ctl.regul.hac.__info__()
 
    def state(self):
        return self._goat_ctl.regul.hac.state
        
    def off(self):
        self._goat_ctl.regul.hac.off()
        
    def on(self):
        self._goat_ctl.regul.hac.reset_error()
        self._goat_ctl.regul.hac.on()

    def error(self):
        """ Print Position error for each fastjack.
        Useful to check if regul is working. """
        print(f"UR: {self._goat_ctl.counter.e_fj_ur_filtered.value : .1f} nm")
        print(f"UH: {self._goat_ctl.counter.e_fj_uh_filtered.value : .1f} nm")
        print(f"D : {self._goat_ctl.counter.e_fj_d_filtered.value : .1f} nm")

    def load(self, radix=None):     
        # First turn off the regulation
        self.off()

        if radix is not None:
            self.radix = radix
        filename = f"{self._data_dir}/{self.radix}.dat"

        # Load Regul File
        K_hac = np.loadtxt(filename, dtype=np.float, delimiter=" ")

        # Enable filters
        self._goat_ctl.filter.K_ur.disable()
        self._goat_ctl.filter.K_uh.disable()
        self._goat_ctl.filter.K_d.disable()
        # Configure Filters
        self._goat_ctl.filter.K_ur.num_coef = K_hac[0]
        self._goat_ctl.filter.K_uh.num_coef = K_hac[0]
        self._goat_ctl.filter.K_d.num_coef  = K_hac[0]
        self._goat_ctl.filter.K_ur.den_coef = K_hac[1]
        self._goat_ctl.filter.K_uh.den_coef = K_hac[1]
        self._goat_ctl.filter.K_d.den_coef  = K_hac[1]
        # Enable filters
        self._goat_ctl.filter.K_ur.enable()
        self._goat_ctl.filter.K_uh.enable()
        self._goat_ctl.filter.K_d.enable()

    def _identify_plant(self, fast_jack="fjpur", duration=10, file_name="identification", save_figure=False, save_data=False):
        """ Computes the transfer function from "fast_jack"
        to the defined counters. """

        # Make sure regulator is Off
        self.off()

        # Make sure regulator is Off
        self.off()

        # Configure Counters
        counters_out = [self._mono._goat.counters.e_fj_ur, self._mono._goat.counters.e_fj_uh, self._mono._goat.counters.e_fj_d]
        counter_in = self._mono._goat.counters[fast_jack]
    
        if (fast_jack == "fjpur"):
            self._goat_ctl.parameter.id_fj_index = 1
        elif (fast_jack == "fjpuh"):
            self._goat_ctl.parameter.id_fj_index = 2
        elif (fast_jack == "fjpd"):
            self._goat_ctl.parameter.id_fj_index = 3

        # Configure Test Signal
        self._goat_ctl.generator.id_plant.type.NormalWhiteNoise
        self._goat_ctl.generator.id_plant.offset = 0.
        self._goat_ctl.generator.id_plant.amplitude = 500.
        self._goat_ctl.generator.id_plant.duration = duration

        # Configure Filter for test signal
        self._goat_ctl.filter.id_plant.cut_off = 80

        self._goat_ctl.utils._identify_plant(self._goat_ctl.generator.id_plant, counter_in, counters_out, directory=self._results_dir+"/dynamics", file_name=file_name)

    @property
    def radix(self):
        return self._radix_setting.get()
        
    @radix.setter
    def radix(self, value):
        self._radix_setting.set(value)
