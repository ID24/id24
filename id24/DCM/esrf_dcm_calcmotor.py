import numpy
import xcalibu

from bliss.config import settings
from bliss.controllers.motor import CalcController
from bliss.physics.units import ur, units
from bliss.common.utils import add_object_method

from bliss.common.logtools import log_debug
from bliss import global_map
from bliss.controllers.monochromator.calcmotor import MonochromatorCalcMotorBase
from bliss.controllers.monochromator.calcmotor import MonochromatorTrackerCalcMotorBase

from id24.DCM.esrf_dcm_acquisition_master import TrajectoryEnergyTrackerMaster
"""
controller:
    class: BraggRotationCalcMotor
    approximation: 0.01
    axes:
        - name: $mbrag
          tags: real bragg
        - name: $msafe
          tags: real safety
        - name: $mcoil
          tags: real coil

        - name: bragg_rot
          tags: bragg_rotation
"""

class BraggRotationCalcMotor(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.approx = float(self.config.get("approximation"))

        global_map.register(self)

    def pseudos_are_moving(self):
        for axis in self.pseudos:
            if axis.is_moving:
                return True
        return False

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        if (numpy.isclose(reals_dict["bragg"] , reals_dict["safety"], atol=self.approx) and \
           numpy.isclose(reals_dict["bragg"], reals_dict["coil"], atol=self.approx)) or \
           self.pseudos_are_moving():
            pseudos_dict["bragg_rotation"] = reals_dict["bragg"]
        else:
            pseudos_dict["bragg_rotation"] = numpy.nan

        _b = reals_dict["bragg"]
        _s = reals_dict["safety"]
        _c = reals_dict["coil"]
        _br = pseudos_dict["bragg_rotation"]
        log_debug(self, "calc_from_real(): %g %g %g -> %g", _b, _s, _c, _br)

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        if not numpy.isnan(pseudos_dict["bragg_rotation"]).any():
            reals_dict["bragg"]  = pseudos_dict["bragg_rotation"]
            reals_dict["safety"] = pseudos_dict["bragg_rotation"]
            reals_dict["coil"]   = pseudos_dict["bragg_rotation"]
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        _br = pseudos_dict["bragg_rotation"]
        _b = reals_dict["bragg"]
        _s = reals_dict["safety"]
        _c = reals_dict["coil"]
        log_debug(self, "calc_to_real(): %g -> %g %g %g", _br, _b, _s, _c)

        return reals_dict

class SafetyCoilCalcMotor(CalcController):

    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)

        self.approx = float(self.config.get("approximation"))

    def pseudos_are_moving(self):
        for axis in self.pseudos:
            if axis.is_moving:
                return True
        return False

    def calc_from_real(self, reals_dict):

        pseudos_dict = {}

        # Return nan if real motors are not close enough.
        if numpy.isclose(reals_dict["coil"], reals_dict["safety"], atol=self.approx) or \
           self.pseudos_are_moving():
            pseudos_dict["safe_coil"] = reals_dict["safety"]
        else:
            pseudos_dict["safe_coil"] = numpy.nan

        return pseudos_dict

    def calc_to_real(self, pseudos_dict):

        reals_dict = {}

        if not numpy.isnan(pseudos_dict["safe_coil"]).any():
            reals_dict["safety"] = pseudos_dict["safe_coil"]
            reals_dict["coil"]   = pseudos_dict["safe_coil"]
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict

class EnergyTrackerCalcMotor(MonochromatorTrackerCalcMotorBase):
    """
    Energy + Trackers Calculated motor
    Trajectory: Energy constant speed
    """
    def master2energy(self, energy_user):
        return energy_user
        
    def master2energy_dial(self, energy_user):
        bragg_motor = self._mono._motors["bragg"]
        bragg_offset = bragg_motor.offset
        bragg_user = self._mono.energy2bragg(energy_user)
        bragg_dial = bragg_user - bragg_offset
        energy_dial = self._mono.bragg2energy(bragg_dial)
        return energy_dial
        
    def get_acquisition_master(self, start, stop, nb_points, time_per_point):
        return TrajectoryEnergyTrackerMaster(self._calc_motor, start, stop, nb_points, time_per_point)

class BraggTrackerCalcMotor(MonochromatorTrackerCalcMotorBase):
    """
    Bragg + Trackers Calculated motor
    Trajectory: Bragg constant speed
    """    
    def master2energy(self, bragg_user):
        return self._mono.bragg2energy(bragg_user)
        
    def master2energy_dial(self, bragg_user):
        bragg_motor = self._mono._motors["bragg"]
        bragg_offset = bragg_motor.offset
        bragg_dial = bragg_user - bragg_offset
        return self._mono.bragg2energy(bragg_dial)
        
    def get_acquisition_master(self, start, stop, nb_points, time_per_point):
        return TrajectoryEnergyTrackerMaster(self._calc_motor, start, stop, nb_points, time_per_point)

class UndulatorTrackerCalcMotor(MonochromatorTrackerCalcMotorBase):
    """
    Energy + Trackers Calculated motor
    Trajectory: Undulator constant speed
    """                
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._master_tracker_tag = f"{self._master_tag}_tracker"
        
    def master2energy(self, undulator_user):
        energy_dial = self._mono._constant_speed_motor.tracking.tracker2energy(undulator_user)
        bragg_motor = self._mono._motors["bragg"]
        bragg_offset = bragg_motor.offset
        bragg_dial = self._mono.energy2bragg(energy_dial)
        bragg_user = bragg_dial + bragg_offset
        return self._mono.bragg2energy(bragg_user)
        
    def master2energy_dial(self, undulator_user):
        if self._mono._constant_speed_motor is not None:
            return  self._mono._constant_speed_motor.tracking.tracker2energy(undulator_user)
        else:
            return numpy.nan
        
    def get_acquisition_master(self, start, stop, nb_points, time_per_point):
        return TrajectoryEnergyTrackerMaster(self._calc_motor, start, stop, nb_points, time_per_point)

class UndulatorTrackerTrajectoryMovedByEnergyCalcMotor(MonochromatorCalcMotorBase):
    """Energy Calculation Motor"""

    def calc_from_real(self, real_positions):
        pseudos_dict = {}

        if self._mono is not None and self._mono._xtals.xtal_sel is not None:
            undulator = self._mono.trajectory._undulator_master
            if undulator is None:
                ene = numpy.nan
            else:
                ene = undulator.tracking.tracker2energy(real_positions["undulator_trajectory"])
        else:
            ene = numpy.nan
        pseudos_dict["energy_undulator_trajectory"] = ene
        return pseudos_dict

    def calc_to_real(self, positions_dict):
        reals_dict = {}
        if (
            self._mono is not None
            and self._mono._xtals.xtal_sel is not None
            and not numpy.isnan(positions_dict["energy_undulator_trajectory"]).any()
        ):
            undulator = self._mono.trajectory._undulator_master
            if undulator is None:
                for axis in self.reals:
                    reals_dict[self._axis_tag(axis)] = axis.position
            else:
                reals_dict["undulator_trajectory"] = undulator.tracking.energy2tracker(real_positions["energy_undulator_trajectory"])
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position
        return reals_dict


"""
energy  -> braggfe  -> braggrot -> mbragg
        |            |          -> msafe
        |            |          -> mcoil
        |            |
        |            -> fjsz    -> mfjsur
        |                       -> mfjsuh
        |
        -> trackers -> track1
                    -> track2
                    .
                    .
                    -> trackn
mode: standard / trajectory
"""
class UltimateEnergyCalcMotor(MonochromatorCalcMotorBase):
    
    def config_is_ok(self):
        if self._mono is not None and self._mono._xtals.xtal_sel is not None:
            return True
        return False
        
    def energy_braggfe(self, from_to, pos_dict):
        pass
        
    def braggfe_braggrot(self, from_to, pos_dict):
        pass
        
    def braggrot_bragg(self, from_to, pos_dict):
        pass

    def energy_tracker(self, from_to, pos_dict):
        pass
        
    def calc_from_real(self, real_positions):

        pseudos_dict = {}

        if self.config_is_ok():
            unit = self.reals[0].unit or "deg"
            value = (
                (real_positions["bragg"] * ur.parse_units(self.reals[0].unit))
                .to("deg")
                .magnitude
            )
            ene = self._mono.bragg2energy(value)
            if not numpy.isnan(ene).any():
                unit = self.pseudos[0].unit or "keV"
                ene = (ene * ur.keV).to(unit).magnitude
            pseudos_dict["energy"] = ene
        else:
            pseudos_dict["energy"] = numpy.nan

        return pseudos_dict

    def calc_to_real(self, positions_dict):
        reals_dict = {}
        if (
            self._mono is not None
            and self._mono._xtals.xtal_sel is not None
            and not numpy.isnan(positions_dict["energy"]).any()
        ):
            unit = self.pseudos[0].unit or "keV"
            ene = (positions_dict["energy"] * ur.parse_units(unit)).to("keV").magnitude
            bragg = self._mono.energy2bragg(ene)
            unit = self.reals[0].unit or "deg"
            reals_dict["bragg"] = (bragg * ur.deg).to(unit).magnitude
        else:
            for axis in self.reals:
                reals_dict[self._axis_tag(axis)] = axis.position

        return reals_dict


# class DxtalCalcMotor(MonochromatorCalcMotorBase):
    # """
    # Calculation of distance between Xtals:
    # *  from fjsz calc motor

    # """
    # def __init__(self, *args, **kwargs):
        # MonochromatorCalcMotorBase.__init__(self, *args, **kwargs)

    # def calc_from_real(self, reals_dict):

        # pseudos_dict = {}

        # if self.mono is not None:
            # pseudos_dict["dxtal"] = self.mono.xtal2dxtal(reals_dict["xtal"])
        # else:
            # pseudos_dict["dxtal"] = numpy.nan
        # return pseudos_dict

    # def calc_to_real(self, pseudos_dict):

        # reals_dict = {}

        # if not numpy.isnan(pseudos_dict["dxtal"]).any():
            # reals_dict["xtal"]  = self.mono.dxtal2xtal(pseudos_dict["dxtal"])
        # else:
            # for axis in self.reals:
                # reals_dict[self._axis_tag(axis)] = axis.position

        # return reals_dict
