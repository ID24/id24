import numpy
import xcalibu
import glob
import time
import gevent
import os
import warnings
from scipy.signal import kaiserord, lfilter, firwin

from bliss.common.logtools import log_debug, log_info
from bliss.common import plot
from bliss.common.cleanup import cleanup
from bliss.shell.standard import umv, umvr, dscan
from bliss.config import settings
from bliss.config.static import get_config
from bliss.scanning import scan_math
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE

from id24.DCM.esrf_dcm_utils import fjs_sync

"""
General Calibration
    - Fastjack stepper lut (restart_all)
    - access to interferometer and QIC calibration
"""
class EsrfDCMcalib:
    
    def __init__(self, mono):
        self._mono = mono
        self._goat_ctl = self._mono._goat._hwc

        self.fjs = EsrfDCMfjsCalib(mono)
        self.interf = EsrfDCMinterfCalib(mono)
        self.quad = EsrfDCMquadCalib(mono)

    def restart_all(self):

        braggfe = self._mono._motors["bragg_fix_exit"]

        print("Remove Regul offset")
        self._mono.regul.off()

        print("Desacivate LUT on Interferometers")
        self.interf.off()

        print("Homing of Fast Jacks")
        self.fjs._fjs_home()

        print("Synchronize FastJack Position with Speedgoat")
        self.fjs._reset_offset_speedgoat()

        print("Go at 10 degrees for reference position")
        umv(braggfe, 10)
        (rx, ry) = self.fjs.offset
        umv(get_config().get("fjsrx"), rx, get_config().get("fjsry"), ry)
        self._mono.calib.fjs.offset_use = True

        # TODO: RC to optimize the position / flux => fjs_offset
        
        # TODO: it seems the realign interf is not performing well even with long sleep
        # However, when run mannually, it works well
        print("Reset Interferometers and Photodiodes")
        gevent.sleep(2.0)
        self.interf._realign_interf()

        self.interf.on()

"""
FJS Calibration (LUT)
"""
class EsrfDCMfjsCalib:
    def __init__(self, mono):
        self._mono = mono
        self._goat = self._mono._goat
        self._goat_ctl = self._mono._goat._hwc
        
        self._fastdaq = self._goat_ctl.fastdaq.fastdaq
        
        fjs_data_dir = self._mono.config.get("calib_fjs_data_dir", None)
        if fjs_data_dir is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No data directory given for FJS calibration")
        self.data_dir = fjs_data_dir
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_radix", default_value="fjs_default")
        
        bragg_interval = self._mono.config.get("calib_fjs_interval", None)
        if bragg_interval is None:
            raise RuntimeError("esrf_dcm/FjsCalib: No Bragg intervals given for FJS calibration")
        bragg_interval = list(bragg_interval)
        bragg_interval.sort()
        self._bragg_interval =  bragg_interval[::-1]
        # TODO: move to YML
        self._max_coil_range = 18
        #
        # TODO: Check if rotation < 16 degree if not raise error
        #
        
        self._thtraj = get_config().get("thtraj")
        self._fjstraj = get_config().get("fjstraj")
        self._mcoil = get_config().get("mcoil")
        self._fjsz = get_config().get("fjsz")
        
        self._fjs_margin = 0.005 # Small Margin in [mm] when making the LUT
        self._fjs_default_speed = 0.125 # [mm/s]
        
        # TODO: move to YML
        fjs_min, fjs_max = self._fjsz.limits
        self._fjsz_min = 0
        self._fjsz_max = 7

        self._fjs_calib = {}
        for name in ("fjsur", "fjsuh", "fjsd"):
            self._fjs_calib[name] = xcalibu.Xcalibu()
            self._fjs_calib[name].set_calib_name(name)
            self._fjs_calib[name].set_calib_time(0)
            self._fjs_calib[name].set_calib_type("TABLE")
            self._fjs_calib[name].set_reconstruction_method("INTERPOLATION")

        # fjsrx and fjsry offset
        self._offset_use = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_use_offset", default_value=False)
        self._offset_rx = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_rx_offset", default_value=0.0)
        self._offset_ry = settings.SimpleSetting(f"EsrfDCM_fjscalib_{self._mono.name}_ry_offset", default_value=0.0)

        # Load last LUT by default
        # Uncomment when default file is there
        try:
            self.load()
        except:
            pass

    @property
    def radix(self):
        return self._radix_setting.get()
        
    @radix.setter
    def radix(self, value):
        self._radix_setting.set(value)

    @property
    def offset(self):
        return (self._offset_rx.get(), self._offset_ry.get())
        
    @offset.setter
    def offset(self, offset):
        self._offset_rx.set(offset[0])
        self._offset_ry.set(offset[1])
    
    @property
    def offset_use(self):
        return self._offset_use.get()
        
    @offset_use.setter
    def offset_use(self, value):
        self._offset_use.set(value)
        
    def _fjs_home(self):
        """ Homing of all three fast jacks """
        fjsz =get_config().get("fjsz") 
        fjsrx =get_config().get("fjsrx") 
        fjsry =get_config().get("fjsry") 
        
        mfjsur =get_config().get("mfjsur") 
        mfjsuh =get_config().get("mfjsuh") 
        mfjsd =get_config().get("mfjsd") 
        
        umv(fjsrx, 0, fjsry, 0)
        umv(fjsz, 0)
        gevent.sleep(0.2)
        
        umvr(fjsz, 0.01)
        gevent.sleep(0.2)
        
        mfjsur.home()
        gevent.sleep(0.2)
        mfjsuh.home()
        gevent.sleep(0.2)
        mfjsd.home()
        gevent.sleep(0.2)

        fjs_sync()
        
    def load(self, radix=None):        
        if radix is not None:
            self.radix = radix
        filename = f"{self.data_dir}/{self.radix}_{self._mono._xtals.xtal_sel}"
        
        print(f"Load LUT Table from :")
        print(f"    - {filename}_ur.dat")
        print(f"    - {filename}_uh.dat")
        print(f"    - {filename}_d.dat")
        
        self._lut_table_ur = numpy.loadtxt(f"{filename}_ur.dat", dtype=numpy.float, delimiter=" ").transpose()
        self._lut_table_uh = numpy.loadtxt(f"{filename}_uh.dat", dtype=numpy.float, delimiter=" ").transpose()
        self._lut_table_d = numpy.loadtxt(f"{filename}_d.dat", dtype=numpy.float, delimiter=" ").transpose()
        
    def _check_file(self, radix, remove=False):
        
        path_radix = f"{self.data_dir}/{radix}_{self._mono._xtals.xtal_sel}_*.dat"
        file_list = glob.glob(path_radix)
        if len(file_list) != 0:
            if remove:
                for f in file_list:
                    os.remove(f)
            else:
                msg_str = f"Radix {radix} already exists in directory {self._data_dir}"
                raise RuntimeError(msg_str)

    def list(self):
        file_list = glob.glob(f"{self.data_dir}/*")
        print("\n")
        for fullname in file_list:
            filename = fullname.split("/")[-1]
            print(f"    {filename}")
        print("\n")

    def save(self):
        data_ur = numpy.transpose(self._lut_table_ur)
        data_uh = numpy.transpose(self._lut_table_uh)
        data_d = numpy.transpose(self._lut_table_d)
        filename = f"{self.data_dir}/{self.radix}_{self._mono._xtals.xtal_sel}"
        fn_ur = f"{filename}_ur.dat"
        fn_uh = f"{filename}_uh.dat"
        fn_d = f"{filename}_d.dat"
        print(f"Save LUT Table in :")
        print(f"    - {filename}_ur.dat")
        print(f"    - {filename}_uh.dat")
        print(f"    - {filename}_d.dat")
        numpy.savetxt(fn_ur, data_ur)
        numpy.savetxt(fn_uh, data_uh)
        numpy.savetxt(fn_d, data_d)

    def _reset_offset_speedgoat(self):
        mfjsur = get_config().get("mfjsur") 
        mfjsuh = get_config().get("mfjsuh") 
        mfjsd  = get_config().get("mfjsd") 

        fjs_sync()

        self._goat_ctl.parameter.fjsur_reset_steps = int(mfjsur.position * mfjsur.steps_per_unit + 0.5)
        self._goat_ctl.parameter.fjsuh_reset_steps = int(mfjsuh.position * mfjsuh.steps_per_unit + 0.5)
        self._goat_ctl.parameter.fjsd_reset_steps = int(mfjsd.position * mfjsd.steps_per_unit + 0.5)
        
        self._goat_ctl.trigger.reset_fj.trig()

    def build(self, radix, remove=False, fjs_speed=None):
        """ Make Several Scans to build a LUT over the Full Stroke.
        Scans are performed from high bragg angles to low bragg angles.

        Args:
            lut_radix: name of the LUT files
            remove: Should remove LUT with same radix?
        """
        
        # First check if LUT can be saved.
        self._check_file(radix, remove=remove)
        self.radix = radix
        
        if fjs_speed is None:
            used_fjs_speed = self._fjs_default_speed
        else:
            used_fjs_speed = fjs_speed
            
        # Initialize LUT Data
        lut_nb_interval = len(self._bragg_interval)-1
        lut_ur_full = numpy.zeros((2,1))
        lut_uh_full = numpy.zeros((2,1))
        lut_d_full  = numpy.zeros((2,1))

        # Scan Bragg angle
        for idx in range(lut_nb_interval):
            fjs1 = self._mono.bragg2xtal(self._bragg_interval[idx+1])
            fjs2 = self._mono.bragg2xtal(self._bragg_interval[idx])
            data = self._acq_zone_fjs(fjs1, fjs2, speed=used_fjs_speed)

            # Calculate/Save LUT:
            lut_ur, lut_uh, lut_d = self._calc_lut(data)            

            # Concatenate the LUT for UR
            merge_ur = (lut_ur_full[0,-1] + lut_ur[0,0])/2
            lut_ur_full = numpy.concatenate((
                lut_ur_full[:,lut_ur_full[0,:] < merge_ur],
                lut_ur[     :,lut_ur[0,:]      > merge_ur]
            ), axis=1)

            # Concatenate the LUT for UH
            merge_uh = (lut_uh_full[0,-1] + lut_uh[0,0])/2
            lut_uh_full = numpy.concatenate((
                lut_uh_full[:,lut_uh_full[0,:] < merge_uh],
                lut_uh[:,lut_uh[0,:] > merge_uh]
                ), axis=1)
                
            # Concatenate the LUT for D
            merge_d = (lut_d_full[0,-1] + lut_d[0,0])/2
            lut_d_full = numpy.concatenate((
                lut_d_full[:,lut_d_full[0,:] < merge_d],
                lut_d[:,lut_d[0,:] > merge_d]
                ), axis=1)

        # Remove dummy first point of the lut
        lut_ur_full = lut_ur_full[:, 1:]
        lut_uh_full = lut_uh_full[:,1:]
        lut_d_full = lut_d_full[:,1:]
        # Add dummy points
        lut_ur, lut_uh, lut_d = self._add_dummy_points_lut(lut_ur_full, lut_uh_full, lut_d_full)
        
        # Save the LUT
        self._lut_table_ur = lut_ur
        self._lut_table_uh = lut_uh
        self._lut_table_d = lut_d
        self.save()

    def _acq_zone_fjs(self, fj1, fj2, speed):
        """ Make fjstraj scan that will be used for making the LUT.
        The scan is made for increasing energies, i.e. for small to large fast jack positions

        Args:
            fj1: Fast Jack position 1 [mm]
            fj2: Fast Jack position 2 [mm]
            speed: Maximum Fast Jack Velocity [mm/s]

        Returns:
            Numpy Array containing the raw measurements acquired by the FastDAQ

        """
        # Make sure fj2 > fj1
        if (fj1 > fj2):
            fj1b = fj1
            fj1 = fj2
            fj2 = fj1b
        assert fj2 > fj1, "fj2 should be greater than fj1"

        # Make sure that mcoil can be disabled
        th1 = self._mono.xtal2bragg(fj1-self._fjs_margin)
        th2 = self._mono.xtal2bragg(fj2+self._fjs_margin)
        if numpy.abs(th1-th2) > self._max_coil_range:
            raise ValueError(f"Stroke ({numpy.abs(th1-th2)}) is too large to disable mcoil (Max: {self._max_coil_range}).")

        # Load Trajectory larger than built LUT
        # self._mono.trajectory.load_fjs(fj1-self.fj_traj_offset, fj2+self.fj_traj_offset, 1000, use_lut=False)
        # Thomas - Changed this otherwise there is some velocity issues (too small)
        self._mono.trajectory.load_fjs(fj1-1.1*self._fjs_margin, fj2+1.1*self._fjs_margin, 5000, use_lut=False)

        # Make sure regul is off (Mode A)
        self._mono.regul.off()

        # Go to Start position
        umv(self._fjstraj, fj1-self._fjs_margin)
        # If scan without mcoil, put mcoil at middle position
        self._fjstraj.disable_axis(axis=self._mcoil)
        print("MCOIL is disabled. Send it at mean position: %.1f [deg]" % ((th1+th2)/2.0))
        umv(self._mcoil, (th1+th2)/2.0)

        # Set maximum velocity
        max_fjs_vel = self._fjstraj._get_max_velocity()
        if (speed < max_fjs_vel):
            self._fjstraj.velocity = speed
        else:
            self._fjstraj.velocity = 0.95 * max_fjs_vel
        print("Fast Jack velocity is set to: %.2f [mm/s]" % self._fjstraj.velocity)

        # Compute approximate time of scan [s]
        scan_time = 2+2*self._fjstraj.acctime+(fj2-fj1)/self._fjstraj.velocity
        print("Scan will last ~ %.1f [s]" % scan_time)

        # create counters
        fast_daq_counters = [
            self._goat.counters.e_fj_ur,
            self._goat.counters.e_fj_uh,
            self._goat.counters.e_fj_d,
            self._goat.counters.fjsur,
            self._goat.counters.fjsuh,
            self._goat.counters.fjsd
        ]
        # Run fast DAC
        self._fastdaq.prepare_time(scan_time, fast_daq_counters)
        self._fastdaq.start(wait=False)

        # Make the scan
        umv(self._fjstraj, fj2+self._fjs_margin)

        # Wait end of fast DAC
        self._fastdaq.wait_finished()

        # If disable mcoil, put it back at end position and re-enable it
        print("Position the MCOIL motor to end position: %.1f [deg]" % th2)
        self._fjstraj.enable_axis(axis=self._mcoil)
        umv(self._fjstraj, fj2)

        # Get Data from Fast DAQ
        daq_data = self._fastdaq.get_data()

        return daq_data

    def _calc_lut(self, daq_data, lut_inc=100e-9):
        """ Computes the LUT for ur/uh/d

        Args:
            data: Acquired data during the LUT scan
            lut_inc: Distance between everypoint of the LUT

        """
        # Get all data in SI units
        fjur = 1e-9*daq_data["fjsur"] # ur Fast Jack Step in [m]
        fjuh = 1e-9*daq_data["fjsuh"] # uh Fast Jack Step in [m]
        fjd  = 1e-9*daq_data["fjsd"] # d Fast Jack Step in [m]
        fjur_e = 1e-9*daq_data["e_fj_ur"] # ur Errors in [m]
        fjuh_e = 1e-9*daq_data["e_fj_uh"] # uh Errors in [m]
        fjd_e  = 1e-9*daq_data["e_fj_d"] # d Errors in [m]

        # Generate Low pass FIR Filter
        nyq_rate = self._mono._Fs / 2.0 # Nyquist Rate [Hz]
        cutoff_hz = 27 # The cutoff frequency of the filter [Hz]

        # Window with specific ripple [dB] and width [Nyquist Fraction]
        N, beta = kaiserord(60, 4/nyq_rate)
        N_delay = int((N-1)/2) # Delay in ticks

        # Fitler generation
        taps = firwin(N, cutoff_hz/nyq_rate, window=('kaiser', beta))

        # Low Pass Filter the Data and compensation of the delay introduced by the FIR filter
        fjur_e_lpf = lfilter(taps, 1.0, fjur_e)[N:]
        fjuh_e_lpf = lfilter(taps, 1.0, fjuh_e)[N:]
        fjd_e_lpf  = lfilter(taps, 1.0, fjd_e)[N:]
        fjur_lpf   = fjur[N_delay+1:-N_delay]
        fjuh_lpf   = fjuh[N_delay+1:-N_delay]
        fjd_lpf    = fjd[ N_delay+1:-N_delay]

        # Get Only Interesting Data (remove points outside the margin)
        filt_array = numpy.where(numpy.logical_or(fjd_lpf < fjd[0] + 1e-3*self._fjs_margin, fjd_lpf > fjd[-1] - 1e-3*self._fjs_margin))

        fjur_e_filt = numpy.delete(fjur_e_lpf, filt_array)
        fjuh_e_filt = numpy.delete(fjuh_e_lpf, filt_array)
        fjd_e_filt  = numpy.delete(fjd_e_lpf , filt_array)
        fjur_filt   = numpy.delete(fjur_lpf, filt_array)
        fjuh_filt   = numpy.delete(fjuh_lpf, filt_array)
        fjd_filt    = numpy.delete(fjd_lpf , filt_array)

        # Build individual LUT
        u,c = numpy.unique(fjur_filt, return_index=True)
        lut_ur = numpy.stack((u-fjur_e_filt[c], u))
        u,c = numpy.unique(fjuh_filt, return_index=True)
        lut_uh = numpy.stack((u-fjuh_e_filt[c], u))
        u,c = numpy.unique(fjd_filt, return_index=True)
        lut_d = numpy.stack((u-fjd_e_filt[c], u))

        return 1e3*lut_ur, 1e3*lut_uh, 1e3*lut_d
        
    def _add_dummy_points_lut(self, lut_ur, lut_uh, lut_d):
        """ Add (theoretical) points at beginning and ending of LUT.
            These points are here only to make sure that there is no problem when loading a trajectory.
        """

        lut_ur = numpy.concatenate(
            (
                numpy.concatenate((numpy.arange(self._fjsz_min, lut_ur[0][0], 1)[numpy.newaxis,:], lut_ur[1][0] - lut_ur[0][0] + numpy.arange(self._fjsz_min, lut_ur[0][0], 1)[numpy.newaxis,:])),
                lut_ur,
                numpy.concatenate((numpy.arange(lut_ur[0][-1]+1, self._fjsz_max, 1)[numpy.newaxis,:], lut_ur[1][-1] - lut_ur[0][-1] + numpy.arange(lut_ur[0][-1]+1, self._fjsz_max, 1)[numpy.newaxis,:])),
            ),
            axis=1
        )
        lut_uh = numpy.concatenate(
            (
                numpy.concatenate((numpy.arange(self._fjsz_min, lut_uh[0][0], 1)[numpy.newaxis,:], lut_uh[1][0] - lut_uh[0][0] + numpy.arange(self._fjsz_min, lut_uh[0][0], 1)[numpy.newaxis,:])),
                lut_uh,
                numpy.concatenate((numpy.arange(lut_uh[0][-1]+1, self._fjsz_max, 1)[numpy.newaxis,:], lut_uh[1][-1] - lut_uh[0][-1] + numpy.arange(lut_uh[0][-1]+1, self._fjsz_max, 1)[numpy.newaxis,:])),
            ),
            axis=1
        )
        lut_d = numpy.concatenate(
            (
                numpy.concatenate((numpy.arange(self._fjsz_min, lut_d[0][0], 1)[numpy.newaxis,:], lut_d[1][0] - lut_d[0][0] + numpy.arange(self._fjsz_min, lut_d[0][0], 1)[numpy.newaxis,:])),
                lut_d,
                numpy.concatenate((numpy.arange(lut_d[0][-1]+1, self._fjsz_max, 1)[numpy.newaxis,:], lut_d[1][-1] - lut_d[0][-1] + numpy.arange(lut_d[0][-1]+1, self._fjsz_max, 1)[numpy.newaxis,:])),
            ),
            axis=1
        )
        
        return lut_ur, lut_uh, lut_d

    #
    # Calibration are used to calculate trajectories
    # This function load the last table in the Fastjack stepper Calibration objects
    #
    def lut_to_calib(self):
        self._fjs_calib["fjsur"].set_raw_x(numpy.copy(self._lut_table_ur[0]))
        self._fjs_calib["fjsur"].set_raw_y(numpy.copy(self._lut_table_ur[1]))

        self._fjs_calib["fjsuh"].set_raw_x(numpy.copy(self._lut_table_uh[0]))
        self._fjs_calib["fjsuh"].set_raw_y(numpy.copy(self._lut_table_uh[1]))

        self._fjs_calib["fjsd"].set_raw_x(numpy.copy(self._lut_table_d[0]))
        self._fjs_calib["fjsd"].set_raw_y(numpy.copy(self._lut_table_d[1]))

                
class EsrfDCMinterfCalib:
    def __init__(self, mono):
        self._mono = mono
        self._goat_ctl = self._mono._goat._hwc
        
        interf_data_dir = self._mono.config.get("calib_interf_data_dir", None)
        if interf_data_dir is None:
            raise RuntimeError("esrf_dcm/InterfCalib: No data directory given for INTERF calibration")
        self.data_dir = interf_data_dir
        
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_interfcalib_{self._mono.name}", default_value="interf_default")

        # TODO - Automatically load calibration tables specified in the YML file
        #self.load(lut="ry", radix="interf_default")
        #self.load(lut="dz", radix="interf_default")
        #self.load(lut="rx", radix="interf_first")

    @property
    def radix(self):
        return self._radix_setting.get()
        
    @radix.setter
    def radix(self, value):
        self._radix_setting.set(value)
        
    def _check_file(self, radix, remove=False):
        path_radix = f"{self.data_dir}/{radix}_{self._mono._xtals.xtal_sel}_*.dat"
        file_list = glob.glob(path_radix)
        if len(file_list) != 0:
            if remove:
                for f in file_list:
                    os.remove(f)
            else:
                msg_str = f"Radix {radix} already exists in directory {self._data_dir}"
                raise RuntimeError(msg_str)

    def load(self, lut=None, radix=None):
        
        if radix is not None:
            self.radix = radix
            
        xtal = self._mono._xtals.xtal_sel
        filename = f"{self.data_dir}/{self.radix}_{xtal}"
        
        if lut is None or lut == "rx":
            data = numpy.loadtxt(f"{filename}_rx.dat", dtype=numpy.float, delimiter=" ").transpose()
            self._calib2speedgoat(self._lut_id("rx"), data)
        
        if lut is None or lut == "ry":
            data = numpy.loadtxt(f"{filename}_ry.dat", dtype=numpy.float, delimiter=" ").transpose()
            self._calib2speedgoat(self._lut_id("ry"), data)
        
        if lut is None or lut == "dz":
            data = numpy.loadtxt(f"{filename}_dz.dat", dtype=numpy.float, delimiter=" ").transpose()
            self._calib2speedgoat(self._lut_id("dz"), data)
        
    def state(self):
        rx_state = self._mono._goat_ctl.lut._luts[self._lut_id("rx")].enabled
        ry_state = self._mono._goat_ctl.lut._luts[self._lut_id("ry")].enabled
        dz_state = self._mono._goat_ctl.lut._luts[self._lut_id("dz")].enabled
        return (rx_state, ry_state, dz_state)
        
    def on(self, lut=None):
        if lut is None or lut == "rx":
            self._mono._goat_ctl.lut._luts[self._lut_id("rx")].enable()
        if lut is None or lut == "ry":
            self._mono._goat_ctl.lut._luts[self._lut_id("ry")].enable()
        if lut is None or lut == "dz":
            self._mono._goat_ctl.lut._luts[self._lut_id("dz")].enable()
        
    def off(self, lut=None):
        if lut is None or lut == "rx":
            self._mono._goat_ctl.lut._luts[self._lut_id("rx")].disable()
        if lut is None or lut == "ry":
            self._mono._goat_ctl.lut._luts[self._lut_id("ry")].disable()
        if lut is None or lut == "dz":
            self._mono._goat_ctl.lut._luts[self._lut_id("dz")].disable()

    def _realign_interf(self):
        """
            Function to reset the interferometers to zeros.
            After realign, xtal1 = 0, metro_frame=0 and xtal2 corresponds to the wanted fjsz
        """
        self._goat_ctl.trigger.reset_interf.trig()
    
    """
    Calibration of rx/ry using Quad Ionisation chamber
    """
    def quad_scan(self, radix, ene_from, ene_to, npoints, time_per_point, method="step_scan", undulator=None):
        
        # Check method
        if method not in ["step_scan", "energy_cst", "undulator_cst"]:
            raise RuntimeError("method must be one of step_scan/energy_cst/undulator_cst")
        
        # Check undulator if method == undulator_cst
        if method == "undulator_cst":
            if undulator is None:
                raise RuntimeError(f"No undulator defined")
            if undulator not in self._mono.tracking._motors.values():
                raise RuntimeError(f"Undulator \"{undulator.name}\" is not a DCM tracked motor")
            if not undulator.tracking.state:
                raise RuntimeError(f"Undulator \"{undulator.name}\" is not tracked")
                
        # Get bliss object
        display = get_config().get("display")
        contscan = get_config().get("contscan")
        if method == "step_scan" or method == "energy_cst":
            traj_mot = get_config().get("Emonotraj")
            traj_und_mot = get_config().get("Emonotrajund")
        if method == "undulator_cst":
            traj_mot = get_config().get("undutraj")
            traj_und_mot = get_config().get("undutrajund")
            
        self._mono._constant_speed_motor = undulator

        # Check energy is increasing
        ene_start = ene_from
        ene_stop = ene_to
        if ene_to < ene_from:
            ene_start = traj_start = ene_to
            ene_stop = traj_stop = ene_from
        if method == "undulator_cst":
            und_start = undulator.tracking.energy2tracker(ene_start)
            und_stop = undulator.tracking.energy2tracker(ene_stop)
            traj_start = und_start
            traj_stop = und_stop
        
        # Get undershoot in case of contscan
        if method == "energy_cst" or method == "undulator_cst":
            undershoot = 2.0 * contscan.get_undershoot(
                traj_und_mot,
                traj_start,
                traj_stop,
                npoints,
                time_per_point
            )            
            
        # Load trajectory
        if method == "energy_cst" or method == "step_scan":
            self._mono.trajectory.load_energy(ene_start-undershoot, ene_stop+undershoot, 5000, use_lut=True)
        if method == "undulator_cst":
            from_ene = undulator.tracking.tracker2energy(und_start-undershoot)
            to_ene = undulator.tracking.tracker2energy(und_stop+undershoot)
            self._mono.trajectory.load_undulator(undulator, from_ene, to_ene, 5000,  use_lut=True)
        umv(traj_und_mot, traj_start)
            
        self._mono.regul.on() # commented KL 12/11/2022
        self._mono.regul.on() # commented KL 12/11/2022
        
        
        if method == "step_scan":
            sc = ascan(
                Emonotrajund,
                ene_start,
                ene_stop,
                npoints,
                time_per_point,
                self._mono._goat.counters.beam_Ry_filtered,
                self._mono._goat.counters.beam_Rz_filtered,
                self._mono._goat.counters.bragg,
            )
        if method in ["energy_cst", "undulator_cst"]:
            contscan.motor(
                traj_und_mot,
                traj_start,
                traj_stop,
                npoints,
                time_per_point,
                self._mono._goat.counters.beam_Ry_filtered,
                self._mono._goat.counters.beam_Rz_filtered,
                self._mono._goat.counters.bragg,
                self._mono._goat.counters.quad_1z_nm_filtered, #KL
            )
            sc = contscan._scan_obj

        quad_data_ry = numpy.copy(sc.get_data()["goat_scc:beam_Ry_filtered"])
        quad_data_rz = numpy.copy(sc.get_data()["goat_scc:beam_Rz_filtered"])
        quad_data_tz = numpy.copy(sc.get_data()["goat_scc:quad_1z_nm_filtered"]) #KL
        
        self._quad_data_bragg = numpy.copy(sc.get_data()["goat_scc:bragg"])
        
        # CALCULATIONS TO CHANGED
        self._quad_data_ry_err = 0.5 * quad_data_ry 
        self._quad_data_rx_err = 0.5 * quad_data_rz / numpy.sin(numpy.radians(self._quad_data_bragg))
        self._quad_data_dz_err = -0.5 * quad_data_tz / numpy.cos(numpy.radians(self._quad_data_bragg)) #KL
        
        
        #####
        
        display.plot_curve("DCM rx", "DCM rx", "InterfCalibQuadx", self._quad_data_bragg, self._quad_data_rx_err, "Bragg", "Rx Error")
        display.plot_curve("DCM ry", "DCM ry", "InterfCalibQuady", self._quad_data_bragg, self._quad_data_ry_err, "Bragg", "Ry error")

        rx_data = numpy.zeros((2, len(self._quad_data_rx_err)))
        rx_data[0] = self._quad_data_bragg
        rx_data[1] = self._quad_data_rx_err
        self.save(radix, "rx", rx_data)
        
        ry_data = numpy.zeros((2, len(self._quad_data_ry_err)))
        ry_data[0] = self._quad_data_bragg
        ry_data[1] = self._quad_data_ry_err
        #self.save(radix, "ry", ry_data) # Commented out in order not to cause confusion.
        
        
        dz_data = numpy.zeros((2, len(self._quad_data_dz_err))) #KL
        dz_data[0] = self._quad_data_bragg #KL
        dz_data[1] = self._quad_data_dz_err #KL
        self.save(radix, "dz", dz_data) #KL
        

    def bas_scan(self, radix, ene_from, ene_to, npoints, time_per_point, bas_distance = 4.775e9, method="step_scan", undulator=None): # All KL in this procedure
        
        if bas_distance > 8e9 or bas_distance < 3e9:
            print(GREEN("Wrong basler distance, it must be between 3e9 and 8e9 (nm)"))
            raise ValueError("Wrong basler distance, it must be between 3e9 and 8e9 (nm)")
        
        # Check method
        if method not in ["step_scan", "energy_cst", "undulator_cst"]:
            raise RuntimeError("method must be one of step_scan/energy_cst/undulator_cst")
        
        # Check undulator if method == undulator_cst
        if method == "undulator_cst":
            if undulator is None:
                raise RuntimeError(f"No undulator defined")
            if undulator not in self._mono.tracking._motors.values():
                raise RuntimeError(f"Undulator \"{undulator.name}\" is not a DCM tracked motor")
            if not undulator.tracking.state:
                raise RuntimeError(f"Undulator \"{undulator.name}\" is not tracked")
                
        # Get bliss object
        display = get_config().get("display")
        contscan = get_config().get("contscan")
        if method == "step_scan" or method == "energy_cst":
            traj_mot = get_config().get("Emonotraj")
            traj_und_mot = get_config().get("Emonotrajund")
        if method == "undulator_cst":
            traj_mot = get_config().get("undutraj")
            traj_und_mot = get_config().get("undutrajund")
        
        bas_eh2_diag=get_config().get("bas_eh2_diag")  # Is that correct?
        
        self._mono._constant_speed_motor = undulator

        # Check energy is increasing
        ene_start = ene_from
        ene_stop = ene_to
        if ene_to < ene_from:
            ene_start = traj_start = ene_to
            ene_stop = traj_stop = ene_from
        if method == "undulator_cst":
            und_start = undulator.tracking.energy2tracker(ene_start)
            und_stop = undulator.tracking.energy2tracker(ene_stop)
            traj_start = und_start
            traj_stop = und_stop
        
        # Get undershoot in case of contscan
        if method == "energy_cst" or method == "undulator_cst":
            undershoot = 2.0 * contscan.get_undershoot(
                traj_und_mot,
                traj_start,
                traj_stop,
                npoints,
                time_per_point
            )            
            
        # Load trajectory
        if method == "energy_cst" or method == "step_scan":
            self._mono.trajectory.load_energy(ene_start-undershoot, ene_stop+undershoot, 5000, use_lut=True)
        if method == "undulator_cst":
            from_ene = undulator.tracking.tracker2energy(und_start-undershoot)
            to_ene = undulator.tracking.tracker2energy(und_stop+undershoot)
            self._mono.trajectory.load_undulator(undulator, from_ene, to_ene, 5000,  use_lut=True)
        umv(traj_und_mot, traj_start)
            
        self._mono.regul.on() 
        self._mono.regul.on() 
        
        
        if method == "step_scan":
            sc = ascan(
                Emonotrajund,
                ene_start,
                ene_stop,
                npoints,
                time_per_point,
                self._mono._goat.counters.beam_Ry_filtered,
                self._mono._goat.counters.beam_Rz_filtered,
                self._mono._goat.counters.bragg,
                bas_eh2_diag.bpm, #KL
            )
        if method in ["energy_cst", "undulator_cst"]:
            #umv(dcmssvg, 2)  #OM
            contscan.motor(
                traj_und_mot,
                traj_start,
                traj_stop,
                npoints,
                time_per_point,
                self._mono._goat.counters.beam_Ry_filtered,
                self._mono._goat.counters.beam_Rz_filtered,
                self._mono._goat.counters.bragg,
                self._mono._goat.counters.quad_1z_nm_filtered, #KL
                bas_eh2_diag.bpm, #KL
            )
            sc = contscan._scan_obj
            #umv(dcmssvg, 0.05)  #OM
            #contscan.motor(
             #   traj_und_mot,
             #   traj_start,
             #   traj_stop,
             #   npoints,
             #   time_per_point,
             #   self._mono._goat.counters.beam_Ry_filtered,
             #   self._mono._goat.counters.beam_Rz_filtered,
             #   self._mono._goat.counters.bragg,
             #   self._mono._goat.counters.quad_1z_nm_filtered, #OM
             #   bas_eh2_diag.bpm, #OM
            #)
            #scangle = contscan._scan_obj  #OM
            #umv(dcmssvg, 2)
            

        #bas_distance = 4.775e9 # nm Now transferred to the arguments of the procedure
        #dcmss_distance = bas_distance -1.8e9  #OM; dcm distance - 1.8m - nm
        bas_gain_y = -1.875e3  # nm per pixel. Higher values = beam is lower, image on the bpm is lower
        bas_gain_x = -1.875e3   # nm per pixel. Higher values = beam is towards the ring, image on the bpm moves to the right
        
        x0=numpy.mean(sc.get_data()["x"])
        y0=numpy.mean(sc.get_data()["y"])    #OM: le faisceau n' est pas forcement a cette position
        #y0angle=numpy.mean(scangle.get_data()["y"])    #OM
        
        print(GREEN('x0= %.3f' % x0))
        print(GREEN('y0= %.3f' % y0))

        
        quad_data_ry = (numpy.copy(sc.get_data()["y"]) - y0)*bas_gain_y/bas_distance*1e9 #nrad THIS IS USELESS, TO BE DELETED
        
        quad_data_rz = (numpy.copy(sc.get_data()["x"]) - x0)*bas_gain_x/bas_distance*1e9 #nrad
        quad_data_tz = (numpy.copy(sc.get_data()["y"]) - y0)*bas_gain_y
        #quad_data_tzangle = (numpy.copy(scangle.get_data()["y"]) - y0angle)*bas_gain_y/dcmss_distance  #OM, rad
        
        self._quad_data_bragg = numpy.copy(sc.get_data()["goat_scc:bragg"])
        
        # CALCULATIONS TO CHANGED
        self._quad_data_ry_err = 0.5 * quad_data_ry 
        self._quad_data_rx_err = 0.5 * quad_data_rz / numpy.sin(numpy.radians(self._quad_data_bragg))
        self._quad_data_dz_err = -0.5 * quad_data_tz / numpy.cos(numpy.radians(self._quad_data_bragg)) #KL
        #self._quad_data_dz_err = -0.5 * (quad_data_tz+quad_data_tzangle*bas_distance) / numpy.cos(numpy.radians(self._quad_data_bragg))  #OM
        
        
        #####
        
        display.plot_curve("DCM rx", "DCM rx", "InterfCalibQuadx", self._quad_data_bragg, self._quad_data_rx_err, "Bragg", "Rx Error")
        display.plot_curve("DCM ry", "DCM ry", "InterfCalibQuady", self._quad_data_bragg, self._quad_data_ry_err, "Bragg", "Ry error")

        rx_data = numpy.zeros((2, len(self._quad_data_rx_err)))
        rx_data[0] = self._quad_data_bragg
        rx_data[1] = self._quad_data_rx_err
        self.save(radix, "rx", rx_data)
        
        ry_data = numpy.zeros((2, len(self._quad_data_ry_err)))
        ry_data[0] = self._quad_data_bragg
        ry_data[1] = self._quad_data_ry_err
        #self.save(radix, "ry", ry_data) # Commented out in order not to cause confusion.
        
        
        dz_data = numpy.zeros((2, len(self._quad_data_dz_err))) #KL
        dz_data[0] = self._quad_data_bragg #KL
        dz_data[1] = self._quad_data_dz_err #KL
        self.save(radix, "dz", dz_data) #KL

        

    """
    Calibration ry
    """
    def _single_rocking_curve(self, from_fjsry, to_fjsry, nbp, inttime):
        fjsry = get_config().get("fjsry")
        diode = get_config().get("dcmoh3outbv").diode
        ry_filtered = self._mono._goat.counters.r_y_filtered
        s = dscan(fjsry, from_fjsry, to_fjsry, nbp, inttime, diode, ry_filtered)
        ry_data = s.get_data()["goat_scc:r_y_filtered"]
        diode_data = s.get_data()["dcmoh3outbv:diode"]
        cen = scan_math.cen(ry_data, diode_data)
        return cen[0]
        
    def _ry_rocking_curves(self, from_th, to_th, nbp, calib=False, nbp_rc=50, radix=None):
        if from_th < to_th:
            aux = from_th
            from_th = to_th
            to_th = aux
            
        thtrajund = get_config().get("thtrajund")

        self._mono.trajectory.load_bragg(to_th-0.1, from_th+0.1, 5000, use_lut=True)

        self._mono.regul.off()
        
        dcmoh3outbv = get_config().get("dcmoh3outbv")
        dcmoh3outbv.screen_in()
        
        if calib:
            self.off(lut="ry")
        else:
            self.on(lut="ry")
        
        delta = (to_th - from_th) / nbp
        self._calib_fjpry = numpy.zeros((2, nbp+1))
        for idx in range(nbp+1):
            print(GREEN(f"RC #{idx+1} / ({nbp+1})\n"))
            self._calib_fjpry[0][nbp-idx] = from_th + idx * delta
            umv(thtrajund, self._calib_fjpry[0][nbp-idx])
            self._calib_fjpry[1][nbp-idx] = self._single_rocking_curve(-0.03, 0.03, nbp_rc, 0.1) # interval was -0.08 0.08 before. Test smaller one to get better precision.
        
        dcmoh3outbv.screen_out()

        if calib:
            self._calib2speedgoat(self._lut_id("ry"), self._calib_fjpry)
            self.save(radix, "ry", self._calib_fjpry)
    
    def save(self, radix, lut, data):
        if radix is not None:
            xtal = self._mono._xtals.xtal_sel
            filename = f"{self.data_dir}/{radix}_{xtal}_{lut}.dat"
            try:
                os.remove(filename)
            except:
                pass
            numpy.savetxt(filename, data.transpose())
            
    def _lut_id(self, lut):
        xtal = self._mono._xtals.xtal_sel
        speedgoat_id = self._mono._xtals.get_xtals_config("speedgoat_id")[xtal]
        return f"{speedgoat_id}_{lut}"

    def _calib2speedgoat(self, lut_id, data):
        
        calib_nb = self._goat_ctl.lut._luts[lut_id].length
        if calib_nb < len(data[0]):
            raise RuntimeError(f"Calibration {lut_id}: Too many data points for speedgoat arrays")
        
        data_x = numpy.zeros(calib_nb)
        data_x[calib_nb-len(data[0]):calib_nb] = data[0]
        
        data_y = numpy.zeros(calib_nb)
        data_y[calib_nb-len(data[0]):calib_nb] = data[1]
        
        self._goat_ctl.lut._luts[lut_id].disable()
        self._goat_ctl.lut._luts[lut_id].x_raw = data_x
        self._goat_ctl.lut._luts[lut_id].y_raw = data_y
        self._goat_ctl.lut._luts[lut_id].enable()
                
class EsrfDCMquadCalib:
    #
    # Ionizsation chamber gain calibration to give to speedgoat the V->nm coeff
    #
    # TODO: Get KYril macros to make the scan
    #
    def __init__(self, mono):
        self._mono = mono
        self._goat_ctl = self._mono._goat._hwc
        
        quad_data_dir = self._mono.config.get("calib_quad_data_dir", None)
        if quad_data_dir is None:
            raise RuntimeError("esrf_dcm/QuadCalib: No data directory given for QUAD calibration")
        self.data_dir = quad_data_dir
        
        self._radix_setting = settings.SimpleSetting(f"EsrfDCM_quadcalib_{self._mono.name}", default_value="quad_default")

        self._data_y = None
        self._data_z = None
        
        # TODO - Automatically load calibration tables specified in the YML file
        #self.load(lut="ry", radix="interf_default")
        #self.load(lut="dz", radix="interf_default")
        #self.load(lut="rx", radix="interf_first")

    @property
    def radix(self):
        return self._radix_setting.get()
        
    @radix.setter
    def radix(self, value):
        self._radix_setting.set(value)
        
    def _check_file(self, radix, remove=False):
        path_radix = f"{self.data_dir}/{radix}_{self._mono._xtals.xtal_sel}_*.dat"
        file_list = glob.glob(path_radix)
        if len(file_list) != 0:
            if remove:
                for f in file_list:
                    os.remove(f)
            else:
                msg_str = f"Radix {radix} already exists in directory {self._data_dir}"
                raise RuntimeError(msg_str)

    def save(self):
        data_y = numpy.transpose(self._data_y)
        data_z = numpy.transpose(self._data_z)

        filename = f"{self.data_dir}/{self.radix}"
        fn_y = f"{filename}_1y.dat"
        fn_z = f"{filename}_1z.dat"
        print(f"Save QIC calibration table in :")
        print(f"    - {filename}_1y.dat")
        print(f"    - {filename}_1z.dat")
        numpy.savetxt(fn_y, data_y, header="Energy(KeV)  Y_gain")
        numpy.savetxt(fn_z, data_z, header="Energy(KeV)  Z_gain")

    def load(self, lut=None, radix=None):
        if radix is not None:
            self.radix = radix

        filename = f"{self.data_dir}/{self.radix}"

        if lut is None or lut == "1y":
            data = numpy.loadtxt(f"{filename}_1y.dat", dtype=numpy.float, delimiter=" ").transpose()
            self._calib2speedgoat(self._lut_id("1y"), data)
        
        if lut is None or lut == "1z":
            data = numpy.loadtxt(f"{filename}_1z.dat", dtype=numpy.float, delimiter=" ").transpose()
            self._calib2speedgoat(self._lut_id("1z"), data)

    def load_constant(self, gain, lut=None):
        """ Just use a simple gain for the QUAD sensors.
        gain should be in V/mm """

        filename = f"{self.data_dir}/{self.radix}"
        
        data = numpy.zeros((2, 50))
        data[0,:] = numpy.linspace(3, 50, 50)
        data[1,:] = 1e3*1/gain

        if lut is None or lut == "1y":
            self._calib2speedgoat(self._lut_id("1y"), data)
        
        if lut is None or lut == "1z":
            self._calib2speedgoat(self._lut_id("1z"), data)
        
    def state(self):
        state_1y = self._mono._goat_ctl.lut._luts[self._lut_id("1y")].enabled
        state_1z = self._mono._goat_ctl.lut._luts[self._lut_id("1z")].enabled
        return (state_1y, state_1z)
        
    def on(self, lut=None):
        if lut is None or lut == "1y":
            self._mono._goat_ctl.lut._luts[self._lut_id("1y")].enable()
        if lut is None or lut == "1z":
            self._mono._goat_ctl.lut._luts[self._lut_id("1z")].enable()
        
    def off(self, lut=None):
        if lut is None or lut == "1y":
            self._mono._goat_ctl.lut._luts[self._lut_id("1y")].disable()
        if lut is None or lut == "1z":
            self._mono._goat_ctl.lut._luts[self._lut_id("1z")].disable()
            
    def _lut_id(self, lut):
        return f"quad_{lut}"        
    
    def build_calibration_data(self, radix, ene_from, ene_to, nbsteps, remove=False, tolerance=0.1):

        # First check if LUT can be saved.
        self._check_file(radix, remove=remove)
        self.radix = radix
        
        # Motors
        Emonotrajund = get_config().get("Emonotrajund")
        dcmkbty = get_config().get("dcmkbty")
        dcmkbtz = get_config().get("dcmkbtz")
        
        # Measurment group
        mg_goat_quad = get_config().get("mg_goat_quad")
        
        # Open dcm shutter
        dcmshut = get_config().get("dcmshut")
        dcmshut.open()
        
        # Load Trajectory
        ene_start = ene_from
        ene_end = ene_to
        if ene_to < ene_from:
            ene_start = ene_to
            ene_end = ene_from
        self._mono.trajectory.load_energy(ene_start-1e-6, ene_end+1e-6, 5000,  use_lut=True)
        
        # Check that beam is centered on QIC
        umv(Emonotrajund,ene_start)
        self._mono.regul.on() # added KL 04/11/2022
        self._mono.regul.on() # addedd KL 04/11/2022
        val_1y = self._mono._goat.counters.quad_1y_filtered.read()
        val_1z = self._mono._goat.counters.quad_1z_filtered.read()
        if not numpy.isclose(val_1y, 0.0, atol=tolerance):
            print(RED("Check QIC centering (Y)"))
            if not numpy.isclose(val_1z, 0.0, atol=tolerance):
                print(RED("Check QIC centering (Z)"))
                return
            return
        
        # Scan
        E_grid = numpy.linspace(ene_start, ene_end, nbsteps+1)
        self._data_y = numpy.zeros((2, nbsteps+1))
        self._data_y[0][:] = E_grid
        self._data_z = numpy.zeros((2, nbsteps+1))
        self._data_z[0][:] = E_grid
        
        self._mono.regul.on()
        self._mono.regul.on()
        
        for ind in range(len(E_grid)):
            headerline = ""
            
            umv(Emonotrajund, E_grid[ind])
            
            scan_ty1 = dscan(dcmkbty, -.1, .1, 20, .1, mg_goat_quad)
            data = scan_ty1.get_data()
            self._data_y[1][ind] = numpy.polyfit(data['axis:dcmkbty'], data['goat_scc:quad_1y_filtered'], 1)[0]
        
            scan_tz1 = dscan(dcmkbtz,-.1,.1,20,.1,mg_goat_quad)    
            data = scan_tz1.get_data()
            self._data_z[1][ind] = numpy.polyfit(data['axis:dcmkbtz'],data['goat_scc:quad_1z_filtered'],1)[0]
        
        self.save()
                
    def _calib2speedgoat(self, lut_id, data):
        calib_nb = self._goat_ctl.lut._luts[lut_id].length
        if calib_nb < len(data[0]):
            raise RuntimeError(f"Calibration {lut_id}: Too many data points for speedgoat arrays")
        
        data_x = numpy.zeros(calib_nb)
        data_x[calib_nb-len(data[0]):calib_nb] = self._mono.energy2bragg(data[0])
        # Sort for increasing bragg angles
        ind = numpy.argsort(data_x)
        
        data_y = numpy.zeros(calib_nb)
        data_y[calib_nb-len(data[0]):calib_nb] = 1e6*1/data[1]

        self._goat_ctl.lut._luts[lut_id].disable()
        self._goat_ctl.lut._luts[lut_id].x_raw = data_x[ind]
        self._goat_ctl.lut._luts[lut_id].y_raw = data_y[ind]
        self._goat_ctl.lut._luts[lut_id].enable()
