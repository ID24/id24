
# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

import numpy
import tabulate
import gevent
import time
from scipy import signal
import glob


from bliss import setup_globals
from bliss.config import settings
from bliss.config.static import get_config
from bliss.shell.standard import umv
from bliss.common.utils import ColorTags, BOLD 
from bliss.common.plot import plot
from bliss.controllers.bliss_controller import BlissController
from bliss.controllers.monochromator.monochromator import MonochromatorFixExit
from bliss.controllers.monochromator.tracker import EnergyTrackingObject

from id24.DCM.esrf_dcm_calib import EsrfDCMcalib
from id24.DCM.esrf_dcm_regul import EsrfDCMregul

class EsrfDCM(MonochromatorFixExit):
    
    def __init__(self, config):
        super().__init__(config)
        
        self._constant_speed_motor = None
        
    """
    Load Configuration
    """
    def _load_config(self):
        super()._load_config()
        """
        Motors
        """
        self._motors["bragg_fix_exit"] = None
        motors_conf = self.config.get("motors", None)        
        for motor_conf in motors_conf:            
            # Bragg Fe
            if "bragg_fix_exit" in motor_conf.keys():
                self._motors["bragg_fix_exit"] = motor_conf.get("bragg_fix_exit")
                self._motors["bragg_fix_exit"].controller._set_mono(self)
        
        """
        Distance between crystals is manage by a motor (xtal) moving 
        second crystals perpendicular to the crystal plane.
        dxtals_at_xtal0 is the distance between first and second crystal when
        the motor (xtal) is at zero position
        Positive motion is intended to get both crystals closer.
        It will be taken into account for all xtals if "dxtal_at_xtal0"
        parameter is present in the monochromator yml file or
        xtal by xtal if present in the xtals yml object
        """
        dxtal_at_xtal0 = self.config.get("dxtal_at_xtal0", None)
        if dxtal_at_xtal0 is not None:
            self._dxtal_at_xtal0 = {}
            self._dxtal_at_xtal0["all"] = float(dxtal_at_xtal0)
        xtal_xtal0 = self._xtals.get_xtals_config("dxtal_at_xtal0")
        if xtal_xtal0 is not None:
            if hasattr(self, "_dxtal_at_xtal0"):
                self._dxtal_at_xtal0.update(self._xtals.get_xtals_config("dxtal_at_xtal0"))        
            else:
                self._dxtal_at_xtal0 = xtal_xtal0
                
        """
        Speedgoat
        """
        self._goat = self.config.get("speedgoat", None)
        self._goat_ctl = self._goat._hwc
        self._goat_init = self.config.get("speedgoat_init", None)
        if self._goat_init is not None:
            self._goat_init._set_mono(self)
        
        """
        get trajectory object
        """
        self.trajectory = self.config.get("trajectory", None)
        if self.trajectory is not None:
            self.trajectory._set_mono(self)

    """
    Initialization
    """
    def _init(self):
        super()._init()
        
        # Calibration
        self.calib = EsrfDCMcalib(self)
        
        # Regulation
        self.regul = EsrfDCMregul(self)
        
        # Trajectory
        
        # Initialyse speedgoat values
        self._goat_init.init()
        
        
    def __info__(self):
        info_str = "\n"
        info_str += self._info_mono()
        info_str += self._info_xtals()
        info_str += self._info_motor_energy()
        info_str += self._info_motor_fix_exit()
        info_str += self._info_motor_tracking()
        info_str += "\n"
        return info_str
        
    def _info_motor_fix_exit(self):
        if self._motors["bragg_fix_exit"] is not None:
            controller = self._motors["bragg_fix_exit"].controller
            # TITLE
            title = [""]
            title.append(self._motors["bragg_fix_exit"].name)
            for axis in controller.reals:
                title.append(axis.name)
            # CALCULATED POSITION ROW
            calculated = ["Calculated"]
            # bragg_fe
            # bragg
            rbragg = controller._tagged["bragg"][0]
            calculated.append(f"{rbragg.position:.3f} {rbragg.unit}")
            calculated.append(f"{rbragg.position:.3f} {rbragg.unit}")
            # xtal
            calc_xtal = self.bragg2xtal(rbragg.position)
            for axis in controller.reals:
                if controller._axis_tag(axis) != "bragg":
                    calculated.append(f"{calc_xtal:.3f} {axis.unit}")
            #
            # CURRENT POSITION ROW
            #
            current = ["Current"]
            # bragg fix exit
            current.append(f"{controller.pseudos[0].position:.3f} {controller.pseudos[0].unit}")
            for axis in controller.reals:
                current.append(f"{axis.position:.3f} {axis.unit}")
                
            info_str = tabulate.tabulate(
                [calculated, current],
                headers=title,
                tablefmt="plain",
                stralign="right",
            )
            return f"{info_str}\n\n"
        else:
            return ""

    """
    Distance between xtals when xtal motor is at 0 position
    """
    def _get_dxtal_at_xtal0(self):
        if hasattr(self, "_dxtal_at_xtal0"):
            if self._xtals.xtal_sel in self._dxtal_at_xtal0.keys():
                return self._dxtal_at_xtal0[self._xtals.xtal_sel]
            if "all" in self._dxtal_at_xtal0.keys():
                return self._dxtal_at_xtal0["all"]
        else:
            raise ValueError(f"Monochromator {self._name}: No dxtal_at_xtal0 defined")
            
    @property
    def dxtal_at_xtal0(self):
        return self._get_dxtal_at_xtal0()
        
    """
    Energy related methods, specific to Fix Exit Mono
    """        
    @property
    def fix_exit_offset(self):
        return self._fix_exit_offset

    @fix_exit_offset.setter
    def fix_exit_offset(self, value):
        self._fix_exit_offset = value
        # TODO: Set fix exit offset value to speedgoat

    def dxtal2xtal(self, dxtal):
        return self.dxtal_at_xtal0 - dxtal

    def xtal2dxtal(self, xtal):
        return self.dxtal_at_xtal0 - xtal

    def bragg2xtal(self, bragg):
        dxtal = self.bragg2dxtal(bragg)
        xtal = self.dxtal2xtal(dxtal)
        return xtal

    def xtal2bragg(self, xtal):
        dxtal = self.xtal2dxtal(xtal)
        bragg = self.dxtal2bragg(dxtal)
        return bragg

    def energy2xtal(self, ene):
        bragg = self.energy2bragg(ene)
        dxtal = self.bragg2dxtal(bragg)
        xtal = self.dxtal2xtal(dxtal)
        return xtal
       
    def xyz2fjs(self, rx, ry, tz):
        leg = get_config().get("fjsz").controller.calc_to_real({"rx": rx, "ry": ry, "tz": tz})
        return (leg["lega"], leg["legb"], leg["legc"])

    def fjs2xyz(self, fjsur, fjsuh, fjsd):
        tab3pos = get_config().get("fjsz").controller.calc_from_real({"lega": fjsur, "legb": fjsuh, "legc": fjsd})
        return (tab3pos["rx"], tab3pos["ry"], tab3pos["tz"])
 
class ID24undulatorTrackingObject(EnergyTrackingObject):     
    
    def __init__(self, config):
        super().__init__(config)
        
        #Constants
        me = 9.1093836e-31  # kg
        qe = 1.6021766e-19  
        hbar = 1.054572e-34 # m^2 kg/s
        c = 299792458 # m/s

        ### Source parameters
        Ering = 6.0 #GeV
        und_step = 0.05001 ### time quant of acceleration of undulator gap, seconds

        ### Calculated constants
        gamma = Ering / 0.511e-3 # Ering is in GeV
        self._c1 = qe / (2.0 * numpy.pi * me * c) / 1e3 # To have E in keV and gap in mm
        self._c2 = 8.0 * numpy.pi * hbar * c * gamma**2 / qe # To have E in keV and gap in mm
        
    def _ene2gap(self, energy, config):
        harm = float(config["harmonic"])
        Uperiod = float(config["Uperiod"])
        alpha = float(config["alpha"])
        
        UPPI = Uperiod / numpy.pi
        C2H = self._c2 * harm
        C1A1U = self._c1 * alpha * Uperiod
        gap = UPPI * numpy.log(C1A1U / numpy.sqrt(C2H /(Uperiod * energy) - 2.0))
        #gap = Uperiod / numpy.pi * numpy.log(self._c1 * self._a_un * Uperiod / (numpy.sqrt(self._c2 * harm /(Uperiod * energy) - 2.0)))
        
        return gap
        
    def _gap2ene(self, gap, config):
        harm = float(config["harmonic"])
        Uperiod = float(config["Uperiod"])
        alpha = float(config["alpha"])
        
        UPPI = Uperiod / numpy.pi
        C2H = self._c2 * harm
        C1A1U = self._c1 * alpha * Uperiod

        exp = numpy.exp(gap / UPPI)
        num = 2 + numpy.power(C1A1U / exp, 2)
        energy = C2H / (Uperiod * (num))
        
        return energy

    def _get_energy_from_table(self, axis_name, param_id, track):
        if axis_name not in self._motors.keys():
            raise ValueError(
                f"EnergyTrackingObject->_get_track_from_table: {axis_name} is not a tracker"
            )
        param_ids = self._parameters[axis_name]["param_id"]
        if param_ids is None or param_id not in param_ids.keys():
            raise ValueError(
                f"EnergyTrackingObject->_get_track_from_table: id {param_id} is not a valid id for {axis_name}"
            )
        calib = param_ids[param_id]["table"]["calib"]
        t_min = calib.calib.min_y()
        t_max = calib.calib.max_y()

        if isinstance(track, numpy.ndarray):
            track_arr = numpy.copy(track)
        else:
            track_arr = numpy.array([track], dtype=float)
        ene_arr = numpy.copy(track_arr)
        
        for i in range(track_arr.size):
            if track_arr[i] >= t_min and track_arr[i] <= t_max:
                ene_arr[i] = calib.calib.get_x(track_arr[i])
            else:
                return numpy.nan

        if track.size == 1:
            return ene_arr[0]

        return ene_arr

    def _get_energy_from_polynom(self, axis_name, param_id, track):
        if axis_name not in self._motors.keys():
            raise ValueError(
                f"EnergyTrackingObject->_get_track_from_polynom: {axis_name} is not a tracker"
            )
        param_ids = self._parameters[axis_name]["param_id"]
        if param_ids is None or param_id not in param_ids.keys():
            raise ValueError(
                f"EnergyTrackingObject->_get_track_from_polynom: id {param_id} is not a valid id for {axis_name}"
            )

        if isinstance(track, numpy.ndarray):
            track_arr = numpy.copy(track)
        else:
            track_arr = numpy.array([track], dtype=float)
        ene_arr = numpy.copy(track_arr)
        
        for i in range(track_arr.size):
            step_ene = 0.0
            curr_ene = self._mono.bragg2energy(self._mono._motors["bragg_rotation"].position)
            curr_track = self._get_track_from_polynom(axis_name, param_id,curr_ene)
            curr_diff = abs(track_arr[i]-curr_track)
            found_it = False
            while not found_it:
                if numpy.isclose(track_arr[i], curr_track, atol=0.01):
                    ene_arr[i] = curr_ene
                    found_it = True
                else:
                    if abs(step_ene) < 0.001:
                        ene_arr[i] = curr_ene
                        found_it = True
                    else:
                        if abs(track_arr[i]-curr_track) > curr_diff:
                            step_ene = - (step_ene / 2)
                        curr_ene = curr_ene + step_ene
                        curr_track = self._get_track_from_polynom(axis_name, param_id, curr_ene)
        
        if track.size == 1:
            return ene_arr[0]
        
        return ene_arr
        
class ID24speedgoatInit(BlissController):
    
    def __init__(self, config):
        super().__init__(config)

    """
    Load Configuration
    """
    def _load_config(self):        
        self._bragg_offset = self.config.get("bragg_offset")
        self._trig_input = self.config.get("trig_input")
        self._trig_invert = self.config.get("trig_invert")
        self._Ts = self.config.get("Ts")
        self._Fs = self.config.get("Fs")
        self._quad_1z_V_to_nm = self.config.get("quad_1z_V_to_nm")
        self._quad_1y_V_to_nm = self.config.get("quad_1y_V_to_nm")
        self._quad_2z_V_to_nm = self.config.get("quad_2z_V_to_nm")
    
    """
    Initialization
    """
    def _init(self):
        self._mono = None
        self._goat = None
        
    def _set_mono(self, mono):
        self._mono = mono
        self._goat = self._mono._goat
    
    def init(self):
        if self._mono is not None and self._goat is not None:
            xtal_sel = self._mono._xtals.xtal_sel
            speedgoat_id = self._mono._xtals.get_xtals_config("speedgoat_id")[xtal_sel]
            if speedgoat_id == "ring":
                self._goat._hwc.parameter.xtal = 0
            else:
                self._goat._hwc.parameter.xtal = 1
            self._goat._hwc.parameter.beam_offset = numpy.abs(self._mono.fix_exit_offset)
            self._goat._hwc.parameter.dz_offset_ring = self._mono._dxtal_at_xtal0["Si311"]
            self._goat._hwc.parameter.dz_offset_hall = self._mono._dxtal_at_xtal0["Si111"]
            self._goat._hwc.parameter.bragg_offset = self._bragg_offset
            self._goat._hwc.parameter.trig_input = self._trig_input
            self._goat._hwc.parameter.trig_invert = self._trig_invert
            self._mono._Ts = self._Ts
            self._mono._Fs = self._Fs
            self._goat._hwc.parameter.quad_1z_V_to_nm = self._quad_1z_V_to_nm
            self._goat._hwc.parameter.quad_1y_V_to_nm = self._quad_1y_V_to_nm
            self._goat._hwc.parameter.quad_2z_V_to_nm = self._quad_2z_V_to_nm
