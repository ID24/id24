import sys
import time
import numpy as np

from bliss.config.settings import ParametersWardrobe
from bliss.common.scans import *
from bliss.common.standard import *
from bliss import setup_globals
from bliss.shell.standard import umv
from bliss.scanning.scan import Scan, ScanState
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.acquisition.motor import MotorMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave
from bliss.scanning.acquisition.ct2 import CT2AcquisitionMaster
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.ct2.device import AcqMode
from bliss.controllers.mca.base import TriggerMode, BaseMCA

from bliss.data.nodes.lima import LimaImageChannelDataNode
from bliss.controllers.emh import EMH
from bliss.scanning.toolbox import ChainBuilder
from bliss.common.cleanup import cleanup
from bliss.common.utils import ColorTags, BOLD
from bliss.controllers.counter import CalcCounterController

from id24.DCM.DCM_V0.DCMunduMaster import DCMTrajMaster
from id24.SPEEDGOAT.speedgoat_bliss import SpeedgoatCountersController

class zap(object):

    def __init__(self, name, config_tree):

        self.name = name

        try:
            self.musst = config_tree.get("musst")
        except Exception as zap_exc:
            print(BOLD("ZAP")+": No MUSST card Available")
            self.musst = None

        try:
            #self.opiom_mux = config_tree.raw_get("opiom_mux")
            self.opiom_mux = config_tree.get("opiom_mux")
        except Exception as zap_exc:
            print(BOLD("ZAP")+": No OPIOM card Available")
            self.opiom_mux = None
            
        self.mtraj = config_tree.get("motor_trajectory")

        self.parameters = ParametersWardrobe("zap")

        # Configured parameters

        # Internal parameters
        self.parameters.add("controllers", None)

        # Common scans parameters
        self.parameters.add("points", 0)
        self.parameters.add("time", 0)

        # zap.traj scan parameters
        self.parameters.add("motor", None)
        self.parameters.add("start", 0)
        self.parameters.add("stop", 0)
        
        # Scan context creation
        self.parameters.switch("time")
        self.parameters.switch("traj")
        self.parameters.switch("trajund")
        
        self.timeout = 30
        
        self._scan_state = None
        self._scan_obj = None

    def set_mono(self, mono):
        self.mono = mono
        
    ############################################
    #
    # SCANS
    #
    def time(self, nb_points, time_per_points, *counters, save=True):

        self.parameters.switch("time")
        self.parameters.points = nb_points
        self.parameters.time = time_per_points

        self.chain = AcquisitionChain()

        musst_master = self.master(self.chain, "time")

        if len(counters) > 0:
            self.builder = self.acquisition(self.chain, musst_master, nb_points, time_per_points, counters)
        else:
            self.builder = None
            
        display = zapdisplay(self.musst, self.parameters, self.builder, self.timeout)

        info_scan = {"title": "zaptime %d %g"%(nb_points, time_per_points), "type":"zaptime"}

        scan = Scan(self.chain, name="zaptime", scan_info=info_scan, data_watch_callback=display, save=save)

        if self.builder is not None:
            self.builder.print_tree()
        print(self.chain._tree)

        scan.run()
        return scan

    def traj(self, mot, start_pos, stop_pos, nb_points, time_per_points, *counters, save=True):

        self.parameters.switch("traj")
        self.parameters.points = nb_points
        self.parameters.time = time_per_points
        self.parameters.motor = mot
        self.parameters.start = start_pos
        self.parameters.stop = stop_pos

        self.chain = AcquisitionChain()

        (traj_master, musst_master) = self.master(self.chain, "traj")

        if len(counters) > 0:
            self.builder = self.acquisition(self.chain, musst_master, nb_points, time_per_points, counters)
        else:
            self.builder = None
            
        #zapmot_calc = AcqTheoreticalCalc(acq_name, "zapmot_aux", start_pos, stop_pos, nb_points + 1)
        #zapmot_acq  = CalcAcquisitionDevice('zapmot_cont', (musst_master,),
        #                                 zapmot_calc, zapmot_calc.acq_channels)
        #chain.add(traj_master, zapmot_acq)
        #motor_master.add_external_channel(zapmot_acq, "zapmot_aux", "zapmot")

        # TODO : add external chan to master in order to make
        # flint able to display something by default.

        display = zapdisplay(self.musst, self.parameters, self.builder, self.timeout)

        info_scan = {
            "title": f"zaptraj {mot.name} {start_pos} {stop_pos} {nb_points} {time_per_points}",
            "type": "zaptraj",
            "npoints": nb_points+1
        }
        
        scan = Scan(self.chain, name="zaptraj", scan_info=info_scan, data_watch_callback=display, save=save)

        if self.builder is not None:
            self.builder.print_tree()
        print(self.chain._tree)

        scan.run()
        return scan

    def trajund(self, mot, start_pos, stop_pos, nb_points, time_per_points, *counters, return_pos=None):
        
        with cleanup(self._scan_end):
            self._scan_state = "PREPARING"
        
            eneund = setup_globals.eneund
            ene = setup_globals.dcmene
            current_pos = ene.position

            regul = self.mono.regul.state()
        
            self.parameters.switch("trajund")
            self.parameters.points = nb_points
            self.parameters.time = time_per_points
            self.parameters.motor = mot
            self.parameters.start = start_pos
            self.parameters.stop = stop_pos

            self.chain = AcquisitionChain()

            (traj_master, musst_master) = self.master(self.chain, "trajund")

            self.builder = self.acquisition(self.chain, musst_master, nb_points, time_per_points, counters)

            display = zapdisplay(self.musst, self.parameters, self.builder, self.timeout)

            info_scan = {
                "title": f"zaptraj {mot.name} {start_pos} {stop_pos} {nb_points} {time_per_points}",
                "type": "zaptrajund",
                "npoints": nb_points+1
            }
            self._scan_obj = Scan(self.chain, name="zaptrajund", scan_info=info_scan, data_watch_callback=display)
            
            self._scan_state = "STARTING"
            
            self.builder.print_tree()
            print(self.chain._tree)

            self._scan_obj.run()
        
            eneund_return_pos = None
            if return_pos is not None:
                if return_pos == "current":
                    eneund_return_pos = current_pos
                if return_pos == "start":
                    eneund_return_pos = start_pos
            
                if eneund_return_pos is not None:
                    print(f"\n\nMove eneund to {return_pos} position ({eneund_return_pos})")
                    umv(eneund, eneund_return_pos)
                    eneund.sync_hard()
                    if np.isnan(eneund.position):
                        umv(eneund, eneund_return_pos)
                    umv(mot, eneund_return_pos)
                
                    if regul == 1:
                        self.mono.regul.on()
                   
            self._scan_state = "FINISHED"
        
            return self._scan_obj

    def _scan_end(self):
        if self._scan_state != "FINISHED":
            self._scan_state = "ABORTED"
        
    ############################################
    #
    # Acquisition master / Acquisition Motor
    #
    def master(self, chain, master_type):

        # OPIOM multiplexer to select MUSST trig source.
        if self.opiom_mux is not None:
            self.opiom_mux.switch("SEL_TRIG_OUT", "ZAP_DCM")

        #zmusst = self.parameters.musst
        zmusst = self.musst
        ztime = self.parameters.time
        zpoint = self.parameters.points
        zmotor = self.parameters.motor
        zstart = self.parameters.start
        zstop = self.parameters.stop

        # MUSST: TO BE REMOVED (WORKAROUND)
        zmusst.ABORT
        zmusst.CLEAR

        # MUSST: channel(s) to be read
        store_mask = 0
        store_mask |= (1<<0) # CH0 = time
        store_mask |= (1<<1) # CH1 = trajectory axis

        # MUSST: if mot => synchro pos on channel 1
        #        else synchro time
        if zmotor is None:
            schan = 0
            sdata = int(0.1 * zmusst.get_timer_factor())
            pchan = 0
            pdata = int(ztime * zmusst.get_timer_factor())
            pscandir = 0
            sscandir = 0
        else:
            if zstart == zstop:
                raise RuntimeError("ZAP: Start and Stop positions are the same")

            # musst CH1 is connected to the indexer of mtraj motor.
            # Get its resolution
            encoder_steps_per_unit = self.mtraj.steps_per_unit

            # reset CH1 musst to the current position of the motor to scan
            zmusst.putget("CH CH1 %d"%int(float(self.mtraj.position)*(float(encoder_steps_per_unit))))

            schan = 1
            sdata = int(zstart * encoder_steps_per_unit)
            pchan = 1
            pdata = int(encoder_steps_per_unit * abs(zstop - zstart) / zpoint)
            if zstop > zstart:
                pscandir = 0
                sscandir = 0
            else:
                pscandir = 1
                sscandir = 1

        # MUSST: acquisition master
        musst_master = MusstAcquisitionMaster(
                            zmusst,
                            program='zapintgen.mprg',
                            program_start_name= "HOOK",
                            vars = {
                                'SMODE'    : 1, # 0=external trigger / 1=internal channel
                                'SCHAN'    : schan, # if SMODE=1 0=time 1=ch1 2=ch2 3=ch3 4=ch4 5=ch5 6=ch6
                                'SDATA'    : sdata,
                                'PMODE'    : 1,
                                'PCHAN'    : pchan,
                                'PDATA'    : pdata,
                                'NPOINT'   : int(zpoint),
                                'PSCANDIR' : pscandir, # 0=positive direction 1=negative direction
                                'SSCANDIR' : sscandir, # 0=positive direction 1=negative direction
                                'STOREMSK' : store_mask
                                }
                            )

        # MUSST: memory configuration
        zmusst.set_histogram_buffer_size(2048, 1)       # Histogram (MCA)
        zmusst.set_event_buffer_size(int(524288/16), 1) # Buffers


        ###########################
        # MUSST: acquisition Device
        musst_acq = MusstAcquisitionSlave(zmusst, store_list=["time", "trajmot"])

        # MUSST: add musst in the acquisition chain
        chain.add(musst_master, musst_acq)
        #musst_master.add_external_channel(musst_acq, 'musst243:time', 'mussttime')
        #musst_master.add_external_channel(musst_acq, 'musst243:trajmot', 'trajmot')

        if master_type == "time":
            self.motor_master = None
            return musst_master
        elif master_type == "traj":
            # MOTOR: create master for trajectory motor
            self.motor_master = MotorMaster(zmotor, zstart, zstop, zpoint*ztime)
            chain.add(self.motor_master, musst_master)
            return (self.motor_master, musst_master)
        elif master_type == "trajund":
            self.motor_master = DCMTrajMaster(self.mono, zmotor, zstart, zstop, zpoint, ztime)
            chain.add(self.motor_master, musst_master)
            return (self.motor_master, musst_master)

    ############################################
    #
    # Acquisition Devices
    #
    def acquisition(self, chain, master, nb_points, time_per_points, counters):

        builder = ChainBuilder(counters)
        scan_params = {"npoints": nb_points+1, "count_time": time_per_points}

        ###################################
        # FalconX
        #
        acq_params = {
                "npoints": nb_points,
                "trigger_mode": TriggerMode.SYNC,
                "read_all_triggers": True,
                "block_size": 10
        }

        for node in builder.get_nodes_by_controller_type(BaseMCA):
            node.set_parameters(acq_params=acq_params)
            chain.add(master, node)
            
        ###################################
        # Lima
        #
        acq_params = {
            'acq_nb_frames'       :   nb_points+1,
            'acq_trigger_mode'    :   'EXTERNAL_TRIGGER_MULTI',
            'acq_expo_time'       :   0.001,
        }
        
        for node in builder.get_nodes_by_controller_type(Lima):
            acq_params["acq_expo_time"] = node.controller.acquisition.expo_time
            ctrl_params = {"saving_frame_per_file": nb_points+1}
            node.set_parameters(acq_params=acq_params, ctrl_params=ctrl_params)
            chain.add(master, node)

        ###################################
        # EMH
        #
        acq_params = {
                "count_time": time_per_points,
                "npoints": nb_points+1,
                "trigger_type": "HARDWARE",
                "trigger": "DIO_1",
            }
        for node in builder.get_nodes_by_controller_type(EMH):
            node.set_parameters(acq_params=acq_params)
            chain.add(master, node)

        ###################################
        # PEPU
        #
        #acq_params = {
        #        "count_time": time_per_points,
        #        "npoints": nb_points+1,
        #        "trigger_type": "HARDWARE",
        #        "trigger": "DIO_1",
        #    }
        #for node in builder.get_nodes_by_controller_type(EMH):
        #    node.set_parameters(acq_params=acq_params)
        #    chain.add(master, node)

        ###################################
        # P201
        #
        acq_node_params = {
                "npoints": nb_points+1,
                "acq_mode": AcqMode.ExtTrigReadout,
                "acq_expo_time":time_per_points,
                "read_all_triggers": True
        }
        acq_child_params = {
            "count_time": time_per_points,
            "npoints": nb_points+1,
        }
        
        for node in builder.get_nodes_by_controller_type(CT2Controller):
            node.set_parameters( acq_params=acq_node_params)
            for child_node in node.children:
                child_node.set_parameters(acq_params=acq_child_params)
            chain.add(master, node)

        ###################################
        # Speedgoat, CalcCounterController
        #
        acq_params = {
                "count_time": time_per_points,
                "npoints": nb_points+1,
                "trigger_type": "HARDWARE",
            }
        for node in builder.nodes:
            if isinstance(node.controller, (SpeedgoatCountersController, CalcCounterController)):
                node.set_parameters(acq_params=acq_params)
                chain.add(master, node)

        return builder

##########################################################
#
# Display acquired nb points on acquisition devices during
# continuous scan
#
class zapdisplay(object):

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, musst, param, builder, timeout):
        self.nb_points = param.points
        self.time_factor = float(musst.get_timer_factor())
        self.motor = param.motor
        
        self.controllers = {}
        if builder is not None:
            for cont_node in builder.get_top_level_nodes():
                if hasattr(cont_node.acquisition_obj, "name"):
                    cont_name = cont_node.acquisition_obj.name
                else:
                    cont_name = cont_node.acquisition_obj._name
                self.controllers[cont_name] = {}
                self.controllers[cont_name]["name"] = cont_node.acquisition_obj.device.name
                self.controllers[cont_name]["updated"] = False
                self.controllers[cont_name]["points"] = 0
        self.mussttime = 0.0
        self.mussttrajmot = -1
        self.end_scan_time = None
        
        self.timeout = timeout
        # print(self.controllers)

    def on_scan_new(self, scan, scan_info):

        title = scan_info["title"]
        print(f"\n  {title}\n")

        if self.motor is not None:
            display_str = "   %8s "%(self.motor.name)
        else:
            display_str = "  "

        display_str += " %8s "%("time")
        
        display_str += " %8s "%("trajmot")

        for cont_name in self.controllers:
            display_str += " %8s "%(self.controllers[cont_name]["name"])

        print(display_str)

    def on_state(self,state):
        return True

    def on_scan_data(self, data_events, nodes, info):

        if info.get('state') != ScanState.PREPARING:

            nev = len(data_events)
            #print(f"\n-------------ENTER - {nev}")
            if len(data_events) >= 1:

                for cont_name in self.controllers:
                    self.controllers[cont_name]["updated"] = False

                for channel, events in data_events.items():

                    data_node = nodes.get(channel)

                    if data_node.name.find(":") == -1:
                        continue
                        
                    #print(f"+++   {data_node.name}")
                    cont_name1 = data_node.parent.name
                    # print(f"---   {data_node.name} - {cont_name1}")
                    if cont_name1 == "musst243":
                        cont_name = cont_name1
                    elif cont_name1 == "axis":
                        cont_name = cont_name1
                    else:
                        if cont_name1 in self.controllers:
                            cont_name = cont_name1
                        else:
                            try:
                                cont_name2 = data_node.parent.parent.name
                            except:
                                cont_name2 = ""
                            if cont_name2 in self.controllers:
                                cont_name = cont_name2

                    if cont_name == "musst243":
                        if data_node.name.lower() == "musst243:time":
                            self.mussttime = float(data_node.get(-1)) / float(self.time_factor)
                            cont_name = ""
                            cont_name_parent = ""
                        if data_node.name.lower() == "musst243:trajmot":
                            self.mussttrajmot = float(data_node.get(-1))/100000
                            cont_name = ""
                            cont_name_parent = ""
                    elif cont_name == "axis":
                        pass
                    else:
                        if cont_name in self.controllers:
                            cont_obj = self.controllers[cont_name]
                            if not cont_obj["updated"]:
                                cont_obj["updated"] = True
                                if isinstance(data_node, LimaImageChannelDataNode):
                                    cont_obj["points"] = data_node.get(-1).ref_status.get("last_image_ready",0)+1
                                else:
                                    cont_obj["points"] = len(data_node)
                            #print(f"===   {data_node.name} - {cont_name}")


                if self.motor is not None:
                    display_str = "   %8.4f "%(self.motor.position)
                else:
                    display_str = "  "

                display_str += " %8.3f "%self.mussttime
                display_str += " %8.3f "%self.mussttrajmot

                end_scan_all = True
                end_scan = False
                for cont_obj in self.controllers.values():
                    display_str += "     %04d "%(cont_obj["points"])
                    if cont_obj["points"] == self.nb_points+1:
                        end_scan = True
                    else:
                        end_scan_all = False

                if end_scan:
                    if self.end_scan_time is None:
                        self.end_scan_time = time.time()
                        
                if self.end_scan_time is not None:
                    end_scan_wait = time.time() - self.end_scan_time
                    if end_scan_wait > self.timeout:
                        raise RuntimeError(f"Scan did not finish")
                    else:
                        display_str += f"       {end_scan_wait:.3f}/{self.timeout:.3f}"
                        
                print(f"{display_str}", end="\r")

        else:

            if self.motor is not None:
                print(" %8.4f "%(self.motor.position), end="\r")

        sys.stdout.flush()

    def on_scan_end(self, *args):
        print("\n")
