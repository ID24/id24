import numpy
import struct
import os

from bliss.config.settings import ParametersWardrobe
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
import bliss.common.plot as plot_module

#####################################################################
#####
##### Streak Detector
#####
#####################################################################
class ID24streakDetector:
    
    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self._image_dir = "/data/id24/inhouse/HPLF/STREAK"
                      
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }
        
        image_files = self._get_image_files()
        for filename in image_files:
            md["_file_"][filename] = {
                "type": "streak",
                "data": f"{self._image_dir}/{filename}"
            }

        return md
                
    ##################################################################
    #
    # Acquisition Methods
    #
    def _get_detector_parameters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq):
        print("Streak Detector - _get_detector_parameters - Not Implemented")
        return {}

    def _get_acq_parameters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq):
        print("Streak Detector - _get_acq_parameters - Not Implemented")
        return {}

    # Data
    def _get_acq_data(self, scan, nbframe, nbacq):
        print("Streak Detector - _get_acq_data - Not Implemented")
    
    def start_live(self, inttime=0.001, rotation="NONE", flipx=False, flipy=False, trig=False):
        print("Streak Detector - start_live - Not Implemented")
            
    def stop_live(self):
        print("Streak Detector - stop_live - Not Implemented")
                
    ##################################################################
    #
    # Streak specific  Methods
    #
    def _get_image_files(self):
        file_list = os.listdir(self._image_dir)
        return file_list
