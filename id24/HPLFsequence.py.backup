
import time
import datetime
import sys
import tabulate
import numpy
import os
import click
import gevent

from bliss import setup_globals
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
from bliss.config.settings import ParametersWardrobe, SimpleSetting
from bliss.shell.standard import newsample
from bliss.common.cleanup import cleanup

from id24.ID24utils import ID24saveAscii, ID24saveImage, ID24profile
from id24.ID24menu import menu_list, menu_number, menu_string, menu_choice

class HPLFsequence:

    def __init__(self, name, config):

        self._name = name
        self._config = config
        
        #
        # SCANS
        #
        self._scans = config.get("_par_scans", None)
        if self._scans is None:
            raise RuntimeError("HPLFsequence: No scans controller defined")
        
        #
        # CITY CONTROLLER
        #
        self._city_ctrl = config.get("_par_city_ctrl", None)
        if self._city_ctrl is None:
            raise RuntimeError("HPLFsequence: No city controller defined")
        
        #
        # baslers
        #
        baslers_config = config.get("_par_baslers", None)
        self._baslers = []
        if baslers_config is not None:
            for basler_config in baslers_config:
                self._baslers.append(basler_config["basler"])
        
        #
        # P100
        #
        self._p100 = config.get("_par_p100", None)
        if self._p100 is None:
            raise RuntimeError("HPLFsequence: No P100 controller defined")
        
        #
        # TRANSPORT
        #
        self._transport = self._config.get("_par_transport", None)
        if self._transport is None:
            raise RuntimeError("HPLFsequence: No transport object defined")
        
        #
        # INTERACTION CHAMBER
        #
        self._ic = self._config.get("_par_ic", None)
        if self._ic is None:
            raise RuntimeError("HPLFsequence: No Interaction Chamber object defined")
        
        #
        # Sequence Parameters
        #
        self._param = self._config.get("_par_parameters")
        
        #
        # SAVING OBJECT
        #   - city_ctrl
        #   - EnergyLaser = enmeter_eh1.energy, enery_calculated + energy_portable (if connected) + 
        #   - tektro54 profile + values
        #   - tektro64 profile + values
        #   - p100.amplifiers
        #   - p100.attenuators
        #   - p100.intrepid
        #   - p100.ModBox
        #   - p100.Modbox profile
        #saved_objects_list = [self._city_ctrl,self._p100, self._transport]
        saved_objects_list = [self._city_ctrl]
        #saved_objects_list.extend(self._baslers)
        self._save = HPLFsaving(saved_objects_list)
    
    def __info__(self):
        print("\n")
        mystr = BOLD("parameters")
        print(f"    {mystr}")
        print("        Sequences parameters\n")
        mystr = BOLD("laser")
        print(f"    {mystr}")
        print("        Laser check - Laser shock - No Xh\n")
        mystr = BOLD("streak_ref")
        print(f"    {mystr}")
        print("        Streak check - Streak signal - No laser shock - No Xh\n")
        mystr = BOLD("shock(inttime, nbframe, I0_pos, sample_pos)")
        print(f"    {mystr}")
        print("        Laser check - Laser Shock - Xh\n")
        return ""
        
    #
    # Sequences
    #
    def test_run(self):
        
        # New run
        self._save.run_open()
        with cleanup(self._save.run_close):
            self._scans.laser(basler=True)
            self._save_metadata()
            
    def laser_only(self):
        # Change newsample
        #self._sample_open("laser_only")
        
        # Check shock is possible
        if self._shock_check():
            rep = click.confirm(
                    "Ready to Shock ?",
                    default=False
                )
                
            if rep:

                print("Shock within next 10s....")
                self._scans.laser()
            
                print("Save data ....")
                self._save_metadata()
        
    def streak_ref(self):
        # Check streak 
        if self._streak_check():
            rep = click.confirm(
                    "Ready to get Streak references ?",
                    default=False
                )
                
            if rep:

                print("Streak signal within next 10s....")
                self._scans.streak_ref()
                
                print("\n")
                saved = click.confirm(
                    "Streak images saved ?",
                    default=False
                )
                
                if saved:
                    print("Save data ....")
                    self._save_metadata()
        
    def shock(self, inttime, nbframe, I0_pos, sample_pos):
        # Check shock is possible
        if self._shock_check():
            rep = click.confirm(
                    "Ready to Shock ?",
                    default=False
                )
                
            if rep:

                print("Shock within next 10s....")
                self._scans.laser_shock(inttime, nbframe, I0_pos, sample_pos)
            
                print("Save data ....")
                self._save_metadata()

    #
    # proposal/sample management
    #
    def _new_sample_open(self, seq_name):
        pass
        
    #
    # parameters
    #
    def parameters(self):
        self._param.parameters()
        
    #
    # Metadata saving
    #
    def _save_metadata(self):
        self._save.save()
        self._save.print_saving()
    
    #
    # Security checks
    #
    def _streak_check(self):
        # init
        states = ""
        shoot_is_allowed = True
        
        # shutter_drive_eh1
        if self._transport.devices.shutter_drive_eh1.state != "CLOSED":
            print(RED("  - Shutter Drive EH1 is not Closed ... Close it ..."))
            self._transport.devices.shutter_drive_eh1.close()
            if self._transport.devices.shutter_drive_eh1.state != "CLOSED":
                print(RED("  - Shutter Drive EH1 is not Closed"))
                shoot_is_allowed = False
            else:
                print(GREEN("  - Shutter Drive EH1 is Closed"))
        else:
            print(GREEN("  - Shutter Drive EH1 is Closed"))
        print("\n")
        
        # uscope_ds_ty    
        if not setup_globals.uscope_ds_ty.state.LIMNEG:
            print(RED("  - uscope_ds_ty is not on NEGATIVE limit ... move it to NEG limit ..."))
            setup_globals.uscope_ds_ty.hw_limit(-1)
            if not setup_globals.uscope_ds_ty.state.LIMNEG:
                shoot_is_allowed = False
                print(RED("  - uscope_ds_ty is NOT on NEGATIVE limit"))
            else:
                print(GREEN("  - uscope_ds_ty is on NEGATIVE limit"))
        else:
            print(GREEN("  - uscope_ds_ty is on NEGATIVE limit"))
        print("\n")
            
        # uscope_us_shield
        if self._ic.devices.uscope_us_shield.state != "IN":
            print(RED("  - uscope_us_shield is not IN ... set it IN ..."))
            try:
                setup_globals.uscope_us_shield.set_in()
            except:
                print("\n")
            if self._ic.devices.uscope_us_shield.state != "IN":
                shoot_is_allowed = False
                print(RED("  - uscope_us_shield is not IN"))
            else:
                print(GREEN("  - uscope_us_shield is IN "))
        else:
            print(GREEN("  - uscope_us_shield is IN "))
        print("\n")
                
        # shutter_visar
        if self._transport.devices.shutter_visar.state != "OPEN":
            print(RED("  - Shutter VISAR is not Open ... Open it ..."))
            self._transport.devices.shutter_visar.open()
            if self._transport.devices.shutter_visar.state != "OPEN":
                print(RED("  - Shutter VISAR is not Open"))
                shoot_is_allowed = False
            else:
                print(GREEN("  - Shutter VISAR is Open"))
        else:
            print(GREEN("  - Shutter VISAR is Open"))
        print("\n")
            
        # shutter_visar_eh1
        if self._transport.devices.shutter_visar_eh1.state != "OPEN":
            print(RED("  - Shutter VISAR EH1 is not Open ... Open it"))
            try:
                self._transport.devices.shutter_visar_eh1.open()
            except:
                print("\n")
            if self._transport.devices.shutter_visar_eh1.state != "OPEN":
                shoot_is_allowed = False
                print(RED("  - Shutter VISAR EH1 is not Open"))
            else:
                print(GREEN("  - Shutter VISAR EH1 is Open"))
        else:
            print(GREEN("  - Shutter VISAR EH1 is Open"))
        print("\n")
           
        if not shoot_is_allowed:
            print("\n")
            print(BOLD(ORANGE("\n  ALL Conditions MUST be validated before the take Streak References\n")))
            return False
            
        return True

    def _shock_check(self):
        # init
        states = ""
        shoot_is_allowed = True
        # BeamBlocker Input
        if self._p100.device.ampli._beamblocker_input != "OPEN":
            print(RED("  - Beamblocker Input is not Open"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Beamblocker Input is Open"))
        print("\n")
        # BeamBlocker Output
        if self._p100.device.ampli._beamblocker_output != "OPEN":
            print(RED("  - Beamblocker Output is not Open"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Beamblocker Output is Open"))
        print("\n")
        # shutter_drive_eh1
        if self._transport.devices.shutter_drive_eh1.state != "OPEN":
            print(RED("  - Shutter Drive EH1 is not Open ... Open it ..."))
            try:
                self._transport.devices.shutter_drive_eh1.open()
            except:
                print("\n")
            if self._transport.devices.shutter_drive_eh1.state != "OPEN":
                shoot_is_allowed = False
                print(RED("  - Shutter Drive EH1 is not Open"))
            else:
                print(GREEN("  - Shutter Drive EH1 is Open"))
        else:
            print(GREEN("  - Shutter Drive EH1 is Open"))
        print("\n")
        # x11
        if self._transport.devices.x11.state != "OUT":
            print(RED("  - X11 is not OUT"))
        else:
            print(GREEN("  - X11 is OUT"))
        print("\n")
        # x12
        if self._transport.devices.x12.state != "OUT":
            print(RED("  - X12 is not OUT"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - X12 is OUT"))
        print("\n")
        # x2
        if self._transport.devices.x2.state != "OUT":
            print(RED("  - X2 is not OUT ... set it OUT ..."))
            self._transport.devices.x2.set_out()
            if self._transport.devices.x2.state != "OUT":
                shoot_is_allowed = False
                print(RED("  - X2 is not OUT"))
            else:
                print(GREEN("  - X2 is OUT"))
        else:
            print(GREEN("  - X2 is OUT"))
        print("\n")
        # x31
        if self._transport.devices.x31.state != "OUT":
            print(RED("  - X31 is not OUT"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - X31 is OUT"))
        print("\n")
        # x32
        if self._transport.devices.x32.state != "OUT":
            print(RED("  - X32 is not OUT"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - X32 is OUT"))
        print("\n")
        # x4
        if self._transport.devices.x4.state != "OUT":
            print(RED("  - X4 is not OUT ... set it OUT"))
            self._transport.devices.x4.set_out()
            if self._transport.devices.x4.state != "OUT":
                print(RED("  - X4 is not OUT"))
                shoot_is_allowed = False
            else:
                print(GREEN("  - X4 is OUT"))
        else:
            print(GREEN("  - X4 is OUT"))
        # x51
        if self._transport.devices.x51.state == "IN":
            print(RED("  - X51 is not OUT"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - X51 is OUT"))
        # Gauge sf1
        if self._transport.devices.gauge_sf1.state != "OK":
            print(RED("  - Gauge sf1 is not OK"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Gauge sf1 is OK"))
        # Gauge sf2
        if self._transport.devices.gauge_sf2.state != "OK":
            print(RED("  - Gauge sf1 is not OK"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Gauge sf1 is OK"))
        
#        # lvisarty
#        if not setup_globals.lvisarty.state.LIMNEG:
#            straux = RED("  - lvisarty is not on NEGATIVE limit")
#            states += f"  {straux}\n"
#            shoot_is_allowed = False
        
        # uscope_ds_ty    
        if not setup_globals.uscope_ds_ty.state.LIMNEG:
            print(RED("  - uscope_ds_ty is not on NEGATIVE limit ... Move it to NEG limit ..."))
            setup_globals.uscope_ds_ty.hw_limit(-1)
            if not setup_globals.uscope_ds_ty.state.LIMNEG:
                shoot_is_allowed = False
                print(RED("  - uscope_ds_ty is not on NEGATIVE limit"))
            else:
                print(GREEN("  - uscope_ds_ty is on NEGATIVE limit"))
        else:
            print(GREEN("  - uscope_ds_ty is on NEGATIVE limit"))
        print("\n")
        # uscope_us_shield
        if self._ic.devices.uscope_us_shield.state != "IN":
            print(RED("  - uscope_us_shield is not IN ... set it IN ..."))
            self._ic.devices.uscope_us_shield.set_in()
            if self._ic.devices.uscope_us_shield.state != "IN":
                print(RED("  - uscope_us_shield is not IN"))
                shoot_is_allowed = False
            else:
                print(GREEN("  - uscope_us_shield is IN"))
        else:
            print(GREEN("  - uscope_us_shield is IN"))
        print("\n")
                
        # diagatt
        if self._transport.devices.diagatt.state != "IN":
            print(RED("  - diagatt is not IN ... set it IN ..."))
            self._transport.devices.diagatt.set_in()
            if self._transport.devices.diagatt.state != "IN":
                shoot_is_allowed = False
                print(RED("  - diagatt is not IN"))
            else:
                print(GREEN("  - diagatt is IN"))
        else:
            print(GREEN("  - diagatt is IN"))
        print("\n")

        # shutter_visar
        if self._transport.devices.shutter_visar.state != "OPEN":
            straux = RED("  - Shutter VISAR is not Open")
            states += f"  {straux}\n"
            shoot_is_allowed = False
        print("\n")
            
        # shutter_visar_eh1
        if self._transport.devices.shutter_visar_eh1.state != "OPEN":
            print(RED("  - Shutter VISAR EH1 is not Open ... Open it ..."))
            if self._transport.devices.shutter_visar_eh1.state != "OPEN":
                print(RED("  - Shutter VISAR EH1 is not Open"))
                shoot_is_allowed = False
            else:
                print(GREEN("  - Shutter VISAR EH1 is Open"))
        else:
            print(GREEN("  - Shutter VISAR EH1 is Open"))
        print("\n")
            
        # P100 Intrepid Shutter
        if not self._p100.device.intrepid.Intracavity_Shutter_State:
            print(RED("  - Shutter Intrepid is not Open"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Shutter Intrepid is Open"))
        print("\n")

        if not shoot_is_allowed:
            print("\n")
            print(BOLD(ORANGE("\n  ALL Conditions MUST be validated before the Laser Shoot\n")))
            return False
            
        return True

class HPLFdelayParameter:

    def __init__(self, name, config):

        self._name = name
        self._config = config
        
        self._delays = {}
        self._channels = {}
        delays = self._config.get("delays")
        for delay in delays:
            delay_name = delay["delay_name"]
            title = delay["title"]
            self._delays[delay_name] = {
                "value": SimpleSetting(f"delay_param_{self._name}_{delay_name}", default_value=0.0),
                "title": delay["title"],
                "channels": [],
            }
            for channel in delay["channels"]:
                self._delays[delay_name]["channels"].append(channel["channel"])
                if channel["channel"] not in self._channels.keys():
                    self._channels[channel["channel"]] = 0.0
                
    def parameters(self):
        while True:
            titles = []
            values = []
            for name, par in self._delays.items():
                titles.append(par["title"])
                values.append(par["value"].get())   
            (rep, ind) = menu_list(f"Laser sequences parameters", titles, values=values)
            if rep == "q":
                return
            if rep in titles:
                delay_name = self._get_name_from_title(rep)
                self._delays[delay_name]["value"].set(
                    menu_number(
                        rep,
                        default=self._delays[delay_name]["value"].get(),
                    )
                )
                self._apply_parameters()
                
    def _get_name_from_title(self, title):
        for name, par in self._delays.items():
            if title == par["title"]:
                return name
        return none
            
    def _apply_parameters(self):
        for channel in self._channels.keys():
            self._channels[channel] = 0.0
        for delay, par in self._delays.items():
            for channel in par["channels"]:
                self._channels[channel] += par["value"].get()
        for channel, val in self._channels.items():
            channel.extra_delay = val
        
class HPLFsequenceParameters:
    def __init__(self, city):
        self._city = city
        self._default_param = {
            "drive_delay": 0.0,
            "probe_delay": 0.0,
            "streak_delay": 0.0,
        }
        self._param = ParametersWardrobe(
            f"hplf_seq_parameters",
            default_values=self._default_param
        )
        
    def parameters(self):
        while True:
            drive_delay_title  = f"Drive Delay ...... : {GREEN(self._param.drive_delay)} ns"
            probe_delay_title  = f"Probe Laser Delay. : {GREEN(self._param.probe_delay)} ns"
            streak_delay_title = f"Streak Delay...... : {GREEN(self._param.streak_delay)} ns"
            param = [drive_delay_title, probe_delay_title, streak_delay_title]
            (rep, ind) = menu_list(f"Laser sequences parameters", param)
            if ind == 0:
                self._param.drive_delay = menu_number(
                    "Drive Delay (ns)",
                    #minmax=minmax,
                    default=self._param.drive_delay,
                    #integer=True,
                )
            if ind == 1:
                self._param.probe_delay = menu_number(
                    "Probe Laser Delay (ns)",
                    #minmax=minmax,
                    default=self._param.probe_delay,
                    #integer=True,
                )
            if ind == 2:
                self._param.streak_delay = menu_number(
                    "Streak Delay (ns)",
                    #minmax=minmax,
                    default=self._param.streak_delay,
                    #integer=True,
                )
            if rep == "q":
                return
                
class HPLFsaving:

    def __init__(self, objects_to_be_saved):

        if len(objects_to_be_saved) <= 0:
            raise RuntimeError("HPLFsaving: No object to save")
            
        self._saved_obj = objects_to_be_saved
        self._saving = {}
        self._run = SimpleSetting("hplf_seq_run_number", default_value=1)

    #
    # RUN management
    #
    def run_reset(self):
        self._run.set(0)
        
    def run_open(self):
        timestamp = self._get_timestamp()
        self._run.set(self._run.get()+1)
        sample_name = f"run_{self._run.get():04d}_{timestamp}"
        newsample(sample_name)
        
    def run_close(self):
        sample_name = f"sample_{self._run.get()+1:04d}"
        newsample(sample_name)        
    
    #
    # Metadata Categorie
    #
    def metadata_create_category(self):
        category = get_user_scan_meta()
        category.add_categories(["HPLF"])
        category._scan_meta_category("HPLF").timing(META_TIMING.END)
        category._scan_meta_category("HPLF").set("HPLF", self.metadata_fill)
        
    def metadata_fill(self):
        metadata = {}
        for save_obj in self._save_list:
            md_dict = save_obj._get_metadata()
            metadata
            if "_h5_" is in md_dict.keys():
            for key, val in md_dict.items():
                if key == "h5":
                    pass
                elif key ==
            
            
            
            
            
            
            
            
            
            
            
            
            # get_data from object
            (save_dict, save_image, save_ascii, save_file) = save_obj._get_metadata()
            # Simple data -> h5 file
            metadata[save_obj._name] = save_dict
            # Save Images
            for name in save_image.keys():
                # Create icat/type directory if necessary
                dir_type = save_image[name]["type"]
                icat_dir = f"{icat_dir}/{dir_type}
                if not os.path.isdir(icat_dir):
                    os.makedirs(icat_dir)
                # Build file name to save image
                filename = f"{icat_dir}/{name}.png"
                # Save image in file
                ID24saveImage(filename, save_image[name]["data"])
                # reference in h5 file
                metadata[save_obj.name][name] = filename
            # save ASCII
            for name in save_ascii.keys():
                # Create icat/type directory if necessary
                dir_type = save_image[name]["type"]
                icat_dir = f"{icat_dir}/{dir_type}
                if not os.path.isdir(icat_dir):
                    os.makedirs(icat_dir)
                # Build file name to save ascii data
                filename = f"{icat_dir}/{name}.dat"
                # Save data in file
                ID24saveAscii(filename, save_acii[name]["data"])
                # reference in h5 file
                metadata[save_obj.name][name] = filename
            # File reference
            for name in save_file.keys():
                # Create icat/type directory if necessary
                dir_type = save_file[name]["type"]
                icat_dir = f"{icat_dir}/{dir_type}
                if not os.path.isdir(icat_dir):
                    os.makedirs(icat_dir)
                # build filename
                filename = save_file[name]["data"][save_file[name]["data"].rfind("/")+1:]
                file_icat = f"{icat_dir}/{filename}"
                # copy file in icat location
                shutil.move(save_file[name]["data"], file_icat)
                # reference in h5 file
                metadata[save_obj.name][name] = file_icat
                
        return metadata
        
    def _get_icat_dict(self, md_dict):
        md = {}
        icat_dir = self._get_icat_dir()
        
        for key, val in md_dict.items():
            # h5
            if key == "_h5_":
                md.update(val)
            # images
            elif key == "_image_":
                for name, data in val.items():
                    # Create icat/type directory if necessary
                    dir_type = data["type"]
                    icat_dir = f"{icat_dir}/{dir_type}
                    if not os.path.isdir(icat_dir):
                        os.makedirs(icat_dir)
                    # Build file name to save image
                    filename = f"{icat_dir}/{name}.png"
                    # Save image in file
                    ID24saveImage(filename, data["data"])
                    # reference in h5 file
                    md[name] = filename
            # ASCII
            elif key == "_image_":
                for name, data in val.items():
                    # Create icat/type directory if necessary
                    dir_type = data["type"]
                    icat_dir = f"{icat_dir}/{dir_type}
                    if not os.path.isdir(icat_dir):
                        os.makedirs(icat_dir)
                    # Build file name to save ascii data
                    filename = f"{icat_dir}/{name}.dat"
                    # Save data in file
                    ID24saveAscii(filename, data["data"])
                    # reference in h5 file
                    md[name] = filename
            # File reference
            elif key == "_file_":
                for name, data in va.items():
                    # Create icat/type directory if necessary
                    dir_type = data["type"]
                    icat_dir = f"{icat_dir}/{dir_type}
                    if not os.path.isdir(icat_dir):
                        os.makedirs(icat_dir)
                    # build filename
                    filename = data["data"][data["data"].rfind("/")+1:]
                    file_icat = f"{icat_dir}/{filename}"
                    # copy file in icat location
                    shutil.move(data["data"], file_icat)
                    # reference in h5 file
                    md[name] = file_icat
            # complex object
            elif isinstance(val, dict):
                md[key] = self._get_icat_dict(val)
            else:
                md[key] = val
                
        return md

    #
    # Saved object
    #
    def saved_object_set(self, obj_list):
        self._save_list = obj_list
        
    #
    # Tools
    #
    def _get_timestamp(self):
        #date
        today = datetime.date.today()
        timestamp = f"{today.year:04d}_{today.month:02d}_{today.day:02d}"
        # time
        currtime = time.ctime().split()[3].split(":")
        timestamp += f"_{currtime[0]:02d}_{currtime[1]:02d}_{currtime[2]:02d}"
        return timestamp 
        
    def _get_icat_dir(self):
        SCAN_SAVING = setup_globals.SCAN_SAVING
        run_dir = SCAN_SAVING.filename
        run_dir = run_dir[:run_dir.rfind("/")]
        return run_dir
        
    ######################################################################
    #####
    ##### Saving
    #####
    def save(self):
        
        # init
        
        # Build metadata filenames
        self._set_files()
        
        # get data object
        param_str = ""
        for dev in self._saved_obj:
            param_str += self._metadata2str(dev._name, dev._get_metadata())
        
        # Save
        fhplf = open(self._saving["file_hplf"], "w")
        fhplf.write(param_str)
        fhplf.close()
        fprop = open(self._saving["file_prop"], "w")
        fprop.write(param_str)
        fprop.close()
        
    #####
    ##### File Tools
    #####
    def _get_prop_dir(self):
        SCAN_SAVING = setup_globals.SCAN_SAVING
        hplf_dir = SCAN_SAVING.filename
        hplf_dir = hplf_dir[:hplf_dir.rfind("/")]
        hplf_dir = f"{hplf_dir}/hplf"
        if not os.path.isdir(hplf_dir):
            os.makedirs(hplf_dir)
        return hplf_dir
        
    def _get_hplf_dir(self):
        SCAN_SAVING = setup_globals.SCAN_SAVING
        id24_dir = f"/users/blissadm/local/beamline_configuration/hplf/SHOCK_DATA/{SCAN_SAVING.proposal_name}"
        if not os.path.isdir(id24_dir):
            os.makedirs(id24_dir)
        return id24_dir

    def _set_files(self):
        timestamp = self._get_timestamp()
        self._saving["timestamp"] = timestamp
        self._saving["dir_hplf"] = f"{self._get_hplf_dir()}/shock_{timestamp}"
        self._saving["dir_prop"] = f"{self._get_prop_dir()}/shock_{timestamp}"
        
        if not os.path.isdir(self._saving["dir_hplf"]):
            os.makedirs(self._saving["dir_hplf"])
        if not os.path.isdir(self._saving["dir_prop"]):
            os.makedirs(self._saving["dir_prop"])
        
        # Files
        self._saving["file_hplf"] = self._saving["dir_hplf"]+"/"+self._saving["timestamp"]+"_metadata.dat"
        self._saving["file_prop"] = self._saving["dir_prop"]+"/"+self._saving["timestamp"]+"_metadata.dat"

    def print_saving(self):
        dir_hplf = self._saving["dir_hplf"]
        dir_prop = self._saving["dir_prop"]
        timestamp = self._saving["timestamp"]
        print("\n")
        print(f"   Data saved with timestamp \"{BOLD(timestamp)}\" in:")
        print(f"        - {dir_hplf}")   
        print(f"        - {dir_prop}\n") 
        
    ######################################################################
    #####
    ##### FORMAT METADATA
    #####    
    def _metadata2str(self,  name, value):
        straux = "--------------------------------------------------\n"
        straux += f"{name}\n"
        straux += f"{self._get_str(name, value, level=1)}"
        return straux
    
    def _get_str(self, name, value, level=1):
        
        space = ""
        for i in range(level):
            space += "    "
            
        if isinstance(value, dict):
            mystr = "\n"
            for key, val in value.items():
                res_val = self._get_str(key, val, level+1)
                mystr += f"{space}{key}: {res_val}\n"
            return mystr
        elif isinstance(value,str):
            if value.find("file") == 0:
                return value
            else:
                return value
        elif isinstance(value,float) or isinstance(value,int):
            return value
        elif isinstance(value, ID24profile):
            f_hplf = self._saving["dir_hplf"]+"/"+self._saving["timestamp"]+f"_{name}.dat"
            f_prop = self._saving["dir_prop"]+"/"+self._saving["timestamp"]+f"_{name}.dat"
            ID24saveAscii(f_hplf, value)
            ID24saveAscii(f_prop, value)
            return self._saving["timestamp"]+f"_{name}.dat"
        elif isinstance(value, numpy.ndarray):
            f_hplf = self._saving["dir_hplf"]+"/"+self._saving["timestamp"]+f"_{name}.png"
            f_prop = self._saving["dir_prop"]+"/"+self._saving["timestamp"]+f"_{name}.png"
            ID24saveImage(f_hplf, value)
            ID24saveImage(f_prop, value)
            return self._saving["timestamp"]+f"_{name}.png"
                
        
