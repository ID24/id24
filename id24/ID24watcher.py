
import time
import gevent
import sys

from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED

from silx.gui import qt

class Watcher:

    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self._title = config.get("title", "Unknown")
        self._wago = config.get("wago")
        self._wago_ch = config.get("wago channel")
        self._logic = config.get("logic")
        self._polling = config.get("polling (s)", 1)
        
        self._sendmail = False
        self._state = "RESTART"
        
        self.update_state()
        
    def poll(self):
        print("\n\n")
        while True:
            t = time.asctime()
            self.update_state()
            if self._state == "OK":
                state = GREEN(self._state)
            elif self._state == "ALARM":
                state = RED(self._state)
            else:
                state = BLUE(self._state)
            print(f"{BOLD(t)} - {self._title} - state: {state}", end="\r")
            gevent.sleep(self._polling)
    
    def update_state(self):
        state = self.read_state()   
        if state == "OK":
            if self._state == "ALARM" or self._state == "LOST CONNECTION":
                self.send_mail(f"{self._title} State: Back to OK")
            if self._state == "RESTART":
                self.send_mail(f"{self._title} State: OK (App. Restarted)")
        elif state == "ALARM":
            if self._state == "OK" or self._state == "LOST CONNECTION":
                self.send_mail(f"{self._title} State: ALARM")
            if self._state == "RESTART":
                self.send_mail(f"{self._title} State: ALARM (App. Restarted)")
        else:
            if self._state == "OK" or self._state == "ALARM":
                self.send_mail(f"{self._title} State: LOST CONNECTION")
            if self._state == "RESTART":
                self.send_mail(f"{self._title} State: LOST CONNECTION (App. Restarted)")
        self._state = state
            
    def read_state(self):
        try:
            if float(self._wago.get(self._wago_ch)) == 0.0:
                if self._logic == "normal":
                    return "OK"
                else:
                    return "ALARM"
            else:
                if self._logic == "normal":
                    return "ALARM"
                else:
                    return "OK"
        except:
            return "LOST CONNECTION"
            
        return float(self._wago.get(self._wago_ch))
        
    def send_mail(self, message):
        receivers = self.get_receivers()
        if self._sendmail:
            import smtplib
            smtpObj = smtplib.SMTP_SSL("smtp.esrf.fr")
            smtpObj.sendmail(f"ID24_{self._name}_State", receivers, "Subject: "+message)
            smtpObj.quit()
        
    def get_receivers(self):
        receivers = self._config.get("receivers", None)
        receivers_list = []
        if receivers is None or len(receivers) <= 0:
            self._sendmail = False
        else:
            self._sendmail = True
            for rec in receivers:
                receivers_list.append(rec.get("receiver"))
        return receivers_list
        
        
class WatcherGUI(qt.QMainWindow):
    def __init__(self, bliss_obj, parent=None):
        super().__init__(parent=parent)
        
        self.bliss_obj = bliss_obj
        
        self.label = qt.QLabel(self)
        self.label.setAlignment(qt.Qt.AlignCenter)
        self.label.setText(f"{self.bliss_obj._title} State: UNKNOWN")
        self.setCentralWidget(self.label)
        self.setGeometry(qt.QRect(100, 100, 400, 100))
        
        self.__timer = qt.QTimer(self)
        self.__timer.start(self.bliss_obj._polling)
        self.__timer.timeout.connect(self.__update)
        
    def __update(self):
        t = time.asctime()
        self.bliss_obj.update_state()
        if self.bliss_obj._state == "OK":
            style_sheet = "QLabel {background-color: #00ff00; font-weight: bold}"
        elif self.bliss_obj._state == "ALARM":
            style_sheet = "QLabel {background-color: #ff0000; font-weight: bold}"
        else:
            style_sheet = "QLabel {background-color: #0000ff; font-weight: bold}"
        self.label.setText(f"{self.bliss_obj._title}\n\nState: {self.bliss_obj._state}\n\n{t}")
        self.label.setStyleSheet(style_sheet)

class WatcherApp:
    def __init__(self, obj_name, gui=False):
        # Get BLISS
        self.get_bliss("session_watcher")
        self.bliss_obj = self.env_dict.get(obj_name)
        
        # get GUI if necessary
        if gui:
            self.get_gui(self.bliss_obj)
        else:
            # Start POLLING
            self.bliss_obj.poll()
        
    def get_bliss(self, session):
        from bliss.config import static
        from types import SimpleNamespace
        config = static.get_config()
        self.session = config.get(session)
        env_dict = {}
        self.session.setup(env_dict)
        self.env = SimpleNamespace(**env_dict)
        self.env_dict = env_dict
    
    def get_gui(self, bliss_obj):
        app = qt.QApplication([])
        viewer = WatcherGUI(bliss_obj)
        viewer.setVisible(True)
        app.exec_()

def main(obj_name, gui=False):
    app = WatcherApp(obj_name, gui)
    
if __name__ == "__main__":
    if len(sys.argv) >= 3 and sys.argv[2] == "-gui":
        main(sys.argv[1], gui=True)
    else:
        main()
