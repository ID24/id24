
import time
import gevent
import sys
import tabulate

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.config.settings import ParametersWardrobe, SimpleSetting

from id24.HPLFtransport import HPLFcrossShutterAttrList

class HPLFinteractionChamber:

    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self._devices = {}
        devices = self._config.get("devices")
        for device in devices:
            dev = device.get("device")
            self._devices[dev._name] = dev
        self.devices = HPLFcrossShutterAttrList(self._devices)
    
        self._temperature = {}
        temperature_list = config.get("temperature")
        if temperature_list is None:
            print(RED("Interaction Chamber: No Temperature device defined"))
        else:
            for temp_config in temperature_list:
                name = temp_config.get("temperature_name")
                self._temperature[name] = {}
                for key, val in temp_config.items():
                    if key != "temperature_name":
                        self._temperature[name][key] = val 
                    
        self._phase_plate = SimpleSetting("interaction_chamber_phase_plate", default_value="")
        
    def __info__(self):
        self.devices.__info__()
        return ""

    def _get_metadata(self):
        md = {
            "phase_plate": self.phase_plate,
        }
        
        for temp_name in self._temperature.keys():
            md[self._temperature[temp_name]["temperature_key"]] = self._get_temperature(temp_name)
        return md
        
    @property
    def phase_plate(self):
        return self._phase_plate.get()
        
    @phase_plate.setter
    def phase_plate(self, value):
        self._phase_plate.set(value)
        
    def _get_temperature(self, temp_name):
        temp = self._temperature[temp_name]
        if "wago" in temp.keys():
            temp_val = float(temp["wago"].get(temp["channel"]))
            return f"{temp_val:.3f} deg"
        elif "ls332" in temp.keys():
            temp = temp["ls332"].input.read()
            unit = temp["ls332"].unit.name
            return f"{temp} {unit}"
        else:
            print(RED(f"Interaction Chamber: Temperature Controller \"{temp_name}\" not implemented"))
    
