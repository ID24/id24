from bliss import setup_globals
from bliss.shell.standard import wm

class ComponentOptics:

    def __init__(self, name, config):
    
        self._name = name
        self._config = config

        self.bl_objects = []
        
        # Frontend Shutter
        self.bl_objects.append({"name": "feshut              ", "desc": "Front End Shutter"})
        
        # RV0
        #self.bl_objects.append({"name": "rv0                ", "desc": "RV0"})
        
        # Primary slits
        self.bl_objects.append({"name": "wps                 ", "desc": "Primary Slits"})
        
        # White Beamviewer
        self.bl_objects.append({"name": "wbv                 ", "desc": "White Beamviewer"})
        
        # Attenuators
        self.bl_objects.append({"name": "att1/att2/att3      ", "desc": "Attenuators"})
        
        # White Beamviewer
        #self.bl_objects.append({"name": "wbv                 ", "desc": "White Beamviewer"})
        
        # MV1
        self.bl_objects.append({"name": "wmv1/mv1pace        ", "desc": "Mirror MV1"})
        
        # MV1 Secondary slits
        self.bl_objects.append({"name": "wmv1ss              ", "desc": "MV1 Secondary Slits"})
        
        # MV1 ss Beamviewer
        self.bl_objects.append({"name": "mv1bv               ", "desc": "MV1 Secondary Slits Beamviewer"})
        
        # MH
        self.bl_objects.append({"name": "wmh                 ", "desc": "Horizontal Mirror (MH)"})
        
        # MH Sec. Slits
        self.bl_objects.append({"name": "wmhss               ", "desc": "MH Secondary Slits"})        

        # MH ss Beamviewer
        self.bl_objects.append({"name": "mhbv                ", "desc": "MH Sec. Slits Beamviewer"})

        # DCM Collimator Beamviewer
        self.bl_objects.append({"name": "dcmcolbv            ", "desc": "DCM Collimator Beamviewer"})

        # ED Collimator Beamviewer
        self.bl_objects.append({"name": "edcolbv             ", "desc": "ED Collimator Beamviewer"})

        # OH1 Beam Shutter
        self.bl_objects.append({"name": "oh1shut             ", "desc": "OH1 Beam Shutter"})
        
    def __info__(self):
        mystr =  "OPTICS components\n"
        mystr += "---------------\n\n"
        
        for comp_ind in range(len(self.bl_objects)):
            obj = self.bl_objects[comp_ind]
            name = obj["name"]
            desc = obj["desc"]
            mystr += f"    {name:5s} - {desc}\n"
        
        return mystr
    
class ComponentEd(object):

    def __init__(self, name, config):
    
        self._name = name
        self._config = config

        self.bl_objects = []
        
        # Xh slits
        self.bl_objects.append({"name": "wxhs                ", "desc": "Xh Slits (EH1)"})

        # ED Secondary slits
        self.bl_objects.append({"name": "wedss               ", "desc": "ED Secondary Slits (OH2)"})

        # ED Secondary Slits Beamviewer
        self.bl_objects.append({"name": "edssbv              ", "desc": "ED Secondary Slits Beamviewer (OH2)"})
        
        # POLYCHROMATOR
        self.bl_objects.append({"name": "wpoly               ", "desc": "Polychromator"})

        # ED Beam Shutter
        self.bl_objects.append({"name": "edshut              ", "desc": "ED Beam Shutter"})
        
        # ED Bench
        self.bl_objects.append({"name": "wbed                ", "desc": "ED Bench Arm"})
        
        # ED Sample Bench
        self.bl_objects.append({"name": "wsed                ", "desc": "ED Sample Bench"})
        
        # ED User/Chemistry Block
        self.bl_objects.append({"name": "wchem               ", "desc": "ED User/Chemistry Bench"})
        
        # ED Detector Bench
        self.bl_objects.append({"name": "wded                ", "desc": "ED Detector Bench"})
        
        # PMF
        self.bl_objects.append({"name": "wpmf                ", "desc": "Pulsed Magnetic Field (PMF)"})
        
        # QWP
        self.bl_objects.append({"name": "wqwp                ", "desc": "Quarter Wave Plate (QWP)"})
        
        # VRM
        self.bl_objects.append({"name": "wvrm                ", "desc": "Vertical refocusing Mirror ED (VRM)"})
        
    def __info__(self):
        mystr =  "ID24 components\n"
        mystr += "---------------\n\n"
        
        for comp_ind in range(len(self.bl_objects)):
            obj = self.bl_objects[comp_ind]
            name = obj["name"]
            desc = obj["desc"]
            mystr += f"    {name:5s} - {desc}\n"
        
        return mystr

class ComponentHplf(object):

    def __init__(self, name, config):
    
        self._name = name
        self._config = config

        self.bl_objects = []
        
        # Sequence 
        self.bl_objects.append({"name": "shock_exp           ", "desc": "Laser Sequences"})
        
        # Synchro 
        self.bl_objects.append({"name": "p100                ", "desc": "Laser Synchro"})
        
        # Motors
        self.bl_objects.append({"name": "wtransport          ", "desc": "Transport Motors"})
        self.bl_objects.append({"name": "wic                   ", "desc": "Interaction Chamber Motors"})
        
        # Shutters
        self.bl_objects.append({"name": "hplfshut            ", "desc": "HPLF shutter Management"})
        self.bl_objects.append({"name": "wic                 ", "desc": "Interaction Chamber Motors"})
        
    def __info__(self):
        mystr =  "HPLF components\n"
        mystr += "---------------\n\n"
        
        for comp_ind in range(len(self.bl_objects)):
            obj = self.bl_objects[comp_ind]
            name = obj["name"]
            desc = obj["desc"]
            mystr += f"    {name:5s} - {desc}\n"
        
        return mystr
    
class ComponentXh(object):

    def __init__(self, name, config):
    
        self._name = name
        self._config = config

        self.bl_objects = []
        
        # Motpos
        self.bl_objects.append({"name": "motpos              ", "desc": "Register positions"})
        
        # Xh slits
        self.bl_objects.append({"name": "wxhs                ", "desc": "Xh Slits (EH1)"})
        
        # Target
        self.bl_objects.append({"name": "wtarget             ", "desc": "Target Holder Translations"})

        # ED Beam Shutter
        self.bl_objects.append({"name": "edshut              ", "desc": "ED Beam Shutter"})
        
        # ED Detector Bench
        self.bl_objects.append({"name": "dedty               ", "desc": "Detector Bench Y translation"})
        
        # seq
        self.bl_objects.append({"name": "seq                 ", "desc": "ID24 sequences"})

        # XH
        self.bl_objects.append({"name": "xh, xh_lima         ", "desc": "Xh Detector (Acquisition)"})
        self.bl_objects.append({"name": "xh_ctrl, xhcalib    ", "desc": "Xh Detector (Control)"})
    
class ComponentDcm(object):

    def __init__(self, name, config):
    
        self._name = name
        self._config = config

        self.bl_objects = []
        
        # Motpos
        self.bl_objects.append({"name": "motpos                 ", "desc": "Register positions"})
        
        # DCM Bench
        self.bl_objects.append({"name": "wdcmbench              ", "desc": "DCM Bench"})
        
        # DCM Slits
        self.bl_objects.append({"name": "wdcmss                 ", "desc": "DCM Sec. Slits"})
        
        # DCM KB
        self.bl_objects.append({"name": "wdcmkb                 ", "desc": "DCM KB mirror"})
        
        # Detector bench
        self.bl_objects.append({"name": "wdcmdet                ", "desc": "Detector Bench"})
        
        # Uxas
        self.bl_objects.append({"name": "wdcmuxas               ", "desc": "MicroXAS"})
        
        # Diag
        self.bl_objects.append({"name": "wdcmdiag, bas_eh2_diag ", "desc": "Diag tool"})
        
        # DCM
        self.bl_objects.append({"name": "dcm                    ", "desc": "Monochromator"})
        
        # Spectro Analysers
        self.bl_objects.append({"name": "spectro                ", "desc": "Spectro"})
        
        
    def __info__(self):
        mystr =  "ID24 DCM components\n"
        mystr += "-------------------\n\n"
        
        for comp_ind in range(len(self.bl_objects)):
            obj = self.bl_objects[comp_ind]
            name = obj["name"]
            desc = obj["desc"]
            mystr += f"    {name:5s} - {desc}\n"
        
        return mystr
