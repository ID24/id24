
import tabulate
import PIL

from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
from bliss.comm.util import get_tango_proxy
from bliss import current_session, setup_globals

from id24.ID24utils import ID24attrList

class HPLFp100TangoDevice:

    def __init__(self, name, config):
        self._name = name
        self._config = config
        self._device = get_tango_proxy(config)
    
    def __info__(self):
        print(self._get_info())
        return ""
        
    def _error_str(self, err, space=""):
        if err == "" or err == "OK" or err == "\r":
            return None
        else:
            lines = err.split("\n")
            nlines = len(lines)
            ret = ""
            for i in range(nlines):
                ret += space + RED(lines[i])
                if i < nlines-1:
                    ret += "\n"
            return ret
    
    @property
    def _status(self):
        return self._device.Status
                    
class HPLFtangoDeviceAttrList(ID24attrList):
    
    def __info__(self):
        mystr = ""
        #lines = [["  ", "", "Channel", "Status", "Burst", "Gate", "Source", "Source State", "Freq", "Delay/D5 [µs]"]]
        for name, dev in self._items.items():
            mystr += f"{name}\n"
            #(title, channel, state, burst_state, gate_state, gate_src, gate_src_state, freq, rel) = dev._get_condensed_info()
            #lines.append(["  ", title, channel, state, burst_state, gate_state, gate_src, gate_src_state, freq, rel])
        #mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        print(mystr)
        return ""
                    
class HPLFtangoCameraAttrList(ID24attrList):
    
    def __info__(self):
        mystr = ""
        #lines = [["  ", "", "Channel", "Status", "Burst", "Gate", "Source", "Source State", "Freq", "Delay/D5 [µs]"]]
        for name, dev in self._items.items():
            mystr += f"{name}\n"
            #(title, channel, state, burst_state, gate_state, gate_src, gate_src_state, freq, rel) = dev._get_condensed_info()
            #lines.append(["  ", title, channel, state, burst_state, gate_state, gate_src, gate_src_state, freq, rel])
        #mystr = "\n"+tabulate.tabulate(lines, tablefmt="plain", stralign="right")
        print(mystr)
        return ""

class HPLFp100Camera(HPLFp100TangoDevice):
    
    def __init__(self, name, config):
        super().__init__(name, config)
        self._title = config.get("title", self._name)
        
    @property
    def EnergyCentroid_X(self):
        return self._device.EnergyCentroid_X
        
    @property
    def EnergyCentroid_Y(self):
        return self._device.EnergyCentroid_Y
        
    @property
    def Image(self):
        return self._device.Image
    
    def _get_info(self):
        mystr = "\n"
        mystr += f"  Centroid X: {self.EnergyCentroid_X}\n"
        mystr += f"  Centroid Y: {self.EnergyCentroid_Y}\n"
        return mystr
    
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }
        
        md["_h5_"]["EnergyCentroid_X"] = self.EnergyCentroid_X
        md["_h5_"]["EnergyCentroid_Y"] = self.EnergyCentroid_Y
        
        md["_image_"][self._title] = {
            "type": "p100",
            "data": self.Image,
        }
            
        return md
        
    def plot(self):
        setup_globals.display.plot_image(self._title, self._title, self._title, self.Image, "Pixel", "Pixel")

    def save(self, filename):
        im = PIL.Image.fromarray(self.Image)
        im.save(filename)
        
class HPLFp100Attenuator(HPLFp100TangoDevice):
    
    def __init__(self, name, config):
        super().__init__(name, config)
        self._title = config.get("title", self._name)
    
    @property
    def Error(self):
        return self._device.Error
        
    @property
    def Current_Transmission(self):
        return self._device.Current_Transmission 

    def _get_info(self):
        mystr = "\n"
        mystr += BOLD(f"  {self._title}\n")
        mystr += f"    Current Transmission  : {GREEN(self.Current_Transmission)}%\n"
        mystr += f"    Setpoint Transmission : {GREEN(self.Set_Point_Transmission)}%\n"
        if self._error() is not None:
            mystr += "  Error\n"
            mystr += self._error("    ")
        return mystr
    
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }
        
        md["_h5_"]["Current_Transmission"] = self.Current_Transmission
        md["_h5_"]["Set_Point_Transmission"] = self.Set_Point_Transmission
            
        return md
       
    def _error(self, space=""):
        err = self._device.Error
        return self._error_str(err, space)
    
    @property
    def Set_Point_Transmission(self):
        return self._device.Set_Point_Transmission
    
    @Set_Point_Transmission.setter
    def Set_Point_Transmission(self, value):
        if value < 0:
            value = 0
        if value >100:
            value = 100
        self._device.Set_Point_Transmission = value
        
class HPLFp100Attenuator1(HPLFp100Attenuator):
    def __init__(self, name, config):
        super().__init__(name, config)
    
    @property
    def Attenuator1_Status(self):
        return self._device.Attenuator1_Status
        
class HPLFp100Attenuator2(HPLFp100Attenuator):
    def __init__(self, name, config):
        super().__init__(name, config)

    @property
    def  Attenuator2_Status(self):
        return self._device.Attenuator2_Status

class HPLFp100Intrepid(HPLFp100TangoDevice):
    
    def __init__(self, name, config):
        super().__init__(name, config)
        self._title = config.get("title", self._name)
        
        self.Pockels_command = HPLFp100IntrepidPockelsCommand(self)
        
    @property
    def Intracavity_Shutter_State(self):
        return self._device.Intracavity_Shutter_State
        
    @property
    def Phi25_Error(self):
        return self._device.Phi25_Error
    
    @property
    def Phi25_Flashlamp_Shotcount_read(self):
        return self._device.Phi25_Flashlamp_Shotcount_read
    
    @property
    def Phi25_Flashlamp_Voltage(self):
        return self._device.Phi25_Flashlamp_Voltage
        
    @property
    def Phi25_mode(self):
        return self._device.Phi25_mode.name
        
    @property
    def Phi5_Error(self):
        return self._device.Phi5_Error 
    
    @property
    def Phi5_Flashlamp_Shotcount(self):
        return self._device.Phi5_Flashlamp_Shotcount
    
    @property
    def Phi5_Flashlamp_Voltage(self):
        return self._device.Phi5_Flashlamp_Voltage
        
    @property
    def Phi5_mode(self):
        return self._device.Phi5_mode.name

    def __info__(self):
        print(self._get_info())
        return ""
        
    def _get_info(self):
        mystr = "\n"
        mystr += BOLD(f"  {self._title}\n")
        mystr += f"    Intracavity Shutter : {self._intracavity_shutter}\n"
        mystr += f"    Pockels command     : {self.Pockels_command.state}\n"
        mystr += f"{self._intrepid}\n"
        if self._phi25_error("") is not None:
            mystr += "  Phi25 Error\n"
            mystr += self._phi25_error("    ")
        if self._phi5_error("") is not None:
            mystr += "  Phi5 Error\n"
            mystr += self._phi5_error("    ")
        return mystr
    
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }

        md["_h5_"]["Intracavity_Shutter_State"] = self.Intracavity_Shutter_State
        md["_h5_"]["Phi25_Error"] = self.Phi25_Error
        md["_h5_"]["Phi25_Flashlamp_Shotcount_read"] = self.Phi25_Flashlamp_Shotcount_read
        md["_h5_"]["Phi25_Flashlamp_Voltage"] = self.Phi25_Flashlamp_Voltage
        md["_h5_"]["Phi25_mode"] = self.Phi25_mode
        md["_h5_"]["Phi5_Error"] = self.Phi5_Error
        md["_h5_"]["Phi5_Flashlamp_Shotcount"] = self.Phi5_Flashlamp_Shotcount
        md["_h5_"]["Phi5_Flashlamp_Voltage"] = self.Phi5_Flashlamp_Voltage
        md["_h5_"]["Phi5_mode"] = self.Phi5_mode
        md["_h5_"]["Pockels_command"] = self.Pockels_command.state
            
        return md
    
    @property
    def _intracavity_shutter(self):
        if self.Intracavity_Shutter_State:
            return GREEN("OPEN")
        else:
            return RED("CLOSED")
    
    def _phi25_error(self, space=""):
        err = self.Phi25_Error
        return self._error_str(err, space)
    
    @property
    def _phi25_counts(self):
        return GREEN(self.Phi25_Flashlamp_Shotcount_read)
        
    @property
    def _phi25_voltage(self):
        return GREEN(self.Phi25_Flashlamp_Voltage)
    
    @property
    def _phi25_mode(self):
        if self.Phi25_mode:
            return GREEN("On")
        else:
            return RED("Off")

    def _phi5_error(self, space=""):
        err = self.Phi5_Error
        return self._error_str(err, space)
    
    @property
    def _phi5_counts(self):
        return GREEN(self.Phi5_Flashlamp_Shotcount)
        
    @property
    def _phi5_voltage(self):
        return GREEN(self._device.Phi5_Flashlamp_Voltage)
    
    @property
    def _phi5_mode(self):
        if self._device.Phi5_mode:
            return GREEN("On")
        else:
            return RED("Off")
            
    @property
    def _intrepid(self):
        
        lines = [["  ",
            "",
            BOLD("Mode"),
            BOLD("Voltage"),
            BOLD("Shots")
        ]]
        lines.append(["  ",
            BOLD("Phi25"),
            self._phi25_mode,
            self._phi25_voltage,
            self._phi25_counts,
        ])
        lines.append(["  ",
            BOLD("Phi5"),
            self._phi5_mode,
            self._phi5_voltage,
            self._phi5_counts,
        ])
        return tabulate.tabulate(lines, tablefmt="plain")

class HPLFp100IntrepidPockelsCommand:
    
    def __init__(self, intrepid):
        self._intrepid = intrepid
        
    @property
    def state(self):
        if self._intrepid._device.Pockels_command:
            rep = "on"
        else:
            rep = "off"
        #rep = self._intrepid._device.Pockels_command == "true" ? "on": "off"
        return rep
        
    def on(self):
        self._intrepid._device.Pockels_command = True       
        
    def off(self):
        self._intrepid._device.Pockels_command = False  

class HPLFp100Modbox(HPLFp100TangoDevice):
    
    def __init__(self, name, config):
        super().__init__(name, config)
        self._title = config.get("title", self._name)
        
    def BoosterStatus(self):
        return self._device.BoosterStatus
    
    def SeederStatus(self):
        return self._device.SeederStatus
            
    def __info__(self):
        print(self._get_info())
        return ""

    def _get_info(self):
        mystr = "\n"
        mystr += BOLD(f"  {self._title}\n")
        mystr += f"    Booster : {self.booster_status}\n"
        mystr += f"    Seeder  : {self.seeder_status}\n"
        if self._error("") is not None:
            mystr += "  Alarms\n"
            mystr += self._error("    ")
        return mystr
    
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }

        md["_h5_"]["Booster"] = self.BoosterStatus()
        md["_h5_"]["Seeder"] = self.SeederStatus()
        md["_h5_"]["Alarms"] = self._device.Alarms
            
        return md
       
    def _error(self, space=""):
        err = self._device.Alarms
        return self._error_str(err, space)
    
    @property
    def booster_status(self):
        if self._device.BoosterStatus:
            return GREEN("On")
        else:
            return RED("Off")
    
    @property
    def seeder_status(self):
        if self._device.SeederStatus:
            return GREEN("On")
        else:
            return RED("Off")

            
class HPLFp100Synchro(HPLFp100TangoDevice):
    
    def __init__(self, name, config):
        super().__init__(name, config)
        self._title = config.get("title", self._name)
        
    @property
    def Delay_Phi25_Phi5_Measured(self):
        return self._device.Delay_Phi25_Phi5_Measured
        
    @property
    def Flash_Phi25_Frequency_Measured(self):
        return self._device.Flash_Phi25_Frequency_Measured
        
    @property
    def Flash_Phi5_Frequency_Measured(self):
        return self._device.Flash_Phi5_Frequency_Measured
        
    @property
    def Frequency_Phi25_Nominal(self):
        return self._device.Frequency_Phi25_Nominal
        
    @property
    def Frequency_Pockels_Phi5_Nominal(self):
        return self._device.Frequency_Pockels_Phi5_Nominal
        
    @property
    def Max_Frequency_Presimmer_DAM(self):
        return self._device.Max_Frequency_Presimmer_DAM
        
    @property
    def Min_acceptable_delay(self):
        return self._device.Min_acceptable_delay
        
    @property
    def Pockels_Frequency_Measured(self):
        return self._device.Pockels_Frequency_Measured
        
    @property
    def Presimmer_DAM_Frequency_Measured(self):
        return self._device.Presimmer_DAM_Frequency_Measured
        
    @property
    def Tolerance_Frequency_Pockels(self):
        return self._device.Tolerance_Frequency_Pockels
        
    @property
    def Width_accepted_delay_window(self):
        return self._device.Width_accepted_delay_window

    def _get_info(self):
        mystr = "\n"
        mystr += BOLD(f"  {self._title}\n")
        mystr += f"    Delay Phi25 Phi5 Measured        : {self.Delay_Phi25_Phi5_Measured}\n"
        mystr += f"    Flash Phi25 Frequency Measured   : {self.Flash_Phi25_Frequency_Measured}\n"
        mystr += f"    Flash Phi5 Frequency Measured    : {self.Flash_Phi5_Frequency_Measured}\n"
        mystr += f"    Frequency Phi25 Nominal          : {self.Frequency_Phi25_Nominal}\n"
        mystr += f"    Frequency Pockels Phi5 Nominal   : {self._frequency_pockels_phi5_nominal}\n"
        mystr += f"    Max Frequency Presimmer DAM      : {self.Max_Frequency_Presimmer_DAM}\n"
        mystr += f"    Min acceptable delay             : {self.Min_acceptable_delay}\n"
        mystr += f"    Pockels Frequency Measured       : {self.Pockels_Frequency_Measured}\n"
        mystr += f"    Presimmer DAM Frequency Measured : {self.Presimmer_DAM_Frequency_Measured}\n"
        mystr += f"    Tolerance Frequency Pockels      : {self.Tolerance_Frequency_Pockels}\n"
        mystr += f"    Width accepted delay window      : {self.Width_accepted_delay_window}\n"
        return mystr
    
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }

        md["_h5_"]["Delay_Phi25_Phi5_Measured"] = self.Delay_Phi25_Phi5_Measured
        md["_h5_"]["Flash_Phi25_Frequency_Measured"] = self.Flash_Phi25_Frequency_Measured
        md["_h5_"]["Flash_Phi5_Frequency_Measured"] = self.Flash_Phi5_Frequency_Measured
        md["_h5_"]["Frequency_Phi25_Nominal"] = self.Frequency_Phi25_Nominal
        md["_h5_"]["Frequency_Pockels_Phi5_Nominal"] = self.Frequency_Pockels_Phi5_Nominal
        md["_h5_"]["Max_Frequency_Presimmer_DAM"] = self.Max_Frequency_Presimmer_DAM
        md["_h5_"]["Min_acceptable_delay"] = self.Min_acceptable_delay
        md["_h5_"]["Pockels_Frequency_Measured"] = self.Pockels_Frequency_Measured
        md["_h5_"]["Presimmer_DAM_Frequency_Measured"] = self.Presimmer_DAM_Frequency_Measured
        md["_h5_"]["Tolerance_Frequency_Pockels"] = self.Tolerance_Frequency_Pockels
        md["_h5_"]["Width_accepted_delay_window"] = self.Width_accepted_delay_window
            
        return md

    @property
    def _frequency_pockels_phi5_nominal(self):
        if self.Frequency_Pockels_Phi5_Nominal:
            return "0.1Hz"
        else:
            return "0.5Hz"
                    
class HPLFp100Amplifier(HPLFp100TangoDevice):
    
    def __init__(self, name, config):
        super().__init__(name, config)
        self._title = config.get("title", self._name)
        
    @property
    def BeamBlocker_Input_High(self):
        return self._device.BeamBlocker_Input_High
        
    @property
    def BeamBlocker_Input_Low(self):
        return self._device.BeamBlocker_Input_Low
        
    @property
    def BeamBlocker_Ouput_High(self):
        return self._device.BeamBlocker_Output_High
        
    @property
    def BeamBlocker_Ouput_Low(self):
        return self._device.BeamBlocker_Output_Low
        
    @property
    def Diode_Mount_Blocked(self):
        return self._device.Diode_Mount_Blocked
        
    @property
    def Diode_Mount_Injected(self):
        return self._device.Diode_Mount_Injected
        
    @property
    def Energy_State(self):
        return self._device.Energy_State
        
    @property
    def Errors(self):
        return self._device.Errors
        
    @property
    def Key1_On_Off(self):
        return self._device.Key1_On_Off
        
    @property
    def Key2_Maintenance_Normal(self):
        return self._device.Key2_Maintenance_Normal
        
    @property
    def PowerSupply0_DAM_Voltage(self):
        return self._device.PowerSupply0_DAM_Voltage
        
    @property
    def PowerSupply0_Flash(self):
        return self._device.PowerSupply0_Flash
        
    @property
    def PowerSupply0_Name(self):
        return self._device.PowerSupply0_Name
        
    @property
    def PowerSupply0_Shot_count_Flashlamp(self):
        return self._device.PowerSupply0_Shot_count_Flashlamp
        
    @property
    def PowerSupply0_Simmer_Status(self):
        return self._device.PowerSupply0_Simmer_Status
        
    @property
    def PowerSupply1_DAM_Voltage(self):
        return self._device.PowerSupply1_DAM_Voltage
        
    @property
    def PowerSupply1_Flash(self):
        return self._device.PowerSupply1_Flash
        
    @property
    def PowerSupply1_Name(self):
        return self._device.PowerSupply1_Name
        
    @property
    def PowerSupply1_Shot_count_Flashlamp(self):
        return self._device.PowerSupply1_Shot_count_Flashlamp
        
    @property
    def PowerSupply1_Simmer_Status(self):
        return self._device.PowerSupply1_Simmer_Status
        
    @property
    def Sync_Modified_For_High_Energy(self):
        return self._device.Sync_Modified_For_High_Energy
        
    @property
    def Sync_Modified_For_Low_Energy(self):
        return self._device.Sync_Modified_For_Low_Energy
        
    @property
    def Vertical_Translation_High(self):
        return self._device.Vertical_Translation_High
        
    @property
    def Vertical_Translation_Low(self):
        return self._device.Vertical_Translation_Low
        
    @property
    def Wait_State_High(self):
        return self._device.Wait_State_High
        
    @property
    def Wait_State_Low(self):
        return self._device.Wait_State_Low

    def _get_info(self):
        mystr = "\n"
        mystr += BOLD(f"  {self._title}\n")
        
        mystr += f"    Beam Blocker Input  : {self._beamblocker_input_str}\n"
        mystr += f"    Beam Blocker Output : {self._beamblocker_output_str}\n"
        mystr += f"    Diode Mount         : {self._diode_mount}\n"
        mystr += f"    Vertical Trans.     : {self._vertical_translation}\n"
        mystr += f"    Energy State        : {self._energy_state}\n"
        mystr += f"    Key #1              : {self._key1}\n"
        mystr += f"    Key #2              : {self._key2}\n"
        mystr += f"    Asked HIGH Energy   : {self.Sync_Modified_For_High_Energy}\n"
        mystr += f"    Wait State HIGH     : {self.Wait_State_High}\n"
        mystr += f"    Asked LOW Energy    : {self.Sync_Modified_For_Low_Energy}\n"
        mystr += f"    Wait State LOW      : {self.Wait_State_Low}\n"
        mystr += f"\n{self.power_supply}\n"
        if self._error() is not None:
            mystr += "  Error\n"
            mystr += self._error("    ")
        
        return mystr
    
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }

        md["_h5_"]["BeamBlocker_Input_High"] = self._device.BeamBlocker_Input_High
        md["_h5_"]["BeamBlocker_Input_Low"] = self._device.BeamBlocker_Input_Low
        md["_h5_"]["BeamBlocker_Ouput_High"] = self._device.BeamBlocker_Output_High
        md["_h5_"]["BeamBlocker_Ouput_Low"] = self._device.BeamBlocker_Output_Low
        md["_h5_"]["Diode_Mount_Blocked"] = self._device.Diode_Mount_Blocked
        md["_h5_"]["Diode_Mount_Injected"] = self._device.Diode_Mount_Injected
        md["_h5_"]["Energy_State"] = self._device.Energy_State
        md["_h5_"]["Errors"] = self._device.Errors
        md["_h5_"]["Key1_On_Off"] = self._device.Key1_On_Off
        md["_h5_"]["Key2_Maintenance_Normal"] = self._device.Key2_Maintenance_Normal
        md["_h5_"]["PowerSupply0_DAM_Voltage"] = self._device.PowerSupply0_DAM_Voltage
        md["_h5_"]["PowerSupply0_Flash"] = self._device.PowerSupply0_Flash
        md["_h5_"]["PowerSupply0_Name"] = self._device.PowerSupply0_Name
        md["_h5_"]["PowerSupply0_Shot_count_Flashlamp"] = self._device.PowerSupply0_Shot_count_Flashlamp
        md["_h5_"]["PowerSupply0_Simmer_Status"] = self._device.PowerSupply0_Simmer_Status
        md["_h5_"]["PowerSupply1_DAM_Voltage"] = self._device.PowerSupply1_DAM_Voltage
        md["_h5_"]["PowerSupply1_Flash"] = self._device.PowerSupply1_Flash
        md["_h5_"]["PowerSupply1_Name"] = self._device.PowerSupply1_Name
        md["_h5_"]["PowerSupply1_Shot_count_Flashlamp"] = self._device.PowerSupply1_Shot_count_Flashlamp
        md["_h5_"]["PowerSupply1_Simmer_Status"] = self._device.PowerSupply1_Simmer_Status
        md["_h5_"]["Sync_Modified_For_High_Energy"] = self._device.Sync_Modified_For_High_Energy
        md["_h5_"]["Sync_Modified_For_Low_Energy"] = self._device.Sync_Modified_For_Low_Energy
        md["_h5_"]["Vertical_Translation_High"] = self._device.Vertical_Translation_High
        md["_h5_"]["Vertical_Translation_Low"] = self._device.Vertical_Translation_Low
        md["_h5_"]["Wait_State_High"] = self._device.Wait_State_High
        md["_h5_"]["Wait_State_Low"] = self._device.Wait_State_Low
            
        return md
       
    def _error(self, space=""):
        err = self._device.Errors
        return self._error_str(err, space)
            
    @property
    def _beamblocker_input(self):
        high = self._device.BeamBlocker_Input_High
        low = self._device.BeamBlocker_Input_Low
        if high:
            if low:
                return "UNKNOWN"
            else:
                return "OPEN"
        else:
            if low:
                return "CLOSED"
            else:
                return "MOVING"
            
    @property
    def _beamblocker_input_str(self):
        state = self._beamblocker_input
        if state == "OPEN":
            return RED("Open")
        if state == "CLOSED":
            return GREEN("Closed")
        if state == "UNKNOWN":
            return RED("Error (Open & Closed)")
        if state == "MOVING":
            return YELLOW("Moving (not Open & not Closed)")
            
    @property
    def _beamblocker_output(self):
        high = self._device.BeamBlocker_Output_High
        low = self._device.BeamBlocker_Output_Low
        if high:
            if low:
                return "UNKNOWN"
            else:
                return "OPEN"
        else:
            if low:
                return "CLOSED"
            else:
                return "MOVING"
            
    @property
    def _beamblocker_output_str(self):
        state = self._beamblocker_output
        if state == "OPEN":
            return RED("Open")
        if state == "CLOSED":
            return GREEN("Closed")
        if state == "UNKNOWN":
            return RED("Error (Open & Closed)")
        if state == "MOVING":
            return YELLOW("Moving (not Open & not Closed)")
                    
    @property
    def _diode_mount(self):
        blocked = self._device.Diode_Mount_Blocked
        injected = self._device.Diode_Mount_Injected
        if blocked:
            if injected:
                return RED("Error (Blocked & Injected)")
            else:
                return BLUE("Blocked")
        else:
            if injected:
                return BLUE("Injected")
            else:
                return YELLOW("Moving (not Blocked & not Injected)")
            
    @property
    def _vertical_translation(self):
        high = self._device.Vertical_Translation_High
        low = self._device.Vertical_Translation_Low
        if high:
            if low:
                return RED("Error (High & Low)")
            else:
                return BLUE("High")
        else:
            if low:
                return BLUE("Low")
            else:
                return YELLOW("Moving (not High & not Low)")
                
    @property
    def _energy_state(self):
        if self._device.Energy_State:
            return BLUE("HIGH")
        else:
            return BLUE("LOW")

    @property
    def _key1(self):
        if self._device.Key1_On_Off:
            return GREEN("ON")
        else:
            return RED("OFF")

    @property
    def _key2(self):
        if self._device.Key1_On_Off:
            return BLUE("Normal")
        else:
            return RED("Maintenance")
    
    def _DAM_name(self, dev):
        if dev == 0:
            return BLUE(self._device.PowerSupply0_Name)
        else:
            return BLUE(self._device.PowerSupply1_Name)

    def _DAM_voltage(self, dev):
        if dev == 0:
            return GREEN(self._device.PowerSupply0_DAM_Voltage)
        else:
            return GREEN(self._device.PowerSupply1_DAM_Voltage)

    def _flashlamp_state(self, dev):
        if dev == 0:
            state = self._device.PowerSupply0_Flash
        else:
            state = self._device.PowerSupply1_Flash
        if state:
            return GREEN("On")
        else:
            return RED("Off")
    
    def _flashlamp_counts(self, dev):
        if dev == 0:
            return GREEN(self._device.PowerSupply0_Shot_count_Flashlamp)
        else:
            return GREEN(self._device.PowerSupply1_Shot_count_Flashlamp)

    def _simmer_state(self, dev):
        if dev == 0:
            state = self._device.PowerSupply0_Simmer_Status
        else:
            state = self._device.PowerSupply1_Simmer_Status
        if state:
            return GREEN("READY")
        else:
            return RED("NOT READY")
            
    @property
    def power_supply(self):
        
        lines = [["  ",
            BOLD("Power Supply"),
            "DAM Name",
            "DAM Voltage",
            "Flashlamp",
            "Shots",
            "Simmer"
        ]]
        lines.append(["  ",
            "      #0",
            self._DAM_name(0),
            self._DAM_voltage(0),
            self._flashlamp_state(0),
            self._flashlamp_counts(0),
            self._simmer_state(0)
        ])
        lines.append(["  ",
            "      #1",
            self._DAM_name(1),
            self._DAM_voltage(1),
            self._flashlamp_state(1),
            self._flashlamp_counts(1),
            self._simmer_state(1)
        ])
        return tabulate.tabulate(lines, tablefmt="plain")
        
