import numpy
import struct
import gevent
import typeguard
import numbers
import datetime
import time

from bliss.config.settings import ParametersWardrobe
from bliss.common.utils import shorten_signature, typeguardTypeError_to_hint
from bliss.common import plot
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan, ScanState, DataWatchCallback
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.lima.limatools import limatake, _limatake_parse_args
from bliss.common.cleanup import cleanup
from bliss.scanning.group import Sequence
from bliss.scanning.chain import AcquisitionChannel
from bliss import setup_globals
from bliss.config.static import get_config
from bliss.common.cleanup import cleanup


from id24.ID24menu import menu_list, menu_number, menu_string, menu_choice
from bliss.shell.standard import umv, umvr


class ID24xhCalib:
    def __init__(self, name, config):

        self.name = name
        self.config = config

        self.lima = config.get("lima", None)
        if self.lima is None:
            raise RuntimeError("No lima DS specified")

        self.xh_device = self.lima._get_proxy("Xh")
        self.xh_shutter = config.get("xh_shutter", None)
        self.bl_shutter = config.get("bl_shutter", None)
        self.srcur = config.get("srcur", None)
        self.sbcur = config.get("sbcur", None)
        self.xh = self.lima._get_proxy("Xh")

        self.xh_max_history = 20
        # 200 s timeout allows for 70 to save IV data.
        self.xh.set_timeout_millis(200000)
        self.orbit_delay=53  # was 70 ?
        self.orbit_delay=10  # was 70 ?
#        self.orbit_delay=88  # For multi-bunch section
        self.orbit_delay_mb = 88
        self.file_index=0
        
    def mymove(self, pos):
        edshut = setup_globals.edshut
        dedty = setup_globals.dedty
        edshut.open()
        umv(dedty, pos)
        #dedty.move(pos)
        edshut.close()

    """
    Shutter
    """
    def oshut(self):
        if self.xh_shutter is not None:
            self.xh_shutter.open()
    
    def cshut(self):
        if self.xh_shutter is not None:
            self.xh_shutter.close()
    
    def oedshut(self):
        setup_globals.edshut.open()

    def cedshut(self):
        setup_globals.edshut.close()
    """
    Low Level Methods
    """
    def xh_timing_orbit_pause(self, nfr , nscan , intt):
        #intt in clock cyles
        self.xh_timing_stop()
        cmd = f"xstrip timing ext-output \'xh0\' -1 integration"
        print(cmd)
        self.xh.sendCommand(cmd)

        cmd = f"xstrip timing setup-group \'xh0\' 0 {nfr} {nscan} {intt} last ext-trig-frame trig-mux 9 orbit-scan orbit-mux 3 lemo-out 65535"
        print(cmd)
        self.xh.sendCommand(cmd)

    def xh_timing_start(self):
        self.xh.sendCommand("xstrip timing start \'xh0\'")

    def xh_timing_stop(self):
        self.xh.sendCommand("xstrip timing stop \'xh0\'")

    def xh_timing_continue(self):
        self.xh.sendCommand("xstrip timing continue \'xh0\'")

    def xh_timing_wait(self):
        self.xh.sendCommand("xstrip timing wait \'xh0\'")

    def set_default_orbit(self, pos):
        self.set_orbit(pos)
        self.orbit_delay = pos
        
    def set_orbit(self, pos):
        self.xh.sendCommand(f"xstrip timing setup-orbit 'xh0' {pos}")

    def set_caps(self, value):
        self.xh.sendCommand(f"xstrip head set-xchip-caps 'xh0' {value} {value}")
        
    """ 
    HV management Methods
    """
    def xh_set_hv_on(self, volts):
        self.xh.sendCommand("xstrip hv enable \'xh0\' auto")
        gevent.sleep(5)
        self.xh.sendCommand(f"xstrip hv set-dac \'xh0\' {volts}")
        gevent.sleep(5)

    def xh_set_hv_off(self):
        self.xh.sendCommand("xstrip hv enable \'xh0\' off")
        gevent.sleep(5)
        
    def xh_hv_set(self, v):
        if v != 0:
            self.xh.sendCommand("xstrip hv enable 'xh0' auto")
        if False:
            start_v = float(self.xh.sendCommandNumber("xstrip hv get-adc 'xh0'"))
            if v>start_v:
                delta_v = 8
            else :
                delta_v = -8
            while abs(v-start_v) > 8 :
                start_v = start_v+delta_v
                self.xh.sendCommand(f"xstrip hv set-dac 'xh0' {start_v}")
            self.xh.sendCommand(f"xstrip hv set-dac 'xh0' {v}")
        else :
            nsteps = 25
            self.xh.sendCommand("set-func 'v_curr' 'xstrip hv get-adc \"xh0\"' local hide")
            self.xh.sendCommand(f"set-func 'vstep' '({v}-v_curr)/{nsteps}' local")
            for i in range(nsteps):
                self.xh.sendCommand("set-func 'v_curr' '%v_curr+vstep' local")
                self.xh.sendCommand("if %v_curr>0 'v_curr=0'")
                self.xh.sendCommand("xstrip hv set-dac 'xh0' %v_curr")
            self.xh.sendCommand(f"xstrip hv set-dac 'xh0' {v}")

    def run(self, nfr, nscans,int_time, beam=1):
        self.set_orbit(self.orbit_delay)
        cmd = f"xstrip timing setup-group 'xh0' 0 {nfr} {nscans} {int_time} last orbit-scan orbit-mux 3 lemo-out 65535"
        print(cmd)
        self.xh.sendCommand(cmd)
        if beam==1 :
            self.oedshut()
            self.oshut()
        else:
            self.cshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.cshut()
        self.cedshut()

    def run_save(self, nfr, nscans,int_time, comment1=""):
        self.set_orbit(self.orbit_delay)
        cmd = f"xstrip timing setup-group 'xh0' 0 {nfr} {nscans} {int_time} last orbit-scan orbit-mux 3 lemo-out 65535"
        print(cmd)
        self.xh.sendCommand(cmd)
        self.oshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.cshut()
        comment2 = f"Frames={nfr}, scans={nscans}, intT={int_time}"
        self.xh_save(comment1, comment2)
    
    def run_off_on(self, nfr, nscans, int_time, comment1=""):
        self.set_orbit(self.orbit_delay)
        cmd = f"xstrip timing setup-group 'xh0' 0 {nfr} {nscans} {int_time} last orbit-scan orbit-mux 3 lemo-out 65535"
        print(cmd)
        comment2 = f"Frames={nfr}, scans={nscans}, intT={int_time}"
        self.xh.sendCommand(cmd)
        self.cshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.cshut()
        self.xh_save(comment1, comment2, "off", 0)
        self.oshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.cshut()
        self.xh_save(comment1, comment2, "on", 1)
        
    def enb16bcorr(self):
        self.xh.sendCommand("unif-set-correction 0 'xchip_lin3_param_row0' chebyshev1d 1.0/8192.0")
        self.xh.sendCommand("unif-set-correction 1 'xchip_lin3_param_row0' chebyshev1d 1.0/8192.0")

    def dis16bcorr(self):
        self.xh.sendCommand("unif-set-correction 0")
        self.xh.sendCommand("unif-set-correction 1")

    """
    Saving Methods
    """
    def xh_save(self, comment1, comment2="", suffix=None, append=0):
        comment1 = comment1.replace("\'", "\"")
        comment2 = comment2.replace("\'", "\"")
        if append!=0 and suffix==None:
            suffix = "append"
        
        if suffix!=None:
            self.xh.sendCommand(f"~save '{comment1}' '{comment2}' -1 '{suffix}' {append}")
        else :
            self.xh.sendCommand(f"~save '{comment1}' '{comment2}'")
            
    def _xh_save_det(self, comment1, comment2="", suffix=None, append=0, unintl=False):
        comment1 = comment1.replace("\'", "\"")
        comment2 = comment2.replace("\'", "\"")
        if append!=0 and suffix==None:
            suffix = "append"
        save_cmd = "save_det_unintl" if unintl else "save_det"
        if suffix!=None:
            self.xh.sendCommand(f"~{save_cmd} '{comment1}' '{comment2}' -1 '{suffix}' {append}")
        else:
            self.xh.sendCommand(f"~{save_cmd} '{comment1}' '{comment2}'")

    def _xh_save_det2(self, comment, tf):
        comment = comment.replace("\'", "\"")
        self.xh.sendCommand(f"~save_det '{comment}' '' {tf}")
    
    def scan_xchip_save_point(self, num_points, npts_rst_hold, npts_rst, comment1, comment2="",  suffix="", append=0, save_raw=False):
        if append==0:
            self.file_index=0
        if append and len(suffix)==0:
            suffix = f"{self.file_index:03d}"
            self.file_index=self.file_index+1
        if len(suffix) > 0:
            suffix = suffix+"_"
        if append :
            cmd = f"module save 'xchip_raw_adc{num_points}' %data_dir current-file '{suffix+'raw_adc.det'}' det xh 'xh0' "
        else:
            cmd = f"module save 'xchip_raw_adc{num_points}' %data_dir next-file '{suffix+'raw_adc.det'}' det xh 'xh0' "
        cnum=1
        comment1 = comment1.replace("\'", "\"")
        comment2 = comment2.replace("\'", "\"")
        if len(comment1) > 0 :
            cmd = cmd + f" comment1 '{comment1}'"
            cnum=cnum+1
        if len(comment2) > 0 :
            cmd = cmd + f" comment{cnum} '{comment2}'"
            cnum=cnum+1
        if cnum==2:
            cmd = cmd + f" comment2 'Impulse response signal-dark'"
        self.xh.sendCommand(cmd)
        if save_raw:
            self._xh_save_det(comment1, comment2, suffix+"adc_unsub", 1)

        if npts_rst_hold > 0 :
            self.xh.sendCommand(f"module save 'xchip_rst_hold{npts_rst_hold}' %data_dir current-file '{suffix+'rst_hold_adc.det'}' det comment1 'Reset hold ADC data' ")
        if npts_rst > 0 :
            self.xh.sendCommand(f"module save 'xchip_rst{npts_rst}' %data_dir current-file '{suffix+'rst_adc.det'}' det comment1 'Reset timing scan ADC data' ")

    def scan_xchip_save_summary(self, num_points, npts_rst_hold, npts_rst, num_rows, num_stability, frac_a, frac_b, comment, setup_cmd):
        comment = comment.replace("\'", "\"")
        setup_cmd = setup_cmd.replace("\'", "\"")
        self.xh.sendCommand(f"module save 'xchip_late_ave{num_rows}' %data_dir next-file 'late_ave.asc' asc-col xh 'xh0' comment1 '{comment}' comment2 '{setup_cmd}'")
        if frac_a > 0:
            self.xh.sendCommand(f"module save 'xchip_frac{frac_a}_{num_rows}' %data_dir current-file 'frac{frac_a}.asc' asc-col ")
        if frac_b > 0:
            self.xh.sendCommand(f"module save 'xchip_frac{frac_b}_{num_rows}' %data_dir current-file 'frac{frac_b}.asc' asc-col ")

        self.xh.sendCommand(f"module save \'xchip_late_sd{num_rows}\' %data_dir current-file \'late_sd.asc\' asc-col")
        self.xh.sendCommand(f"module save \'xchip_mid_ave{num_rows}\' %data_dir current-file \'mid_ave.asc\' asc-col")
        self.xh.sendCommand(f"module save \'xchip_mid_sd{num_rows}\' %data_dir current-file \'mid_sd.asc\' asc-col")
        if (num_stability):
            self.xh.sendCommand(f"module save \'xchip_off_sd{num_rows}\' %data_dir current-file \'off_sd.asc\' asc-col")
            self.xh.sendCommand(f"module save \'xchip_on_sd{num_rows}\' %data_dir current-file \'on_sd.asc\' asc-col")
        self.xh.sendCommand(f"module save \'xchip_op_rms{num_rows}\' %data_dir current-file \'op_rms.asc\' asc-col")
        self.xh.sendCommand(f"module save \'xchip_op_sig{num_rows}\' %data_dir current-file \'op_sig.asc\' asc-col")
        self.xh.sendCommand(f"module save \'xchip_op_unscaled_rms{num_rows}\' %data_dir current-file \'op_unscaled_rms.asc\' asc-col")
        self.xh.sendCommand(f"module save \'xchip_overshoot{num_rows}\' %data_dir current-file \'overshoot.asc\' asc-col")

        self.xh.sendCommand(f"module save \'xchip_raw_shape{num_points}x{num_rows}\' %data_dir current-file \'raw_shape.asc\' asc-col")
        self.xh.sendCommand(f"module save \'xchip_shape{num_points}x{num_rows}\' %data_dir current-file \'shape.asc\' asc-col")
        self.xh.sendCommand(f"module save \'xchip_raw_shape{num_points}x{num_rows}\' %data_dir current-file \'raw_shape.det\' det")
        self.xh.sendCommand(f"module save \'xchip_shape{num_points}x{num_rows}\' %data_dir current-file \'shape.det\' det")
        self.xh.sendCommand(f"module save \'xchip_beam_current{num_rows}\' %data_dir current-file \'beam_current.asc\' asc-col")
        if npts_rst_hold>0 :
            self.xh.sendCommand(f"module save 'xchip_rst_hold_shape{npts_rst_hold}x{num_rows}' %data_dir current-file 'rst_hold_shape.asc' asc-col")
        if npts_rst>0 :
            self.xh.sendCommand(f"module save 'xchip_rst_shape{npts_rst}x{num_rows}' %data_dir current-file 'rst_shape.asc' asc-col")

    """
    measure_iv.... Methods
    """
    def post_measure(self):
        # DO what you want after the end of the cleanup and a potential ctrl-c
        self.xh_hv_set(0)
        self.xh.sendCommand("xstrip head set-dac 'xh0' %xstrip_ref vref")
        self.xh.sendCommand("xstrip offsets write 'xh0' 0 32 from-local-file %cal_dir+'xh0_400.offsets'")
        self.xh.sendCommand("ke2410 set-mode v -202.0 i 0.025")
        self.xh.sendCommand("ke2410 set-i-range i20mA")
        
    def measure_iv_xh_og_ke2410(self, npts, comment, save, measure_og):

        with cleanup(self.post_measure):
            #tango_io( XH_DEV["Xh"], "timeout", 200)
            self.xh_hv_set(0)
            print ("Finished setting HV back to zero")
            self.xh.sendCommand("xstrip hv enable 'xh0' auto")
            if measure_og:
                self.xh.sendCommand("client define '169.254.2.2' 1972 user1")
                self.xh.sendCommand("send user1 '~config'")
            if save >= 0:
                if save >= self.xh_max_history:
                    print(f"ERROR: XH max historty too small to save row {save}\nContinuing without save\n")
                    save = -1
                else:
                    self.xh.sendCommand(f"set-string \"mname_hist\" \"iv_og{npts}_hist\" local")
                    self.xh.sendCommand(f"module create %mname_hist {npts+1} {self.xh_max_history} header double num-t 2")
                    self.xh.sendCommand(f"set-string \"mname_xh_hist\" \"iv_og{npts}_xh_hist\" local")
                    self.xh.sendCommand(f"module create %mname_xh_hist 1024 {2*self.xh_max_history} header")
                    if save == 0:
                        self.xh.sendCommand("module clear %mname_hist")
                        self.xh.sendCommand("module clear %mname_xh_hist")
            # Moved to config_xhx3.cmd.
            #    self.xh.sendCommand",sprintf("ke2410 send \":OUTP:ENAB:STAT 0\"")) # Not if using external Output enable
            #    self.xh.sendCommand",sprintf("ke2410 enable 1"))
            self.xh.sendCommand("ke2410 set-mode v -202.0 ui 100.0")

            #    self.xh.sendCommand",sprintf("ke2410 send \":SENS:AVER:TCON REP\""))
            #    self.xh.sendCommand",sprintf("ke2410 send \':SENS:AVER:COUNT 2\'"))
            #    self.xh.sendCommand",sprintf("ke2410 send \':SENS:AVER:STATE ON\'"))
            #    self.xh.sendCommand",sprintf("ke2410 send \':SENS:CURR:NPLC 10\'"))
            self.xh.sendCommand("ke2410 set-i-range i100uA")

            self.xh.sendCommand("ke2410 pause-log")
            print("Warning: Setting Offset and Vref to all XHcip to see oposite direction current. Reset using config_xhx3.cmd")
            self.xh.sendCommand("xstrip head set-dac 'xh0' 0.9 vref")
            self.xh.sendCommand("xstrip offsets set 'xh0' 0 32 20000")
            self.xh.sendCommand("xstrip head set-cal-en 'xh0' 0")
            self.xh.sendCommand("xstrip head set-led 'xh0' 0")
            self.set_caps(10)
            self.xh.sendCommand("set-func \"upath\" \'xstrip open \"xh0\" un-intl1024\' local hide")
            self.xh.sendCommand(f"set-string \"mname\" \"iv_og{npts}\" local")
            self.xh.sendCommand(f"module create %mname {npts+1} 10 header double")

            self.xh.sendCommand("xstrip timing stop \'xh0\'")
            for i in range(npts+1):
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*i} 1 1000 5 ext-trig-group trig-mux 9")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*i+1} 1 1000 50")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*i+2} 1 1000 500")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*i+3} 1 1000 5000")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*i+4} 1 1000 50000")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*i+5} 1 100  500000")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*i+6} 1 10   5000000")
            self.xh.sendCommand(f"set-func \"nfrm\" \"xstrip timing modify-group \'xh0\' {7*npts+6} last\" local hide")
            self.xh.sendCommand("xstrip timing start \'xh0\'")

            #display_tool = get_config().get("display")
            #label_y = ["VS", "I"]
            for i in range(npts+1):
                #if i == 0:
                #    data_v = numpy.zeros((1))
                #    data_vs = numpy.zeros((1))
                #    data_i = numpy.zeros((1))

                print(f"Setting V={-i*2.0:6.2f}", end="\r")
                self.xh.sendCommand(f"xstrip hv set-dac 'xh0' {-2.0*i}")
                gevent.sleep(1)
                self.xh.sendCommand("set-func 'tmp2' \"ke2410 get var 'a'\" local hide")
                if measure_og:
                    self.xh.sendCommand("send user1 \"ke2700 measure var 'b'\"")
                    self.xh.sendCommand("send user1 \"%b.mean\" double \"og_i\"")
                    self.xh.sendCommand(f"module set-point %mname {i} 5 1E6*%og_i")
                else:
                    self.xh.sendCommand(f"module set-point %mname {i} 5 0.0")
                self.xh.sendCommand("xstrip timing continue 'xh0'")
                self.xh.sendCommand(f"module set-point %mname {i} 0 %a.vs")
                #vs_val = float(self.xh.sendCommandNumber("%a.vs"))
                #if i == 0:
                #    data_vs[0] = vs_val
                #else:
                #    data_vs = numpy.append(data_vs, [vs_val])
                self.xh.sendCommand(f"module set-point %mname {i} 1 %a.v")
                #v_val = float(self.xh.sendCommandNumber("%a.v"))
                #if i == 0:
                #    data_v[0] = v_val
                #else:
                #    data_v = numpy.append(data_v, [v_val])
                self.xh.sendCommand(f"module set-point %mname {i} 2 %a.i")
                #i_val = float(self.xh.sendCommandNumber("%a.i"))
                #if i == 0:
                #    data_i[0] = i_val
                #else:
                #    data_i = numpy.append(data_i, [i_val])

                #data_y = numpy.zeros((2, len(data_vs)))
                #data_y[0] = data_vs
                #data_y[1] = data_i
                #print(f"v={v_val}\tvs={vs_val}\ti={i_val}")
                ###display_tool.plot_curvestack("iv_og", "iv_og", data_y, data_v, labels, "V", "iv_og")
                #display_tool.plot_curve("iv_og", "iv_og", "iv_og", data_v, data_y, "V", "VS / I", names=label_y)
                
                self.xh.sendCommand(f"module set-point %mname {i} 3 0.0")
                self.xh.sendCommand(f"module set-point %mname {i} 4 0.0")
                for j in range(4):
                    self.xh.sendCommand(f"set-func 't' 'xstrip tc get `xh0` ch {j} t' local hide")
                    self.xh.sendCommand(f"module set-point %mname {i} {6+j} %t")
                self.xh.sendCommand("xstrip timing wait 'xh0'")
            
            
            self.xh.sendCommand(f"module save %mname %data_dir next-file 'iv_og.asc' asc-col xh 'xh0' comment1 'BLISS:measure_iv_xh_og_ke2410({npts},{comment},{save})' comment2 '{comment}'")
            self.xh.sendCommand("ke2410 continue-log")

            self.xh.sendCommand("file get-next-index %data_dir query var \"seq\"")
            self.xh.sendCommand("file get-next-index %data_dir 'File:'+(string)seq+'_xh_iv.asc' append")
            if save >= 0:
                self.xh.sendCommand(f"module copy %mname %mname_hist src-y 2 dst-y {save}")
                self.xh.sendCommand(f"module copy %mname %mname_hist src-y 5 dst-y {save} dst-t 1")
                self.xh.sendCommand(f"read 0 0 {7*npts+1} 1024 1 1 from %upath to-module %mname_xh_hist raw start-y {2*save}")
                self.xh.sendCommand(f"read 0 0 {7*npts+5} 1024 1 1 from %upath to-module %mname_xh_hist raw start-y {2*save+1}")
            
            self.xh.sendCommand(f"read 0 0 0 1024 1 {(npts+1)*7} from %upath to-local-file %data_dir+(string)seq+'_xh_iv.asc' asc-col")
            self.xh.sendCommand("close %upath")

    def scan_atten_iv(self):
#        attens= [ "Al160",  "Al80", "Al40", "Al20", "Al10", "Al0"]
        attens= [ "Al10", "Al0"]
        nattens= len(attens)
        for i in range(nattens):
            self.move_target(attens[i%nattens])
            self.measure_iv_xh_og_ke2410_beam(50, f"Scanning atten={attens[i]}", i, measure_og=False, beam=True, suffix=attens[i])

    def measure_iv_xh_og_ke2410_beam(self, npts=50, comment="", save=0, measure_og=False, beam=True, suffix=""):
        num_on_off = 2
        vstep = 4
        #tango_io( XH_DEV["Xh"], "timeout", 200)
        self.cedshut()
        if len(suffix) > 0 :
            suffix = suffix + "_"
        self.xh_hv_set(0)
        print ("Finished setting HV back to zero")
        self.xh.sendCommand("xstrip hv enable 'xh0' auto")
        if measure_og:
            self.xh.sendCommand("client define '169.254.2.2' 1972 user1")
            self.xh.sendCommand("send user1 '~config'")
        if save >= 0:
            if save >= self.xh_max_history:
                print(f"ERROR: XH max historty too small to save row {save}\nContinuing without save\n")
                save = -1
            else:
                self.xh.sendCommand(f"set-string \"mname_hist\" \"iv_og{npts}_hist\" local")
                self.xh.sendCommand(f"module create %mname_hist {npts+1} {self.xh_max_history} header double num-t 2")
                self.xh.sendCommand(f"set-string \"mname_xh_hist\" \"iv_og{npts}_xh_hist\" local")
                self.xh.sendCommand(f"module create %mname_xh_hist 1024 {2*self.xh_max_history} header")
                if save == 0:
                    self.xh.sendCommand("module clear %mname_hist")
                    self.xh.sendCommand("module clear %mname_xh_hist")
        # Moved to config_xhx3.cmd.
        #    self.xh.sendCommand",sprintf("ke2410 send \":OUTP:ENAB:STAT 0\"")) # Not if using external Output enable
        #    self.xh.sendCommand",sprintf("ke2410 enable 1"))
        self.xh.sendCommand("ke2410 set-mode v -202.0 i 0.025")

#        self.xh.sendCommand("ke2410 send ':SENS:AVER:TCON REP'")
#        self.xh.sendCommand("ke2410 send ':SENS:AVER:COUNT 2'")
        #    self.xh.sendCommand("ke2410 send \':SENS:AVER:STATE ON\'")
        self.xh.sendCommand("set-func 'tmp' \"ke2410 send ':SENS:CURR:NPLC 1'\" hide ")  # Reduced to 1 cycle to speed up. set-func used to hide string rtunr from Lima
        self.xh.sendCommand("ke2410 set-i-range i20mA")

        self.xh.sendCommand("ke2410 pause-log")
#        print("Warning: Setting Offset and Vref to all XHcip to see oposite direction current. Reset using config_xhx3.cmd")
#        self.xh.sendCommand("xstrip head set-dac 'xh0' 0.9 vref")
#        self.xh.sendCommand("xstrip offsets set 'xh0' 0 32 20000")
        self.xh.sendCommand("xstrip head set-cal-en 'xh0' 0")
        self.xh.sendCommand("xstrip head set-led 'xh0' 0")

        self.xh.sendCommand("set-func 'upath' 'xstrip open \"xh0\" un-intl1024' local hide")
        mname= f"iv_og{npts}"
        self.xh.sendCommand(f"module create '{mname}' {npts+1} 10 header double")

        self.xh.sendCommand("xstrip timing stop 'xh0'")
        for i in range(npts+1):
            for j in range(num_on_off):
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)} 1 1000 3 ext-trig-group trig-mux 9")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+1} 1 1000 30")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+2} 1 1000 50")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+3} 1 1000 500")
                if j==0:
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+4} 1 1000 1000")
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+5} 1 1000 2000")
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+6} 1 1000 5000")
                else:
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+4} 1 1000 50000")
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+5} 1 100  500000")
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+6} 1 10   5000000")
        self.xh.sendCommand(f"set-func 'nfrm' 'xstrip timing modify-group \"xh0\" {7*(npts+1)*num_on_off-1} last' local hide")

        if beam:
            self.oedshut()
        self.xh.sendCommand("xstrip timing start 'xh0'")
        for i in range(npts+1):
            print(f"Setting V={-i*vstep:6.2f}")
            self.xh.sendCommand(f"xstrip hv set-dac 'xh0' {-vstep*i}")
            gevent.sleep(1)
            self.oshut()
            self.xh.sendCommand("set-func 'tmp2' \"ke2410 get var 'a'\" local hide")
            if measure_og:
                self.xh.sendCommand("send user1 \"ke2700 measure var 'b'\"")
                self.xh.sendCommand(f"module set-point '{mname}' {i} 5 1E6*%og_i")
#                    else:
#                        self.xh.sendCommand(f"module set-point '{mname}' {i} 5 0.0")
            self.xh.sendCommand("xstrip timing continue 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.cshut()
            self.xh.sendCommand(f"module set-point '{mname}' {i} 0 %a.vs")
            self.xh.sendCommand(f"module set-point '{mname}' {i} 1 %a.v")
            self.xh.sendCommand(f"module set-point '{mname}' {i} 2 %a.i")
            self.xh.sendCommand(f"set-func 'vref0' 'xstrip head get-adc \"xh0\" 0 vref'")
            self.xh.sendCommand(f"set-func 'vref1' 'xstrip head get-adc \"xh0\" 1 vref'")
            self.xh.sendCommand(f"module set-point '{mname}' {i} 4 %vref0")
            self.xh.sendCommand(f"module set-point '{mname}' {i} 5 %vref1")
            for j in range(4):
                self.xh.sendCommand(f"set-func 't' 'xstrip tc get `xh0` ch {j} t' local hide")
                self.xh.sendCommand(f"module set-point '{mname}' {i} {6+j} %t")
            if num_on_off == 2:
                self.xh.sendCommand("set-func 'tmp2' \"ke2410 get var 'a_off'\" local hide")
                self.xh.sendCommand(f"module set-point '{mname}' {i} 3 %a_off.i")
                self.xh.sendCommand("xstrip timing continue 'xh0'")
                self.xh.sendCommand("xstrip timing wait 'xh0'")
            else:
                self.xh.sendCommand(f"module set-point '{mname}' {i} 3 0.0")
        self.cedshut()
        self.xh.sendCommand(f"module save '{mname}' %data_dir next-file '{suffix}iv_og.asc' asc-col xh 'xh0' comment1 'BLISS:measure_iv_xh_og_ke2410_beam({npts},{comment},{save})' comment2 '{comment}'")
        self.xh.sendCommand("set-func 'tmp' \"ke2410 send ':SENS:CURR:NPLC 10'\" hide ")  # Restore NPLC 10
        self.xh.sendCommand("ke2410 continue-log")

        self.xh.sendCommand("file get-next-index %data_dir query var 'seq'")
        self.xh.sendCommand(f"file get-next-index %data_dir 'File:'+(string)seq+'_{suffix}xh_iv.asc' append")
        if save >= 0:
            self.xh.sendCommand(f"module copy '{mname}' %mname_hist src-y 2 dst-y {save}")
            self.xh.sendCommand(f"module copy '{mname}' %mname_hist src-y 5 dst-y {save} dst-t 1")
            self.xh.sendCommand(f"read 0 0 {7*npts*num_on_off+1} 1024 1 1 from %upath to-module %mname_xh_hist raw start-y {2*save}")
            self.xh.sendCommand(f"read 0 0 {7*npts*num_on_off+5} 1024 1 1 from %upath to-module %mname_xh_hist raw start-y {2*save+1}")
        
        self.xh.sendCommand(f"read 0 0 0 1024 1 {(npts+1)*num_on_off*7} from %upath to-local-file %data_dir+(string)seq+'_xh_iv.asc' asc-col")
        self.xh.sendCommand("close %upath")
        
    def scan_volt_atten_vref(self):
        attens= [ "Al160", "Al20" ]
        nattens= len(attens)
        volts= [ -100, -150, -200]
        for i in range(nattens):
            self.move_target(attens[i%nattens])
            for j in range(len(volts)):
                self.scan_vref_iv_xh_og_ke2410_beam(20, f"Scanning Vref, atten={attens[i]}, Vbias={volts[j]}", j+len(volts)*i, measure_og=False, beam=True, suffix=attens[i]+f"V{-volts[j]}", vbias=volts[i])
        
    def scan_vref_iv_xh_og_ke2410_beam(self, npts=20, comment="", save=0, measure_og=False, beam=True, suffix="", vbias=-150):
        num_on_off = 2
        vstep = 0.02
        #tango_io( XH_DEV["Xh"], "timeout", 200)
        self.cedshut()
        if len(suffix) > 0 :
            suffix = suffix + "_"
        self.xh_hv_set(vbias)
        print (f"Finished setting HV back to {vbias} V")
        self.xh.sendCommand("xstrip hv enable 'xh0' auto")
        if measure_og:
            self.xh.sendCommand("client define '169.254.2.2' 1972 user1")
            self.xh.sendCommand("send user1 '~config'")
        if save >= 0:
            if save >= self.xh_max_history:
                print(f"ERROR: XH max historty too small to save row {save}\nContinuing without save\n")
                save = -1
            else:
                self.xh.sendCommand(f"set-string \"mname_hist\" \"iv_og{npts}_hist\" local")
                self.xh.sendCommand(f"module create %mname_hist {npts+1} {self.xh_max_history} header double num-t 2")
                self.xh.sendCommand(f"set-string \"mname_xh_hist\" \"iv_og{npts}_xh_hist\" local")
                self.xh.sendCommand(f"module create %mname_xh_hist 1024 {2*self.xh_max_history} header")
                if save == 0:
                    self.xh.sendCommand("module clear %mname_hist")
                    self.xh.sendCommand("module clear %mname_xh_hist")
        # Moved to config_xhx3.cmd.
        #    self.xh.sendCommand",sprintf("ke2410 send \":OUTP:ENAB:STAT 0\"")) # Not if using external Output enable
        #    self.xh.sendCommand",sprintf("ke2410 enable 1"))
        self.xh.sendCommand("ke2410 set-mode v -202.0 i 0.025")

#        self.xh.sendCommand("ke2410 send ':SENS:AVER:TCON REP'")
#        self.xh.sendCommand("ke2410 send ':SENS:AVER:COUNT 2'")
        #    self.xh.sendCommand("ke2410 send \':SENS:AVER:STATE ON\'")
        self.xh.sendCommand("set-func 'tmp' \"ke2410 send ':SENS:CURR:NPLC 1'\" hide ")  # Reduced to 1 cycle to speed up. set-func used to hide string rtunr from Lima
        self.xh.sendCommand("ke2410 set-i-range i20mA")

        self.xh.sendCommand("ke2410 pause-log")
        print("Warning: Setting Offset and Vref to all XHcip to see oposite direction current. Reset using config_xhx3.cmd")
        self.xh.sendCommand("xstrip head set-dac 'xh0' 0.9 vref")
        self.xh.sendCommand("xstrip offsets set 'xh0' 0 32 20000")
        self.xh.sendCommand("xstrip head set-cal-en 'xh0' 0")
        self.xh.sendCommand("xstrip head set-led 'xh0' 0")

        self.xh.sendCommand("set-func 'upath' 'xstrip open \"xh0\" un-intl1024' local hide")
        mname= f"iv_og{npts}"
        self.xh.sendCommand(f"module create '{mname}' {npts+1} 10 header double")

        self.xh.sendCommand("xstrip timing stop 'xh0'")
        for i in range(npts+1):
            for j in range(num_on_off):
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)} 1 1000 3 ext-trig-group trig-mux 9")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+1} 1 1000 30")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+2} 1 1000 50")
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+3} 1 1000 500")
                if j==0:
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+4} 1 1000 1000")
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+5} 1 1000 2000")
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+6} 1 1000 5000")
                else:
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+4} 1 1000 50000")
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+5} 1 100  500000")
                    self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {7*(i*num_on_off+j)+6} 1 10   5000000")
        self.xh.sendCommand(f"set-func 'nfrm' 'xstrip timing modify-group \"xh0\" {7*(npts+1)*num_on_off-1} last' local hide")

        if beam:
            self.oedshut()
        self.xh.sendCommand("xstrip timing start 'xh0'")
        for i in range(npts+1):
            vref = 0.9 + vstep*(i-int(npts/2))
            print(f"Setting Vvref={vref:6.2f}")
            self.xh.sendCommand(f"xstrip head set-dac 'xh0' {vref} head 1 vref vrefc")
            gevent.sleep(1)
            self.oshut()
            self.xh.sendCommand("set-func 'tmp2' \"ke2410 get var 'a'\" local hide")
            if measure_og:
                self.xh.sendCommand("send user1 \"ke2700 measure var 'b'\"")
                self.xh.sendCommand(f"module set-point '{mname}' {i} 5 1E6*%og_i")
#                    else:
#                        self.xh.sendCommand(f"module set-point '{mname}' {i} 5 0.0")
            self.xh.sendCommand("xstrip timing continue 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.cshut()
            self.xh.sendCommand(f"module set-point '{mname}' {i} 0 %a.vs")
            self.xh.sendCommand(f"module set-point '{mname}' {i} 1 %a.v")
            self.xh.sendCommand(f"module set-point '{mname}' {i} 2 %a.i")
            self.xh.sendCommand(f"set-func 'vref0' 'xstrip head get-adc \"xh0\" 0 vref'")
            self.xh.sendCommand(f"set-func 'vref1' 'xstrip head get-adc \"xh0\" 1 vref'")
            self.xh.sendCommand(f"module set-point '{mname}' {i} 4 %vref0")
            self.xh.sendCommand(f"module set-point '{mname}' {i} 5 %vref1")
            for j in range(4):
                self.xh.sendCommand(f"set-func 't' 'xstrip tc get `xh0` ch {j} t' local hide")
                self.xh.sendCommand(f"module set-point '{mname}' {i} {6+j} %t")
            if num_on_off == 2:
                self.xh.sendCommand("set-func 'tmp2' \"ke2410 get var 'a_off'\" local hide")
                self.xh.sendCommand(f"module set-point '{mname}' {i} 3 %a_off.i")
                self.xh.sendCommand("xstrip timing continue 'xh0'")
                self.xh.sendCommand("xstrip timing wait 'xh0'")
            else:
                self.xh.sendCommand(f"module set-point '{mname}' {i} 3 0.0")
        self.cedshut()
        self.xh.sendCommand(f"module save '{mname}' %data_dir next-file '{suffix}iv_og.asc' asc-col xh 'xh0' comment1 'BLISS:measure_iv_xh_og_ke2410_beam({npts},{comment},{save})' comment2 '{comment}'")
        self.xh.sendCommand("set-func 'tmp' \"ke2410 send ':SENS:CURR:NPLC 10'\" hide ")  # Restore NPLC 10
        self.xh.sendCommand("ke2410 continue-log")

        self.xh.sendCommand("file get-next-index %data_dir query var 'seq'")
        self.xh.sendCommand(f"file get-next-index %data_dir 'File:'+(string)seq+'_{suffix}xh_iv.asc' append")
        if save >= 0:
            self.xh.sendCommand(f"module copy '{mname}' %mname_hist src-y 2 dst-y {save}")
            self.xh.sendCommand(f"module copy '{mname}' %mname_hist src-y 5 dst-y {save} dst-t 1")
            self.xh.sendCommand(f"read 0 0 {7*npts*num_on_off+1} 1024 1 1 from %upath to-module %mname_xh_hist raw start-y {2*save}")
            self.xh.sendCommand(f"read 0 0 {7*npts*num_on_off+5} 1024 1 1 from %upath to-module %mname_xh_hist raw start-y {2*save+1}")
        
        self.xh.sendCommand(f"read 0 0 0 1024 1 {(npts+1)*num_on_off*7} from %upath to-local-file %data_dir+(string)seq+'_xh_iv.asc' asc-col")
        self.xh.sendCommand("close %upath")

    """
    Scan Voltage Method
    """
    def xh_calib_scan_volts(self, comment1):
                
        rep = click.prompt(mystr, default=default)

        int_time = int(click.prompt("    Integration time (cycles)", default=31))
        nscans = int(click.prompt("    Number of scans", default=32000))
        first_volts = float(click.prompt("    First Bias Voltage", defaults=-40))
        last_volts = float(click.prompt("    Last Bias Voltage", defaults=-100))
        volt_inc   = float(click.prompt("    Voltage increment", defaults=-10))
        
        n = (last_volts - first_volts) / volt_inc
        
        if n < 1 or (first_volts+n*volt_inc != last_volts):
            print(f"Error: Calculated number of steps = {n}, (first_volts={first_volts})  + (volts_inc={volts_inc})*(n={n}) = {first_volts+n*volt_inc} != (last_volts={last_volts})")
            return

        volts = first_volts
        self.xh_timing_orbit_pause(2+n, nscans, int_time)

        self.cshut()
        self.xh_timing_start()
        self.xh_timing_continue()
        self.xh_timing_wait()
        self.oshut()

        for i in range(n):
            print(f"Setting Detector bias Volts = {volts}\n")
            self.xh_hv_set(volts)
            volts = volts + volt_inc
            self.xh.sendCommand("xstrip hv get-adc \'xh0\'")
            xh_timing_continue()
            xh_timing_wait()

        self.cshut()           
        description = f"XH Bias Voltage scan from {first_volts} to {last_volts} in {n} steps, nscans={nscans}, int_time={int_time}"
        self.xh_save(comment1, description)
        
        self.xh_hv_set(first_volts)
    
    """
    Investigate the Pre-Amp response capturing a single bunch
    """
    def scan_orbit_private(self, start, num, intt,comment=None):
        if comment == None :
            comment = f"Scanning orbit delay from {start} to {start+num-1}"
        else:
            comment = f"Scanning orbit delay from {start} to {start+num-1} "+comment
            
        self.xh_timing_orbit_pause(num+1, 1000, intt)
        self.cshut()
        self.xh_timing_start()
        self.xh_timing_continue()
        self.xh_timing_wait()
        self.oshut()
        for ii in range(num):
            cmd = f"xstrip timing setup-orbit \'xh0\' {start+ii}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.xh_timing_continue()
            self.xh_timing_wait()
        self.cshut()
        self._xh_save_det(comment)

    def scan_integration(self, start_int, npts_int, incr_int=1, num_fine=2, save_raw=False, orbit_offset=0, suffix="", index=0, total_points=-1, save_summary=False):
        nscans = 2
        num_stability=0
        ncycles = 2000
        fixed_rstr_s1r = 2
        frac_a=-1
        frac_b=-1
        cfb=10
#        cfb=15
        
        comment2 = f"scan_integration: t_int from {start_int} for {npts_int} step {incr_int}, num_fine={num_fine}"
        self.xh_timing_stop()
        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} first-int {start_int} incr-int {incr_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset 0 scan-period -1"
        if num_fine==4:
            setup_cmd = setup_cmd + " fine"
        elif num_fine==2:
            setup_cmd = setup_cmd + " fine2" 
        print(setup_cmd)
        self.xh.sendCommand(setup_cmd)

        if total_points>0:
            self.xh.sendCommand(f"module create 'xchip_beam_current{total_points}' {total_points} 2 header double")
        self.set_caps(cfb)
        self.set_orbit(self.orbit_delay+orbit_offset)
        self.cshut()
        self.xh.sendCommand("xstrip timing start 'xh0'")
        self.xh.sendCommand("xstrip timing wait 'xh0'")
        self.oshut()
        self.oedshut()
        self.xh.sendCommand("xstrip timing continue 'xh0'")
        if total_points > 0 :
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{total_points}' {index} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{total_points}' {index} 1 {self.sbcur.raw_read}")
        
        self.xh.sendCommand("xstrip timing wait 'xh0'")
        self.cshut()
        self.cedshut()
        if total_points>0:
            cmd = f"xstrip xchip process 'xh0' un-interleave row {index} num-rows {total_points} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.scan_xchip_save_point(npts_int*num_fine, 0, 0, comment2, setup_cmd, suffix, index, save_raw)
    
        if save_summary:
            self.scan_xchip_save_summary(num_fine*npts_int, 0, 0, total_points, 0, frac_a, frac_b, comment2, setup_cmd)

    def scan_orbit_integration(self, start, num, num_stability=0, quarters=False, npts_int=31, save_raw=False):
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        frac_a=-1
        frac_b=-1

        comment = f"scan_orbit_integration: Scanning orbit delay from {start} for {num}"
        self.xh_timing_stop()
        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset 0 scan-period -1"
        if quarters:
            setup_cmd = setup_cmd + " fine"
            num_fine = 4
        else:
            setup_cmd = setup_cmd + " fine2"
            num_fine = 2
        print(setup_cmd)
        self.xh.sendCommand(setup_cmd)

        self.xh.sendCommand(f"module create 'xchip_beam_current{num}' {num} 2 header double")
        self.oedshut()
        for i in range(num):
            cmd = f"xstrip timing setup-orbit 'xh0' {start+i}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.cshut()
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue 'xh0'")
            print ("About to read sr current")
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")
        
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.cshut()

            #cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b}"
            print(cmd)
            self.xh.sendCommand(cmd)
            if save_raw:
                if i==0:
                    self.save_det(comment, setup_cmd, f"orbit{start+i:03d}.det", 0)
                else:
                    self.save_det("", "", f"orbit{start+i:03d}.det", 1)
        self.cedshut()    
        self.scan_xchip_save_summary(num_fine*npts_int, 0, 0, num, 0, frac_a, frac_b, comment, setup_cmd)

    def scan_integration_stability(self):
        quarters=False
        npts_int=31
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        frac_a=-1
        frac_b=-1
        num_stability=[10, 20, 50, 100, 200, 500, 1000]
        num = len(num_stability)

        self.xh.sendCommand(f"module create 'xchip_beam_current{num}' {num} 2 header double")

        self.xh_timing_stop()

        for i in range(num):
            self.set_orbit(self.orbit_delay)
            setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset 0 scan-period -1 stability {num_stability[i]}"
            if quarters:
                setup_cmd = setup_cmd+" fine"
                num_fine = 4
            else:
                setup_cmd = setup_cmd+" fine2"
                num_fine = 2
            print(setup_cmd)
            self.xh.sendCommand(setup_cmd)
            self.cshut()
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue 'xh0'")
        
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.cshut()
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")

            #cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b}"
            print(cmd)
            self.xh.sendCommand(cmd)
    
        self.scan_xchip_save_summary(num_fine*npts_int, 0, 0, num, 1, frac_a, frac_b, f"scan_integration_stability: Trying stability={num_stability}", setup_cmd )

    def scan_t_rst_s1(self):

        for i in range(11):
            self.set_orbit(self.orbit_delay)
            self.xh.sendCommand(f"xstrip timing fixed2 'xh0' rstr-s1r {i-5}" )
            self.scan_orbit_private(self.orbit_delay, 20, 12, f"trst-s1r={i-5}")

    def scan_ibias_integration(self):
        quarters=False
        npts_int=31
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        num = 1
        frac_a=8
        frac_b=10
        npts_rst = 16
        first_rstr_sr1= 12
        npts_rst_hold=0
        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset {npts_rst} scan-period -1 first-rstr-s1r {first_rstr_s1r}"
        if quarters:
            setup_cmd = setup_cmd+" fine"
            num_fine = 4
        else:
            setup_cmd = setup_cmd+" fine2"
            num_fine = 2
        print(setup_cmd)
        self.xh.sendCommand(f"module create 'xchip_beam_current{num}' {num} 2 header double")
        self.set_caps(2)
        # self.xh_hv_set(-200)
        self.xh.sendCommand(f"xstrip tc set 'xh0' autoinc -1 ")
        old_ibias=0
        self.xh_timing_stop()
        for i in range(num):
            self.cshut()
            ibias = i % 7
            cmd = f"xstrip head set-xchip3-default  'xh0' Ibias-in {ibias} Ibias-SF {ibias} clamp-neg"
            print(cmd)
            self.xh.sendCommand(cmd)
            if old_ibias != ibias:
                old_ibias=ibias
                for j in range(5):
                    gevent.sleep(60)
            self.set_orbit(self.orbit_delay)
            self.xh.sendCommand(setup_cmd)
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue 'xh0'")
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.cshut()

        #    cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.scan_xchip_save_point(npts_int*num_fine, npts_rst_hold*num_fine, nm_fine*npts_rst, f"Ibias={ibias}", setup_cmd, f"ibias{ibias:02d}", int(i!=0))
            # self.scan_orbit_private(self.orbit_delay, 20, 12)
        
        self.xh.sendCommand("xstrip head set-xchip3-default  'xh0' min-power clamp-neg")
        self.scan_xchip_save_summary(npts_int, num_fine*npts_rst_hold, num_fine*npts_rst, num, 0, frac_a, frac_b, f"scan_ibias_integration: Scanning Ibias-in & Ibias-SF from 0 to {num-1}", setup_cmd)
        self.xh.sendCommand(f"module save 'XH_tc_log' %data_dir next-file 'temperature.asc' asc-col comment1 'Temperature log during above'")

    def scan_r_reset_integration(self):
        npts_int = 31
        quarters = False
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        num = 8
        frac_a=8
        frac_b=10
        npts_rst = 16
        first_rstr_s1r= 12
        npts_rst_hold=31
        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset {npts_rst} scan-period -1 first-rstr-s1r {first_rstr_s1r} npts-reset-hold {npts_rst_hold}"
        if quarters:
            setup_cmd = setup_cmd+" fine"
            num_fine = 4
        else:
            setup_cmd = setup_cmd+" fine2"
            num_fine = 2
        
        print(setup_cmd)
        self.xh.sendCommand(f"module create 'xchip_beam_current{num}' {num} 2 header double")
        self.set_caps(2)
        # self.xh_hv_set(-200)
        self.xh.sendCommand(f"xstrip tc set 'xh0' autoinc -1 ")
        old_ibias=2
        self.xh_timing_stop()
        for i in range(num):
            self.cshut()
            ibias = 2
            reset = i
            cmd = f"xstrip head set-xchip3-default  'xh0' Ibias-in {ibias} Ibias-SF {ibias} clamp-neg reset-ab {reset} reset-cd {reset}"
            print(cmd)
            self.xh.sendCommand(cmd)
            if old_ibias != ibias:
                old_ibias=ibias
                for j in range(5):
                    gevent.sleep(60)
            self.set_orbit(self.orbit_delay)
            self.xh.sendCommand(setup_cmd)
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue 'xh0'")
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.cshut()

        #    cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.scan_xchip_save_point(npts_int*num_fine, npts_rst_hold*num_fine, npts_rst*num_fine, f"reset-r={ibias}", setup_cmd, f"reset_r{reset}", int(i!=0))
            # self.scan_orbit_private(self.orbit_delay, 20, 12)
        
        self.xh.sendCommand("xstrip head set-xchip3-default  'xh0' min-power clamp-neg")
        self.scan_xchip_save_summary(num_fine*npts_int, num_fine*npts_rst_hold, num_fine*npts_rst, num, 0, frac_a, frac_b, f"scan_r_reset_integration: Scanning reset-[ab|cd] 0 to {num-1}", setup_cmd)
        self.xh.sendCommand(f"module save 'XH_tc_log' %data_dir current-file 'temperature.asc' asc-col comment1 'Temperature log during above'")

    def scan_preamp_caps(self):
#        capValues=(5, 7, 10)
        capValues=[5]
        self.xh_hv_set(-200)
        for c in capValues:
            self.scan_preamp_integration(c)
        self.xh_hv_set(-100)

    def scan_preamp_integration(self, capVal):
        quarters=False
        npts_int=31
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        num = 56
        frac_a=8
        frac_b=10
        npts_rst=16
        first_rstr_s1r = 12
        npts_rst_hold=0
        num_stability=100
        rst_fixed_int = 12 # reduce to 7 or 8 for 16 bunhc mode use
        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset {npts_rst} scan-period -1 stability {num_stability} rst-fixed-int {rst_fixed_int} first-rstr-s1r {first_rstr_s1r}"
        if quarters:
            setup_cmd = setup_cmd+" fine"
            num_fine = 4
        else:
            setup_cmd = setup_cmd+" fine2"
            num_fine = 2
        print(setup_cmd)
        self.xh.sendCommand(f"module create 'xchip_beam_current{num}' {num} 2 header double")
        self.set_caps(capVal)
#        self.xh_hv_set(-200)
        self.xh.sendCommand(f"xstrip tc set 'xh0' autoinc -1 ")
        old_ibias=-1
        self.xh_timing_stop()
        for i in range(num):
            self.cshut()
            biasrc = i % 8
            ibias = int(i/8)
            biasrc = 128+8*biasrc
            cmd = f"xstrip head set-xchip3-default  'xh0' Ibias-in {ibias} Ibias-SF {ibias} bias-RC {biasrc} clamp-neg"
            print(cmd)
            self.xh.sendCommand(cmd)
            if old_ibias != ibias:
                old_ibias=ibias
                for j in range(5):
                    gevent.sleep(60)
            self.set_orbit(self.orbit_delay)
            self.xh.sendCommand(setup_cmd)
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue 'xh0'")
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.cshut()

        #    cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b} mask-dead"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.scan_xchip_save_point(num_fine*npts_int, num_fine*npts_rst_hold, num_fine*npts_rst, f"Cfb={capVal}, Ibias={ibias},Bias-RC={biasrc}", setup_cmd, f"{i:02d}", int(i!=0))
            self.scan_orbit_private(self.orbit_delay, 20, 12)
        
        self.xh.sendCommand("xstrip head set-xchip3-default  'xh0' min-power clamp-neg")
        self.scan_xchip_save_summary(num_fine*npts_int, num_fine*npts_rst_hold, num_fine*npts_rst, num, num_stability, frac_a, frac_b, f"scan_preamp_integration: Cfb={capVal}, Scanning Ibias-in & Ibias-SF from 0 to 6, biasRC=128..174", setup_cmd)
        self.xh.sendCommand(f"module save 'XH_tc_log' %data_dir next-file 'temperature.asc' asc-col comment1 'Temperature log during above'")
        self.xh.sendCommand(f"module save 'xchip_beam_current{num}' %data_dir current-file 'beam_current.asc' asc-col comment1 'Beam current log during above'")


    def scan_ibias_vbias_integration(self, comment=""):
        quarters=False
        npts_int=31
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        num = 7 # 21
        npts_rst = 16
        first_rstr_s1r = 12
        npts_rst_hold = 0
        frac_a=8
        frac_b=10

        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset 0 scan-period -1 npts-reset-hold {npts_rst_hold} npts-reset {npts_rst} first-rstr-s1r {first_rstr_s1r}"
        if quarters:
            setup_cmd = setup_cmd+" fine"
            num_fine = 4
        else:
            setup_cmd = setup_cmd+" fine2"
            num_fine = 2
        print(setup_cmd)

        self.xh.sendCommand(f"module create 'xchip_beam_current{num}' {num} 2 header double")

        prev_vbias=0
        self.set_caps(2)
        self.xh_timing_stop()
        for i in range(num):
#            ibias = 2+2 *(i % 3);
#            vbias = -50 -25*int(i/3)
            ibias = 2
            vbias = -50  - 25*(i%7)
            print(f"Setting Ibias={ibias}, Vbias={vbias}")
            self.xh.sendCommand(f"xstrip head set-xchip3-default  'xh0' Ibias-in {ibias} Ibias-SF {ibias} clamp-neg")
            if vbias != prev_vbias:
                self.xh_hv_set(vbias)
                prev_vbias = vbias
            self.set_orbit(self.orbit_delay)
            self.xh.sendCommand(setup_cmd)
            self.cshut()
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue \'xh0\'")
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")
            self.xh.sendCommand("xstrip timing wait \'xh0\'")
            self.cshut()

        #    cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.scan_xchip_save_point(num_fine*npts_int, num_fine*npts_rst_hold, 0, f"Ibias={ibias}, Vbias={vbias}", setup_cmd, f"{i:02d}", int(i!=0))
            self.scan_orbit_private(self.orbit_delay, 20, 12)
            self.set_orbit(self.orbit_delay)
        
        self.xh.sendCommand("xstrip head set-xchip3-default  \'xh0\' min-power clamp-neg")
        self.xh_hv_set(-100)
        # comment = comment+"scan_ibias_vbias_integration: Scanning Ibias-in & Ibias-SF 2,4,6Vbias= -50, -75 to -200"
        comment = comment+"scan_ibias_vbias_integration: Scanning Vbias= -50, -75 to -200"
        self.scan_xchip_save_summary(num_fine*npts_int, num_fine*npts_rst_hold, num_fine*npts_rst, num, num_fine*npts_rst, frac_a, frac_b, comment, setup_cmd)

    def scan_temp_vbias_integration(self):
        npts_int=31
        quarters=False
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        num = 28
        npts_iv = 0
        npts_rst_hold = 0
        frac_a=8
        frac_b=10
        #tango_io( XH_DEV["Xh"], "timeout",200)
        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset 0 scan-period -1"
        if quarters:
            setup_cmd = setup_cmd+" fine"
            npts_int = npts_int*4
        else:
            setup_cmd = setup_cmd+" fine2"
            npts_int = npts_int*2
        print(setup_cmd)
        cmd = f"module create \'xchip_beam_current{num}\' {num} 2 header double"
        print(cmd)
        self.xh.sendCommand(cmd)
        # self.xh.sendCommand("client define \'169.254.4.2\' 1972 user1")
        # self.xh.sendCommand("send user1 \"~config\"")
        self.xh.sendCommand(f"xstrip tc set 'xh0' num-ave 5")
        self.xh.sendCommand(f"xstrip tc set 'xh0' autoinc -1 ")

        prev_vbias = 0
        prev_temp = -100.0
        self.xh_timing_stop()
        for i in range(num):
            ibias = 2
            vbias = -50 -25*(i%7)
            if int(i/7.0) == 0:
                temp = -50.0
            else:
                temp = -50 + int(i/7)*20
            print(f"Setting Ibias={ibias}, Vbias={vbias}, temp={temp}\n")
            cmd = f"xstrip head set-xchip3-default  \'xh0\' Ibias-in {ibias} Ibias-SF {ibias} clamp-neg"
            print(cmd)
            self.xh.sendCommand(cmd)
            if temp != prev_temp:
                self.cshut()
                self.xh.sendCommand(f"xstrip tc set \'xh0\' ch 0 setpoint {temp:3.1g}")
                self.xh.sendCommand(f"xstrip tc set \'xh0\' ch 1 setpoint {temp:3.1g}")
                print(f"Settling temperature to {temp}")
                prev_temp = temp
                self.xh_hv_set(0)
                prev_vbias = 0
                for j in range(110):
                    print(f"Sleeping {j} of 110 minutes", end="\r")
                    gevent.sleep (60)
                if npts_iv > 0:
                    self.measure_iv_xh_og(npts_iv, f"IV During impulse response scan, temp={temp}", int(i/7))
                self.xh.sendCommand(setup_cmd)
            if vbias != prev_vbias:
                self.xh_hv_set(vbias)
                prev_vbias = vbias
            self.scan_orbit_private(self.orbit_delay, 20, 12)
            self.set_orbit(self.orbit_delay)
            self.xh.sendCommand(setup_cmd)
            self.cshut()
            self.xh.sendCommand("xstrip timing start \'xh0\'")
            self.xh.sendCommand("xstrip timing wait \'xh0\'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue \'xh0\'")
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")
            self.xh.sendCommand("xstrip timing wait \'xh0\'")
            self.cshut()

        #    cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.scan_xchip_save_point(62, npts_rst_hold*2, 0, f"Ibias={ibias}, Vbias={vbias}, Temperature={temp}", setup_cmd, f"{i:02d}", int(i!=0))
            
        cmd = "xstrip head set-xchip3-default  \'xh0\' min-power clamp-neg"
        self.xh.sendCommand(cmd)
        self.xh_hv_set(0)
        self.scan_xchip_save_summary(62, 2*npts_rst_hold, 0, num, 0, frac_a, frac_b, "scan_temp_vbias_integration: Scanning Vbias-50 to -200 and temp -50, -30,-10 and 10", setup_cmd)
        if npts_iv>0 :
            self.xh.sendCommand(f"module save \'iv_og{npts_iv}_hist\' %data_dir next-file \'iv_og_hist.asc\' asc-col")
            self.xh.sendCommand(f"module save \'iv_og{npts_iv}_xh_hist\' %data_dir current-file \'iv_og_xh_hist.asc\' asc-col")
        self.xh.sendCommand(f"xstrip tc set 'xh0' ch 0 setpoint -50.0")
        self.xh.sendCommand(f"xstrip tc set 'xh0' ch 1 setpoint -50.0")
        self.xh.sendCommand(f"module save 'XH_tc_log' %data_dir next-file 'temperature.asc' asc-col comment1 'Temperature log during above'")
        self.xh.sendCommand(f"module save 'xchip_beam_current{num}' %data_dir current-file 'beam_current.asc' asc-col comment1 'Beam current log during above'")
        self.xh.sendCommand(f"xstrip tc set 'xh0' num-ave 1")

    """
    Previous Bunch Rejection
    """
    def scan_power(self):
        self.xh.sendCommand("xstrip tc set \'xh0\' ch 0 setpoint -65.0")
        self.xh.sendCommand("xstrip tc set \'xh0\' ch 1 setpoint -65.0")
        
        for k in range(2):
            if k == 0:
                self.xh.sendCommand("xstrip timing set-mode \"xh0\" no-overlap long-s2")
            else:
                self.xh.sendCommand("xstrip timing set-mode \"xh0\" delay-mux-reset-no-overlap long-s2")

            for i in range(5):
                self.xh.sendCommand(f"xstrip head set-xchip3-default \'xh0\' Ibias-SF {2*i} Ibias-in {2*i}")
                print(f"Set Ibias-in=IBias-SF={2*i}\n")
                self.xh.sendCommand("xstrip tc set \'xh0\' autoinc -1")
                for j in range(60):
                    print(f"Waiting {j} of 60")
                    gevent.sleep(60);
                self.xh.sendCommand(f"~save_tlog \'Settling with Ibias-in=IBias-SF={2*i}, delayed-mux-reset={k}\'")
                comment = f"IV to -200V Ibias-in=IBias-SF={2*i}, delayed-mux-reset={k}"
                self.measure_iv_xh_og_ke2410(100, comment, i+5*k)
                self.xh_hv_set_ke2410(0)

        self.xh.sendCommand("module save \"iv_og100_hist\" %data_dir next-file \"iv_hist.asc\" asc-col")

    def impulse_inl_scan4b(self):
        num_blocks=55
        first_fine=8*4
        last_fine=11*4
        nscans_fine=2
        first_late=24
        last_late=27
        nscans_late=2
        ncycles=10000
        fixed_rst_s1= -25
        first_pos=-268.0
        last_pos=-228.0
        
        self.set_orbit(self.orbit_delay)
        inc = (last_pos-first_pos)/(num_blocks-2)
        
        dedty = setup_globals.dedty

        self.cshut()

        self.xh_timing_stop()
        cmd = f"xstrip xchip setup-lin-beam 'xh0' {num_blocks} fixed-rst-s1 {fixed_rst_s1} first-fine {first_fine} last-fine {last_fine} nscans-fine {nscans_fine} first-late {first_late} last-late {last_late} nscans-late {nscans_late} cycles {ncycles}"
        print (cmd)
        self.xh.sendCommand(cmd)
        self.xh_hv_set(-200)
        self.set_caps(5)
        self.xh_timing_start()
        self.xh_timing_wait()
        for i in range(num_blocks-1):
            umv(dedty, first_pos+inc*i)
            self.oshut()
            self.xh_timing_continue()
            self.xh_timing_wait()
            self.cshut()
        
        self._xh_save_det2(f"XH Impulse scan detector posn from {first_pos} to {last_pos} in {num_blocks} steps: Command={cmd}", num_blocks)
        self.xh.sendCommand("xstrip set-num-y 'xh0' 1")

    def test_16b_corr(self, nscans, int_short, int_long, comment=""):
        self.set_orbit(self.orbit_delay)
        nfr = 1
        self.xh.sendCommand(f"xstrip timing setup-group 'xh0' 0 1 {nscans} {int_long} last orbit-scan orbit-mux 3 lemo-out 65535")
        self.dis16bcorr()
        self.cshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.cshut()
        self.xh_save(comment, f" Frames={nfr}, scans={nscans}, intT={int_long}", "off_long", 0)
        self.oshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.cshut()
        self.xh_save(comment, "on_long", 1)

        self.xh.sendCommand(f"xstrip timing setup-group 'xh0' 0 1 {nscans} {int_short} last orbit-scan orbit-mux 3 lemo-out 65535")
        self.dis16bcorr()
        self.cshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.cshut()
        self.xh_save(comment, f" Frames={nfr}, scans={nscans}, intT={int_short}", "off_short", 1)
        self.enb16bcorr()
        self.xh_save(comment, f"Corrected", "off_corr", 1)
        self.oshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.cshut()
        self.dis16bcorr()
        self.xh_save(comment,"", "on_short", 1)
        self.enb16bcorr()
        self.xh_save(comment, f"Corrected", "on_corr", 1)
        self.dis16bcorr()

#   def scan_posn_vbias_integration(self):
#        first_pos=-256.0
#        last_pos=-228.0
#        nsteps=10
#        inc=(last_pos-first_pos)/(nsteps-1)
#        
#        dedty = setup_globals.dedty
#        for i in range(nsteps):
#            umv(dedty, first_pos+inc*i)
#            self.scan_ibias_vbias_integration(f"Posn={first_pos+inc*i}")

    def scan_posn_vbias_integration(self, comment1=""):
        first_pos=-256.0
        last_pos=-228.0
        nsteps=10
        inc=(last_pos-first_pos)/(nsteps-1)
        quarters=False
        npts_int=31
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        num = 7*nsteps # 21
        npts_rst = 16
        first_rstr_s1r = 12
        npts_rst_hold = 0
        frac_a=8
        frac_b=10
        motor = setup_globals.dedty
        save_raw = False
        
        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset 0 scan-period -1 npts-reset-hold {npts_rst_hold} npts-reset {npts_rst} first-rstr-s1r {first_rstr_s1r}"
        if quarters:
            setup_cmd = setup_cmd+" fine"
            num_fine = 4
        else:
            setup_cmd = setup_cmd+" fine2"
            num_fine = 2
        print(setup_cmd)

        self.xh.sendCommand(f"module create 'xchip_beam_current{num}' {num} 2 header double")

        prev_vbias=0
        prev_pos=-999
        self.set_caps(2)
        self.xh_timing_stop()
        for i in range(num):
#            ibias = 2+2 *(i % 3);
#            vbias = -50 -25*int(i/3)
            pos = first_pos+(i%nsteps)*inc
#            umv(motor, pos)
            if pos!=prev_pos :
                umv(dedty, pos)
                prev_pos = pos
            ibias = 2
            vbias = -50  - 25*int(i/nsteps)
            print(f"Setting Ibias={ibias}, Vbias={vbias}")
            self.xh.sendCommand(f"xstrip head set-xchip3-default  'xh0' Ibias-in {ibias} Ibias-SF {ibias} clamp-neg")
            if vbias != prev_vbias:
                self.xh_hv_set(vbias)
                prev_vbias = vbias
            self.set_orbit(self.orbit_delay)
            self.xh.sendCommand(setup_cmd)
            self.cshut()
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue \'xh0\'")
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")
            self.xh.sendCommand("xstrip timing wait \'xh0\'")
            self.cshut()

        #    cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.scan_xchip_save_point(num_fine*npts_int, npts_rst_hold*num_fine, 0, f"Ibias={ibias}, Vbias={vbias}, pos={pos}", setup_cmd, f"{i:02d}", int(i!=0))
            self.scan_orbit_private(self.orbit_delay, 20, 12)
            self.set_orbit(self.orbit_delay)
        
        self.xh.sendCommand("xstrip head set-xchip3-default  \'xh0\' min-power clamp-neg")
        self.xh_hv_set(-100)
        # comment = comment+"scan_ibias_vbias_integration: Scanning Ibias-in & Ibias-SF 2,4,6Vbias= -50, -75 to -200"
        comment = f"scan_posn_vbias_integration: Scanning Vbias= -50, -75 to -200 and pos {first_pos} to {last_pos}"
        self.scan_xchip_save_summary(num_fine*npts_int, num_fine*npts_rst_hold, num_fine*npts_rst, num, num_fine*npts_rst, frac_a, frac_b, comment, setup_cmd)

    def scan_reset_r(self):
        capValues=[2,5,7,10]
        self.xh_hv_set(-200)
        self.xh.sendCommand("xstrip timing fixed2 'xh0' rstr-s1r 4")
        for c in capValues:
            for i in range(8):
                self.set_caps(c)
                self.xh.sendCommand(f"xstrip head set-xchip3-default  'xh0' Ibias-in 2 Ibias-SF 2 reset-ab {i} reset-cd {i} clamp-neg" )
                self.scan_orbit_private(self.orbit_delay, 20, 12, f"Cfb={c} pF, reset-[ab|cd]={i}")
        self.set_orbit(self.orbit_delay)

    def test_narrow_s1(self):
        capValues=[2,5,7,10]
        self.xh_hv_set(-200)
        for c in capValues:
            self.set_caps(c)
            self.xh.sendCommand(f"xstrip head set-xchip3-default  'xh0' Ibias-in 2 Ibias-SF 2 clamp-neg" )
            for trstr in range(5):
                for s1 in range(1,10):
                    if s1==8:
                        s1pw=25
                    else:
                        s1pw = s1
                    self.xh.sendCommand(f"xstrip timing fixed 'xh0' s1-pw {s1pw} s2-pw-min {s1pw}")
                    self.xh.sendCommand(f"xstrip timing fixed2 'xh0' rstr-s1r {trstr}")
                    self.scan_orbit_private(self.orbit_delay+25-s1pw, 20, 12, f"Cfb={c} pF, Trstrs1r={trstr} S1-pw={s1pw}")
        self.set_orbit(self.orbit_delay)
        
    def scan_reset_s2(self):
        capValues=[2,5,7,10]
        # capValues=[2]
        #self.xh_hv_set(-200)
        for c in capValues:
            self.set_caps(c)
            for j in range(8):
                for i in range(8):
                    self.xh.sendCommand(f"xstrip head set-xchip3-default  'xh0' Ibias-in 2 Ibias-SF 2 clamp-neg reset-ab {i} reset-cd {i}" )
                    trstr = -12+j-3 # set time from reset to S2, 3 after to 5 before
                    self.xh.sendCommand(f"xstrip timing set-mode 'xh0' no-overlap")
                    self.xh.sendCommand(f"xstrip timing fixed2 'xh0' rstr-s1r {trstr}")
                    self.scan_orbit_private(self.orbit_delay-14, 20, 12, f"Cfb={c} pF, Trstrs1r={trstr} reset-ab={i}")
        self.set_orbit(self.orbit_delay)
        
    """
        Scan S1 position by varying orbit delay, shortening Tint to keep S2 in the same place
    """
    def scan_s1_fixed_s2(self, start, num, intt,comment1="", suffix="fixed_s2", trstr_s1r = 2, num_fine = 2, append=0):
        nscans = 1000
        comment2 = f"Scanning orbit delay from {start} to {start+num-1}, Fixed S2 pos, start Tint={intt}, num_fine={num_fine} "
        self.xh_timing_stop()
        for off_on in range(2):
            for i in range(num):
                for fine in range(num_fine):
                    g = fine+num_fine*i+off_on*num_fine*num
                    cmd = f"xstrip timing setup-group 'xh0' {g} 1 {nscans} {intt-i} orbit-scan orbit-mux 3 lemo-out 65535"
                    if off_on==1 and fine==0:
                        cmd = cmd + " ext-trig-frame trig-mux 9"
                    if num_fine==2 :
                        cmd = cmd + f" s1-delay {fine*2}"
                    elif num_fine==4:
                        cmd = cmd + f" s1-delay {fine}"
                    self.xh.sendCommand(cmd)
                    self.xh.sendCommand(f"xstrip timing modify-group 'xh0' {g} fixed-rstr-s1r {trstr_s1r}")
        self.xh.sendCommand(f"xstrip timing modify-group 'xh0' {g} last")
        self.cshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.oedshut()
        self.oshut()
        for i in range(num):
            cmd = f"xstrip timing setup-orbit 'xh0' {start+i}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.xh_timing_continue()
            self.xh_timing_wait()
        self.cshut()
        self.cedshut()
        self.xh_save(comment1, comment2, suffix, append=append)

    """
        This version uses the t_s1r per group feature to provide the same scanning, with in group with cycles
    """    
    def scan_s1_fixed_s2_cycles(self, start, num, intt,comment1="", suffix="fixed_s2", trstr_s1r = 2, num_fine = 2, append=0, diff_first=-1):
        nscans = 4
        ncycles = 500
        comment2 = f"Scanning orbit delay from {start} to {start+num-1}, Fixed S2 pos, start Tint={intt}, num_fine={num_fine} "
            
        g = 0
        self.xh_timing_stop()
        self.set_orbit(start)
        for off_on in range(2):
            self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {g} 1 1 {intt} orbit-scan orbit-mux 3 lemo-out 65535 ext-trig-frame trig-mux 9")
            g = g+1
            for i in range(num):
                for fine in range(num_fine):
                    cmd = f"xstrip timing setup-group 'xh0' {g} 1 {nscans} {intt-i} orbit-scan orbit-mux 3 lemo-out 65535"
                    if i==0 and fine==0:
                        cmd = cmd + f" cycles-start {ncycles}"
                    if i == num-1 and fine == num_fine-1 and diff_first < 0:
                        cmd = cmd + " cycles-end"
                    if num_fine==2 :
                        cmd = cmd + f" s1-delay {fine*2}"
                    elif num_fine==4:
                        cmd = cmd + f" s1-delay {fine}"
                    self.xh.sendCommand(cmd)
                    self.xh.sendCommand(f"xstrip timing modify-group 'xh0' {g} fixed-rstr-s1r {trstr_s1r} t-s1r {25+i}" )
                    g = g+1

            if diff_first >= 0:        
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {g} 1 {nscans} {diff_first} orbit-scan orbit-mux 3 lemo-out 65535")
                g = g+1
                self.xh.sendCommand(f"xstrip timing setup-group 'xh0' {g} 1 {nscans} {diff_first+8} orbit-scan orbit-mux 3 lemo-out 65535 cycles-end")
                g = g+1
        self.xh.sendCommand(f"xstrip timing modify-group 'xh0' {g-1} last")
        #elf.cshut()
        self.xh_timing_start()
        self.xh_timing_continue()
        self.xh_timing_wait()
        self.oedshut()
        self.oshut()
        self.xh_timing_continue()
        self.xh_timing_wait()
        self.cshut()
        self.cedshut()
        self.xh_save(comment1, comment2, suffix, append=append)
        
    def scan_rstr_s1r_s1_fixed_s2(self, comment1="", suffix="", append=0):
        num=8
        first = True
        if len(comment1) > 0:
            comment1 = comment1 + " "
        comment1 = comment1 + f"Scanning Trstr_s1r {num} to {0}"
        self.oedshut()
        for trstr_s1r in range(num,-1,-1):
            self.scan_s1_fixed_s2_cycles(42, 10, 18, comment1, suffix+f"trstr_s1r{trstr_s1r}", trstr_s1r, 2, int(append or not first), diff_first=10)
            first = False
        self.cedshut()
    """
        Scan trstr-s1r keeping S1 and S2 fixed by varying orbit delay
    """
    def scan_trstr_s1r_fixed_s1_s2(self, orbit, num, intt, comment1=""):
        nscans = 1000
        num_fine = 1

        comment2 = f"Scanning trstr_s1r 0 ... {num-1} orbit delay {orbit} ... {orbit-(num-1)}, Fixed S1 and S2 Tint={intt}, num_fine={num_fine} "
        g=0            
        for off_on in range(2):
            for trstr_s1r in range(num):
                for fine in reversed(range(num_fine)):
                    cmd = f"xstrip timing setup-group 'xh0' {g} 1 {nscan} {intt} orbit-scan orbit-mux 3 lemo-out 65535"
                    if off_on==1 and fine==num_fine-1:
                        cmd = cmd + "ext-trig-frame trig-mux 9"
                    if num_fine==2 :
                        cmd = cmd + f" rst-r-delay {fine*2}"
                    elif num_fine==4:
                        cmd = cmd + f" rst-r-delay {fine}"
                    self.xh.sendCommand(cmd)
                    self.xh.sendCommand(f"xstrip timing modify-group 'xh0' {g} fixed-rstr-s1r {trstr_s1r}")
                    g = g+1
        self.xh.sendCommand(f"xstrip timing modify-group 'xh0' {g-1} last")
                    
        self.cshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.oshut()
        for i in range(num):
            cmd = f"xstrip timing setup-orbit 'xh0' {orbit-i}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.xh_timing_continue()
            self.xh_timing_wait()
        self.cshut()
        xh_save(self, comment1, comment2, "fixed_s1_s2", append=0)

    """
        Scan integration time but keep S2 fixed by varying orbit delay
        This is all but a repeat of scan_s1_fixed_s2
    """
    def scan_integration_fixed_s2(self, orbit_first, int_first, num, incr, comment1=""):
        nscans = 1000
        num_fine = 1
        trstr_s1r = 2

        comment2 = f"Scanning integrationfrom {int_first} to {int_first+incr*(num-1)} step={incr}, Fixed S2 pos, starting orbit={orbit_first} "
            
        g = 0
        for off_on in range(2):
            for i in range(num):
                cmd = f"xstrip timing setup-group 'xh0' {g} 1 {nscan} {int_first+i*incr} orbit-scan orbit-mux 3 lemo-out 65535"
                if off_on==1 :
                    cmd = cmd + "ext-trig-frame trig-mux 9"
                self.xh.sendCommand(cmd)
                self.xh.sendCommand(f"xstrip timing modify-group 'xh0' {g} fixed-rstr-s1r {trstr_s1r}")
                g = g +1
        self.xh.sendCommand(f"xstrip timing modify-group 'xh0' {g-1} last")
                    
        self.cshut()
        self.xh_timing_start()
        self.xh_timing_wait()
        self.oshut()
        for i in range(num):
            cmd = f"xstrip timing setup-orbit 'xh0' {(orbit_first-incr*i)%128}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.xh_timing_continue()
            self.xh_timing_wait()
        self.cshut()
        xh_save(self, comment1, comment2, "fixed_s2", append=0)

    def scan_vbias_atten_integration(self, comment1=""):
        attens= ["Al240", "Al160", "Al120", "Al80", "Al60", "Al40", "Al30", "Al20", "Al10", "Al0"]
#        attens= ["Al120", "Al80", "Al60", "Al40", "Al30", "Al20", "Al10", "Al0"]
        nattens = len(attens)
        nvolts = 7
        quarters=False
        npts_int=11
        orbit_offset=-10
        nscans = 2
        ncycles = 2000
        fixed_rstr_s1r = 2
        num = nvolts*nattens
        npts_rst = 0
        first_rstr_s1r = 12
        npts_rst_hold = 0
        frac_a=-1
        frac_b=-1
        flat_from = 8  # For 4 bunch experiment this is 16, but with shorter npts_int have to reduce this 
        save_raw = False
        
        setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset 0 scan-period -1 npts-reset-hold {npts_rst_hold} npts-reset {npts_rst} first-rstr-s1r {first_rstr_s1r}"
        if quarters:
            setup_cmd = setup_cmd+" fine"
            num_fine = 4
        else:
            setup_cmd = setup_cmd+" fine2"
            num_fine = 2
        print(setup_cmd)

        self.xh.sendCommand(f"module create 'xchip_beam_current{num}' {num} 2 header double")
        self.oedshut()
        prev_vbias=0
        self.set_caps(5)
        self.xh_timing_stop()
        for i in range(num):
            self.move_target(attens[i%nattens])
            ibias = 4
            vbias = -50  - 25*int(i/nattens)
            print(f"Setting Ibias={ibias}, Vbias={vbias}")
            self.xh.sendCommand(f"xstrip head set-xchip3-default  'xh0' Ibias-in {ibias} Ibias-SF {ibias} clamp-neg")
            if vbias != prev_vbias:
                self.xh_hv_set(vbias)
                prev_vbias = vbias
            self.set_orbit(self.orbit_delay+orbit_offset)
            self.xh.sendCommand(setup_cmd)
            self.cshut()
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            self.xh.sendCommand("xstrip timing continue \'xh0\'")
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}' {i} 1 {self.sbcur.raw_read}")
            self.xh.sendCommand("xstrip timing wait \'xh0\'")
            self.cshut()

        #    cmd = sprintf("xstrip xchip process \'xh0\' un-interleave row %d num-rows %d bk-lhs-first0 8 bk-lhs-last0 19 bk-lhs-first1 492 bk-lhs-last1 511 bk-rhs-first0 0+512 bk-rhs-last0 19+512 bk-rhs-first1 492+512 bk-rhs-last1 511+512 weight0 %f weight1 %f weight2 %f weight3 %f settle-fraction 0.05 pulse-sep 0 ", row, num_rows, wght_ave[0], wght_ave[1],wght_ave[2],wght_ave[3]   )
            cmd = f"xstrip xchip process 'xh0' un-interleave row {i} num-rows {num} settle-fraction 0.05 pulse-sep 0  frac-time {frac_a} frac-b-time {frac_b} flat-from {flat_from}"
            print(cmd)
            self.xh.sendCommand(cmd)
            self.scan_xchip_save_point(num_fine*npts_int, npts_rst_hold*num_fine, 0, f"Ibias={ibias}, Vbias={vbias}, atten={attens[i%nattens]}", setup_cmd, f"{i:02d}", int(i!=0))
#            self.scan_orbit_private(self.orbit_delay, 20, 12)
#            self.set_orbit(self.orbit_delay)
        
        self.xh.sendCommand("xstrip head set-xchip3-default  \'xh0\' min-power clamp-neg")
        self.cedshut()
        self.xh_hv_set(-100)
        # comment = comment+"scan_ibias_vbias_integration: Scanning Ibias-in & Ibias-SF 2,4,6Vbias= -50, -75 to -200"
        comment = f"scan_vbias_atten_integration: Scanning Vbias= -50, -75 to -200 and Atten"
        self.scan_xchip_save_summary(num_fine*npts_int, num_fine*npts_rst_hold, num_fine*npts_rst, num, num_fine*npts_rst, frac_a, frac_b, comment, setup_cmd)

    def move_target(self, name):
        tp=setup_globals.target_pos._get_position_by_name(name)
        tp.move()
        

    #def scan_volts_atten(self, comment1, multibunch=False):
    def scan_volts_atten(self, comment1, multibunch=False):
        attens= ["Al300", "Al240", "Al160", "Al120", "Al80", "Al60", "Al40", "Al30", "Al20", "Al10", "Al0"]
#        attens= ["Al120", "Al80", "Al60", "Al40", "Al30", "Al20", "Al10", "Al0"]
        nattens = len(attens)
                
    #        rep = click.prompt(mystr, default=default)

    #        int_time = int(click.prompt("    Integration time (cycles)", default=8))
    #        nscans = int(click.prompt("    Number of scans", default=32000))
    #        first_volts = float(click.prompt("    First Bias Voltage", defaults=-50))
    #        last_volts = float(click.prompt("    Last Bias Voltage", defaults=-200))
    #        volt_inc   = float(click.prompt("    Voltage increment", defaults=-25))

        int_time = 8
        nscans=32000
        first_volts=-50
        last_volts=-200
        volt_inc=-25
        cfb=5
        
        nvolts = int ((last_volts - first_volts) / volt_inc)
        
        if nvolts < 1 or (first_volts+nvolts*volt_inc != last_volts):
            print(f"Error: Calculated number of steps = {nvolts}, (first_volts={first_volts})  + (volts_inc={volts_inc})*(n={nvolts}) = {first_volts+n*volt_inc} != (last_volts={last_volts})")
            return

        nvolts = nvolts+1
        num = nvolts*nattens
        self.set_caps(cfb)
        if multibunch :
            npts = num*2
        else:
            npts = num
        self.xh_timing_stop()
        self.xh_timing_orbit_pause(1+npts, nscans, int_time)

        self.cshut()
        self.set_orbit(self.orbit_delay)
        self.xh_timing_start()
        self.xh_timing_continue()
        self.xh_timing_wait()
        prev_volts = -999

        self.oedshut()
        for i in range(num):
            self.move_target(attens[i%nattens])
            volts = first_volts+volt_inc*int(i/nattens)
            print(f"Setting atten={attens[i%nattens]}, Detector bias Volts = {volts}\n")
            if volts != prev_volts:
                self.cedshut()
                self.xh_hv_set(volts)
                prev_volts = volts
                self.oedshut()
            self.oshut()
            #self.xh.sendCommand("xstrip hv get-adc \'xh0\'")
            self.xh_timing_continue()
            self.xh_timing_wait()
            if multibunch :
                self.set_orbit(self.orbit_delay_mb)
                self.xh_timing_continue()
                self.xh_timing_wait()
                self.set_orbit(self.orbit_delay)
            self.cshut()

        self.cshut()           
        self.cedshut()
        description = f"XH Bias Voltage scan from {first_volts} to {last_volts} in {nvolts} steps, nscans={nscans}, int_time={int_time}, nattens={nattens}"
        self.xh_save(comment1, description)
        
        self.xh_hv_set(first_volts)

    def scan_volts_atten_slit(self, use_slits=True):
#        attens= ["Al240", "Al160", "Al120", "Al80", "Al60", "Al40", "Al30", "Al20", "Al10", "Al0"]
        if use_slits:
#            attens= ["Al240", "Al160", "Al120", "Al80", "Al60", "Al40", "Al30", "Al20", "Al10", "Al0"]
#            attens= ["Al240", "Al160", "Al120", "Al80", "Al60", "Al40", "Al30", "Al20"]
            attens= ["Al160", "Al80", "Al40", "Al20", "I0"]
#            first_volts=-100
            first_volts=-100
        else:
 #           first_volts=-50
            first_volts=-100
#            attens= ["Al240", "Al160", "Al120", "Al80", "Al60", "Al40", "Al30", "Al20"]
            attens= ["Al160", "Al80", "Al40", "Al20", "I0"]
#        attens= ["Al120", "Al80", "Al60", "Al40", "Al30", "Al20", "Al10", "Al0"]
        nattens = len(attens)
        
    #        rep = click.prompt(mystr, default=default)

    #        int_time = int(click.prompt("    Integration time (cycles)", default=8))
    #        nscans = int(click.prompt("    Number of scans", default=32000))
    #        first_volts = float(click.prompt("    First Bias Voltage", defaults=-50))
    #        last_volts = float(click.prompt("    Last Bias Voltage", defaults=-200))
    #        volt_inc   = float(click.prompt("    Voltage increment", defaults=-25))

        last_volts=-200
#        volt_inc=-25
        volt_inc=-50
        restart_volts = 0
        if use_slits :
#            setup_globals.xhslity.move(-140.1)
            setup_globals.xhslity.move(-137.6)
        else :
            setup_globals.xhslity.move(-228.6)
        nvolts = int ((last_volts - first_volts) / volt_inc)
        
        if nvolts < 1 or (first_volts+nvolts*volt_inc != last_volts):
            print(f"Error: Calculated number coof steps = {nvolts}, (first_volts={first_volts})  + (volts_inc={volts_inc})*(n={nvolts}) = {first_volts+n*volt_inc} != (last_volts={last_volts})")
            return

        nvolts = nvolts+1
        num = nvolts*nattens
        self.xh.sendCommand(f"module create 'xchip_beam_current{num}_outer' {num} 2 header double")
        self.xh.sendCommand(f"xstrip timing setup-orbit 'xh0' {self.orbit_delay}")
        self.cedshut()
        self.cshut()
        description = f"XH Bias Voltage scan from {first_volts} to {last_volts} in {nvolts} steps, nattens={nattens}"
        prev_volts= -999
        for i in range(restart_volts*nattens, num):
            self.move_target(attens[i%nattens])
            volts = first_volts+volt_inc*int(i/nattens)
            print(f"Setting atten={attens[i%nattens]}, Detector bias Volts = {volts}\n")
            if volts != prev_volts:
                self.xh_hv_set(volts)
                prev_volts = volts
            suffix=attens[i%nattens]+f"V{-volts}"
            self.oedshut()
#            self.scan_slits(description, suffix, i)        
#            self.scan_slits_dark_flood(description, suffix, i)  
#            self.scan_s1_fixed_s2(42, 10, 18,description, suffix, 2, 2, i)
#            self.scan_s1_fixed_s2_cycles(42, 10, 18,description, suffix, 2, 2, i, 10)
#            self.scan_rstr_s1r_s1_fixed_s2(description, suffix, i)
            if use_slits:
                self.scan_slits_integration(description, suffix, i, "coarse")      
            else:
                self.scan_integration(0, 21, save_raw=True, orbit_offset=-10, suffix=suffix, index=i, total_points=num, save_summary=i==num-1)
            self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}_outer' {i} 0 {self.srcur.raw_read}")
            if self.sbcur is not None:
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{num}_outer' {i} 1 {self.sbcur.raw_read}")
            self.cedshut()
        self.xh.sendCommand(f"module save 'xchip_beam_current{num}_outer' %data_dir current-file 'beam_current_outer.asc' asc-col")
        self.xh.sendCommand(f"file get-next-index %data_dir \"{attens}\" \"first_volts={first_volts}, last_volts={last_volts}, inc={volt_inc}\" append")
        self.xh_hv_set(first_volts)

    def scan_slits_integration(self, comment, suffix, append, scan_type="lhs"):
        if scan_type=="coarse" :
#            posns = [-0.1, -0.05, 0, 0.1, 1, 5]  # use for data set 22006
            posns = [0.090, 0.125, 0.175, 0.3, 1, 5]
            nposns = len(posns)
            first_posn = posns[0]
        elif scan_type=="symmetric" :
            first_posn = 0.05
            last_posn=3
            step= 0.1
        elif scan_type == "lhs-fast"  :
            first_posn = 0.025
            step= 0.05
            nposns = 30            
        else :
            first_posn = 0.025
            last_posn=3.0
            step= 0.05
            nposns = int((last_posn-first_posn)/step)+1
        quarters=False
        npts_int=21
        nscans = 2
        ncycles = 1000
        fixed_rstr_s1r = 2
        npts_rst = 0
        first_rstr_s1r = 12
        npts_rst_hold = 0
        frac_a=-1
        frac_b=-1
        flat_from = 8  # For 4 bunch experiment this is 16, but with shorter npts_int have to reduce this 
#        cfb = 5
        cfb = 10
        if quarters:
            num_fine = 4
        else:
            num_fine = 2
        max_block = int(510/((npts_int+npts_rst_hold+npts_rst)*num_fine+2))-1
        block_start = 0
        self.xh.sendCommand(f"module create 'xchip_beam_current{nposns}' {nposns} 2 header double")
        self.oedshut()
        self.set_caps(cfb)
        self.xh_timing_stop()
        ibias = 4
        self.xh.sendCommand(f"xstrip head set-xchip3-default  'xh0' Ibias-in {ibias} Ibias-SF {ibias} clamp-neg")
        self.set_orbit(self.orbit_delay-10)
        remaining = nposns
        posn = first_posn
        if scan_type=="coarse" :
            setup_globals.xhslithg.move(first_posn)
        elif scan_type == "symmetric" :
            setup_globals.xhslitr.move(first_posn)
            setup_globals.xhslith.move(first_posn)
        else :
            setup_globals.xhslitr.move(first_posn)
        while remaining > 0 :
            to_do = max_block if remaining > max_block else remaining
            setup_cmd = f"xstrip xchip setup 'xh0' npts {npts_int} nscans {nscans} fixed-rstr-s1r {fixed_rstr_s1r} cycles {ncycles} npts-reset 0 scan-period -1 npts-reset-hold {npts_rst_hold} npts-reset {npts_rst} first-rstr-s1r {first_rstr_s1r}"
            setup_cmd = setup_cmd + f" num-blocks {to_do}" 
            if quarters:
                setup_cmd = setup_cmd+" fine"
            else:
                setup_cmd = setup_cmd+" fine2"
            print(setup_cmd)
            self.xh.sendCommand(setup_cmd)
            self.cshut()
            self.xh.sendCommand("xstrip timing start 'xh0'")
            self.xh.sendCommand("xstrip timing wait 'xh0'")
            self.oshut()
            for i in range(to_do):
                if scan_type=="coarse" :
                    setup_globals.xhslithg.move(posns[i])
                elif scan_type == "symmetric":
                    setup_globals.xhslithg.move(first_posn+step*(i+block_start))
                elif scan_type == "lhs-fast" :
                    setup_globals.xhslith.move(first_posn+step*(i+block_start))
                    if i+block_start >= 15 :
                        posn = posn+0.2
                    else :
                        posn = posn + step
                else :
                    setup_globals.xhslith.move(first_posn+step*(i+block_start))
                self.xh.sendCommand("xstrip timing continue \'xh0\'")
                self.xh.sendCommand(f"module set-point 'xchip_beam_current{nposns}' {i+block_start} 0 {self.srcur.raw_read}")
                if self.sbcur is not None:
                    self.xh.sendCommand(f"module set-point 'xchip_beam_current{nposns}' {i+block_start} 1 {self.sbcur.raw_read}")
                self.xh.sendCommand("xstrip timing wait \'xh0\'")
            self.cshut()
            self._xh_save_det(comment, setup_cmd, suffix+f"_strip{block_start}", append or block_start)
            block_start = block_start+to_do
            remaining = remaining - to_do
        self.xh.sendCommand(f"module save 'xchip_beam_current{nposns}' %data_dir current-file 'beam_current.asc' asc-col")

    def scan_slits(self, comment, suffix="", append=0):
        first_posn = 0.025
        last_posn=3.0
        step= 0.05
        int_time = 8
        cfb = 5
        nscans= 2000
        nposns = int((last_posn-first_posn)/step)+1
            
        self.xh.sendCommand(f"xstrip timing setup-orbit 'xh0' {self.orbit_delay}")
        self.set_caps(cfb)
        self.xh_timing_orbit_pause(1+nposns, nscans, int_time)

        self.cshut()
        self.xh_timing_start()
        self.xh_timing_continue()
        self.xh_timing_wait()
        setup_globals.xhslith.move(first_posn)
        self.oshut()
        for i in range(nposns):
            setup_globals.xhslith.move(first_posn+step*i)
            self.xh_timing_continue()
            self.xh_timing_wait()
        self.cshut()
        self.xh_save(comment, f"Scanning xhslith from {first_posn} to {last_posn} step {step} in {nposns}", suffix, append)
        
    def scan_slits_dark_flood(self, comment, suffix="", append=0):
        int_time = 50  # 8 for lone bunch
        cfb = 40 # 5 for lone bunch
        nscans= 2000
        nposns = 1
            
        self.xh.sendCommand(f"xstrip timing setup-orbit 'xh0' {self.orbit_delay}")
        self.set_caps(cfb)
        self.xh_timing_orbit_pause(1+nposns, nscans, int_time)

        self.cshut()
        self.xh_timing_start()
        self.xh_timing_continue()
        self.xh_timing_wait()
        self.oshut()
        for i in range(nposns):
            self.xh_timing_continue()
            self.xh_timing_wait()
        self.cshut()
        self.xh_save(comment, f"Scanning xhslith dark and flood", suffix, append)
