
import numpy
import click
import tabulate
import datetime
import time
import sys

from bliss import setup_globals
from bliss.shell.standard import plotselect
from bliss.config import settings
from bliss.config.static import get_config
from bliss.data.node import DataNodeContainer
from bliss.scanning.group import Sequence
from bliss.scanning.chain import AcquisitionChannel, AcquisitionChain
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.scan import Scan, ScanState, DataWatchCallback
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.scan_tools import goto_cen
from bliss.controllers.lima.lima_base import Lima
from bliss.common import plot
from bliss.common.utils import ColorTags, BOLD, BLUE, GREEN, YELLOW, RED
from bliss.common.cleanup import cleanup
from bliss.common.axis import Motion
from bliss.shell.standard import umv, umvr, dscan
from bliss.scanning.acquisition.motor import VariableStepTriggerMaster
from bliss.scanning.acquisition.motor import MotorMaster

from id24.DCMscan_tools import (
    scantool_get_step_position,
    scantool_set_scan_parameters,
    scantool_stepscan_get_acquisition_chain,
    ScanOutput,
    ScanWatchdog
)


class DCMsequence:
   
    def __init__(self, name, config):

        # intern
        self._name = name
        self._config = config

        # simulation
        self._simulation = config.get("_par_simulation")

        # shutter
        self._shutter = config.get("_par_shutter", None)
        
        # Mono
        self._mono = config.get("_par_monochromator", None)
        
        # Motors
        self._fjsry = config.get("_par_fjsry")
        
        # Counters
        self._diode = config.get("_par_dcmoh3outbv").counters.diode
        self._mg = config.get("_par_mg")

        # SCANS
        self._scan_state = None
        self._scan_obj = None
        self._scan_name = None
        self._scan_par = None
        self._scan_par_back = None
        self._builder = None
        self._builder_back = None
        self._chain = None
        self._chain_back = None
        self._master = None
        self._master_back = None
        self._backlash = {}
        self._timeout = 10

    def __info__(self):
        param = "None"
        if self._shutter is not None:
            param = self._shutter.name            
        mystr = f"Shutter : {param}\n\n"
        mystr = f"Scans         : spectrum / \n\n"
        return mystr
    

    def rockingcurve(self, d_ry_min=-0.1, d_ry_max=0.1, nbp=100, inttime=0.1, detector=None):
        if detector is not None:
            plotselect(detector)
        dscan(self._fjsry, d_ry_min, d_ry_max, nbp, inttime, self._mg)
        goto_cen()

    def _scan_end(self):
        """
        Method called at the end of the scans
        """
        if self._scan_name == "exafs_cont":
            self._scan_par["motor"].backlash = self._backlash[self._scan_par["motor"]]
            
        if self._scan_name == "exafs_cont" or self._scan_name == "time_cont":
            self._multpx.switch("SEL_TRIG", "STEP")
            
        if self._scan_state != "FINISHED":
            self._scan_state = "ABORTED"
            
        self._scan_name = None
    
    """
    EXAFS_STEP Scan
    """                
    def exafs_step(
        self,
        exafs_param, 
        *counters, 
        RC=False, 
        sleep_time=0.2, 
        step=False, 
        nbscan=1,
        ascii=False,
        Emono=None,
    ):
        """
        Step Scans with variable step size and variable step time
        Positions and times are taken from exafs_param object
        """
        
        # CHECKS
        if Emono is not None:
            energy_motor = Emono
        else:
            energy_motor = self._mono._motors["energy"]
            
        # SCAN STATE
        self._scan_state = "PREPARING"
        self._scan_name = "exafs_step"
        
        # INIT VARIABLES        
        positions = scantool_get_step_position(exafs_param, self._mono, step)
        if positions[0] > positions[-1]:
            way = "down"
        else:
            way = "up"
        
        # BEAMLINE SETUP
        # Open beamshutter
        if self._shutter is not None:
            self._shutter.open()

        with cleanup(self._scan_end):

            # MULTIPLEXER
            #self._multpx.switch("SEL_TRIG", "STEP")

            # COUNTERS
            #plotinit("iexafs:mu_trans")
            
            # SCAN PARAMETERS
            self._scan_par = scantool_set_scan_parameters(
                motor=energy_motor,
                positions=positions,         
                time=exafs_param.Time, 
                points=len(positions),
                way=way,
            )
            
            for nscan in range(nbscan):
                # MOTOR MASTER
                self._master = VariableStepTriggerMaster(
                    self._scan_par["motor"],
                    self._scan_par["positions"],
                    stab_time=sleep_time
                )
                
                
                # ACQUISITION CHAIN
                if len(counters) > 0:
                    (self._builder, self._chain) = scantool_stepscan_get_acquisition_chain(
                        self._master,
                        self._scan_par,
                        counters,
                        #fx_trigger=self._fx_trigger_mode,
                        #fx_block_size=self._fx_block_size,
                    )
                print(self._chain._tree)
                
                # SCAN DISPLAY
                scan_display = ScanDisplay(
                    None,
                    self._scan_par["points"],
                    self._scan_par["motor"],
                    self._builder
                )
                
                # METADATA
                #metadata = setup_globals.metadata
                #metadata._sel["ExafsElement"] = exafs_param
                #metadata._state["ExafsElement"].set(True)
                #instrument = metadata.get_meta_data()
                nbp = self._scan_par["points"]
                scan_info = {
                    "title": f"{self._name}.exafs_step {exafs_param.name} {nbp} points", 
                    "type": f"{self._name}.exafs_step",
                    #"instrument": instrument
                }
                    
                print(f"\nSCAN #{nscan+1}(/{nbscan})")

                # SCAN
                self._scan_obj = Scan(
                    self._chain,
                    name=f"{self._name}.exafs_step",
                    scan_info=scan_info,
                    data_watch_callback=scan_display
                )
                self._scan_state = "RUNNING"
                self._scan_obj.run()
                #self.plotter.run(self._scan_obj)
                
                if ascii:
                    save_exafs_ascii(self._scan_obj)


##########################################################
#
# Display acquired nb points on acquisition devices during
# continuous scan
#
class ScanDisplay(object):

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, musst, points, motor, builder):
        self.nb_points = points
        self.musst_used = False
        if musst is not None:
            self.musst_used = True
            self.time_factor = float(musst.get_timer_factor())
        self.motor = motor
        self.controllers = {}
        for cont_node in builder.get_top_level_nodes():
            self.controllers[cont_node.acquisition_obj.name] = {}
            self.controllers[cont_node.acquisition_obj.name]["name"] = cont_node.acquisition_obj.device.name
            self.controllers[cont_node.acquisition_obj.name]["updated"] = False
            self.controllers[cont_node.acquisition_obj.name]["points"] = 0
        self.mussttime = 0.0

    def on_scan_new(self, scan, scan_info):

        title = scan_info["title"]
        print(f"\n  {title}\n")

        if self.motor is not None:
            display_str = "   %8s "%(self.motor.name)
        else:
            display_str = "  "

        if self.musst_used:
            display_str += " %8s "%("time")

        for cont_name in self.controllers:
            name = self.controllers[cont_name]["name"]
            if name == "generic_tg_controller":
                name = "tango"
            display_str += " %8s "%(name)

        print(display_str)

    def on_state(self,state):
        return True

    def on_scan_data(self, data_events, nodes, info):

        #if info.get('state') != ScanState.PREPARING:


        if self.motor is not None:
            display_str = "   %8.4f "%(self.motor.position)
        else:
            display_str = "  "
            
        if len(data_events) >= 1:
            for cont_name in self.controllers:
                self.controllers[cont_name]["updated"] = False

            for channel, events in data_events.items():

                data_node = nodes.get(channel)
                
                if not isinstance(data_node, DataNodeContainer):
                    #print("-------------------------------------------")
                    #print(f"{data_node.name}")
                    #print(f"{data_node.parent.name}")
                    #print(f"{data_node.parent.parent.name}")
                    if data_node.name.lower() == "musst232:time":
                        self.mussttime = float(data_node.get(-1)) / float(self.time_factor)
                        cont_name = ""
                    else:
                        cont_name = data_node.name
                        if cont_name not in self.controllers:                        
                            cont_name = data_node.parent.name
                            if cont_name not in self.controllers:
                                cont_name = data_node.parent.parent.name

                    if cont_name in self.controllers:
                        
                        cont_obj = self.controllers[cont_name]
                        if not cont_obj["updated"]:
                            cont_obj["updated"] = True
                            cont_obj["points"] = len(data_node)

        if self.musst_used:   
            display_str += " %8.3f "%self.mussttime

        for cont_obj in self.controllers.values():
            display_str += "     %04d "%(cont_obj["points"])

        print(f"{display_str}", end="\r")

        sys.stdout.flush()

    def on_scan_end(self, *args):
        print(f"\n")
