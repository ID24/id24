
import time
import datetime
import sys
import numpy
import os
import click
import gevent
import shutil
from ruamel.yaml import YAML
from ruamel.yaml.compat import StringIO

from bliss import setup_globals
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
from bliss.common.scans.ct import ct
from bliss.config.settings import ParametersWardrobe, SimpleSetting
from bliss.shell.standard import newsample, newdataset
from bliss.common.cleanup import cleanup
from bliss.scanning.scan_meta import get_user_scan_meta, META_TIMING
from bliss.config.static import get_config

from id24.ID24utils import ID24saveAscii, ID24saveImage, ID24profile
from id24.ID24menu import menu_list, menu_number, menu_string, menu_choice

class HPLFsequence:

    def __init__(self, name, config):

        self._name = name
        self._config = config
        
        #
        # SCANS
        #
        self._scans = config.get("_par_scans", None)
        if self._scans is None:
            print(RED("\n\n hplf_seq:    NO SCANS AVAILABLE"))
            #raise RuntimeError("HPLFsequence: No scans controller defined")
        
        #
        # OPIOM_HPLF: use to enable/diable trigger on camera
        #
        self._opiom_hplf = config.get("_par_opiom", None)
        if self._opiom_hplf is None:
            raise RuntimeError("HPLFsequence: No Opiom controller defined")
        
        #
        # CITY CONTROLLER
        #
        self._city_ctrl = config.get("_par_city_ctrl", None)
        if self._city_ctrl is None:
            raise RuntimeError("HPLFsequence: No city controller defined")
        
        #
        # baslers
        #
        baslers_config = config.get("_par_baslers", None)
        self._baslers = []
        if baslers_config is not None:
            for basler_config in baslers_config:
                self._baslers.append(basler_config["basler"])
        
        #
        # streak cameras
        #
        self._streak = config.get("_par_streak", None)
        if self._streak is None:
            raise RuntimeError("HPLFsequence: No Streak controller defined")
        
        #
        # P100
        #
        self._p100 = config.get("_par_p100", None)
        if self._p100 is None:
            raise RuntimeError("HPLFsequence: No P100 controller defined")
        
        #
        # TRANSPORT
        #
        self._transport = self._config.get("_par_transport", None)
        if self._transport is None:
            raise RuntimeError("HPLFsequence: No transport object defined")
        
        #
        # INTERACTION CHAMBER
        #
        self._ic = self._config.get("_par_ic", None)
        if self._ic is None:
            raise RuntimeError("HPLFsequence: No Interaction Chamber object defined")
        
        #
        # X-ray Shutter
        #
        self._shutter = self._config.get("_par_shutter", None)
        if self._shutter is None:
            raise RuntimeError("HPLFsequence: No X-Ray Shutter object defined")
        
        #
        # Sequence Parameters
        #
        self._param = self._config.get("_par_parameters")
        
        #
        # DATA DISPLAY MANAGEMENT
        #
        self._data_display = self._config.get("_par_data_display", None)
        
        #
        # SAVING OBJECT
        #   - city_ctrl
        #   - tektro54 profile + values
        #   - tektro64 profile + values
        #   - p100.amplifiers
        #   - p100.attenuators
        #   - p100.intrepid
        #   - p100.ModBox
        #   - p100.Modbox profile
        self._save = HPLFsaving()
    
    @property
    def _last_run_number(self):
        return self._save._run.get()
        
    @property
    def _last_run_path(self):
        return self._save._run_path.get()
        
    @property
    def _last_run_type(self):
        return self._save._run_type.get()
        
    def __info__(self):
        print("\n")
        mystr = BOLD("parameters")
        print(f"    {mystr}")
        print("        Sequences parameters\n")
        
        mystr = BOLD("xh_diode(inttime, nbframe, I0_pos, sample_pos)")
        print(f"    {mystr}")
        print("        City - Xh\n")
        
        mystr = BOLD("laser")
        print(f"    {mystr}")
        print("        Laser check - Laser shock - No Xh\n")
        
        mystr = BOLD("streak_ref")
        print(f"    {mystr}")
        print("        Streak check - Streak signal - No laser shock - No Xh\n")
        
        mystr = BOLD("shock(inttime, nbframe, I0_pos, sample_pos)")
        print(f"    {mystr}")
        print("        Laser check - Laser Shock - Xh\n")
        return ""
        
    #
    # Shutters
    #
    @property
    def use_target_shutter(self):
        return self._scans.use_target_shutter
        
    @use_target_shutter.setter
    def use_target_shutter(self, state):
        self._scans.use_target_shutter = state
        
    #
    # Sequences
    #
    def test_run(self):
        
        # New run
        self._save.run_open()
        with cleanup(self._hplf_post_metadata):
            
            # add category for metadat h5 saving
            self._save.metadata_create_category()
            
            # define objects to be saved in metadata
            saved_objects_list = [self._city_ctrl, self._p100]
            self._save.saved_object_set(saved_objects_list)
            
            # Scan
            #self._scans.laser(basler=True)
            self._scans.spectrum(0.001, 1, 1, 1, self._scans._hamamatsu)
            
            # Get metadata from object
            md_dict = self._save.metadata_fill()
            # Save Metadata
            self._save.save()
            
    def laser_only(self, basler=True):
        
        # New run
        self._scan_name = "laser_only"
        self._run_number = self._save.run_open(self._scan_name)
        self._run_path = self._save._run_path.get()
        
        # Stop Data Display
        self._stop_data_display()
        
        with cleanup(self._hplf_post_actions):

            # Check shock is possible
            if self._shock_check(basler=basler):
                
                self._city_ctrl._synchro.config_synchro_off()
                  
                print("\n--------------------------------------------\n")
                streak_rep = click.confirm("  Streak in acquisition mode?")
                        
                rep = click.confirm(
                    "  Ready to Shock. Proceed?",
                    default=False
                )
                print("\n--------------------------------------------\n")
                if rep:
                    # add category for metada h5 saving
                    self._save.metadata_create_category()
                    
                    # define objects to be saved in metadata
                    saved_objects_list = [
                        self._city_ctrl,
                        self._p100,
                        self._transport,
                        self._ic,
                    ]
                    if streak_rep:
                        saved_objects_list.append(self._streak)
                    if basler:
                        saved_objects_list.extend(self._baslers)
                    self._save.saved_object_set(saved_objects_list)
                    
                    # Scan
                    self._scans.laser(basler=basler)

                    # Save Metadata (Dictionnary saved in save._md_dict during scan
                    self._save.save()
            else:
                print("\n\n")
                print(RED("--------------------------------------------------"))
                print(RED("------                                      ------"))
                print(RED("------    LASER_ONLY Check FAILED !!!       ------"))
                print(RED("------                                      ------"))
                print(RED("--------------------------------------------------"))
                        
    def xh_diode(self, inttime, nbframe, I0_pos, sample_pos, nbscan=1, nbacq=[1]):
        # New run
        self._scan_name = "xh_diode"
        self._run_number = self._save.run_open(self._scan_name)
        self._run_path = self._save._run_path.get()
        
        # Stop Data Display
        self._stop_data_display()
        
        with cleanup(self._xh_diode_post_actions):
            
            # add category for metada h5 saving
            self._save.metadata_create_category()
                    
            # define objects to be saved in metadata
            saved_objects_list = [
                self._city_ctrl,
            ]
            self._save.saved_object_set(saved_objects_list)
            
            if nbscan == 1:
                # Scan
                self._scans.xh_diode(inttime, nbframe, 1, I0=I0_pos, sample=sample_pos)
                        
                # Save Metadata (Dictionnary saved in save._md_dict during scan
                self._save.save()
                        
                # Save Xh data in ASCII format
                self._save.save_ascii(self._scans)
            else:
                if len(nbacq) != nbscan:
                    acq_list = numpy.zeros((nbscan))
                    acq_list += nbacq[0]
                else:
                    acq_list = nbacq
                for nscan in range(nbscan):
                    # Scan
                    self._scans.xh_diode(inttime, nbframe, acq_list[nscan], I0=I0_pos, sample=sample_pos)
        
    def streak_ref(self):
        # New run
        self._scan_name = "streak_ref"
        self._run_number = self._save.run_open(self._scan_name)
        self._run_path = self._save._run_path.get()
        
        # Stop Data Display
        self._stop_data_display()
        
        with cleanup(self._hplf_post_actions):
            
            # Check streak 
            if self._streak_check():
                
                self._city_ctrl._synchro.config_synchro_off()
                print("\n--------------------------------------------\n")
                rep = click.confirm(
                        "  Ready to get Streak references?",
                        default=False
                    )
                print("\n--------------------------------------------\n")
                if rep:

                    # add category for metada h5 saving
                    self._save.metadata_create_category()
                    
                    # define objects to be saved in metadata
                    saved_objects_list = [
                        self._city_ctrl,
                        self._streak,
                    ]
                    self._save.saved_object_set(saved_objects_list)
                    
                    # Scan
                    print("\n  Streak signal within next 10s....")
                    self._scans.streak_ref()
                    
                    # Save Metadata (Dictionnary saved in save._md_dict during scan
                    self._save.save()
                    
            else:
                print("\n\n")
                print(RED("--------------------------------------------------"))
                print(RED("------                                      ------"))
                print(RED("------    STREAK_REF Check FAILED !!!       ------"))
                print(RED("------                                      ------"))
                print(RED("--------------------------------------------------"))
        
    def shock(self, inttime, nbframe, I0_pos, sample_pos, basler=True):
        
        # New run
        self._scan_name = "shock"
        self._run_number = self._save.run_open(self._scan_name)
        self._run_path = self._save._run_path.get()
        
        # Stop Data Display
        self._stop_data_display()
        
        with cleanup(self._hplf_post_actions):

            # Check shock is possible
            if self._shock_check(basler=basler):
                
                self._city_ctrl._synchro.config_synchro_off()
                  
                print("\n--------------------------------------------\n")
                streak_rep = click.confirm("  Streak in acquisition mode?")
                        
                rep = click.confirm(
                    "  Ready to Shock. Proceed?",
                    default=False
                )
                print("\n--------------------------------------------\n")
                if rep:
                    # add category for metada h5 saving
                    self._save.metadata_create_category(shutter=self._shutter)
                    
                    # define objects to be saved in metadata
                    saved_objects_list = [
                        self._city_ctrl,
                        self._p100,
                        self._transport,
                        self._ic,
                    ]
                    if streak_rep:
                        saved_objects_list.append(self._streak)
                    if basler:
                        saved_objects_list.extend(self._baslers)
                    self._save.saved_object_set(saved_objects_list)
                    
                    # Scan
                    self._scans.laser_shock(inttime, nbframe, I0_pos, sample_pos, basler=basler)
                    
                    # Save Metadata (Dictionnary saved in save._md_dict during scan
                    self._save.save()
                    
                    # Save Xh data in ASCII format
                    self._save.save_ascii(self._scans)

            else:
                print("\n\n")
                print(RED("--------------------------------------------------"))
                print(RED("------                                      ------"))
                print(RED("-------------  SHOCK Check FAILED !!! ------------"))
                print(RED("------                                      ------"))
                print(RED("--------------------------------------------------"))
                        
    #
    # Data Display
    #
    def _stop_data_display(self):
        if self._data_display is not None:
            if self._data_display.state() == "Running":
                start_time = time.time()
                print(RED("Stop data display ... ({time.time()-start_time} s)", end="\r"))
                self._data_display.stop()
                while self._data_display.state() == "Running":
                    print(RED("Stop data display ... ({time.time()-start_time} s)", end="\r"))
                    gevent.sleep(1)
                print(GREEN("Stop data display ... DONE ({time.time()-start_time} s)"))
                
    #
    # parameters
    #
    def parameters(self):
        self._param.parameters()
    
    #
    # Post sequence
    #        
    def _hplf_post_metadata(self):
        self._save.metadata_remove_category()
        self._save.run_close()
                    
    def _hplf_data_print(self):
        mystr = BLUE(f"{self._scan_name} - run {self._run_number} - saved in:")
        print("\n\n")
        print(f"  {mystr}\n")
        print(f"    Definition: {GREEN(self._save._definition)}")
        print(f"    Saved in  : {GREEN(self._last_run_path)}")
        print("\n\n")
    
    def _xh_diode_post_actions(self):
        self._hplf_post_metadata()
        self._hplf_data_print()
        
    def _hplf_post_actions(self):
                            
        # Stop trigger on all camera
        self._opiom_hplf.comm("IM 0x00 0x01")

        self._hplf_post_metadata()
                
        # shutter_visar
        if self._transport.devices.shutter_visar.state != "CLOSED":
            print(ORANGE("  - Shutter VISAR is not closed ... Close it ..."))
            self._transport.devices.shutter_visar.close()
            if self._transport.devices.shutter_visar.state != "CLOSED":
                print(RED("     Shutter VISAR is NOT closed"))
            else:
                print(GREEN("     Shutter VISAR is closed"))
        else:
            print(GREEN("  - Shutter VISAR is closed"))
        #print("\n")
            
        # shutter_visar_eh1
        if self._transport.devices.shutter_visar_eh1.state != "CLOSED":
            print(ORANGE("  - Shutter VISAR EH1 is not closed ... Close it"))
            try:
                self._transport.devices.shutter_visar_eh1.close()
            except:
                print("\n")
            if self._transport.devices.shutter_visar_eh1.state != "CLOSED":
                print(RED("     Shutter VISAR EH1 is NOT closed"))
            else:
                print(GREEN("     Shutter VISAR EH1 is closed"))
        else:
            print(GREEN("  - Shutter VISAR EH1 is closed"))
        #print("\n")
        
        # shutter_drive_eh1
        if self._transport.devices.shutter_drive_eh1.state != "CLOSED":
            print(ORANGE("  - Shutter Drive EH1 is not closed ... Close it ..."))
            self._transport.devices.shutter_drive_eh1.close()
            if self._transport.devices.shutter_drive_eh1.state != "CLOSED":
                print(RED("    Shutter Drive EH1 is NOT closed"))
            else:
                print(GREEN("    Shutter Drive EH1 is closed"))
        else:
            print(GREEN("  - Shutter Drive EH1 is closed"))
        #print("\n")
        
        # uscope_us_shield
        if self._ic.devices.uscope_us_shield.state != "OUT":
            print(ORANGE("  - uscope_us_shield is not OUT ... set it OUT ..."))
            self._ic.devices.uscope_us_shield.set_out()
            if self._ic.devices.uscope_us_shield.state != "OUT":
                print(RED("    uscope_us_shield is NOT OUT"))
            else:
                print(GREEN("    uscope_us_shield is OUT"))
        else:
            print(GREEN("  - uscope_us_shield is OUT"))
        #print("\n")
                
        # diagatt
        if self._transport.devices.diagatt.state != "OUT":
            print(ORANGE("  - diagatt is not OUT ... set it OUT ..."))
            self._transport.devices.diagatt.set_out()
            if self._transport.devices.diagatt.state != "OUT":
                shoot_is_allowed = False
                print(RED("    diagatt is NOT OUT"))
            else:
                print(GREEN("    diagatt is OUT"))
        else:
            print(GREEN("  - diagatt is OUT"))
        #print("\n")
        
        self._hplf_data_print()
        
    #
    # Security checks
    #
    def _streak_check(self):
        # init
        states = ""
        shoot_is_allowed = True
        
        # shutter_drive_eh1
        if self._transport.devices.shutter_drive_eh1.state != "CLOSED":
            print(ORANGE("  - Shutter Drive EH1 is not Closed ... Close it ..."))
            self._transport.devices.shutter_drive_eh1.close()
            if self._transport.devices.shutter_drive_eh1.state != "CLOSED":
                print(RED("    Shutter Drive EH1 is not Closed"))
                shoot_is_allowed = False
            else:
                print(GREEN("    Shutter Drive EH1 is Closed"))
        else:
            print(GREEN("  - Shutter Drive EH1 is Closed"))
        #print("\n")
        
        # uscope_ds_ty
        # modif Nico + Gilles 20221118
        # if not setup_globals.uscope_ds_ty.state.LIMPOS:
            # print(ORANGE("  - uscope_ds_ty is not on POSITIVE limit ... move it to POS limit ..."))
        print(ORANGE("  - uscope_ds_ty to parking position ..."))
        motpos = get_config().get("motpos")
        scopepos = motpos._motpos_list["scopepos"]
        scopepos.parking.move()
        print(GREEN("    uscope_ds_ty is now at parking position"))
            #setup_globals.uscope_ds_ty.hw_limit(+1)
            #if not setup_globals.uscope_ds_ty.state.LIMPOS:
            #    shoot_is_allowed = False
            #    print(RED("    uscope_ds_ty is NOT on POSITIVE limit"))
            #else:
            #    print(GREEN("    uscope_ds_ty is on POSITIVE limit"))
        # else:
            # print(GREEN("  - uscope_ds_ty is on POSITIVE limit"))
        #print("\n")
            
        # uscope_us_shield
        if self._ic.devices.uscope_us_shield.state != "IN":
            print(ORANGE("  - uscope_us_shield is not IN ... set it IN ..."))
            try:
                setup_globals.uscope_us_shield.set_in()
            except:
                print("\n")
            if self._ic.devices.uscope_us_shield.state != "IN":
                shoot_is_allowed = False
                print(RED("    uscope_us_shield is not IN"))
            else:
                print(GREEN("    uscope_us_shield is IN "))
        else:
            print(GREEN("  - uscope_us_shield is IN "))
        #print("\n")
                
        # shutter_visar
        if self._transport.devices.shutter_visar.state != "OPEN":
            print(ORANGE("  - Shutter VISAR is not Open ... Open it ..."))
            self._transport.devices.shutter_visar.open()
            if self._transport.devices.shutter_visar.state != "OPEN":
                print(RED("    Shutter VISAR is not Open"))
                shoot_is_allowed = False
            else:
                print(GREEN("    Shutter VISAR is Open"))
        else:
            print(GREEN("  - Shutter VISAR is Open"))
        #print("\n")
            
        # shutter_visar_eh1
        if self._transport.devices.shutter_visar_eh1.state != "OPEN":
            print(ORANGE("  - Shutter VISAR EH1 is not Open ... Open it"))
            try:
                self._transport.devices.shutter_visar_eh1.open()
            except:
                print("\n")
            if self._transport.devices.shutter_visar_eh1.state != "OPEN":
                shoot_is_allowed = False
                print(RED("    Shutter VISAR EH1 is not Open"))
            else:
                print(GREEN("    Shutter VISAR EH1 is Open"))
        else:
            print(GREEN("  - Shutter VISAR EH1 is Open"))
        #print("\n")
           
        if not shoot_is_allowed:
            print("\n")
            print(BOLD(ORANGE("\n  ALL Conditions MUST be validated before the take Streak References\n")))
            return False
            
        return True

    def _shock_check(self, basler=True):
        # init
        states = ""
        shoot_is_allowed = True
        
        #print(GREEN("  - Stop bas_uscope_ds and bas_uscope_us acquisition"))
        #setup_globals.bas_uscope_ds.lima.stopAcq()
        #setup_globals.bas_uscope_us.lima.stopAcq()
        #print("\n")

        # Check balsers
        if basler:
            ct(0.1, *[bas.lima for bas in self._baslers])
            print(GREEN("  - Check Baslers OK"))
            
        # BeamBlocker Input
        if self._p100.device.ampli._beamblocker_input != "OPEN":
            print(RED("  - Beamblocker Input is not Open"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Beamblocker Input is Open"))
        #print("\n")
        # BeamBlocker Output
        if self._p100.device.ampli._beamblocker_output != "OPEN":
            print(RED("  - Beamblocker Output is not Open"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Beamblocker Output is Open"))
        #print("\n")
        
        # shutter_drive_eh1
        if self._transport.devices.shutter_drive_eh1.state != "OPEN":
            print(ORANGE("  - Shutter Drive EH1 is not Open ... Open it ..."))
            try:
                self._transport.devices.shutter_drive_eh1.open()
            except:
                print("\n")
            if self._transport.devices.shutter_drive_eh1.state != "OPEN":
                shoot_is_allowed = False
                print(RED("    Shutter Drive EH1 is not Open"))
            else:
                print(GREEN("    Shutter Drive EH1 is Open"))
        else:
            print(GREEN("  - Shutter Drive EH1 is Open"))
        #print("\n")
        
        # x11
        if self._transport.devices.x11.state != "OUT":
            print(RED("  - X11 is not OUT"))
        else:
            print(GREEN("  - X11 is OUT"))
        #print("\n")
        
        # x12
        if self._transport.devices.x12.state != "OUT":
            print(RED("  - X12 is not OUT"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - X12 is OUT"))
        #print("\n")
        
        # x2
        if self._transport.devices.x2.state != "OUT":
            print(ORANGE("  - X2 is not OUT ... set it OUT ..."))
            self._transport.devices.x2.set_out()
            if self._transport.devices.x2.state != "OUT":
                shoot_is_allowed = False
                print(RED("    X2 is not OUT"))
            else:
                print(GREEN("    X2 is OUT"))
        else:
            print(GREEN("  - X2 is OUT"))
        #print("\n")
        
        # x31
        if self._transport.devices.x31.state != "OUT":
            print(RED("  - X31 is not OUT"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - X31 is OUT"))
        #print("\n")
        
        # x32
        if self._transport.devices.x32.state != "OUT":
            print(RED("  - X32 is not OUT"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - X32 is OUT"))
        #print("\n")
        
        # x4
        if self._transport.devices.x4.state != "OUT":
            print(ORANGE("  - X4 is not OUT ... set it OUT"))
            self._transport.devices.x4.set_out()
            if self._transport.devices.x4.state != "OUT":
                print(RED("    X4 is not OUT"))
                shoot_is_allowed = False
            else:
                print(GREEN("    X4 is OUT"))
        else:
            print(GREEN("  - X4 is OUT"))
            
        # x51
        if self._transport.devices.x51.state == "IN":
            print(RED("  - X51 is not OUT"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - X51 is OUT"))
            
        # Gauge sf1
        if self._transport.devices.gauge_sf1.state != "OK":
            print(RED("  - Gauge sf1 is not OK"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Gauge sf1 is OK"))
            
        # Gauge sf2
        if self._transport.devices.gauge_sf2.state != "OK":
            print(RED("  - Gauge sf2 is not OK"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Gauge sf2 is OK"))
        
#        # lvisarty
#        if not setup_globals.lvisarty.state.LIMNEG:
#            straux = RED("  - lvisarty is not on NEGATIVE limit")
#            states += f"  {straux}\n"
#            shoot_is_allowed = False
        
        # uscope_ds_ty    
        # uscope_ds_ty
        # modif Nico + Gilles 20221118
        # if not setup_globals.uscope_ds_ty.state.LIMPOS:
            # print(ORANGE("  - uscope_ds_ty is not on POSITIVE limit ... move it to POS limit ..."))
        print(ORANGE("  - uscope_ds_ty to parking position ..."))
        motpos = get_config().get("motpos")
        scopepos = motpos._motpos_list["scopepos"]
        scopepos.parking.move()
        print(GREEN("    uscope_ds_ty is now at parking position"))
            #setup_globals.uscope_ds_ty.hw_limit(+1)
            #if not setup_globals.uscope_ds_ty.state.LIMPOS:
            #    shoot_is_allowed = False
            #    print(RED("    uscope_ds_ty is NOT on POSITIVE limit"))
            #else:
            #    print(GREEN("    uscope_ds_ty is on POSITIVE limit"))
        # else:
            # print(GREEN("  - uscope_ds_ty is on POSITIVE limit"))
        #print("\n")
        
        # uscope_us_shield
        if self._ic.devices.uscope_us_shield.state != "IN":
            print(ORANGE("  - uscope_us_shield is not IN ... set it IN ..."))
            self._ic.devices.uscope_us_shield.set_in()
            if self._ic.devices.uscope_us_shield.state != "IN":
                print(RED("    uscope_us_shield is not IN"))
                shoot_is_allowed = False
            else:
                print(GREEN("    uscope_us_shield is IN"))
        else:
            print(GREEN("  - uscope_us_shield is IN"))
        #print("\n")
                
        # diagatt
        if self._transport.devices.diagatt.state != "IN":
            print(ORANGE("  - diagatt is not IN ... set it IN ..."))
            self._transport.devices.diagatt.set_in()
            if self._transport.devices.diagatt.state != "IN":
                shoot_is_allowed = False
                print(RED("    diagatt is not IN"))
            else:
                print(GREEN("    diagatt is IN"))
        else:
            print(GREEN("  - diagatt is IN"))
        #print("\n")

        # shutter_visar
        if self._transport.devices.shutter_visar.state != "OPEN":
            print(ORANGE("  - Shutter VISAR is not Open ... Open it ..."))
            self._transport.devices.shutter_visar.open()
            if self._transport.devices.shutter_visar.state != "OPEN":
                print(RED("    Shutter VISAR is not Open"))
                shoot_is_allowed = False
            else:
                print(GREEN ("    Shutter VISAR is Open"))
        else:
            print(GREEN ("  - Shutter VISAR is Open"))
        #print("\n")
            
        # shutter_visar_eh1
        if self._transport.devices.shutter_visar_eh1.state != "OPEN":
            print(ORANGE("  - Shutter VISAR EH1 is not Open ... Open it ..."))
            self._transport.devices.shutter_visar_eh1.open()
            if self._transport.devices.shutter_visar_eh1.state != "OPEN":
                print(RED("    Shutter VISAR EH1 is not Open"))
                shoot_is_allowed = False
            else:
                print(GREEN("    Shutter VISAR EH1 is Open"))
        else:
            print(GREEN("  - Shutter VISAR EH1 is Open"))
        #print("\n")
            
        # P100 Intrepid Shutter
        if not self._p100.device.intrepid.Intracavity_Shutter_State:
            print(RED("  - Shutter Intrepid is not Open"))
            shoot_is_allowed = False
        else:
            print(GREEN("  - Shutter Intrepid is Open"))
        #print("\n")

        if not shoot_is_allowed:
            print("\n")
            print(BOLD(ORANGE("\n  ALL Conditions MUST be validated before the Laser Shoot\n")))
            return False
            
        return True

class HPLFdelayParameter:

    def __init__(self, name, config):

        self._name = name
        self._config = config
        
        self._p100_ampli = config.get("_par_p100_ampli")
        
        self._delays = {}
        self._channels = {}
        delays = self._config.get("delays")
        for delay in delays:
            delay_name = delay["delay_name"]
            title = delay["title"]
            self._delays[delay_name] = {
                "value": SimpleSetting(f"delay_param_{self._name}_{delay_name}", default_value=0.0),
                "title": delay["title"],
                "channels": [],
            }
            for channel in delay["channels"]:
                self._delays[delay_name]["channels"].append(channel["channel"])
                if channel["channel"] not in self._channels.keys():
                    self._channels[channel["channel"]] = 0.0
                
    def parameters(self):
        while True:
            titles = []
            values = []
            for name, par in self._delays.items():
                titles.append(par["title"])
                values.append(par["value"].get())   
            (rep, ind) = menu_list(f"Laser sequences parameters", titles, values=values)
            if rep == "q":
                return
            if rep in titles:
                delay_name = self._get_name_from_title(rep)
                self._delays[delay_name]["value"].set(
                    menu_number(
                        rep,
                        default=self._delays[delay_name]["value"].get(),
                    )
                )
                self._apply_parameters()
                
    def _get_name_from_title(self, title):
        for name, par in self._delays.items():
            if title == par["title"]:
                return name
        return none
    
    @property
    def available_delay_name(self):
        return " ".join(self._delays.keys())
        
    def _set_extra_delay(self, delay_name, extra_delay):
        if delay_name in self._delays.keys():
            self._delays[delay_name]["value"].set(extra_delay)
            self._apply_parameters()
        else:
            available_delay_name = " ".join(self._delays.keys())
            print(f"Delay name \"{delay_name}\" does not exist. ({available_delay_name})")
            
    def _apply_parameters(self):
        for channel in self._channels.keys():
            self._channels[channel] = 0.0
        for delay, par in self._delays.items():
            for channel in par["channels"]:
                self._channels[channel] += par["value"].get()
        for channel, val in self._channels.items():
            channel.extra_delay = val
        gevent.sleep(1)
        self._p100_ampli._device.Reset_Errors_command = True
        
class HPLFsequenceParameters:
    def __init__(self, city):
        self._city = city
        self._default_param = {
            "drive_delay": 0.0,
            "probe_delay": 0.0,
            "streak_delay": 0.0,
        }
        self._param = ParametersWardrobe(
            f"hplf_seq_parameters",
            default_values=self._default_param
        )
        
    def parameters(self):
        while True:
            drive_delay_title  = f"Drive Delay ...... : {GREEN(self._param.drive_delay)} ns"
            probe_delay_title  = f"Probe Laser Delay. : {GREEN(self._param.probe_delay)} ns"
            streak_delay_title = f"Streak Delay...... : {GREEN(self._param.streak_delay)} ns"
            param = [drive_delay_title, probe_delay_title, streak_delay_title]
            (rep, ind) = menu_list(f"Laser sequences parameters", param)
            if ind == 0:
                self._param.drive_delay = menu_number(
                    "Drive Delay (ns)",
                    #minmax=minmax,
                    default=self._param.drive_delay,
                    #integer=True,
                )
            if ind == 1:
                self._param.probe_delay = menu_number(
                    "Probe Laser Delay (ns)",
                    #minmax=minmax,
                    default=self._param.probe_delay,
                    #integer=True,
                )
            if ind == 2:
                self._param.streak_delay = menu_number(
                    "Streak Delay (ns)",
                    #minmax=minmax,
                    default=self._param.streak_delay,
                    #integer=True,
                )
            if rep == "q":
                return
                
class HPLFsaving:

    def __init__(self):
        self._run = SimpleSetting("hplf_seq_run_number", default_value=1)
        self._run_path = SimpleSetting("hplf_seq_run_path", default_value="")
        self._run_type = SimpleSetting("hplf_seq_run_type", default_value="")
        self._shutter = None

    #
    # RUN management
    #
    def run_reset(self):
        self._run.set(0)
        
    def run_open(self, name):
        self._run_type.set(name)
        timestamp = self._get_timestamp()
        self._run.set(self._run.get()+1)
        sample_name = f"run_{self._run.get():04d}_{name}_{timestamp}"
        newsample(sample_name)
        newdataset(sample_name)
        self._definition = click.prompt(BLUE("\n    Definition"))
        setup_globals.SCAN_SAVING.dataset["definition"] = self._definition
        self._run_path.set(self._get_icat_dir())
        print("\n")

        return self._run.get()
        
    def run_close(self):
        self.metadata_remove_category()
        sample_name = f"sample_{self._run.get()+1:04d}"
        newsample(sample_name)        
    
    #
    # Metadata Categorie
    #
    def metadata_create_category(self, shutter=None):
        self._shutter = shutter
        category = get_user_scan_meta()
        category.add_categories(["HPLF"])
        category._scan_meta_category("HPLF").timing = META_TIMING.END
        category._scan_meta_category("HPLF").set("HPLF", self.metadata_fill)
        self._already_done = False
        
    def metadata_fill(self, scan=None):
        if scan is not None:
            if hasattr(scan, "scan_info"):
                if scan.scan_info["type"].startswith("seq_"):
                    if not self._already_done:
                        self._pre_metadata()
                        if self.is_saved("streak"):
                            print("\n")
                            saved = click.confirm(
                                "  Are Streak images saved?",
                                default=False
                            )
                            if not saved:
                                self.pop_obj("streak")
                                
                        metadata = {}
                        for obj in self._save_list:
                            obj_dict = obj._get_metadata()
                            metadata[obj._name] = self._get_icat_dict(obj_dict)
                        metadata["definition"] = self._definition
                        
                        self._md_dict = metadata
                        self._already_done = True
                        return metadata

        return None
    
    def _pre_metadata(self):
        if self._shutter is not None:
            self._shutter.close()
        
    def metadata_remove_category(self):
        category = get_user_scan_meta()
        category.remove_categories(["HPLF"])  
        
    def _get_icat_dict(self, md_dict):
        md = {}
        icat_dir = self._get_icat_dir()
        for key, val in md_dict.items():
            # h5
            if key == "_h5_":
                md.update(val)
            # images
            elif key == "_image_":
                for name, data in val.items():
                    # Create icat/type directory if necessary
                    dir_type = data["type"]
                    save_dir = f"{icat_dir}/{dir_type}"
                    if not os.path.isdir(save_dir):
                        os.makedirs(save_dir)
                    # Build file name to save image
                    filename = f"{save_dir}/{name}.png"
                    # Save image in file
                    ID24saveImage(filename, data["data"])
                    # reference in h5 file
                    md[name] = filename
            # ASCII
            elif key == "_ascii_":
                for name, data in val.items():
                    # Create icat/type directory if necessary
                    dir_type = data["type"]
                    save_dir = f"{icat_dir}/{dir_type}"
                    if not os.path.isdir(save_dir):
                        os.makedirs(save_dir)
                    # Build file name to save ascii data
                    filename = f"{save_dir}/{name}.dat"
                    # Save data in file
                    ID24saveAscii(filename, data["data"])
                    # reference in h5 file
                    md[name] = filename
            # File reference
            elif key == "_file_":
                for name, data in val.items():
                    # Create icat/type directory if necessary
                    dir_type = data["type"]
                    save_dir = f"{icat_dir}/{dir_type}"
                    if not os.path.isdir(save_dir):
                        os.makedirs(save_dir)
                    # build filename
                    filename = data["data"][data["data"].rfind("/")+1:]
                    file_icat = f"{save_dir}/{filename}"
                    # copy file in icat location
                    shutil.move(data["data"], file_icat)
                    # reference in h5 file
                    md[name] = file_icat
            # complex object
            elif isinstance(val, dict):
                md[key] = self._get_icat_dict(val)
            else:
                md[key] = val
                
        return md

    #
    # Saved object
    #
    def saved_object_set(self, obj_list):
        self._save_list = obj_list
        
    def is_saved(self, obj_name):
        for obj in self._save_list:
            if obj_name == obj._name:
                return True
        return False
        
    def pop_obj(self, obj_name):
        for obj in self._save_list:
            if obj_name == obj._name:
                ind = self._save_list.index(obj)
                self._save_list.pop(ind)        
        
    def save(self):
        icat_dir = self._get_icat_dir()
        filename = f"{icat_dir}/metadata.dat"
        print(f"    Save metadata in : {filename}")
        yaml = YAML(pure=True)
        yaml.default_flow_style = False
        stream = StringIO()
        yaml.dump(self._md_dict, stream=stream)
        with open(filename, "w") as file_out:
            file_out.write(stream.getvalue())
    
    def save_ascii(self, scan):
        # Directory
        icat_dir = self._get_icat_dir()
        save_dir = f"{icat_dir}/XAS"
        if not os.path.isdir(save_dir):
            os.makedirs(save_dir)

        run = self._run.get()
        nbsp = scan._acq_params["nbframe"]
        nbpx = len(scan._acq_channels["I_norm_curr"][0])
        data_index = numpy.linspace(0, nbpx-1, nbpx)
        data_energy = numpy.copy(scan._acq_channels["Energy"]*1000.0)

        """
        Save I -> self._acq_channels["I_norm_curr"]
        """
        # Filename
        filename = f"{save_dir}/run_{run:04d}_I.dat"

        # Index + Energy
        data_header = ["px", "E(eV)"]
        self._data_ascii = numpy.zeros((nbsp+2, nbpx))
        self._data_ascii[0] = data_index
        self._data_ascii[1] = data_energy

        # Xh Data
        for sp in range(nbsp):
            data_header.append(f"I_fr{sp+1}")
            self._data_ascii[sp+2] = scan._acq_channels["I_norm_curr"][sp]

        # Save Ascii Data
        numpy.savetxt(
            filename,
            numpy.array(self._data_ascii).transpose(),
            delimiter="\t",
            header="\t".join(data_header),
        ) 

        """
        Save I0 -> self._acq_channels["I0_norm_curr"]
        """
        # Filename
        filename = f"{save_dir}/run_{run:04d}_I0.dat"
        
        # Index + Energy
        data_header = ["px", "E(eV)"]
        self._data_ascii = numpy.zeros((3, nbpx))
        self._data_ascii[0] = data_index
        self._data_ascii[1] = data_energy
        
        # Xh Data
        data_header.append(f"I0")
        self._data_ascii[2] = scan._acq_channels["I0_norm_curr"]
        
        # Save Ascii Data
        numpy.savetxt(
            filename,
            numpy.array(self._data_ascii).transpose(),
            delimiter="\t",
            header="\t".join(data_header),
        ) 
        
        """
        Save Mu -> self._acq_channels["Mu_norm_curr"]
        """
        # Filename
        filename = f"{save_dir}/run_{run:04d}_mu.dat"
        
        # Index + Energy
        data_header = ["px", "E(eV)"]
        self._data_ascii = numpy.zeros((nbsp+2, nbpx))
        self._data_ascii[0] = data_index
        self._data_ascii[1] = data_energy
        
        # Xh Data
        for sp in range(nbsp):
            data_header.append(f"mu_fr{sp+1}")
            self._data_ascii[sp+2] = scan._acq_channels["Mu_norm_curr"][sp]
        
        # Save Ascii Data
        numpy.savetxt(
            filename,
            numpy.array(self._data_ascii).transpose(),
            delimiter="\t",
            header="\t".join(data_header),
        ) 
        
    #
    # Tools
    #
    def _get_timestamp(self):
        #date
        today = datetime.date.today()
        timestamp = f"{today.year:04d}_{today.month:02d}_{today.day:02d}"
        # time
        currtime = time.ctime().split()[3].split(":")
        timestamp += f"_{currtime[0]}_{currtime[1]}_{currtime[2]}"
        return timestamp 
        
    def _get_icat_dir(self):
        SCAN_SAVING = setup_globals.SCAN_SAVING
        run_dir = SCAN_SAVING.filename
        run_dir = run_dir[:run_dir.rfind("/")]
        return run_dir
        
