
import numpy
 
from bliss.scanning.scan import ScanPreset
from bliss.common.utils import ColorTags, BOLD, RED
from bliss.shell.standard import umv

from fscan.fscantools import (
    FScanMode,
    FScanAxisMode,
    FScanTrigMode,
    FScanCamSignal,
    FScanParamBase,
    FScanParamStruct,
    FScanDisplay,
)

class ID24diffrScan:
   
    def __init__(self, name, config):
    
        self._name = name
        self._config = config

        # fscan
        self._fscan = self._config.get("fscan")
        if self._fscan is None:
            raise RuntimeError("ID24diffrScan: No fscan defined")

        # ccd
        self._ccd = self._config.get("ccd", None)
        if self._ccd is None:
            raise RuntimeError("ID2iffrScan: No CCD defined")

        # multiplexer
        self._multiplexer = self._config.get("multiplexer", None)
        if self._multiplexer is None:
            raise RuntimeError("ID24diffrScan: No multiplexer defined")

        # Rotation Motor
        self._rotmot = self._config.get("rotation_motor", None)
        if self._rotmot is None:
            raise RuntimeError("ID24diffrScan: No rotation_motor defined")

        # shutter
        self._shutter = self._config.get("shutter", None)
        
        # Scan Preset
        self._scan_preset = DiffrScanPreset(self._multiplexer)
                
        # Install scan preset
        self._fscan.add_scan_preset(self._scan_preset, "diffr_scan_preset")
    
    def oscill(self, rel_start, rel_stop, acq_time):
                      
        # Set scan title
        title = "oscill "
        title += f"{rel_start} {rel_stop} {acq_time}"
        scan_info = {"title": title}
        
        self._scan_preset.set_return_position(self._rotmot, self._rotmot.position)
        
        # Set fscan Parameters
        self._fscan.fscan.pars.motor = self._rotmot
        self._fscan.fscan.pars.start_pos = self._rotmot.position + rel_start
        speed = numpy.fabs(rel_stop - rel_start) / acq_time
        overshut = 0.003 * speed
        self._fscan.fscan.pars.step_size = rel_stop - rel_start + overshut        
        self._fscan.fscan.pars.step_time = acq_time+0.003
        self._fscan.fscan.pars.acq_time = acq_time
        self._fscan.fscan.pars.npoints = 1
        self._fscan.fscan.pars.scan_mode     = FScanMode.POSITION
        self._fscan.fscan.pars.gate_mode     = FScanMode.TIME
        self._fscan.fscan.pars.camera_signal = FScanCamSignal.LEVEL
        self._fscan.fscan.pars.save_flag     = True
        self._fscan.fscan.pars.display_flag  = True
        self._fscan.fscan.pars.sync_encoder  = True
        self._fscan.fscan.pars.home_rotation = False
        self._fscan.fscan.pars.acc_margin    = 0.0
        self._fscan.fscan.pars.sampling_time = 0.5
        self._fscan.fscan.pars.latency_time  = 0.0
        
        # Take pilatus background
        #self.take_background()
        
        # Set pilatus background calculation
        #self._ccd.processing.use_background = True

        # Start the scan
        self._fscan.fscan.run(scan_info=scan_info)
    
    def oscill_step(self, rel_start, rel_stop, angular_step, acq_time):
                      
        # Set scan title
        title = "oscill_step "
        title += f"{rel_start} {rel_stop} {angular_step} {acq_time}"
        scan_info = {"title": title}
        
        self._scan_preset.set_return_position(self._rotmot, self._rotmot.position)
        
        # Set fscan Parameters
        self._fscan.fscan.pars.motor = self._rotmot
        self._fscan.fscan.pars.start_pos = self._rotmot.position + rel_start
        self._fscan.fscan.pars.step_size = angular_step  
        self._fscan.fscan.pars.step_time = acq_time
        self._fscan.fscan.pars.acq_time = acq_time - 0.003
        self._fscan.fscan.pars.npoints = int(numpy.fabs((rel_stop-rel_start)/angular_step))
        self._fscan.fscan.pars.scan_mode     = FScanMode.POSITION
        self._fscan.fscan.pars.gate_mode     = FScanMode.TIME
        self._fscan.fscan.pars.camera_signal = FScanCamSignal.LEVEL
        self._fscan.fscan.pars.save_flag     = True
        self._fscan.fscan.pars.display_flag  = True
        self._fscan.fscan.pars.sync_encoder  = True
        self._fscan.fscan.pars.home_rotation = False
        self._fscan.fscan.pars.acc_margin    = 0.0
        self._fscan.fscan.pars.sampling_time = 0.5
        self._fscan.fscan.pars.latency_time  = 0.0
        
        # Take pilatus background
        #self.take_background()
        
        # Set pilatus background calculation
        #self._ccd.processing.use_background = True

        # Start the scan
        self._fscan.fscan.run(scan_info=scan_info)

    def take_background(self, inttime=0.1):
        self._background_image = None
        
        file_format = self._ccd.saving.file_format
        self._ccd.saving.file_format = "EDF"
        
        try:
            use_background = self._ccd.processing.use_background
            use_flatfield  = self._ccd.processing.use_flatfield
            self._ccd.processing.use_background = False
            self._ccd.processing.use_flatfield = False
            
            # take dark image without any correction!
            if self._shutter is not None:
                shutter_open = self._shutter.is_open
                if shutter_open:
                    print ("close shutter")
                    self._shutter.close()
            
            lt = limatake(inttime, 1, self._ccd, save=True)
            
            if self._shutter is not None:
                if shutter_open:
                    print ("open shutter")
                    self._shutter.open()
            
            node = lt.get_data()[f"{self._ccd.name}:image"]
            self._background_image = node.get_image(0)
            
            #self.__dark_image = self.detector.proxy.saving_directory + "/" + self.detector.proxy.saving_prefix + \
            #              "{:>04}".format(self.detector.proxy.last_image_saved) + self.detector.proxy.saving_suffix
        except:
            self._ccd.saving.file_format = file_format
            raise
        
        self._ccd.saving.file_format = file_format
        
        self._ccd.processing.background = self._background_image
        print(f"Background image = {self._background_image}")
        
        # set back the original correctction state
        self._ccd.processing.use_background = use_background
        self._ccd.processing.use_flatfield = use_flatfield
        
class DiffrScanPreset(ScanPreset):
    
    def __init__(self, multiplexer):
        self.multiplexer = multiplexer
        self._return_motor = None
        self._return_motor = 0
    
    def set_return_position(self, motor, position):
        self._return_position = position
        self._return_motor = motor
        
    def prepare(self, scan):
        self.multiplexer.switch("SEL_TRIG", "FSCAN")        
        
    def start(self, scan):
        pass     
           
    def stop(self, scan):
        self.multiplexer.switch("SEL_TRIG", "STEP")
        if self._return_motor is not None:
            umv(self._return_motor, self._return_position)
        self._return_motor = None

