

class ID24energymeterController:

    def __init__(self, name, config):
        self._name = name
        self._config = config
        
        self._enmeters = []
        enmeter_list = config.get("energymeter", None)
        if enmeter_list is not None:
            for enmeter in enmeter_list:
                enmeter_obj = {
                    "name": enmeter.get("energymeter_name"), 
                    "channel": enmeter.get("channel", None)
                }
                self._enmeters.append(enmeter_obj)
         
    def _get_metadata(self):
        md = {"Energy Meter": {}}
        for enmeter in self._enmeters:
            name = enmeter["name"].name
            if enmeter["channel"] is not None:
                energy = enmeter["name"].energy(enmeter["channel"])
                energy_raw = enmeter["name"].energy_raw(enmeter["channel"])
                conv_factor = enmeter["name"]._chan_conf[enmeter["channel"]]["conv_factor"]
            else:
                energy = enmeter["name"].energy
                energy_raw = enmeter["name"].energy_raw
                conv_factor = enmeter["name"]._conv_factor
            md["Energy Meter"][name] = {
                "energy": f"{energy} J",
                "energy_raw": f"{energy_raw} J",
                "convertion Factor": conv_factor
            }
        return md
