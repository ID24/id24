# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2020 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from bliss.controllers.motor import CalcController
from bliss.common.logtools import user_warning, log_debug
from bliss.scanning.scan_meta import get_user_scan_meta
from bliss.common.protocols import HasMetadataForScan

"""
- "support": real motor holding the second motor
- "move": real motor supported by the motor "support"
- "support" motor MUST move positive when moving away from the motor block
- "move" motor MUST move positive when moving to the motor block
- "offset" calc motor is moving positive from (r)ing/(b)ottom to (h)all/(t)op
- "support_sign" yml parameter is 1.0 when "support" is moving as "offset", -1.0 if opposite
- when moving "offset", only "support" is moving

Horizontal slits
    support     : ring
    move        : hall
    sign ring   : positive to ring
    sign hall   : positive to hall
Vertical slits
    support     : top
    move        : bottom
    sign top    : positive up
    sign bottom : positive down

-
  controller:
    class: stable_slits
    axes:
        - name: $rup
          tags: real support
        - name: $rdown
          tags: real move
        - name: vgap
          tags: gap
        - name: voff
          tags: offset
"""
class StableSlits(CalcController, HasMetadataForScan):
    
    def __init__(self, *args, **kwargs):
        CalcController.__init__(self, *args, **kwargs)
        self.support_sign = self.config.get("support_sign", 1.0)

    def scan_metadata(self):
        """ 
        this is about metadata publishing to the h5 file AND ICAT
        """
        cur_pos = self._calc_from_real()
        meta_dict = {
            "gap": cur_pos["offset"],
            "offset": cur_pos["gap"],
            "@NX_class": "NXslit"
        }
        return meta_dict

    def calc_from_real(self, positions_dict):
        log_debug(self, "[STABLE SLITS] calc_from_real()")
        log_debug(self, "[STABLE SLITS]\treal: %s" % positions_dict)

        sign = self.support_sign
        support = positions_dict["support"]        
        move = positions_dict["move"]  
              
        calc_dict = dict()
        calc_dict["offset"] = sign * (support - 0.5 * move)
        calc_dict["gap"] = move

        log_debug(self, "[STABLE SLITS]\tcalc: %s" % calc_dict)

        return calc_dict

    def calc_to_real(self, positions_dict):
        log_debug(self, "[STABLE SLITS] calc_to_real()")
        log_debug(self, "[STABLE SLITS]\tcalc: %s" % positions_dict)

        sign = self.support_sign
        offset = positions_dict["offset"]        
        gap = positions_dict["gap"]  

        real_dict = dict()
        real_dict["support"] = sign * offset + 0.5 * gap
        real_dict["move"] = gap

        log_debug(self, "[STABLE SLITS]\treal: %s" % real_dict)

        return real_dict
