import os
import click
import tabulate
import numpy
import PIL

from bliss import setup_globals, current_session
from bliss.shell.standard import wm, newproposal
from bliss.common.axis import Axis
from bliss.common import measurementgroup, plot
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED, ORANGE
from bliss.controllers.motor import CalcController
from bliss.scanning.scan_tools import get_selected_counter_name
from bliss.config.static import get_config

##################################################################
#####
##### SESSION
#####
def newsession(session=""):
    SCAN_SAVING = setup_globals.SCAN_SAVING

    if "session_dcm" in current_session.env_dict.keys():
        exafs_param = setup_globals.exafs_param
        exafs_param.clean()
    
    motpos = get_config().get("motpos")
    
    newproposal(session)
    
    # Create default directories
    proposal_dir = os.path.join(
        SCAN_SAVING.base_path,
        SCAN_SAVING.proposal_name,
        SCAN_SAVING.beamline
    )
    macros_dir = os.path.join(proposal_dir, "macros")
    if not os.path.isdir(macros_dir):
        os.makedirs(macros_dir)
    current_session.user_script_homedir(macros_dir)
    params_dir = os.path.join(proposal_dir, "exafs_param")
    if not os.path.isdir(params_dir):
        os.makedirs(params_dir)
    motpos_dir = os.path.join(proposal_dir, "motpos")
    if not os.path.isdir(motpos_dir):
        os.makedirs(motpos_dir)
    motpos.clean(remove_file=False)
    
    # For Laser experiment, reset the run number to 1
    if "session_laser" in current_session.env_dict.keys():
        if "hplf_seq" in current_session.env_dict.keys():
            config = get_config()
            hplf_seq = config.get("hplf_seq")
            hplf_seq._save.run_reset()
            

##################################################################
#####
##### FLINT
#####
def edit_mask(detector):
    from bliss.common import plot as plot_mdl
    f = plot_mdl.get_flint()
    plot_id = f.get_live_scan_plot(detector.image.fullname, "image")
    plot_mdl.plot_image(existing_id=plot_id)
    p = plot_mdl.plot_image(existing_id=plot_id)
    return p.select_mask()

def curs():
    cnt_name = get_selected_counter_name()
    print("Selected counter: {BLUE(cnt_name}")
    print("\n")
    print("Select a point on flint ...")
    f = plot.get_flint()
    c = f.get_live_plot("default-curve")
    res = c.select_point(1)
    position = c[0]
    

##################################################################
#####
##### MOTOR POSITIONS PER DEVICES
#####
def wps():
    id24wm(
        get_config().get("pst"),
        get_config().get("psb"),
        get_config().get("psr"),
        get_config().get("psh")
    )
    id24wm(
        get_config().get("psvg"),
        get_config().get("psvo"),
        get_config().get("pshg"),
        get_config().get("psho")
    )

def wmv1ss():
    id24wm(
        get_config().get("mv1sst"),
        get_config().get("mv1ssb"),
        get_config().get("mv1ssr"),
        get_config().get("mv1ssh")
    )
    id24wm(
        get_config().get("mv1ssvg"),
        get_config().get("mv1ssvo"),
        get_config().get("mv1sshg"),
        get_config().get("mv1ssho")
    )

def wmv1():
    id24wm(
        get_config().get("mv1ty"),
        get_config().get("mv1tz"),
        get_config().get("mv1ry")
    )
    id24wm(
        get_config().get("mv1tzu"),
        get_config().get("mv1tzd")
    )

def wmh():
    id24wm(
        get_config().get("mhtyu"),
        get_config().get("mhtyd")
    )
    id24wm(
        get_config().get("mh1ty"),
        get_config().get("mh1rz")
    )
    id24wm(
        get_config().get("mh2mh3ty"),
        get_config().get("mh2mh3rz"),
        get_config().get("mh2rz"),
        get_config().get("mhtz"), 
        get_config().get("mhtyvac")
    )
    val = GREEN(get_config().get("mhty_enc").read())
    print(f"    mhty encoder: {val}")
    val = GREEN(get_config().get("mhrz_enc").read())
    print(f"    mhrz encoder: {val}")

def wmhss():
    id24wm(
        get_config().get("mhssr"),
        get_config().get("mhssh"),
        get_config().get("mhsst"),
        get_config().get("mhssb")
    )
    id24wm(
        get_config().get("mhssvg"),
        get_config().get("mhssvo"),
        get_config().get("mhsshg"),
        get_config().get("mhssho"),
        get_config().get("mhsstyz")
    )

def wedss():
    id24wm(
        get_config().get("edssr"),
        get_config().get("edssh"),
        get_config().get("edsst"),
        get_config().get("edssb")
    )
    id24wm(
        get_config().get("edsshg"),
        get_config().get("edssho"),
        get_config().get("edssvg"),
        get_config().get("edssvo")
    )

def wxhs():
    id24wm(
        get_config().get("xhslitr"),
        get_config().get("xhslith"),
        get_config().get("xhslitt"),
        get_config().get("xhslitb")
    )
    id24wm(
        get_config().get("xhslithg"),
        get_config().get("xhslitho"),
        get_config().get("xhslitvg"),
        get_config().get("xhslitvo")
    )
    
def wpoly():
    id24wm(
        get_config().get("polyth"),
        get_config().get("polyshut")
    )
    id24wm(
        get_config().get("polylegu"),
        get_config().get("polylegdr"),
        get_config().get("polylegdh")
    )
    id24wm(
        get_config().get("polytz"),
        get_config().get("polychi"),
        get_config().get("polypsi")
    )
    id24wm(
        get_config().get("polybl"),
        get_config().get("polybh")
    )
    id24wm(
        get_config().get("polybend"),
        get_config().get("polyellipse"),
        get_config().get("polytwist")
    )

def wbed():
    id24wm(
        get_config().get("bed2th"),
        get_config().get("bedtx1"),
        get_config().get("bedtx2")
    )

def wsed():
    id24wm(
        get_config().get("sedtx"),
        get_config().get("sedty"),
        get_config().get("sedtz"),
        get_config().get("sedrz"),
        get_config().get("sedtzu"),
        get_config().get("sedtzd")
    )
    id24wm(
        get_config().get("seddiagtx"),
        get_config().get("seddiagty"),
        get_config().get("seddiagtz"),
        get_config().get("seddiagscr")
    )

def wded():
    id24wm(
        get_config().get("dedtx"),
        get_config().get("dedty"),
        get_config().get("dedtz"),
        get_config().get("dedtzu"),
        get_config().get("dedtzd")
    )
    id24wm(
        get_config().get("dediris"),
        get_config().get("dedfoc"),
        get_config().get("dedrx"),
        get_config().get("dedshut")
    )

def wtransport():
    id24wm(
        get_config().get("m0rx"),
        get_config().get("m0ry")
    )
    id24wm(
        get_config().get("m1rx"),
        get_config().get("m1ry")
    )
    id24wm(
        get_config().get("sf1tx"),
        get_config().get("sf1ty"),
        get_config().get("sf1tz")
    )
    id24wm(
        get_config().get("m2rx"),
        get_config().get("m2ry")
    )
    id24wm(
        get_config().get("m3rx"),
        get_config().get("m3ry")
    )
    id24wm(
        get_config().get("sf2tx"),
        get_config().get("sf2ty"),
        get_config().get("sf2tz")
    )
    id24wm(
        get_config().get("m4rx"),
        get_config().get("m4ry")
    )
    id24wm(
        get_config().get("m5rx"),
        get_config().get("m5ry")
    )
    id24wm(
        get_config().get("inj_visar_pol"),
        get_config().get("inj_visar_trans")
    )
    
    val = get_config().get("shutter_drive_eh1")._get_info()
    print(f"\n  {val}")
    val = get_config().get("x2")._get_info()
    print(f"\n  {val}")
    val = get_config().get("x4")._get_info()
    print(f"\n  {val}")
    val = get_config().get("x51")._get_info()
    print(f"\n  {val}")
    val = get_config().get("shutter_visar")._get_info()
    print(f"\n  {val}")
    val = get_config().get("shutter_visar_eh1")._get_info()
    print(f"\n  {val}")

def wtarget():
    id24wm(
        get_config().get("targettx"),
        get_config().get("targetty"),
        get_config().get("targettz")
    )
    

def wic():
    id24wm(
        get_config().get("blocktx"),
        get_config().get("blockty"),
        get_config().get("blocktz")
    )
    id24wm(
        get_config().get("lfoctx"),
        get_config().get("lfocty"),
        get_config().get("lfoctz")
    )
    id24wm(
        get_config().get("targettx"),
        get_config().get("targetty"),
        get_config().get("targettz")
    )
    id24wm(
        get_config().get("lvisartx"),
        get_config().get("lvisarty"),
        get_config().get("lvisartz")
    )
    id24wm(
        get_config().get("ldrivetx"),
        get_config().get("ldrivety")
    )
    id24wm(
        get_config().get("mvisar_ry"),
        get_config().get("mvisar_rz")
    )
    id24wm(
        get_config().get("uscope_ds_foc"),
        get_config().get("uscope_ds_ty")
    )
    id24wm(
        get_config().get("shieldvisar")
    )
    print("")
    val = get_config().get("uscope_us_shield")._get_info()
    print(f"  {val}")
    val = get_config().get("x51")._get_info()
    print(f"\n  {val}")
    
def wvisar():
    id24wm(
        get_config().get("mvisar_ry"),
        get_config().get("mvisar_rz")
    )
    id24wm(
        get_config().get("lvisartx"),
        get_config().get("lvisarty"),
        get_config().get("lvisartz")
    )
#    id24wm(
#        get_config().get("visartz")
#    )
    id24wm(
        get_config().get("shieldvisar")
    )
    print("")
    val = get_config().get("shutter_visar")._get_info()
    print(f"  {val}")
    val = get_config().get("shutter_visar_eh1")._get_info()
    print(f"  {val}")
    print("")
    
def wshutters():
    print("Drive laser:")
    val = get_config().get("shutter_drive_eh1")._get_info()
    print(f"  {val}")
    print("")
    print("Probe laser:")
    val = get_config().get("shutter_visar")._get_info()
    print(f"  {val}")
    val = get_config().get("shutter_visar_eh1")._get_info()
    print(f"  {val}")
    print("")
    print("Shields:")
    val = get_config().get("uscope_us_shield")._get_info()
    print(f"  {val}")
    val = get_config().get("diagatt")._get_info()
    print(f"  {val}")
    print("")

def wpmf():
    id24wm(get_config().get("pmftxr"), get_config().get("pmftxh"))
    id24wm(
        get_config().get("pmftzur"),
        get_config().get("pmftzuh"),
        get_config().get("pmftzd")
    )
    id24wm(get_config().get("pmftx"), get_config().get("pmfty"), get_config().get("pmftz"))
    id24wm(get_config().get("pmfrx"), get_config().get("pmfry"), get_config().get("pmfrz"))
    id24wm(
        get_config().get("pmfdiagtx"),
        get_config().get("pmfdiagty"),
        get_config().get("pmfdiagtz"),
        get_config().get("pmfdiagscr")
    )

def wchem():
    id24wm(get_config().get("chemtx"), get_config().get("chemty"), get_config().get("chemtz"))
    id24wm(get_config().get("chemrz"))

def wqwp():
    id24wm(get_config().get("qwpty"), get_config().get("qwptz"))
    id24wm(get_config().get("qwppsi"), get_config().get("qwpth"))

def wvrm():
    id24wm(get_config().get("mvrmtz"), get_config().get("mvrmrx"), get_config().get("mvrmry"), get_config().get("mvrmcut"))
    id24wm(get_config().get("mvrmbendu"), get_config().get("mvrmbendd"))
    id24wm(get_config().get("vrmbend"), get_config().get("vrmellipse"))
    id24wm(get_config().get("mvrmth"))

# DCM
def wdcmdet():
    id24wm(get_config().get("dcmdettx"), get_config().get("dcmdetty"))
    id24wm(get_config().get("dcmdettz"), get_config().get("dcmdetry"))
    id24wm(get_config().get("dcmdettzu"), get_config().get("dcmdettzd"))
    print("")
    
def wdcmss():
    id24wm(get_config().get("dcmssr"), get_config().get("dcmssh"), get_config().get("dcmsst"), get_config().get("dcmssb"))
    id24wm(get_config().get("dcmssvg"), get_config().get("dcmssvo"), get_config().get("dcmsshg"), get_config().get("dcmssho"))
    print("")
    
def wdcmbench():
    id24wm(get_config().get("dcmsamtx"), get_config().get("dcmdettx"))
    
def wkb():
    id24wm(get_config().get("kbho"), get_config().get("kbvo"))
    id24wm(get_config().get("kbhg"), get_config().get("kbvg"))
    id24wm(get_config().get("kbbenderhu"), get_config().get("kbbenderhd"))
    id24wm(get_config().get("kbbendervu"), get_config().get("kbbendervd"))
    id24wm(get_config().get("kbvrot"), get_config().get("kbvecrot"))
    id24wm(get_config().get("kbvry"), get_config().get("kbvtz"))
    id24wm(get_config().get("kbhrot"), get_config().get("kbhecrot"))
    id24wm(get_config().get("kbhrz"), get_config().get("kbhty"))
    print("")
    
def wuxas():
    id24wm(get_config().get("dcmsamtx"))
    id24wm(get_config().get("uxasty"), get_config().get("uxastz"))
    id24wm(get_config().get("uxassamtx"), get_config().get("uxassamty"), get_config().get("uxassamtz"))
    id24wm(get_config().get("uxassamrz"))
    print("")
    
def wdcmdiag():
    id24wm(get_config().get("dcmdiagtx"), get_config().get("dcmdiagty"), get_config().get("dcmdiagtz"))
    id24wm(get_config().get("dcmdiagscr"))
    print("")

##################################################################
#####
##### LIMA
#####
def lima_start_live(ccd, inttime):
    f = plot.get_flint()
    f.start_image_monitoring(ccd.image.fullname, ccd.proxy.name())
    ccd.proxy.acq_time = inttime
    ccd.proxy.video_source = "BASE_IMAGE"
    ccd.proxy.video_exposure = inttime
    ccd.proxy.video_live = True

def lima_stop_live(ccd):
    ccd.proxy.video_live=False
    
##################################################################
#####
##### MOTORS
#####
def motor_esync(axis):
    axis.hw_state
    ch = axis.address
    axis.controller.raw_write(f"{ch}:esync")
    axis.controller.raw_write(f"{ch}:power on")
    axis.sync_hard()

def icepap_smaract_enable(axis):
    ch = axis.address
    axis.controller.raw_write(f"{ch}:infoa low normal")
    axis.controller.raw_write(f"{ch}:infoa high normal")
    
def get_axes_iter():
    for name in dir(setup_globals):
        elem = getattr(setup_globals, name)
        if isinstance(elem, Axis):
            yield elem

def MotorsForceInit():
    for motor in get_axes_iter():
        motor.hw_state

def ApplySessionAxisConfig():
    for motor in get_axes_iter():
        print(f"    \"{motor.name}\"")
        motor.hw_state

def id24wm(*motors):
    print("")
    lines = []
    line1 = [BOLD("    Name")]
    line2 = [BOLD("    Pos.")]
    for mot in motors:
        if isinstance(mot.controller, CalcController):
            line1.append(f"{ORANGE(mot.name)}({mot.unit})")
        else:
            line1.append(f"{BLUE(mot.name)}({mot.unit})")
        line2.append(f"{mot.position:.3f}")
    lines.append(line1)
    lines.append(line2)
    mystr = tabulate.tabulate(lines, tablefmt="plain")
    print(mystr)


##################################################################
#####
##### Attribute List
#####
class ID24attrList:
    
    def __init__(self, items):
        self._items = items
        for name, dev in self._items.items():
            self._set_device_attr(name, dev)
    
    def __info__(self):
        info_str = ""
        for name, dev in self._items.items():
            info_str += dev.__info__()
        return info_str
        
    def _set_device_attr(self, name, device):
        setattr(self, name, device)  
        
    def _del_device_attr(self, name):
        delattr(self, name)

##################################################################
#####
##### Column named array
#####
class ID24profile:
    def __init__(self):
        self._data_x = None
        self._data_y = []
        self._headers = []
            
    def set_data(self, x_label, x_data, y_labels, y_datas):
        self._header = [x_label]
        for label in y_labels:
            self._header.append(label)
            
        nbc = len(x_data)
        nbl = len(y_datas)+1
        self._data = numpy.zeros((nbl, nbc))
        self._data[0] = numpy.copy(x_data)
        for i in range(len(y_datas)):
            self._data[i+1] = numpy.copy(y_datas[i])
        
    def header(self):
        return self._header
    
    def data(self):
        return self._data

##################################################################
#####
##### Saving data
#####

# HPLF
def ID24saveAscii(filename, data):
    header = ""
    for head in data.header():
        header += f"{head}\t"
    numpy.savetxt(
        filename,
        numpy.array(data.data()).transpose(),
        delimiter="\t",
        header=header,
    ) 
    
def ID24saveImage(filename, data):
    im = PIL.Image.fromarray(data)
    im.save(filename)

# Mono scans
def save_contscan_ascii(scn, comment=None):
    headers = [
        "energy_enc",
        "mu_trans",
        "I0",
        "I1",
    ]
    
    data=[]
    header = []
    
    scan_data = scn.get_data()
    scan_keys = scan_data.keys()
    for name in headers:
        key = find_in_keys(name, scan_keys)
        if key is not None:
            if key == "Emono":
                data_emono = numpy.copy(scan_data[key]) * 1000.0
                data.append(data_emono)
            else:
                data.append(scan_data[key])
            header.append(name)
    
    header_str = ""
    if comment is not None:
        header_str += f"{comment}\n"
    header_str += "\t".join(header)

    save_ascii(data, header_str)
    
def find_in_keys(name, keys):
    for key in keys:
        nkey = key.split(":")[-1]
        if nkey == name:
            return key
    return None
    
def save_ascii(data, header_str):
    
    SCAN_SAVING = setup_globals.SCAN_SAVING
    filename = SCAN_SAVING.filename.split(".")[0]
    scann = SCAN_SAVING.writer_object.last_scan_number
    filename = f"{filename}_scan{scann}.dat"
    
    numpy.savetxt(
        filename,
        numpy.array(data).transpose(),
        delimiter="\t",
        header=header_str,
    ) 
