import numpy
import tabulate
import functools
import gevent
import re

from bliss.comm.util import get_comm
from bliss.comm.tcp import Command


class VAPS:
    def __init__(self, config):
        self._config = config
        self._name = config.get("name")
        hostname = config.get("host")
        self._cnx = Command(hostname, 5000, eol="\n")
        self._channel = config.get("channel")

        gains = self._putget(f"?GAIN {self._channel} LIST").split("\n")
        self._gains = {}
        for gain in gains:
            if gain != "" and gain.find(self._channel.upper()) == -1:
                gain_str = gain.replace(".", "_")
                gain_str = gain_str.replace("LN", "lo_")
                key = f"gain_{gain_str}"
                self._gains[key] = gain
                setattr(self, key, functools.partial(self._set_range, gain))

    def __info__(self):
        mystr = f"  {self._channel}: {self.gain}\n"
        return mystr

    @property
    def gain(self):
        return self._putget(f"?GAIN {self._channel}").split(" ")[1]

    def _set_range(self, gain):
        self._putget(f"GAIN {self._channel} {gain}")

    def _putget(self, cmd, data=None):
        """Raw connection to the Maestrio card.

        cmd -- the message you want to send
        data -- binnary or ascii data
        """
        _check_reply = re.compile(r"^[#?]")
        reply_flag = _check_reply.match(cmd)
        cmd_raw = cmd.encode()
        cmd_name = cmd_raw.split()[0].strip(b" #").upper()
        if data is not None:
            # check if string; i.e program
            if isinstance(data, str) or isinstance(data, bytes):
                if isinstance(data, str):
                    raw_data = data.encode()
                else:
                    raw_data = data

                header = struct.pack(
                    "<III",
                    self.BINARY_SIGNATURE | self.BINARY_NOCHECKSUM | 0x1,
                    len(raw_data),
                    0x0,
                )  # no checksum
                full_cmd = b"%s\n%s%s" % (cmd_raw, header, raw_data)
                transaction = self._cnx._write(full_cmd)
            elif isinstance(data, numpy.ndarray):
                if data.dtype in [
                    numpy.uint8,
                    numpy.int8,
                    numpy.uint16,
                    numpy.int16,
                    numpy.uint32,
                    numpy.int32,
                    numpy.uint64,
                    numpy.int64,
                ]:
                    raw_data = data.tostring()
                    header = struct.pack(
                        "<III",
                        self.BINARY_SIGNATURE
                        | self.BINARY_NOCHECKSUM
                        | data.dtype.itemsize,
                        int(len(raw_data) / data.dtype.itemsize),
                        0x0,
                    )  # no checksum
                    full_cmd = b"%s\n%s%s" % (cmd_raw, header, raw_data)
                    transaction = self._cnx._write(full_cmd)
                else:
                    raise RuntimeError(f"Numpy datatype {data.dtype} not yet managed")
            else:
                raise RuntimeError("Not implemented yet ;)")
        else:
            transaction = self._cnx._write(cmd_raw + b"\n")
        with self._cnx.Transaction(self._cnx, transaction):
            if reply_flag:
                msg = self._cnx._readline(
                    transaction=transaction, clear_transaction=False
                )
                if not msg.startswith(cmd_name):
                    print(msg)
                    raise RuntimeError(
                        f"Unknown error, send {cmd}, reply:", msg.decode()
                    )
                if msg.find(b"ERROR") > -1:
                    # patch to workaround protocol issue in maestrio
                    # in Error, maestrio send two '\n' instead of one
                    if not transaction.empty() and transaction.peek()[0:1] == b"\n":
                        data = transaction.get()[1:]
                        if len(data):
                            transaction.put(data)
                    raise RuntimeError(msg.decode())
                elif msg.find(b"$") > -1:  # multi line
                    msg = self._cnx._readline(
                        transaction=transaction, clear_transaction=False, eol=b"$\n"
                    )
                    return msg.decode()
                elif msg.find(b"?*") > -1:  # binary reply
                    header_size = struct.calcsize("<III")
                    header = self._cnx._read(
                        transaction, size=header_size, clear_transaction=False
                    )
                    magic_n_type, size, check_sum = struct.unpack("<III", header)
                    magic = magic_n_type & 0xFFFF0000
                    assert magic == self.BINARY_SIGNATURE
                    raw_type = magic_n_type & 0xF
                    numpy_type = {1: numpy.uint8, 2: numpy.uint16, 4: numpy.uint32}.get(
                        raw_type
                    )
                    data = self._cnx._read(
                        transaction,
                        size=size * numpy_type().dtype.itemsize,
                        clear_transaction=False,
                    )
                    return numpy.frombuffer(data, dtype=numpy_type)
                return msg[len(cmd_name) :].strip(b" ").decode()


class RVElectrometer:
    def __init__(self, config):
        self._config = config
        self._name = config.get("name")
        self._resistor_suffix = ["hl", "r20M", "r1M", "r50k", "r2k5"]
        self._gain_suffix = ["g10", "g5", "g2"]

        self._load_config()

    """
    Load Configuration
    """

    def _load_config(self):
        # wago
        self._wago = self._config.get("wago", None)
        if self._wago is None:
            raise RuntimeError(f"RVElectrometer {self._name}: No wago controller given")
        # channel
        self._channel = self._config.get("channel", None)
        if self._channel is None:
            raise RuntimeError(f"RVElectrometer {self._name}: No channel given")
        # build wago channel names
        self._resistor_name = []
        for radix in self._resistor_suffix:
            self._resistor_name.append(f"{self._channel}{radix}")
        self._gain_name = []
        for radix in self._gain_suffix:
            self._gain_name.append(f"{self._channel}{radix}")
        # Get range Object
        self._range = self._config.get("range", None)
        if self._range is None:
            raise RuntimeError(f"RVElectrometer {self._name}: No range oject given")
        # build range attributes
        for range_name in self._range.range_name:
            setattr(self, range_name, functools.partial(self._set_range, range_name))

    """
    Initialization
    """

    def __info__(self):
        mystr = ""
        return mystr

    def _set_range(self, name):

        # Intermediate values
        # resistor value
        current_resistor = self._wago.get(*self._resistor_name)
        range_resistor = self._range.get_resistor_value(name)
        set_resistor = []
        for ind in range(len(self._resistor_name)):
            set_resistor.append(self._resistor_name[ind])
            set_resistor.append((current_resistor[ind] or range_resistor[ind]))

        # gain value
        # set all gain to 0
        set_gain = []
        for ind in range(len(self._gain_name)):
            set_gain.append(self._gain_name[ind])
            set_gain.append(0.0)

        # Set wago channels with intermediate values
        values = set_resistor + set_gain
        self._wago.set(*values)
        gevent.sleep(0.01)

        # FInal value
        # set final resistor value
        set_resistor = []
        for ind in range(len(current_resistor)):
            set_resistor.append(self._resistor_name[ind])
            set_resistor.append(range_resistor[ind])
        # set final gain value
        range_gain = self._range.get_gain_value(name)
        set_gain = []
        for ind in range(len(self._gain_name)):
            set_gain.append(self._gain_name[ind])
            set_gain.append(range_gain[ind])
        # set wago channels with final values
        values = set_resistor + set_gain
        self._wago.set(*values)
        gevent.sleep(0.01)

    def get_range(self):
        values = self._resistor_name + self._gain_name
        return self._range.get_range_name(self._wago.get(*values))


class RVErange:
    def __init__(self, config):
        self._config = config
        self._name = config.get("name")
        self.resistor_suffix = ["hl", "r20M", "r1M", "r50k", "r2k5"]
        self.gain_suffix = ["g10", "g5", "g2"]
        self._load_config()

    """
    Load Configuration
    """

    def _load_config(self):
        range_list = self._config.get("range_list", None)
        if range_list is None:
            raise RuntimeError(f"RVErange {self._name}: No range list given")

        self.range_name = []
        self.range_resistor = {}
        self.range_gain = {}
        for range_conf in range_list:
            range_name = range_conf.get("range_name")
            self.range_name.append(range_name)
            self.range_resistor[range_name] = list(range_conf.get("range_resistor"))
            self.range_gain[range_name] = list(range_conf.get("range_gain"))

    def get_resistor_value(self, name):
        return self.range_resistor[name]

    def get_gain_value(self, name):
        return self.range_gain[name]

    def get_range_name(self, value):
        resistor = value[0:5]
        gain = value[5:8]
        for range_name in self.range_name:
            if (
                resistor == self.range_resistor[range_name]
                and gain == self.range_gain[range_name]
            ):
                return range_name
        return None
