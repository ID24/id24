
import numpy
import gevent
import tabulate

from bliss.comm.util import get_comm
from bliss.common.greenlet_utils import protect_from_kill
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED, ORANGE

from bliss.comm.tcp import SocketTimeout

class Streak:
    def __init__(self, config):
        self._name = config.get("name")
        self._config = config
        self._delay_channel = config.get("delay_channel")
        self._cnx = get_comm(config, timeout=3)
        
        self._error_str = [
            "Success",
            "InvalidSyntax",
            "UnknownCommand",
            "CommandNotAllowed",
            "Message",
            "DefaultValue",
            "MissingParameter",
            "CannotExec",
            "ErrorDuringExec",
            "CannotSendData",
            "ValueOutOfRange"
        ]
        self.info = ""
        
    def __info__(self):
        print(self._get_info())
        return ""
        
    def _get_info(self):
        # Nicolas, 2023-2024
        mystr = YELLOW(f"{self._name}:")
        
        # AppInfo()
        progName = self.command("AppInfo(Title)")
        progVersion = self.command("AppInfo(Version)")

        #temperature = self.command("MainParamGet(Temperature)") # Not available on HPLF

        streakModel = self.command("MainParamGet(StreakCamera)")

        # CamParamGet()
        liveExposure = self.command("CamParamGet(Live,Exposure)")
        aquireExposure = self.command("CamParamGet(Acquire,Exposure)")
        
        mystr += "\n" + streakModel + \
        f" - running {progName} (version {progVersion})" +\
        f"\n\tTime window: {self.timeRange()} [{self.timeRangeMax()} max]" +\
        f"\n\tShutter:     {self.shutterState()}" +\
        f"\n\tGate mode:   {self.gateMode()}"
        if self.operatingMode() == "Focus":
            mytempstr = RED("⚠ "+ self.operatingMode() + " ⚠")
        elif self.operatingMode() == "Operate":
            mytempstr = GREEN(self.operatingMode())
        else:
            mytempstr = YELLOW(self.operatingMode())
        
        mystr += f"\n\tMode:        {mytempstr}" +\
        f"\n\tGain:        {self.MCPGain()}" +\
        f"\n\tSynchro:     {self.synchroStatus()}" +\
        f"\n\tDelay:       {self.trigger.delay_picoseconds():13_d} ps" +\
        f"\n\nCamera is {self.acquireStatus()} [{self._imageSize()}]" +\
        ""
        return mystr
    
    def command(self, cmd, debug=False):
        if debug:
            print(BLUE(cmd))
        self._cnx.write(f"{cmd}\r".encode())
        self.info = ""
        while True:
            rep = self._cnx.readline(eol="\r").decode()
            
            if debug:
                print(RED(rep))
                      
            if rep == 'RemoteEx Ready' or rep == "":
                continue
            
            rep_arr = rep.split(",")
            error_code = int(rep_arr[0])
            
            if error_code == 4:
                answer = rep[rep.rfind(",")+1:]
                self.info += f"{answer}\n"
            elif error_code == 5:
                answer = rep[rep.rfind(",")+1:]
                self.info += f"Default Value: {answer}\n"
            elif error_code == 0:
                if rep.count(",") >= 2:
                    answer = rep[rep.rfind(",")+1:]
                else:
                    answer = None
                if isinstance(answer, str):
                    return answer.strip()  # to remove the trailing white space if any
                else:
                    return answer
            else:
                print(f"Error code: {error_code} {self._error_str[error_code]} for command {cmd} on {self._name}")
                #raise RuntimeError(f"CommandError: {self._error_str[error_code]} for command {cmd} on {self._name}")
                return "Error"

    def write(self, cmd):
        self._cnx.open()
        with self._cnx._lock:
            self._cnx._write((cmd + "\r").encode())
            
    def read(self, timeout=3):
        self._cnx.open()
        with self._cnx._lock:
            msg = self._cnx._readline(eol="\r", timeout=timeout).decode()
            return msg
            
    def readlines(self, nb_lines=int(1e9), eol='\r', timeout=3):
        self._cnx.open()
        with self._cnx._lock:
            with gevent.Timeout(timeout, SocketTimeout):
                str_list = []
                for ii in range(nb_lines):
                    try:
                        str_list.append(self._cnx._readline(eol=eol, timeout=timeout).decode())
                    except SocketTimeout:
                        break
                return str_list

    def write_readlines(self, cmd, nb_lines=int(1e9), eol='\r', timeout=3):
        self._cnx.open()
        with self._cnx._lock:
            self._cnx._write((cmd + "\r").encode())
            with gevent.Timeout(timeout, SocketTimeout):
                str_list = []
                for ii in range(nb_lines):
                    try:
                        str_list.append(self._cnx._readline(eol=eol, timeout=timeout).decode())
                    except SocketTimeout:
                        break
                return str_list
                
    def start_app(self):
        str_list = self.write_readlines("AppStart()", timeout=5) # longer timeout required for starting app.
        # printing msg
        for ii in str_list:
            print(ii)
        # checking if frame grabber is well initialized
        err_msg = ""
        #"4,Error during initialization of grabber-module."+\
         #           " Starting application without frame grabber:"+\
          #          " Cannot initialize silicon software frame grabber" 
        if err_msg in str_list:
            print("Frame grabber not initialized. PC must be restarted. Closing app.")
            self.write_readlines(self, "AppEnd()")
            
    def shutterState(self):
        shutterStatus = self.command("MainParamGet(Shutter)")
        return shutterStatus
    
    def shutterClose(self):
        self.command("DevParamSet(Streak,Shutter,Closed)")
        
    def shutterOpen(self):
        self.command("DevParamSet(Streak,Shutter,Open)")
    
    def acquire(self):
        self.command("AcqStart(Acquire)")
    
    def acquireStatus(self):
        acqStatus = self.command("AcqStatus()")
        return acqStatus
    
    def synchroStatus(self):
        syncStatus = self.command("MainSyncGet()").split()[1]
        return syncStatus
    
    def timeRange(self):
        timeRange = self.command("MainParamGet(TimeRange)")
        return timeRange
    
    def timeRangeMax(self):
        timeRangeMax = self.command("DevParamInfoEx(TD,Time Range)")
        return timeRangeMax
    
    def operatingMode(self):
        mode = self.command("MainParamGet(Mode)")
        return mode
    
    def gateMode(self):
        gateMode = self.command("MainParamGet(GateMode)")
        return gateMode
    
    def MCPGain(self):
        MCPGain = self.command("MainParamGet(MCPGain)")
        return MCPGain
            
    def _imageSize(self):
        imageSize = self.command("MainParamGet(ImageSize)")
        return imageSize
    
    def imageSave(self, filename):
        """
        syntax: ImgSave(Destination,ImageType,FileName,Overwrite)
        """
        self.command(f"ImgSave(Current,TIFF,{filename},False)")

 #   def readwrite(self, cmd, timeout=3):
 #       self._cnx.open()
 #       with self._cnx._lock:
 #           self._cnx._write((cmd + "\r").encode())
 #           msg = ''
 #           msg += self._cnx._readline(eol="\r", timeout=timeout).decode()
 #           return msg

class StreakDelay(Streak):

    def __init__(self, config):
        
        super().__init__(config)
        
        delay_conf = config.get("delays")
        for conf in delay_conf:
            streak = conf.get("streak")
            streak.trigger = Trigger(streak, self)
            
        self.trigger = Trigger(self, self)
    
class Trigger:
    def __init__(self, streak, streak_delay):
        self._streak = streak
        self._streak_delay = streak_delay
        self._name = self._streak._name
        
        self._delay_channel = self._streak._delay_channel
        self._width_channel = chr(ord(self._streak._delay_channel)+1)
    
    def __info__(self):
        print(self._get_info())
        return ""
    
    def _get_info(self):
        dgTrigMode = self._streak_delay.command("DevParamGet(Delaybox,Trig. Mode)")
        dgSsTrigger = self._streak_delay.command("DevParamGet(Delaybox,Ss Trigger)")
        
        mystr = f"Channel {self._delay_channel}{self._width_channel}\n"
        mystr += f"\tDelay = {self.delay_picoseconds():13_d} ps\n"
        mystr += f"\tWidth = {self.width_microseconds():13.0f} us\n"
        mystr += f"\nTrigger mode: {dgTrigMode}, status: {dgSsTrigger}"
        
        return mystr
    
    def _raw_info__(self):
        """
        syntax: DevParamGet(Delaybox,Command)
        Here "Delaybox" is the DG645
        """
        dgTrigMode = self._streak_delay.command("DevParamGet(Delaybox,Trig. Mode)")
        dgRepRate = self._streak_delay.command("DevParamGet(Delaybox,Repetition Rate)")
        dgSetting = self._streak_delay.command("DevParamGet(Delaybox,Setting)")
        # delays are in seconds
        dgDelays = dict()
        channels = [chr(i) for i in range(ord('A'),ord('I'))]
        for ch in channels:
            dgDelays[ch] = int(1e12*float(self._streak_delay.command(f"DevParamGet(Delaybox,Delay {ch})")))
        dgSsTrigger = self._streak_delay.command("DevParamGet(Delaybox,Ss Trigger)")
        dgBurstMode = self._streak_delay.command("DevParamGet(Delaybox,Burst Mode)")
    
        mystr = f"\nDG645 (Connected to {self._streak_delay._name})\n\n" +\
                f"\tTrigger mode: {dgTrigMode}\n"
        
        if dgTrigMode != "Ext. rising":
            mystr += f"\tRepetition rate: {dgRepRate} Hz\n"
        
        mystr += f"\tChannels setting: {dgSetting}\n"
        for ch in channels:
            mystr += f"\n\tDelay {ch}:  {dgDelays[ch]:13_d} ps"
        
        mystr += f"\n\n" +\
                f"\tSs trigger: {dgSsTrigger}\n" +\
                f"\tBurst mode: {dgBurstMode}"
        print(mystr)
    
    def delay_picoseconds(self):
        """
        in picosecond
        """
        delay = int(1e12*float(self._streak_delay.command(f"DevParamGet(Delaybox,Delay {self._delay_channel})")))
        return delay

    def delay_nanoseconds(self):
        """
        in nanosecond
        """
        delay = 1e9*float(self._streak_delay.command(f"DevParamGet(Delaybox,Delay {self._delay_channel})"))
        return delay
    
    def width_microseconds(self):
        """
        in microsecond
        """
        delay = 1e6*float(self._streak_delay.command(f"DevParamGet(Delaybox,Delay {self._width_channel})"))
        return delay
