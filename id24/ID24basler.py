import numpy
import struct

from bliss.config.settings import ParametersWardrobe
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED,ORANGE
import bliss.common.plot as plot_module

from id24.ID24menu import menu_list, menu_number, menu_string, menu_choice

#####################################################################
#####
##### Basler Detector
#####
#####################################################################
class ID24baslerDetector:
    
    def __init__(self, name, config):
        self._name = name
        self._config = config
              
        self.lima = config.get("_par_lima", None)
        if self.lima is None:
            raise RuntimeError("No Lima device specified")
                
        # Parameters
        self._default_param = {
            "title": "Basler",
            "I_time": 0.001,
            "unit": "s"
        }
        self._param = ParametersWardrobe(f"detector_parameters_{self._name}", default_values=self._default_param)
        
    def _get_metadata(self):
        md = {
            "_h5_": {},
            "_image_": {},
            "_ascii_": {},
            "_file_": {},
        }
        md["_h5_"]["Detector name"] = self._name
        md["_h5_"]["Time units"] = self._param.unit
        md["_h5_"]["I time"] = self._param.I_time
        md["_image_"][self._name] = {
            "type": "cameras",
            "data": self._last_image
        }
        return md
                
    ##################################################################
    #
    # Acquisition Methods
    #
    
    
    def _get_detector_parameters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq):
        par_dict = self._get_acq_parameters(synchro_mode, acq_mode, inttime, nbframe, nbacq)
        det_dict = {
            "ctrl_params" : {"saving_format": "EDF"},
            "acq_params": par_dict
        }
        return det_dict

    def _get_acq_parameters(self, synchro_mode, acq_mode, inttime, nbframe, nbacq):
        if acq_mode != "I":
            raise RuntimeError(f"Hamamatsu Acq: acquisition mode {acq_mode} not implemented (I only)")
        acq_par = {
            "acq_mode": "SINGLE",
            "prepare_once": False,
            "start_once": False,
            "acq_nb_frames": 1,
            "acq_expo_time": self._param.I_time,
        }
        # acq_trigger_mode
        if synchro_mode == "software":
            acq_par["acq_trigger_mode"] = "INTERNAL_TRIGGER_MULTI"
        else:
            acq_par["acq_trigger_mode"] = "EXTERNAL_TRIGGER_MULTI"
        return acq_par

    # Data
    def _get_acq_data(self, scan, nbframe, nbacq):
        node = scan.get_data()[f"{self.lima.name}:image"]
        self._last_image = node.get_image(0)
    
    def start_live(self, inttime=0.001, rotation="NONE", flipx=False, flipy=False, trig=False, flint=True):
        proxy = self.lima.proxy
        proxy.stopAcq()
        # flint
        if flint:
            flint = plot_module.get_flint()
            flint.start_image_monitoring(self.lima.image.fullname, proxy.name(), kind="limaimage")
        # Attribute
        if trig:
            proxy.acq_trigger_mode = "EXTERNAL_TRIGGER_MULTI"
        else:
            proxy.acq_trigger_mode = "INTERNAL_TRIGGER"
        proxy.image_rotation = rotation
        proxy.image_flip = [flipx, flipy]
        proxy.acq_expo_time = inttime
        proxy.acq_nb_frames = 0
        proxy.prepareAcq()
        proxy.startAcq()
        #self.lima.start_live(inttime)
            
    def stop_live(self, flint=True):
        proxy = self.lima.proxy
        proxy.stopAcq()
        if flint:
            flint = plot_module.get_flint()
            flint.stop_image_monitoring(self.lima.image.fullname)
                
    ##################################################################
    #
    # Acquisition Parameters
    #
    def parameters(self):
        param = ["Integration Time"]
        while True:
            (rep, ind) = menu_list(f"Parameters [{self._name}]", param)
            if rep == "Integration Time":
                minmax = [0.00005, 10]
                self._param.I_time = menu_number(
                    "Integration Time (s)",
                    minmax=minmax,
                    default=self._param.I_time,
                    integer=False
                )
            if rep == "q":
                return

