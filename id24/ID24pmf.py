import gevent

from bliss import global_map
from bliss.comm.util import get_comm
from bliss.common.greenlet_utils import protect_from_kill
from bliss.common.cleanup import cleanup
from bliss.common.utils import ColorTags, BOLD, BLUE, GREEN, YELLOW, RED, ORANGE
from bliss.scanning.chain import AcquisitionMaster
from bliss.config import settings

class ID24pmf:
    def __init__(self, name, config_tree):
        self._name = name

        # Communication
        self._cnx = get_comm(config_tree, timeout=3)
        global_map.register(self, children_list=[self._cnx])

        # Synchronisation object
        self._synchro = PmfSynchro(self)
        
        # Constant
        self._trigger_name = {
            "A": "Thyristor",
            "B": "Shutter 1",
            "C": "Shutter 2"
        }
        self._simul_fire = settings.SimpleSetting(f"pmf_{self._name}_simul_fire", default_value=False)
        self._simul_charge = settings.SimpleSetting(f"pmf_{self._name}_simul_charge", default_value=False)

    def __info__(self):
        info_str = f"PMF\n\n"
        info_str += f"Name:   {self._name}\n"
        info_str += f"Host:   {self._cnx._host}\n"
        info_str += f"Socket: {self._cnx._port}\n\n"
        
        info_str += self._simulation_state()
        info_str += "\n"
            
        return info_str

    def _simulation_state(self):
        info_str = ""
        if self._simul_fire.get():
            info_str += GREEN("FIRE is NOT activated\n")
        else:
            info_str += RED("FIRE is activated\n")
        if self._simul_charge.get():
            info_str += GREEN("CHARGE is NOT activated\n")
        else:
            info_str += RED("CHARGE is activated\n")
        return info_str
        
    def simulation(self, fire=True, charge=True):
        self._simul_fire.set(fire)
        self._simul_carge.set(charge)
        print(self._simulation_state())
        
    #
    # Air ON on pmfty
    #
    def premove(self, axis_num):
        pass
        
    #
    # Air OFF on pmfty
    #
    def postmove(self, axis_num):
        pass

    ###########################################################
    #
    # Communication & Status
    #
    def command(self, cmd):
        cmd2send = f"{cmd}$"
        self._cnx.write(cmd2send.encode())
        returnstr = self._cnx.raw_read().decode()
        returnlength = int(len(returnstr)/2)
        returnstr1 = returnstr[0:returnlength]
        returnstr2 = returnstr[returnlength:2*returnlength]
        if returnstr1 == returnstr2:
            return returnstr1
        else:
            return returnstr
            
    def connect(self):
        answer = "empty"
        while answer != "Connected":
            answer = self.command("get_Connectionstatus")
            
    def status(self):
        return self.command("get_Status")
            
    def ready(self):
        return self.command("get_Status") == "Ready"
        
    def wait_ready(self):
        print("\n")
        last_answer = "xxx"
        answer = "xxx"
        while answer != "Ready":
            answer = self.status()
            if answer != last_answer:
                print(f"    STATUS: {answer}\r")
                last_answer = answer
            gevent.sleep(0.1)
        print("\n")
        
    ###########################################################
    #
    # Values
    #
    
    #
    # Voltage
    #
    #
    # maximum authorised charging voltage
    # Saved in config file on PMF Control computer
    # C:/1MJ/bin/UsrDefVal
    #
    def Vmax_get(self):
        return float(self.command("get_VCharger_max"))
        
    def V_set(self, voltage):
        if voltage < 100 or voltage > self.Vmax_get():
            raise RuntimeError(f"PMF: Voltage range is 100 to {self.Vmax_get()} V")
        ret = self.command(f"set_VCharger_set({voltage})")
        print(f"\n    {ret}\n")
    
    #
    # Polarity
    #
    def polarity_get(self):
        ret = self.command("get_Updown")
        print(f"The current polarity is {GREEN(ret)}.\n")
               
    def polarity_set(self, polarity):
        if int(polarity) not in [-1, 1]:
            raise RuntimeError("PMF: polarity musst be 1 or -1")
        
        if polarity == -1:
            pol = 0
        else:
            pol = 1
        self.command(f"set_Updown({pol})")
        
    #
    # Resistance
    #
    def Rcold(self):
        ret = self.command("get_Rcold")
        print(f"The saved value for R_cold is {ret} mOhm.\n")
        
    def Rtolerance(self):
        ret = self.command("get_Rtoll")
        print(f"The saved value for R_tolerance is {ret} mOhm.\n")
        
    def Ractual(self):
        ret = self.command("get_Ractual")
        print(f"Actual coil resistance is {ret} mOhm.\n")
        
    def Rwait(self, finalR):
        print("\n")
        actR = -9999999
        while actR < finalR:
            actR = self.get_Ractual()
            print("    Waiting for R={finalR} mOhm. Current: {actR} mOhm. \r")
            gevent.sleep(0.5)
        print("    Sufficiantly cooled coil. R= {actR} mOhm             \n\n"); 
    
    #
    # Triggers
    #
    def trigger_get(self, trigger):
        if trigger not in self._trigger_name.keys():
            raise RuntimeError(f"PMF: Trigger keys should be A/B/C")
        trigger_name = self._trigger_name[trigger]
        answer = self.command(f"get_trigger{trigger}")
        ret = split(answer, ";")
        start = ret[0]
        width = ret[1]
        print(f"\n    Trigger {trigger_name} start [{start} ms] - width [{width} ms]\n")
        
    def trigger_send(self):
        answer = self.command(f"do_Manual_trigger")
    
    #
    # V Charger
    #
    def Vcharger_get(self):
        answer = self.command("get_VCharger_actual")
        return answer
         
    def dump(self):
        self.command("do_Dump")
        
    def _Vcharger_dump(self):
        if not self._normal_end:
            self.dump()

    def _Vcharger_wait(self, askedV):
        actV = self.Vcharger_get()
        status = self.status()
        #while abs(float(actV) - float(askedV)) >= 100:
        while status != "Charged":
            actV = self.Vcharger_get()
            status = self.status()
            print(f"    Charging: {actV} V - Status: {status}    ", end="\r")
            gevent.sleep(0.5)
        gevent.sleep(1)
        status = self.status()
        actV = self.Vcharger_get()
        print(f"\n    Status: {status} - {actV} volts") 
        
    def _Vcharger_charge(self, Vcharge):
        gevent.sleep(5)
        answer = self.command(f"do_Charge({Vcharge})")

    #
    # Synchro
    #        

    # Check charge is valid
    def _synchro_check_charge(self, askedV):
        vmax = self.Vmax_get()
        if askedV < 200 or askedV > vmax:
            raise RuntimeError(f"PMF: Voltage must be in range 200 to {vmax} V")
        self._cnx.flush()
        
    # Synchro prepare
    def _synchro_prepare(self):
        # Wait PMF reay
        self.wait_ready()
        
    # Synchro start
    def _synchro_start(self, askedV):
        # Shoot
        print("\n    Send request to charge to PMF controller\n")
        self._normal_end = False
        with cleanup(self._Vcharger_dump):
            if self._simul_charge.get():
                print("CHARGE in simulation mode {askedV}V")
            else:
                self._Vcharger_charge(askedV)
            
                actV = self.Vcharger_get()
                status = self.status()
                while status != "Charged":
                    actV = self.Vcharger_get()
                    status = self.status()
                    if status == "Error":
                        self.wait_ready()
                        gevent.sleep(1)
                        self._Vcharger_charge(askedV)
                    else:
                        print(f"    Charging: {actV} V - Status: {status}    ", end="\r")
                    gevent.sleep(0.5)
                gevent.sleep(1)
                status = self.status()
                actV = self.Vcharger_get()
                print(f"\n    Status: {status} - {actV} volts") 
            
            if self._simul_charge.get():
                print("CHARGE in simulation mode (NO FIRE)")
            else:
                if self._simul_fire.get():
                    print("FIRE in simulation mode (NO FIRE)")
                else:
                    #self._Vcharger_wait(askedV)
                    vcharger = self.Vcharger_get()
                    mystr = BOLD(RED(f"FIRE at {vcharger} V"))
                    print(f"    {mystr}")
                    res = self.command("do_Fire")
                self._normal_end = True

    #
    # Sequence
    #            
    def pulse_charge_test(self, askedV, shutter=None):
        # Checks
        vmax = self.Vmax_get()
        if askedV < 200 or askedV > vmax:
            raise RuntimeError(f"PMF: Voltage must be in range 200 to {vmax} V")
        self._cnx.flush()
        
        # Wait PMF reay
        self.wait_ready()
            
        # Shutter if necessary
        if shutter is not None:
            shutter.open()
        
        # Shoot
        print("\n    Send request to charge to PMF controller\n")
        self._normal_end = False            
        with cleanup(self._Vcharger_dump):
            # CHARGE
            if self._simul_charge.get():
                print("CHARGE in simulation mode {askedV}V")
            else:
                self._Vcharger_charge(askedV)
                self._Vcharger_wait(askedV)

            #FIRE
            if self._simul_charge.get():
                print("CHARGE in simulation mode (NO FIRE)")
            else:
                if self._simul_fire.get():
                    print("FIRE in simulation mode (NO FIRE)")
                else:
                    vcharger = self.Vcharger_get()
                    mystr = BOLD(RED(f"FIRE at {vcharger} V"))
                    print(f"    {mystr}")
                    res = self.command("do_Fire")
                self._normal_end = True
            print("\n")

class PmfSynchro:
    def __init__(self, controller):
        self._controller = controller
        
        self._available_mode = "pmf_xmcd/pmf"
         
    def get_acquisition_master(self, synchro_mode, use_shutter, use_opiom):
        if synchro_mode not in self._available_mode.split("/"):
            raise RuntimeError(f"Synchronization mode {synchro_mode} not in [{self._available_mode}]")
        self._controller._synchro_check_charge(self._charge)
        return PmfSynchroAcqMaster(synchro_mode, self)
               
    def prepare_synchro(self, synchro_mode):
        if synchro_mode not in self._available_mode.split("/"):
            raise RuntimeError(f"Synchronization mode {synchro_mode} not in [{self._available_mode}]")
        self._controller._synchro_prepare()
        
    def start_synchro(self, synchro_mode):
        self._controller._synchro_start(self._charge)
        
    def set_polarization(self, polarization):
        if polarization == "+":
            self._controller.polarity_set(1)
        if polarization == "-":
            self._controller.polarity_set(-1)
        
    def set_charge(self, charge):
        self._charge = charge                        
 
class PmfSynchroAcqMaster(AcquisitionMaster):
    def __init__(self, synchro_mode, synchro_ctrl):
        AcquisitionMaster.__init__(self, None, name="PmfSynchroAcqMaster")
        self._synchro_mode = synchro_mode
        self._synchro_ctrl = synchro_ctrl
        
    def prepare(self):
        self._synchro_ctrl.prepare_synchro(self._synchro_mode)
            
    def start(self):
        if self.parent:
            return
        self.trigger()

    def trigger(self):
        self.trigger_slaves()
        self._synchro_ctrl.start_synchro(self._synchro_mode)

    def trigger_ready(self):
        return True

    def wait_ready(self):
        pass

    def stop(self):
        pass
            
            
            
