
import os
import tabulate
from ruamel.yaml import YAML
import gevent

from bliss import setup_globals
from bliss.config import settings
from bliss.common.utils import ColorTags, BOLD, GREEN, YELLOW, BLUE, RED
from bliss.scanning.chain import AcquisitionMaster
from bliss.common.motor_group import Group

class DCMfjsCorrected:
    
    def __init__(self, name, config):
        
        self._name = name
        self._config = config
        
        self._mono = config.get("mono")
        
        self._fjsur = config.get("fjsur")
        self._fjsuh = config.get("fjsuh")
        self._fjsd = config.get("fjsd")
        
        self._fjsrx = config.get("fjsrx")
        self._fjsry = config.get("fjsry")
        self._fjsz = config.get("fjsd")
        
        self._pos = {
            "rx": self._fjsrx.position,
            "ry": self._fjsry.position,
            "z": self._fjsz.position
        }
        
        self._fjs_name = ("fjsur", "fjsuh", "fjsd")

        self._g = Group(self._fjsur,self._fjsuh,self._fjsd)
        
    def get_rx(self):
        return self._pos["rx"]
        
    def set_rx(self, new_rx):
        self._pos["rx"] = new_rx
        self._pos["ry"] = self._fjsry.position
        self._pos["z"] = self._fjsz.position
                
        (self._pos["fjsur"], self._pos["fjsuh"], self._pos["fjsd"]) = self._mono.xyz2fjs(self._pos["rx"], self._pos["ry"], self._pos["z"])
        
        for name in self._fjs_name:
             calib = self._mono.calib.fjs._fjs_calib[name]
             self._pos[name] = calib.get_y(self._pos[name])

        self._g.move(self._fjsur, self._pos["fjsur"], self._fjsuh, self._pos["fjsuh"], self._fjsd, self._pos["fjsd"])
        
    def get_ry(self):
        return self._pos["ry"]
        
    def set_ry(self, new_ry):
        self._pos["rx"] = self._fjsrx.position
        self._pos["ry"] = new_ry
        self._pos["z"] = self._fjsz.position
                
        (self._pos["fjsur"], self._pos["fjsuh"], self._pos["fjsd"]) = self._mono.xyz2fjs(self._pos["rx"], self._pos["ry"], self._pos["z"])
        
        for name in self._fjs_name:
             calib = self._mono.calib.fjs._fjs_calib[name]
             self._pos[name] = calib.get_y(self._pos[name])

        self._g.move(self._fjsur, self._pos["fjsur"], self._fjsuh, self._pos["fjsuh"], self._fjsd, self._pos["fjsd"])

    def stop(self):
        self._g.stop()
        
        
