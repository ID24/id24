
import numpy
import click
import tabulate
import datetime
import time

from bliss import setup_globals
from bliss.config import settings
from bliss.scanning.group import Sequence
from bliss.scanning.chain import AcquisitionChannel, AcquisitionChain
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.scan import Scan, ScanState, DataWatchCallback
from bliss.controllers.lima.lima_base import Lima
from bliss.common import plot
from bliss.common.utils import ColorTags, BOLD, BLUE, GREEN, YELLOW, RED
from bliss.common.cleanup import cleanup
from bliss.common.axis import Motion
from bliss.shell.standard import umv, umvr
from bliss.scanning.scan_info import ScanInfo


class ID24sequence:
   
    def __init__(self, name, config):

        # intern
        self._name = name
        self._config = config

        # simulation
        self._simulation = config.get("_par_simulation")

        # synchronization
        self._synchro = {}
        
        # Xh
        try:
            self._xh = config.get("_par_xh", None)
            if self._xh is None:
                print(RED("    NO XH"))
        except:
            print(RED("    NO XH"))
            self._xh = None
        
        # Hamamatsu
        try:
            self._hamamatsu = config.get("_par_hamamatsu", None)
            if self._hamamatsu is None:
                print(RED("    NO HAMAMATSU"))
        except:
            print(RED("    NO HAMAMATSU"))
            self._hamamatsu = None
            
        # city
        try:
            self._city = config.get("_par_city", None)
            if self._city is not None:
                self._synchro["city"] = self._city._synchro
            else:
                self._synchro["city"] = None
                print(RED("    NO CITY"))
        except:
            print(RED("    NO CITY"))
            self._city = None
            self._synchro["city"] = None
        
        # baslers
        baslers_config = config.get("_par_baslers", None)
        self._baslers = []
        if baslers_config is not None:
            for basler_config in baslers_config:
                self._baslers.append(basler_config["basler"])
            
        # pmf
        try:
            self._pmf = config.get("_par_pmf", None)
            if self._pmf is not None:
                self._synchro["pmf"] = self._pmf._synchro
            else:
                print(RED("    NO PMF"))
                self._synchro["pmf"] = None
        except:
            print(RED("    NO PMF"))
            self._pmf = None
            self._synchro["pmf"] = None
            
        # Opiom
        try:
            self._opiom = config.get("_par_opiom", None)
            if self._opiom is not None:
                self._synchro["opiom"] = self._opiom._synchro
            else:
                print(RED("    NO ED OPIOM"))
                self._synchro["opiom"] = None
        except:
            print(RED("    NO ED OPIOM"))
            self._opiom = None
            self._synchro["opiom"] = None
            

        # srcurr        
        self._srcurr = config.get("_par_srcurr", None)
        self._max_srcurr = config.get("_par_max_srcurr", None)

        # shutterm (EDSHUT - Xh protect)
        self._shutter = config.get("_par_shutter", None)

        # fast shutter (hplf_fs - target protect)
        self._target_shutter = config.get("_par_target_shutter", None)
        self._use_target_shutter = settings.SimpleSetting(f"{self._name}_target_shutter_use", default_value=True)
        if self._target_shutter is None:
            self._use_target_shutter.set(False)
            
    def __info__(self):
        param = "None"
        if self._shutter is not None:
            param = self._shutter.name            
        mystr = f"Shutter : {param}\n\n"
        mystr = f"Scans         : spectrum / \n\n"
        return mystr
    
    ###############################################################
    #####
    ##### Shutters
    #####
    @property
    def use_target_shutter(self):
        return self._use_target_shutter.get()
        
    @use_target_shutter.setter
    def use_target_shutter(self, state):
        if self._target_shutter is not None:
            self._use_target_shutter.set(state)
            self._target_shutter.manual()
            if not state:
                self._target_shutter.open()
        
    ###############################################################
    #####
    ##### User's Sequences
    #####
    #
    # Spectrum
    #
    def spectrum(self, inttime, nbspectrum, nbframe, nbacq, detector, I0=None, sample=None, shutter_mode="xh_secure"):
        acq_params = {
            "type": "seq_spectrum",
            "title": "spectrum",
            "seq_title": f"Spectrum {inttime} {nbspectrum} {nbframe} {nbacq}",
            "inttime": inttime,
            "nbspectrum": nbspectrum,
            "nbframe": nbframe,
            "nbacq": nbacq,
            "I0_mode": detector._param.I0_mode,
        }
        if nbspectrum > 1:
            acq_params["average_mu"] = True,
        self._sequence(acq_params, detector=detector, I0=I0, sample=sample, shutter_mode=shutter_mode)
    
    #
    # Xh scans
    #
    def xh_take(self, inttime, nbframe, nbacq):
        acq_params = {
            "type": "seq_xh_take",
            "title": "xh_take",
            "seq_title": f"xh_take {inttime} {nbframe} {nbacq}",
            "inttime": inttime,
            "nbspectrum": 1,
            "nbframe": nbframe,
            "nbacq": nbacq,
            "I0_mode": "no_I0",
            "no_I0": True,
            "no_mu": True,
            "no_bck": True
        }
        self._sequence(acq_params, detector=setup_globals.xh)
        
    def xh_dscan(
        self,
        axis, rel_start, rel_stop, interval,
        inttime, nbacq,
    ):
            
        title = "xh_dscan"
        seq_title = "xh_dscan "
        seq_title += f"{axis.name} {rel_start} {rel_stop} {interval} "
        seq_title += f"{inttime} {nbacq}"
        
        start = axis.position + rel_start
        stop = axis.position + rel_stop
        
        self._xh_scan(
            title, seq_title,
            axis, start, stop, interval,
            inttime, nbacq,
        )
        
    def xh_ascan(
        self,
        axis, start, stop, interval,
        inttime, nbacq,
    ):
        title = "xh_ascan"
        seq_title = "xh_ascan "
        seq_title += f"{axis.name} {start} {stop} {interval} "
        seq_title += f"{inttime} {nbacq}"
        
        self._xh_scan(
            title, seq_title,
            axis, start, stop, interval,
            inttime, nbacq,
        )
        
    def _xh_scan(
        self,
        title, seq_title,
        axis, start, stop, interval,
        inttime, nbacq,
    ):        
        acq_params = {
            "type": f"seq_{title}",
            "title": title,
            "seq_title": seq_title,
            "inttime": inttime,
            "nbspectrum": 1,
            "nbframe": 1,
            "nbacq": nbacq,
            "I0_mode": "no_I0",
            "no_mu": True,
            "no_bck": True,
            "no_I0": True,
        }
        
        current_pos = axis.position
        delta = (stop - start) / interval   
        axis_params = {
            "line_axis": None,
            "line_start": 0,
            "line_delta": 0,
            "line_interval": 0,
            "line_current_pos": 0,
            "col_axis": axis,
            "col_start": start,
            "col_delta": delta,
            "col_interval": interval,
            "col_current_pos": current_pos,
        }
        
        self._sequence(
            acq_params,
            detector=setup_globals.xh,
            axis_params=axis_params,
            shutter_mode="always_open"
        )
                
    def xh_scan_orbit(self, start, num, bunch):
        acq_params = {
            "type": "seq_xh_scan_orbit",
            "title": "xh_scan_orbit",
            "seq_title": f"xh_scan_orbit {start} {num} {bunch}",
            "inttime": bunch,
            "nbspectrum": num,
            "nbframe": 1,
            "nbacq": 1,
            "I0_mode": "no_I0",
            "no_mu": True,
            "no_I0": True,
            "scan_orbit_start": start,
        }
        synchro_params = {
            "device": None,
            "mode": "scan_orbit"
        }
        self._sequence(
            acq_params,
            synchro_params=synchro_params,
            detector=setup_globals.xh,
            shutter_mode="always_open",
        )
                
    #
    # Mapping
    #
    def dmap(
        self,
        z_axis, z_rel_start, z_delta, z_interval,
        y_axis, y_rel_start, y_delta, y_interval,
        inttime, nbacq,
        detector,
        I0=None, sample=None,
        shutter_mode="always_open",
    ):
        
        if sample is not None:
            print(RED("Move to Sample Position"))
            sample.move()
            
        title = "dmap"
        seq_title = "dmap "
        seq_title += f"{z_axis.name} {z_rel_start} {z_delta} {z_interval} "
        seq_title += f"{y_axis.name} {y_rel_start} {y_delta} {y_interval} "
        seq_title += f"{inttime} {nbacq}"
        z_start = z_axis.position + z_rel_start
        y_start = y_axis.position + y_rel_start
        
        self._map(
            title, seq_title,
            z_axis, z_start, z_delta, z_interval,
            y_axis, y_start, y_delta, y_interval,
            inttime, nbacq, detector, I0=I0, sample=sample,
            shutter_mode=shutter_mode,
        )
        
    def amap(
        self,
        z_axis, z_start, z_delta, z_interval,
        y_axis, y_start, y_delta, y_interval,
        inttime, nbacq,
        detector,
        I0=None,
        shutter_mode="always_open",
    ):
        title = "amap"
        seq_title = "amap "
        seq_title += f"{z_axis.name} {z_start} {z_delta} {z_interval} "
        seq_title += f"{y_axis.name} {y_start} {y_delta} {y_interval} "
        seq_title += f"{inttime} {nbacq}"
        
        self._map(
            title, seq_title,
            z_axis, z_start, z_delta, z_interval,
            y_axis, y_start, y_delta, y_interval,
            inttime, nbacq, detector, I0=None, sample=None,
            shutter_mode=shutter_mode,
        )
        
    def _map(
        self,
        title, seq_title,
        line_axis, line_start, line_delta, line_interval,
        col_axis, col_start, col_delta, col_interval,
        inttime, nbacq,
        detector,
        I0=None, sample=None,
        shutter_mode="always_open",
    ):        
        acq_params = {
            "type": f"seq_{title}",
            "title": title,
            "seq_title": seq_title,
            "inttime": inttime,
            "nbspectrum": 1,
            "nbframe": 1,
            "nbacq": nbacq,
            "I0_mode": detector._param.I0_mode,
        }
        
        line_current_pos = line_axis.position
        col_current_pos = col_axis.position        
        axis_params = {
            "line_axis": line_axis,
            "line_start": line_start,
            "line_delta": line_delta,
            "line_interval": line_interval,
            "line_current_pos": line_current_pos,
            "col_axis": col_axis,
            "col_start": col_start,
            "col_delta": col_delta,
            "col_interval": col_interval,
            "col_current_pos": col_current_pos,
        }
        
        self._sequence(acq_params, detector=detector, axis_params=axis_params, I0=I0, sample=sample, shutter_mode=shutter_mode)
    
    #
    # PMF (Pulsed Magnetic Field)
    #
    def pmf_abs(self, pmf_charge, inttime, nbshot, nbframe, detector, I0=None, sample=None):
        acq_params = {
            "type": "seq_pmf_abs",
            "title": "pmf_abs",
            "seq_title": f"PMF_abs {inttime} {nbshot} {nbframe}",
            "inttime": inttime,
            "nbspectrum": nbshot,
            "nbframe": nbframe,
            "nbacq": 1,
            "I0_mode": detector._param.I0_mode,
            "pmf_charge": pmf_charge,
            "average_mu": True,
        }
        synchro_params = {
            "device": self._synchro["pmf"],
            "mode": "pmf"
        }
        self._sequence(acq_params, detector=detector, synchro_params=synchro_params, I0=I0, sample=sample)
        
    def pmf_xmcd(self, pmf_charge, inttime, nbpair, nbaverage, nbframe, detector, I0=None, sample=None):
        if detector._param.I0_mode == "after":
            raise RuntimeError("pmf_xmcd: I0 mode \"after\" is not compatible with XMCD sequence")
        acq_params = {
            "type": "seq_pmf_xmcd",
            "title": "pmf_xmcd",
            "seq_title": f"PMF_xmcd {inttime} {nbpair} {nbaverage} {nbframe}",
            "inttime": inttime,
            "nbpair": nbpair,
            "nbaverage": nbaverage, 
            "nbspectrum": 2*nbpair*nbaverage,
            "nbframe": nbframe,
            "nbacq": 1,
            "I0_mode": detector._param.I0_mode,
            "pmf_charge": pmf_charge,
            "xmcd": True
        }
        synchro_params = {
            "device": self._synchro["pmf"],
            "mode": "pmf"
        }
        self._sequence(acq_params, detector=detector, synchro_params=synchro_params, I0=I0, sample=sample)
        
    #
    # Chemistry
    #
    def chemistry(self, inttime, nbframe, nbacq, detector, trigger="intern", I0=None, sample=None):
        acq_params = {
            "type": f"seq_chemistry",
            "title": "chemistry",
            "seq_title": f"Chemistry {inttime} {nbframe} {nbacq} {trigger}",
            "inttime": inttime,
            "nbspectrum": 1,
            "nbframe": nbframe,
            "nbacq": nbacq,
            "I0_mode": detector._param.I0_mode,
        }
        synchro_params = {
            "device": self._synchro["opiom"],
            "mode": trigger
        }
        self._sequence(acq_params, detector=detector, synchro_params=synchro_params, I0=I0, sample=sample)
        
    #
    # Laser
    #
    def xh_diode(self, inttime, nbframe, nbacq, I0=None, sample=None):
        detector = self._xh
        acq_params = {
            "type": "seq_xh_diode",
            "title": "xh_diode",
            #"seq_title": f"XH_diode {inttime} {nbframe} {nbacq}",
            "seq_title": f"XH_diode {inttime} {nbframe}",
            "inttime": inttime,
            "nbspectrum": 1,
            "nbframe": nbframe,
            "nbacq": nbacq,
            "I0_mode": detector._param.I0_mode,
        }
        synchro_params = {
            "device": self._synchro["city"],
            "mode": "xh_diode"
        }
        self._sequence(acq_params, detector=detector, synchro_params=synchro_params, I0=I0, sample=sample)
    
    def laser(self, basler=False):
        acq_params = {
            "type": "seq_laser",
            "title": "laser",
            "seq_title": "Laser",
            "nbspectrum": 1,
            "inttime": 1,
            "nbframe": 1,
            "nbacq": 1,
            "I0_mode": "no_I0",
            "no_mu": True,
            "no_I0": True,
       }
        synchro_params = {
            "device": self._synchro["city"],
            "mode": "laser"
        }
        self._sequence(
            acq_params,
            synchro_params=synchro_params,
            basler=basler,
            shutter_mode="no_shutter",
        )
        
    def streak_ref(self):        
        acq_params = {
            "type": "seq_streak_ref",
            "title": "Streak References",
            "seq_title": "Streak References",
            "nbspectrum": 1,
            "nbframe": 1,
            "nbacq": 1,
            "inttime": 1,
            "I0_mode": "no_I0",
            "no_mu": True,
            "no_I0": True,
        }
        synchro_params = {
            "device": self._synchro["city"],
            "mode": "streak_ref"
        }

        self._sequence(
            acq_params,
            synchro_params=synchro_params,
            shutter_mode="no_shutter",
        )
       
    def laser_shock(self, inttime, nbframe, I0_pos, sample_pos, basler=True):
        detector = self._xh
        acq_params = {
            "type": "seq_laser_shock",
            "title": "laser shock",
            "seq_title": f"Laser Shock {inttime} {nbframe}",
            "inttime": inttime,
            "nbspectrum": 1,
            "nbframe": nbframe,
            "nbacq": 1,
            "I0_mode": detector._param.I0_mode,
            "basler": True,
        }
        synchro_params = {
            "device": self._synchro["city"],
            "mode": "shock"
        }
        self._sequence(
            acq_params,
            detector=detector,
            synchro_params=synchro_params,
            I0=I0_pos,
            sample=sample_pos,
            basler=basler
        )

    ###############################################################
    #####
    ##### SEQUENCE Tools
    #####
        
    def _machine_get_meta_data(self):
        metadata = {}
        try:
            metadata = {"max_srcur": self._max_machine_current}
            metadata["srcur"] = setup_globals.srcur.raw_read
            metadata["sbcur"] = setup_globals.sbcur.raw_read
        except:
            pass
        
        return metadata

    def _acquisition_channel_plot(self, plot_list):
        for channel in plot_list:
            # Plot Results
            print(f"Plot {channel} in Flint .... ", end="")
            f = plot.get_flint()
            p = f.get_plot(plot_class="curvestack", name=channel, unique_name=channel)
            p.set_data(curves=self._acq_channels[channel], x=self._acq_channels["Energy"][0])
            p.xlabel="Energy (eV)"
            p.ylabel=channel
            p.title = f"{self._seq.title} (Scans #{self._seq.scan.scan_number})"
    
    def _acquisition_channel_select(self):
        
        self._channels_0d = []
        self._channels_1d = []
        self._channels_y_1d = []
        
        if self._detector is not None:
            
            # Channels 0d
            self._channels_0d = ["I0_curr", "I_curr"]
            if "no_curr" in self._acq_params.keys() or "no_bck" in self._acq_params.keys():
                self._channels_0d = []
            else:
                self._channels_0d = ["I0_curr", "I_curr"]
                if "no_I0" in self._acq_params.keys():
                    self._channels_0d.pop(self._channels_0d.index("I0_curr"))
                    
            # Channels 1d
            self._channels_1d = [
                "Energy",
                "I0_bck", "I0_raw", "I0", "I0_norm_curr",
                "I_bck", "I_raw", "I", "I_norm_curr",         
                "Mu", "Mu_norm_curr", "Mu_aver", "Mu_aver_curr",
                "Xmcd", "Xmcd_interm", "Xmcd_norm_curr", "Xmcd_interm_norm_curr",
            ]
                    
            # Channels Detector ROI's
            self._channels_roi = [
                "roi_I1", "roi_I2", "roi_I3", "roi_I4", "roi_Isum",
            ]
            
            if "xmcd" not in self._acq_params.keys():
                self._channels_1d.pop(self._channels_1d.index("Xmcd"))
                self._channels_1d.pop(self._channels_1d.index("Xmcd_interm"))
                self._channels_1d.pop(self._channels_1d.index("Xmcd_norm_curr"))
                self._channels_1d.pop(self._channels_1d.index("Xmcd_interm_norm_curr"))
                
            if "no_mu" in self._acq_params.keys():
                self._channels_1d.pop(self._channels_1d.index("Mu"))
                self._channels_1d.pop(self._channels_1d.index("Mu_aver"))
                self._channels_1d.pop(self._channels_1d.index("Mu_norm_curr"))
                self._channels_1d.pop(self._channels_1d.index("Mu_aver_curr"))
                
            if "no_curr" in self._acq_params.keys():
                self._channels_1d.pop(self._channels_1d.index("I0_norm_curr"))
                self._channels_1d.pop(self._channels_1d.index("I_norm_curr"))
                try:
                    self._channels_1d.pop(self._channels_1d.index("Mu_norm_curr"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Mu_aver_curr"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Xmcd_norm_curr"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Xmcd_interm_norm_curr"))
                except:
                    pass

            if "no_I0" in self._acq_params.keys():
                self._channels_1d.pop(self._channels_1d.index("I0_bck"))
                self._channels_1d.pop(self._channels_1d.index("I0_raw"))
                self._channels_1d.pop(self._channels_1d.index("I0"))
                try:
                    self._channels_1d.pop(self._channels_1d.index("I0_norm_curr"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Mu"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Mu_norm_curr"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Mu_aver_curr"))
                except:
                    pass
                    
            if "no_bck" in self._acq_params.keys():
                try:
                    self._channels_1d.pop(self._channels_1d.index("I0_bck"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("I0"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("I0_norm_curr"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("I_bck"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("I"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("I_norm_curr"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Mu"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Mu_norm_curr"))
                except:
                    pass
                try:
                    self._channels_1d.pop(self._channels_1d.index("Mu_aver_curr"))
                except:
                    pass        
                try:
                    self._channels_1d.pop(self._channels_1d.index("Xmcd"))
                except:
                    pass        
                try:
                    self._channels_1d.pop(self._channels_1d.index("Xmcd_interm"))
                except:
                    pass        
                try:
                    self._channels_1d.pop(self._channels_1d.index("Xmcd_norm_curr"))
                except:
                    pass        
                try:
                    self._channels_1d.pop(self._channels_1d.index("Xmcd_interm_norm_curr"))
                except:
                    pass
            self._channels_y_1d = self._channels_1d.copy()
            self._channels_y_1d.pop(self._channels_y_1d.index("Energy"))
            self._channels_y_roi = self._channels_roi.copy()   
               
    def _acquisition_channel_create_detector(self):
        if self._detector is not None:
            for channel_name in self._channels_0d:
                self._acq_channels[channel_name] = None
                self._seq.add_custom_channel(AcquisitionChannel(channel_name,numpy.float,()))
            for channel_name in self._channels_1d:
                self._acq_channels[channel_name] = None
                self._seq.add_custom_channel(AcquisitionChannel(channel_name,numpy.float,(self._detector._param.nb_pixel,)))
            for channel_name in self._channels_roi:
                self._acq_channels[channel_name] = None
                self._seq.add_custom_channel(AcquisitionChannel(channel_name,numpy.float,()))
    
    def _acquisition_channel_create_axis(self):
        if self._axis_params is not None:
            if self._axis_params["line_axis"] is not None:
                line_axis_name = self._axis_params["line_axis"].name
                self._acq_channels[line_axis_name] = None
                self._seq.add_custom_channel(AcquisitionChannel(line_axis_name,numpy.float,()))
            if self._axis_params["col_axis"] is not None:
                col_axis_name = self._axis_params["col_axis"].name
                self._acq_channels[col_axis_name] = None
                self._seq.add_custom_channel(AcquisitionChannel(col_axis_name,numpy.float,()))
    
    def _acquisition_channel_create_plot(self):
        self._seq.add_custom_channel(AcquisitionChannel("Frames_y",numpy.float,()))
        self._seq.add_custom_channel(AcquisitionChannel("Energy_x",numpy.float,()))
        self._seq.add_custom_channel(AcquisitionChannel("Detector_data",numpy.float,()))
                   
    def _acquisition_channel_emit(self, name="all"):
        if name == "all":
            for channel_name, data in self._acq_channels.items():
                self._seq.custom_channels[channel_name].emit(data)
        else:
            if name in self._acq_channels.keys():
                self._seq.custom_channels[name].emit(self._acq_channels[name])
        
    def _create_scan_sequence(self):
        
        # Select sequence channels
        self._acquisition_channel_select()
        
        #
        # SCAN INFO
        machine_param = self._machine_get_meta_data()
        instrument = {"machine": machine_param}
        if self._detector is not None:
            nbpixel = self._detector._param.nb_pixel
            det_param = self._detector._get_meta_data()
            try:
                det_ctrl_param = self._detector.controller._get_metadata()
                det_param.update(det_ctrl_param)
            except:
                pass
            instrument[f"{self._detector._name}_param"] = det_param
        title = self._acq_params["title"]
        scan_info = ScanInfo()
        scan_info.update({
            "title": self._acq_params["seq_title"],
            "type": self._acq_params["type"],
            "instrument": instrument,
            "dim": 1,
        })
        scan_info.add_curve_plot()
        scan_info.set_channel_meta("scans", group="sequence")
        scan_info.set_channel_meta("scan_numbers", group="sequence")
                    
        if "no_curr" not in self._acq_params.keys():
            scan_info.set_channel_meta("I_curr", group="g2")
        
        # Roi's
        scan_info.set_channel_meta("roi_I1", group="roi")
        scan_info.set_channel_meta("roi_I2", group="roi")
        scan_info.set_channel_meta("roi_I3", group="roi")
        scan_info.set_channel_meta("roi_I4", group="roi")
        scan_info.set_channel_meta("roi_Isum", group="roi")
        
        # 1d Plots
        scan_info.add_1d_plot(name="Seq Detector", x="Energy",y=self._channels_y_1d)
        #scan_info.add_1d_plot(name="Seq Roi's", y=self._channels_y_roi)
        
        if self._detector is not None:
            scan_info.set_channel_meta(
                "Energy_x",
                group="scatter",
                axis_id=0,
                axis_kind="forth",
                axis_points = self._detector._param.nb_pixel,
            )
            scan_info.set_channel_meta(
                "Frames_y",
                group="scatter",
                axis_id=1,
                axis_kind="forth",
                axis_points=self._acq_params["nbframe"],
                start=0,
                stop=self._acq_params["nbframe"]-1,
            )
            scan_info.set_channel_meta("Detector_data", group="scatter")
            scan_info.add_scatter_plot(name="Detector Image", x="Energy_x", y="Frames_y", value="Detector_data")

        """
        # Specify the same group for all this channels (axis or values)
        scan_info.set_channel_meta("axis1",
                                   # The group have to be the same for all this channels
                                   group="foo",
                                   # This is the fast axis
                                   axis_id=0,
                                   # In forth direction only
                                   axis_kind="forth",
                                   # The grid have to be specified
                                   start=0, stop=9, axis_points=10,
                                   # Optionally the full number of points can be specified
                                   points=100)
        scan_info.set_channel_meta("axis2", group="foo", axis_id=1,
                                   axis_kind="forth",
                                   start=0, stop=9,
                                   axis_points=10, points=100)

        # Request a specific scatter to be displayed
        scan_info.add_scatter_plot(x="axis1", y="axis2", value="diode1")
        """
        #
        # SEQUENCE
        #
        self._seq = Sequence(scan_info=scan_info, title=title)
        
        #
        # CHANNELS
        #
        self._acq_channels = {}
        self._seq.custom_channels = {}
        self._acquisition_channel_create_detector()
        self._acquisition_channel_create_axis()
        self._acquisition_channel_create_plot()
                        
    def _sequence_end(self):
        # Close shutter
        if self._shutter_mode != "no_shutter":
            if self._shutter is not None:
                self._shutter.close()
            if self._shutter_mode == "always_open":
                self.use_target_shutter = self._target_shutter_last_state

            
        if self._axis_params is not None:
            
            if self._axis_params["return"]:
                mv_list = []
                if self._axis_params["line_axis"] is not None:
                    mv_list.append(self._axis_params["line_axis"])
                    mv_list.append(self._axis_params["line_current_pos"])
                if self._axis_params["col_axis"] is not None:
                    mv_list.append(self._axis_params["col_axis"])
                    mv_list.append(self._axis_params["col_current_pos"])
                if len(mv_list) >= 2:
                    umv(*mv_list)
                
            # Manage Hook of mapping motors
            if self._axis_params["line_axis"] is not None:
                for hook in self._axis_params["line_axis"].motion_hooks:
                    hook.post_scan([self._axis_params["line_axis"],])
            if self._axis_params["col_axis"] is not None:
                for hook in self._axis_params["col_axis"].motion_hooks:
                    hook.post_scan([self._axis_params["col_axis"],])

    # 
    # ACQUISITION MODE
    #
    def _take_I_bck(self):        
        # close shutter
        if self._shutter_mode != "no_shutter":
            if self._shutter is not None:
                self._shutter.close()
        # acquisition
        (scan, self._acq_channels["I_bck"], data_roi) = self._acquisition(
            self._acq_params["title"],
            self._acq_params["inttime"],
            self._acq_params["nbframe"],
            self._acq_params["nbacq"],
            detector=self._detector,
            acq_mode="I_bck",
        )
        self._acquisition_channel_emit(name="I_bck")
        return scan
        
    def _take_I0_bck(self):
        (scan, self._acq_channels["I0_bck"], data_roi) = self._acquisition(
            self._acq_params["title"],
            self._acq_params["inttime"],
            self._acq_params["nbframe"],
            self._acq_params["nbacq"],
            detector=self._detector,
            acq_mode="I0_bck",
        )
        self._acquisition_channel_emit(name="I0_bck")
        return scan
        
    def _take_I0(self, I0):
        if I0 is not None:
            print(RED("Move to I0 Position"))
            I0.move()
                        
        # open shutter
        if self._shutter_mode == "xh_secure":
            if self._shutter is not None:
                self._shutter.open()
            if self._target_shutter is not None:
                if self.use_target_shutter:
                    self._target_shutter.manual()
                    self._target_shutter.open()

        # Acquire
        (scan, self._acq_channels["I0_raw"], data_roi) = self._acquisition(
            self._acq_params["title"],
            self._acq_params["inttime"],
            self._acq_params["nbframe"],
            self._acq_params["nbacq"],
            detector=self._detector,
            acq_mode="I0",
        )
                        
        # close shutter
        if self._shutter_mode == "xh_secure":
            if self._shutter is not None:
                self._shutter.close()
            if self._target_shutter is not None:
                if self.use_target_shutter:
                    self._target_shutter.close()
            
        self._acquisition_channel_emit(name="I0_raw")
        if "no_bck" not in self._acq_params.keys():
            if self._simulation:
                self._acq_channels["I0"] = self._acq_channels["I0_raw"]
            else:
                self._acq_channels["I0"] = self._acq_channels["I0_raw"]- self._acq_channels["I0_bck"]
            self._acquisition_channel_emit(name="I0")
            if "no_curr" not in self._acq_params.keys():
                self._acq_channels["I0_curr"] = self._srcurr.raw_read
                self._acq_channels["I0_norm_curr"] =  self._acq_channels["I0"] * self._max_machine_current / self._acq_channels["I0_curr"]
                self._acquisition_channel_emit(name="I0_curr")
                self._acquisition_channel_emit(name="I0_norm_curr")
        return scan
        
    def _take_I(self):

        if self._synchro_params is not None:
            synchro_ctrl = self._synchro_params["device"]
            synchro_mode = self._synchro_params["mode"]
        else:
            synchro_ctrl = None
            synchro_mode = "software"
                        
        # open shutter
        if self._shutter_mode == "xh_secure":
            if self._shutter is not None:
                self._shutter.open()
            if self._target_shutter is not None:
                if self.use_target_shutter:
                    if synchro_mode not in ["xh_diode", "shock"]:
                        self._target_shutter.manual()
                        self._target_shutter.open()
            
        (scan, self._acq_channels["I_raw"], data_roi) = self._acquisition(
            self._acq_params["title"],
            self._acq_params["inttime"],
            self._acq_params["nbframe"],
            self._acq_params["nbacq"],
            synchro_ctrl=synchro_ctrl,
            synchro_mode=synchro_mode,
            detector=self._detector,
            acq_mode="I",
            basler=self._use_basler,
        )
                        
        # close shutter
        if self._shutter_mode == "xh_secure":
            if self._shutter is not None:
                self._shutter.close()
            if self._target_shutter is not None:
                if self.use_target_shutter:
                    self._target_shutter.manual()
        
        if self._detector is not None:
            self._acquisition_channel_emit(name="I_raw")
            
            # Scatter plot
            self._seq.custom_channels["Detector_data"].emit(numpy.copy(self._acq_channels["I_raw"].flatten()))

            # Detector roi's
            self._acq_channels["roi_I1"] = data_roi["roi_I1"]
            self._acquisition_channel_emit(name="roi_I1")
            
            self._acq_channels["roi_I2"] = data_roi["roi_I2"]
            self._acquisition_channel_emit(name="roi_I2")
            
            self._acq_channels["roi_I3"] = data_roi["roi_I3"]
            self._acquisition_channel_emit(name="roi_I3")
            
            self._acq_channels["roi_I4"] = data_roi["roi_I4"]
            self._acquisition_channel_emit(name="roi_I4")
            
            self._acq_channels["roi_Isum"] = data_roi["roi_Isum"]
            self._acquisition_channel_emit(name="roi_Isum")

            if "no_bck" not in self._acq_params.keys():
                if not self._simulation:
                    data_I = self._acq_channels["I_raw"] - self._acq_channels["I_bck"]
                else:
                    data_I = numpy.copy(self._acq_channels["I_raw"])
                if "no_curr" not in self._acq_params.keys():
                    self._acq_channels["I_curr"] = numpy.full((self._acq_params["nbframe"],), self._srcurr.raw_read)
                    data_I_norm_curr =  data_I * self._max_machine_current / self._acq_channels["I_curr"][0]
                    self._acquisition_channel_emit(name="I_curr")
                    return (scan, data_I, data_I_norm_curr)
                else:
                    return (scan, data_I, None)                    
            else:
                return (scan, None, None)
        else:
            return (scan, None, None)
    
    ###############################################################
    #####
    ##### Sequences
    #####
    def _sequence(
        self,
        # params["title"]
        # params["seq_title"]
        # params["inttime"]
        # params["nbpair"]
        # params["nbaverage"]
        # params["nbspectrum"]
        # params["nbframe"]
        # params["nbacq"]
        # params["I0_mode"] = before/after/each_spetrum/last_acquired
        # params["xmcd"]
        # params["no_curr"]
        # params["no_bck"]
        # params["no_I0"]
        # params["no_mu"]
        acq_params,
        detector=None,
        # synchro_params["device"]
        # synchro_params["mode"]
        synchro_params=None,
        # axis_params["line_axis"]
        # axis_params["line_start"]
        # axis_params["line_delta"]
        # axis_params["line_interval"]
        # axis_params["line_current_pos"]
        # axis_params["col_axis"]
        # axis_params["col_start"]
        # axis_params["col_delta"]
        # axis_params["col_interval"]
        # axis_params["col_current_pos"]
        axis_params=None,
        I0=None,
        sample=None,
        basler=False,
        shutter_mode="xh_secure",
    ):
        
        #
        # INITIALIZATION
        #
        self._detector = detector
        self._acq_params = acq_params
        self._axis_params = axis_params
        self._synchro_params = synchro_params
        self._acq_channels = {}
        self._use_basler = basler
        
        # Display
        tiret_spectrum = BOLD(GREEN("----------"))
        tiret_mapping = BOLD(GREEN("--------------------"))
        nbs = self._acq_params["nbspectrum"]
        
        # Shutter Mode
        self._shutter_mode = shutter_mode
        if self._shutter_mode == "always_open":
            self._target_shutter_last_state = self.use_target_shutter
            self.use_target_shutter = False
        
        # XMCD
        if "xmcd" in self._acq_params.keys():
            data_xmcd = {"+": None, "-": None, "xmcd": None}
            data_xmcd_curr = {"+": None, "-": None, "xmcd": None}

        # Mapping...
        if self._axis_params is not None:
            
            # Do we have to move to current position at the end of the scan?
            self._axis_params["return"] = (self._acq_params["title"] == "dmap") or (self._acq_params["title"] == "xh_dscan")
            
            # Create line positions arrays
            if self._axis_params["line_axis"] is not None:
                delta = self._axis_params["line_delta"]
                interval = self._axis_params["line_interval"]
                start_pos = self._axis_params["line_start"]
                line_positions = numpy.zeros((interval+1,))
                for i in range(interval+1):
                    line_positions[i] = start_pos + i * delta
                # Manage Hook of mapping motors
                for hook in self._axis_params["line_axis"].motion_hooks:
                    hook.pre_scan([self._axis_params["line_axis"],])
            else:
                line_positions = [1]

            # Create column positions arrays
            if self._axis_params["col_axis"] is not None:
                delta = self._axis_params["col_delta"]
                interval = self._axis_params["col_interval"]
                start_pos = self._axis_params["col_start"]
                col_positions = numpy.zeros((interval+1,))
                for i in range(interval+1):
                    col_positions[i] = start_pos + i * delta
                # Manage Hook of mapping motors
                for hook in self._axis_params["col_axis"].motion_hooks:
                    hook.pre_scan([self._axis_params["col_axis"],])
            else:
                col_positions = [1]
            
        # ... or not Mapping
        else:
            line_positions = [1]
            col_positions = [1]
        
        # in case of detector and I0_mode=after, we must register 
        # all I datas to calculate MU at the end
        if self._detector is not None and \
           "no_I0" not in self._acq_params.keys() and \
           self._acq_params["I0_mode"] == "after":
               
            nbi = self._acq_params["nbspectrum"] * self._acq_params["nbframe"]
            if self._axis_params is not None:
                nbi  *= (self._axis_params["line_interval"]+1)
                nbi  *= (self._axis_params["col_interval"]+1)
            data_I = numpy.zeros((nbi, detector._param.nb_pixel))
            data_I_curr = numpy.zeros((nbi, detector._param.nb_pixel))
            
        # register Maximum Machine current for the scan
        if self._srcurr is None:
            self._acq_params["no_curr"] = True
        else:
            if "no_curr" not in self._acq_params.keys():
                if self._max_srcurr is not None:
                    self._max_machine_current =  float(self._max_srcurr.raw_read)
                else:
                    self._max_machine_current = 200

        #
        # SEQUENCE
        #
        with cleanup(self._sequence_end):

            # Create Sequence and Scans Group
            self._create_scan_sequence()
            
            with self._seq.sequence_context() as scan_seq:
            
                self._scan_seq = scan_seq
                
                # Emit Energy Data
                if self._detector is not None:
                    self._acq_channels["Energy"] = self._detector._get_energy_data()
                    self._acquisition_channel_emit(name="Energy")
                    
                    # Scatter plot
                    frames_ind, energy_ind = numpy.mgrid[
                        0:self._acq_params["nbframe"],
                        0:len(self._acq_channels["Energy"][0])
                    ]
                    energy_x = self._acq_channels["Energy"][0][energy_ind]
                    self._seq.custom_channels["Frames_y"].emit(frames_ind.flatten())
                    self._seq.custom_channels["Energy_x"].emit(energy_x.flatten())

                # Emit Axis Positions Data
                if self._axis_params is not None:
                    if self._axis_params["line_axis"] is not None:                
                        line_axis_name = self._axis_params["line_axis"].name
                        nbl = len(line_positions)
                    else:
                        nbl = 1
                    if self._axis_params["col_axis"] is not None:                
                        col_axis_name = self._axis_params["col_axis"].name
                        nbc = len(col_positions)
                    else:
                        nbc = 1
                    nbp = nbl*nbc
                    
                    if self._axis_params["line_axis"] is not None:                
                        self._acq_channels[line_axis_name] = numpy.zeros((nbp,))
                    if self._axis_params["col_axis"] is not None:                
                        self._acq_channels[col_axis_name] = numpy.zeros((nbp,))
                        
                    for line in range(nbl):
                        for col in range(nbc):
                            if self._axis_params["line_axis"] is not None:                
                                self._acq_channels[line_axis_name][line*nbc+col] = line_positions[line]
                            if self._axis_params["col_axis"] is not None:                
                                self._acq_channels[col_axis_name][line*nbc+col] = col_positions[col]                            
                    if self._axis_params["line_axis"] is not None:                
                        self._acquisition_channel_emit(name=line_axis_name)
                    if self._axis_params["col_axis"] is not None:                
                        self._acquisition_channel_emit(name=col_axis_name)
                
                #
                # Background
                #
                if self._detector is not None and "no_bck" not in self._acq_params.keys():                 
                    # I background
                    sc = self._take_I_bck()
                    #scan_seq.add(sc)
    
                    # I0 background
                    if "no_I0" not in self._acq_params.keys():
                        if self._acq_params["I0_mode"] == "last_acquired":
                            if self._acq_channels["I0_bck"] is None or self._acq_channels["I0"] is None:
                                raise RuntimeError("No data available for I0 in mode=last_acquired")
                            if self._acq_channels["I0"].shape[0] != nbframe:
                                raise RuntimeError("I0 data do not fit scan number of frames")
                            if self._acq_channels["I0"].shape[1] != self._detector._param.nb_pixel:
                                raise RuntimeError("I0 data do not fit detector number of pixels")
                        else:
                            sc = self._take_I0_bck()
                            #scan_seq.add(sc)
                
                # open shutter
                if self._shutter_mode == "always_open":
                    if self._shutter is not None:
                        self._shutter.open()
        
                # I0 Before
                if self._detector is not None and "no_I0" not in self._acq_params.keys():
                    if self._acq_params["I0_mode"] == "before":
                        print(GREEN("Take I0 BEFORE"))
                        sc = self._take_I0(I0)
                        #scan_seq.add(sc)

                #
                # Loop on Line Axis
                #
                np_line_positions = len(line_positions)
                for line_npos in range(np_line_positions):
                    
                    # Move line axis to next position
                    if self._axis_params is not None:
                        if self._axis_params["line_axis"] is not None:
                            print(f"\n{tiret_mapping} Mapping: Z motion {GREEN(line_npos+1)}/({np_line_positions}) {tiret_mapping}")
                            umv(self._axis_params["line_axis"], line_positions[line_npos])
                        
                    #
                    # Loop on Column Axis
                    #
                    np_col_positions = len(col_positions)
                    for col_npos in range(np_col_positions):

                        # Move olumn axis to next position
                        if self._axis_params is not None:
                            if self._axis_params["col_axis"] is not None:
                                print(f"\n{tiret_mapping} Mapping: Y motion {GREEN(col_npos+1)}/({np_col_positions}) {tiret_mapping}")
                                umv(self._axis_params["col_axis"], col_positions[col_npos])
                            
                        #
                        # Loop on nbspectrum
                        #
                        for nspectrum in range(self._acq_params["nbspectrum"]):
                            
                            print(f"\n{tiret_spectrum} Acquisition {BOLD(GREEN(nspectrum+1))}/({BOLD(nbs)}) {tiret_spectrum}")
                            
                            if self._synchro_params is not None:
                            
                                if self._synchro_params["mode"] == "scan_orbit":
                                    scan_orbit_scan = self._acq_params["scan_orbit_start"]
                                    self._detector.controller.xh_send(f"xstrip timing setup-orbit \'xh0\' {scan_orbit_scan+nspectrum}")
                                    
                                # PMF: set Charge
                                if "pmf_charge" in self._acq_params.keys():     
                                    self._synchro_params["device"].set_charge(self._acq_params["pmf_charge"])
                                    
                                # PMF / XMCD: determine which group / pair
                                if "xmcd" in self._acq_params.keys():
                                    # index of spetrum in a polarization group
                                    nspectrum_in_polarization = nspectrum % self._acq_params["nbaverage"]
                                    # should we set the polarization?
                                    set_polar = (nspectrum_in_polarization == 0)
                                    # which polarization?
                                    if (int(nspectrum/self._acq_params["nbaverage"])%2) == 0:
                                        polarization = "+"
                                    else:
                                        polarization = "-"
                                    if set_polar:
                                        self._synchro_params["device"].set_polarization(polarization)
                            
                            if self._axis_params is None:
                                # move sample in 
                                if sample is not None:
                                    print(RED("Move to Sample Position"))
                                    sample.move()
                                
                            # Take I
                            (sc, data_I_aux, data_I_norm_curr_aux) = self._take_I()
                            #scan_seq.add(sc)

                            if self._detector is not None and "no_bck" not in self._acq_params.keys():
                                   
                                if "no_I0" not in self._acq_params.keys() and self._acq_params["I0_mode"] == "after":
                                    from_index = nspectrum * self._acq_params["nbframe"]
                                    if self._axis_params is not None:
                                        from_index += line_npos * (self._axis_params["col_interval"]+1) * self._acq_params["nbspectrum"] * self._acq_params["nbframe"]
                                        from_index += col_npos * self._acq_params["nbspectrum"] * self._acq_params["nbframe"]
                                    to_index = from_index + self._acq_params["nbframe"]
                                    data_I[from_index:to_index] = data_I_aux
                                    if "no_curr" not in self._acq_params.keys():
                                        data_I_curr[from_index:to_index] = data_I_norm_curr_aux                            
                                else:
                                    self._acq_channels["I"] = numpy.copy(data_I_aux)
                                    self._acquisition_channel_emit(name="I")
                                    if "no_curr" not in self._acq_params.keys():
                                        self._acq_channels["I_norm_curr"] = numpy.copy(data_I_norm_curr_aux)
                                        self._acquisition_channel_emit(name="I_norm_curr")

                                # I0 after each absorption spectrum
                                if "no_I0" not in self._acq_params.keys() and self._acq_params["I0_mode"] == "each_spectrum":
                                    print(GREEN("Take I0 for EACH spectrum"))
                                    sc = self._take_I0(I0)
                                    #scan_seq.add(sc)
                                    
                                if "no_I0" not in self._acq_params.keys() and self._acq_params["I0_mode"] != "after":                                    

                                    # Calulate I0 Normalized by time
                                    data_I0 = self._acq_channels["I0"] * self._detector._param.I_time / self._detector._param.I0_time                        
                                    if "no_curr" not in self._acq_params.keys():
                                        # Calculate I0 normalized by srcurr
                                        data_I0_curr = self._acq_channels["I0_norm_curr"] * self._detector._param.I_time / self._detector._param.I0_time             
                                
                                    if "no_mu" not in self._acq_params.keys():
                                        # Calculate mu
                                        print("Calculate MU")
                                        I_gooddata = numpy.copy(self._acq_channels["I"])
                                        I_gooddata[I_gooddata == 0] = 1
                                        gooddata = data_I0 / I_gooddata
                                        gooddata[gooddata <= 0] = 1
                                        self._acq_channels["Mu"] = numpy.log(gooddata)
                                        #self._acq_channels["Mu"] = numpy.log(data_I0 / self._acq_channels["I"])
                                        self._acquisition_channel_emit(name="Mu")
                                        # Mu Average
                                        if "average_mu" in self._acq_params.keys():
                                            if nspectrum == 0:
                                                self._acq_channels["Mu_aver"] = self._acq_channels["Mu"] / self._acq_params["nbspectrum"]
                                            else:
                                                self._acq_channels["Mu_aver"] += (self._acq_channels["Mu"] / self._acq_params["nbspectrum"])
                                        
                                        if "no_curr" not in self._acq_params.keys():
                                            I_gooddata = numpy.copy(self._acq_channels["I_norm_curr"])
                                            I_gooddata[I_gooddata == 0] = 1
                                            gooddata = data_I0_curr / I_gooddata
                                            gooddata[gooddata <= 0] = 1
                                            self._acq_channels["Mu_norm_curr"] = numpy.log(gooddata)
                                            self._acquisition_channel_emit(name="Mu_norm_curr")
                                            # Mu Average
                                            if "average_mu" in self._acq_params.keys():
                                                if nspectrum == 0:
                                                    self._acq_channels["Mu_aver_curr"] = self._acq_channels["Mu_norm_curr"] / self._acq_params["nbspectrum"]
                                                else:
                                                    self._acq_channels["Mu_aver_curr"] += (self._acq_channels["Mu_norm_curr"] / self._acq_params["nbspectrum"])
                                    
                                    # XMCD
                                    if "xmcd" in self._acq_params.keys():
                                        # start of a new pair
                                        if nspectrum_in_polarization == 0:
                                            #if polarization == "+":
                                            #    if data_xmcd["+"] is not None:
                                            #        self._acq_channels["Xmcd_interm"] = numpy.log(data_xmcd["-"] / data_xmcd["+"])/2.0
                                            #        self._acquisition_channel_emit(name="Xmcd_interm")
                                            #        self._acquisition_channel_plot(["Xmcd_interm"])
                                            #        if "no_curr" not in self._acq_params.keys():
                                            #            self._acq_channels["Xmcd_interm_norm_curr"] = numpy.log(data_xmcd_curr["-"] / data_xmcd_curr["+"])/2.0
                                            #            self._acquisition_channel_emit(name="Xmcd_interm_norm_curr")
                                            #            self._acquisition_channel_plot(["Xmcd_interm_norm_curr"])
                                            #        if data_xmcd["xmcd"] is None:
                                            #            data_xmcd["xmcd"] = numpy.copy(self._acq_channels["Xmcd_interm"]) / self._acq_params["nbpair"]
                                            #            if "no_curr" not in self._acq_params.keys():
                                            #                data_xmcd_curr["xmcd"] = numpy.copy(self._acq_channels["Xmcd_interm_norm_curr"]) / self._acq_params["nbpair"]
                                            #        else:
                                            #            data_xmcd["xmcd"] += (self._acq_channels["Xmcd_interm"] / self._acq_params["nbpair"])
                                            #            if "no_curr" not in self._acq_params.keys():
                                            #                data_xmcd_curr["xmcd"] += (self._acq_channels["Xmcd_interm_norm_curr"] / self._acq_params["nbpair"])
                                            
                                            # Sum I signal
                                            data_xmcd[polarization] = numpy.copy(self._acq_channels["I"]) / self._acq_params["nbaverage"]                 
                                            if "no_curr" not in self._acq_params.keys():
                                                data_xmcd_curr[polarization] = numpy.copy(self._acq_channels["I_norm_curr"]) / self._acq_params["nbaverage"]
                                        else:
                                            # Sum I signal
                                            data_xmcd[polarization] += (self._acq_channels["I"] / self._acq_params["nbaverage"])             
                                            if "no_curr" not in self._acq_params.keys():
                                                data_xmcd_curr[polarization] += (self._acq_channels["I_norm_curr"] / self._acq_params["nbaverage"])
                                                
                                        # End of pair: save intermediate result + calculate xmcd
                                        if nspectrum_in_polarization == self._acq_params["nbaverage"]-1:
                                            if polarization == "-":
                                                self._acq_channels["Xmcd_interm"] = numpy.log(data_xmcd["-"] / data_xmcd["+"])/2.0
                                                self._acquisition_channel_emit(name="Xmcd_interm")
                                                self._acquisition_channel_plot(["Xmcd_interm"])
                                                if "no_curr" not in self._acq_params.keys():
                                                    self._acq_channels["Xmcd_interm_norm_curr"] = numpy.log(data_xmcd_curr["-"] / data_xmcd_curr["+"])/2.0
                                                    self._acquisition_channel_emit(name="Xmcd_interm_norm_curr")
                                                    self._acquisition_channel_plot(["Xmcd_interm_norm_curr"])
                                                if data_xmcd["xmcd"] is None:
                                                    data_xmcd["xmcd"] = numpy.copy(self._acq_channels["Xmcd_interm"]) / self._acq_params["nbpair"]
                                                    if "no_curr" not in self._acq_params.keys():
                                                        data_xmcd_curr["xmcd"] = numpy.copy(self._acq_channels["Xmcd_interm_norm_curr"]) / self._acq_params["nbpair"]
                                                else:
                                                    data_xmcd["xmcd"] += (self._acq_channels["Xmcd_interm"] / self._acq_params["nbpair"])
                                                    if "no_curr" not in self._acq_params.keys():
                                                        data_xmcd_curr["xmcd"] += (self._acq_channels["Xmcd_interm_norm_curr"] / self._acq_params["nbpair"])

                        # End of loop on nbspectrum
                        
                        # Emit Mu Average
                        if self._detector is not None:
                            if self._acq_params["I0_mode"] != "after":
                                if "average_mu" in self._acq_params.keys():
                                    self._acquisition_channel_emit(name="Mu_aver")
                                    self._acquisition_channel_emit(name="Mu_aver_curr")

                                
                        # XMCD: Emit XMCD 
                        if "xmcd" in self._acq_params.keys():
                            self._acq_channels["Xmcd"] = data_xmcd["xmcd"]
                            self._acq_channels["Xmcd_norm_curr"] = data_xmcd_curr["xmcd"]
                            self._acquisition_channel_emit(name="Xmcd")
                            self._acquisition_channel_emit(name="Xmcd_norm_curr")
                            
                
                    #
                    # End of loop on axis2
                    #
            
                #
                # End of loop on axis1
                #

                #
                # I0 mode is after => Mu/Xmcd can be calculated only at the end
                # 
                if self._detector is not None:
                    
                    if "no_I0" not in self._acq_params.keys() and self._acq_params["I0_mode"] == "after":
                        
                        self._acq_channels["I"] = numpy.copy(data_I)
                        self._acquisition_channel_emit(name="I")
                        if "no_curr" not in self._acq_params.keys():
                        #if self._srcurr is not None and self._max_srcurr is not None:
                            self._acq_channels["I_norm_curr"] = numpy.copy(data_I_curr)
                            self._acquisition_channel_emit(name="I_norm_curr")
                        
                        # Take I0 AFTER
                        print(GREEN("Take I0 AFTER"))
                        sc = self._take_I0(I0)
                        #scan_seq.add(sc)
                                
                        # Calulate I0 Normalized by time
                        data_I0 = self._acq_channels["I0"] * detector._param.I_time / detector._param.I0_time                        
                        if "no_curr" not in self._acq_params.keys():
                        #if self._srcurr is not None and self._max_srcurr is not None:
                            data_I0_curr = self._acq_channels["I0_norm_curr"] * detector._param.I_time / detector._param.I0_time     
                    
                        # Calculate mu
                        print("Calculate MU")
                        I_gooddata = numpy.copy(data_I)
                        I_gooddata[I_gooddata == 0] = 1
                        gooddata = data_I0 / I_gooddata
                        gooddata[gooddata <= 0] = 1
                        self._acq_channels["Mu"] = numpy.log(gooddata)
                        self._acquisition_channel_emit(name="Mu")
                            
                        # Mu Average
                        if "average_mu" in self._acq_params.keys():
                            for line_ind in range(np_line_positions):
                                for col_ind in range(np_col_positions):
                                    start_index = (line_ind * np_col_positions + col_ind) * self._acq_params["nbspectrum"]
                                    for sp_ind in range(self._acq_params["nbspectrum"]):
                                        from_ind = start_index+sp_ind*self._acq_params["nbframe"]
                                        to_ind = from_ind + self._acq_params["nbframe"]
                                        if sp_ind == 0:
                                            self._acq_channels["Mu_aver"] = self._acq_channels["Mu"][from_ind:to_ind] / self._acq_params["nbspectrum"]
                                        else:
                                            self._acq_channels["Mu_aver"] += (self._acq_channels["Mu"][from_ind:to_ind] / self._acq_params["nbspectrum"])
                                    self._acquisition_channel_emit(name="Mu_aver")
                                    
                        if "no_curr" not in self._acq_params.keys():
                        #if self._srcurr is not None and self._max_srcurr is not None:
                            I_gooddata = numpy.copy(data_I_curr)
                            I_gooddata[I_gooddata == 0] = 1
                            gooddata = data_I0_curr / I_gooddata
                            gooddata[gooddata <= 0] = 1
                            self._acq_channels["Mu_norm_curr"] = numpy.log(gooddata)
                            #self._acq_channels["Mu_norm_curr"] = numpy.log(data_I0_curr / data_I_curr)
                            self._acquisition_channel_emit(name="Mu_norm_curr")
                            
                            # Mu_morm_curr Average
                            if "average_mu" in self._acq_params.keys():
                                for line_ind in range(np_line_positions):
                                    for col_ind in range(np_col_positions):
                                        start_index = (line_ind * np_col_positions + col_ind) * self._acq_params["nbspectrum"]
                                        for sp_ind in range(self._acq_params["nbspectrum"]):
                                            from_ind = start_index+sp_ind*self._acq_params["nbframe"]
                                            to_ind = from_ind + self._acq_params["nbframe"]
                                            if sp_ind == 0:
                                                self._acq_channels["Mu_aver_curr"] = self._acq_channels["Mu_norm_curr"][from_ind:to_ind] / self._acq_params["nbspectrum"]
                                            else:
                                                self._acq_channels["Mu_aver_curr"] += (self._acq_channels["Mu_norm_curr"][from_ind:to_ind] / self._acq_params["nbspectrum"])
                                        self._acquisition_channel_emit(name="Mu_aver_curr")
                    
                    plotlist = []
                    if "no_mu" not in self._acq_params.keys():
                        plotlist.append("Mu")
                    if synchro_params is not None and synchro_params["mode"] == "scan_orbit":
                        plotlist.append("I")
                    self._acquisition_channel_plot(plotlist)

            print("DONE")
        
    
    ###############################################################
    #####
    ##### Acquisition
    #####
    def _acquisition_prepare(self):
        if self._detector is not None:
            self._detector._acq_prepare()
        
    def _acquisition_end(self):
        if self._detector is not None:
            self._detector._acq_end()
        
    def _acquisition(
        self,
        title,
        expotime,
        nbframe,
        nbacq,
        synchro_ctrl=None,
        synchro_mode="software",
        detector=None,
        acq_mode=None,
        basler=False
    ):
        
        # Title
        scan_title = title
        if acq_mode is not None:
            scan_title += f" {acq_mode}"
            
        # Chain
        self._chain = AcquisitionChain(parallel_prepare=True)

        # Synchronisation module
        master = None
        if synchro_ctrl is not None:
            master = synchro_ctrl.get_acquisition_master(synchro_mode, self.use_target_shutter, self._use_basler)
            self._chain.add(master)
            
        # Builder
        self._builder = None
        self._lima_list = []
        self._par_list = {}
        if detector is not None:
            self._lima_list.append(detector.lima)
            self._par_list[detector.lima.name] = detector._get_detector_parameters(
                    synchro_mode,
                    acq_mode,
                    expotime,
                    nbframe,
                    nbacq
            )
        if acq_mode == "I" and basler:
            print(RED("\n\n ADD BASLER TO ACQUISITION\n\n"))
            for bas in self._baslers:
                self._lima_list.append(bas.lima)
                self._par_list[bas.lima.name] = bas._get_detector_parameters(synchro_mode, acq_mode, expotime, nbframe, nbacq)
        if len(self._lima_list) != 0:
            self._builder = ChainBuilder(self._lima_list)

        # Acquisition Chain
        if self._builder is not None:

            for node in self._builder.get_nodes_by_controller_type(Lima):
                acq_params = self._par_list[node.controller.name]["acq_params"]
                ctrl_params = self._par_list[node.controller.name]["ctrl_params"]
                node.set_parameters(acq_params=acq_params, ctrl_params=ctrl_params)
                if master is not None:
                    self._chain.add(master, node)
                else:
                    for child in node.children:
                        self._chain.add(node, child)
        
        # Scan Info
        scan_info = {}
        if detector is not None:
            nf = detector._get_acq_param_value("nbframe", acq_mode)
            et = detector._get_acq_param_value("expotime", acq_mode)
            scan_info["title"] = f"{scan_title} {nf} {et}"
        else:
            scan_info["title"] = scan_title
        scan_info["type"] = scan_title

        # Create Scan Object
        self._scan = Scan(
            self._chain,
            scan_info=scan_info,
            name=scan_title,
            save=False,
            data_watch_callback=AcquisitionDisplay(self._lima_list),
        )
        
        with cleanup(self._acquisition_end):
            
            # Prepare Detector Acquisition
            self._acquisition_prepare()
            self._scan_seq.add(self._scan)
            # Start Acquisition
            self._scan.run()
                    
        if acq_mode == "I" and self.use_target_shutter:
            print(RED("SET FAST SHUTTER IN MANUAL MODE"))
            self._target_shutter.manual()

        # Get data
        data = None
        data_roi = {}
        if detector is not None:
            # Data
            nf = detector._get_acq_param_value("nbframe", acq_mode)
            na = detector._get_acq_param_value("nbacq", acq_mode)
            data = detector._get_acq_data(self._scan, nf, na)
            
            # ROI's
            if acq_mode == "I":
                if detector is not None:
                    data_roi = detector._get_acq_roi(self._scan, nf, na)
                
        if basler:
            if acq_mode == "I":
                for bas in self._baslers:
                    bas._get_acq_data(self._scan, 1, 1)
            
        return (self._scan, data, data_roi)

class AcquisitionDisplay(DataWatchCallback):
    """Callback used by acquisition method to print acquisition status.
    """

    HEADER = (
        "Scan {scan_nb} {start_time_str} {filename} "
        + "{session_name} user = {user_name}\n"
        + "{title}"
    )

    def __init__(self, limas):
        self.__limas = limas

    def on_state(self, state):
        return True

    def on_scan_new(self, scan, info):
        title = info["title"]
        print(f"{BOLD(RED(title))}")
        #print(self.HEADER.format(**info))
        self.__state = None
        self.__infos = dict()
        self.__save_flag = info.get("save", False)

        for lima in self.__limas:
            self.__infos[lima.name] = 0

    def on_scan_end(self, info):
        msg = self.__update_cam_infos()
        print(msg)
        start = datetime.datetime.fromtimestamp(info["start_timestamp"])
        end = datetime.datetime.fromtimestamp(time.time())
        msg = "Finished (took {0})\n".format(end - start)
        print(msg)

    def __update_cam_infos(self):
        for cam in self.__limas:
            last_acq = cam._proxy.last_image_ready + 1
            msg = "acq #{0}".format(last_acq)
            if self.__save_flag:
                last_saved = cam._proxy.last_image_saved + 1
                msg += " save #{0}".format(last_saved)
            last_status = cam._proxy.acq_status
            if last_status == "Ready":
                msg = BOLD(msg)
            elif last_status == "Fault":
                msg = RED(msg)
            self.__infos[cam.name] = msg
        msg = ""
        for (name, value) in self.__infos.items():
            msg += "{0} {1}  ".format(BOLD(name), value)
        return msg

    def on_scan_data(self, data_events, data_nodes, info):
        # look for scan state
        state = info.get("state", None)
        if state != self.__state:
            self.__state = state
            if state == ScanState.PREPARING:
                msg = "Preparing "
                for lima in self.__limas:
                    msg += "{0} ".format(lima.name)
                #print(msg + "...")
            if state == ScanState.STARTING:
                pass
                #print("Running ...")
        if state == ScanState.STARTING:
            # print last images acquired and saved
            msg = self.__update_cam_infos()
            print(msg + "\r", end="")

