
import numpy as np
from bliss.controllers.counter import CalcCounterController
from bliss.config import settings

class EncMonoEnergyCalcCounterController(CalcCounterController):
    
    def __init__(self, name, config):
        
        self.mono = config.get("mono", None)
        self.settings = settings.SimpleSetting("offset", default_value=0.0)
        super().__init__(name, config)
        
            
    def calc_function(self, input_dict):

        enc_mono = input_dict["mono_enc"] + self.settings.get()

        output_dict = {}
        output_dict["ene_enc"] = self.mono.bragg2energy(enc_mono)
       
        return output_dict
        
    def setE(self, energy):
        new_enc_value = self.mono.energy2bragg(energy)
        
        self.settings.set(new_enc_value - self.inputs[0].raw_read)
        
class FluoCalcCounterController(CalcCounterController):
    
    def __init__(self, name, config):
        super().__init__(name, config)
            
    def calc_function(self, input_dict):

        output_dict = {}       
        output_dict["mu_fluo"] = input_dict["fluo"] / input_dict["I0"]
        return output_dict
        
class FluoTimeCalcCounterController(CalcCounterController):
    
    def __init__(self, name, config):
        super().__init__(name, config)
            
    def calc_function(self, input_dict):

        output_dict = {}
                
        if len(input_dict["fluo"].shape) > 1:
            val = np.copy(input_dict["fluo"]).astype(np.float)
            for i in range(input_dict["fluo"].shape[0]):
                val[i][:] = input_dict["realtime"][i] * input_dict["fluo"][i][:] / input_dict["livetime"][i]
        else:
            val = np.copy(input_dict["fluo"]).astype(np.float)
            val = input_dict["realtime"] * input_dict["fluo"] / input_dict["livetime"]
        output_dict["fluo"] = np.copy(val)

        if len(input_dict["fluo"].shape) > 1:
            output_dict["mu_fluo"] = np.copy(input_dict["fluo"]).astype(np.float)
            for i in range(input_dict["fluo"].shape[0]):
                output_dict["mu_fluo"][i][:] = val[i][:] / input_dict["I0"][i]
        else:
            output_dict["mu_fluo"] = np.copy(input_dict["fluo"]).astype(np.float)
            #output_dict["mu_fluo"] = val / float(input_dict["I0"])
            output_dict["mu_fluo"] = val / input_dict["I0"]
        
        #print("\n\n {0} {1}".format(output_dict["fluo"].shape, output_dict["mu_fluo"].shape))
        return output_dict
