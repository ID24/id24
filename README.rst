============
ID24 project
============

[![build status](https://gitlab.esrf.fr/ID24/id24/badges/master/build.svg)](http://ID24.gitlab-pages.esrf.fr/ID24)
[![coverage report](https://gitlab.esrf.fr/ID24/id24/badges/master/coverage.svg)](http://ID24.gitlab-pages.esrf.fr/id24/htmlcov)

ID24 software & configuration

Latest documentation from master can be found [here](http://ID24.gitlab-pages.esrf.fr/id24)
